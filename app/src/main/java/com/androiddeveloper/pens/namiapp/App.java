package com.androiddeveloper.pens.namiapp;

import android.app.Application;
import android.content.Context;

import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.di.componen.ApplicationComponent;
import com.androiddeveloper.pens.namiapp.di.componen.DaggerApplicationComponent;
import com.androiddeveloper.pens.namiapp.di.module.ApplicationModule;
import com.androiddeveloper.pens.namiapp.utils.NetworkUtils;
import com.crashlytics.android.Crashlytics;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;

import io.fabric.sdk.android.Fabric;
import javax.inject.Inject;

/**
 * Created by miftahun on 6/11/18.
 * <p>
 * Author Miftahun Najat
 * Email miftahunajat@gmail.com
 * Github https://github.com/miftahunajat
 */

public class App extends Application {

    @Inject
    DataManager mDataManager;

    private ApplicationComponent mApplicationComponent;

    private static App mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
//        Fabric.with(this, new Crashlytics());

        final Fabric fabric = new Fabric.Builder(this)
                .kits(new Crashlytics())
                .debuggable(true)           // Enables Crashlytics debugger
                .build();
        Fabric.with(fabric);


        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this)).build();

        mApplicationComponent.inject(this);
        mInstance = this;

    }

    public static synchronized App getInstance() {
        return mInstance;
    }

    public void setConnectionListener(NetworkUtils.ConnectionReceiverListener listener) {
        NetworkUtils.connectionReceiverListener = listener;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    public ApplicationComponent getComponent() {
        return mApplicationComponent;
    }


    // Needed to replace the component with a test specific one
    public void setComponent(ApplicationComponent applicationComponent) {
        mApplicationComponent = applicationComponent;
    }
}
