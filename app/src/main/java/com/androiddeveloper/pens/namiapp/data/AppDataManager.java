package com.androiddeveloper.pens.namiapp.data;


import android.app.Activity;
import android.content.Context;

import com.androiddeveloper.pens.namiapp.data.firebase.FirebaseHelper;
import com.androiddeveloper.pens.namiapp.data.network.ApiHelper;
import com.androiddeveloper.pens.namiapp.data.network.model.ActualCallListResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.BackCheckingLocationListResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.BackcheckingMain;
import com.androiddeveloper.pens.namiapp.data.network.model.BackcheckingMainResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.BackcheckingResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.BackcheckingStatusResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.BasicConfigResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.BasicResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.ConfigPermitResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.Event;
import com.androiddeveloper.pens.namiapp.data.network.model.Feedback;
import com.androiddeveloper.pens.namiapp.data.network.model.FeedbackLeaderNote;
import com.androiddeveloper.pens.namiapp.data.network.model.FeedbackLocationResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.LoginResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.NotificationResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceBackcheckingResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceEventResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformancePoiOskResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceShareResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceSummary;
import com.androiddeveloper.pens.namiapp.data.network.model.SooBackCheckingLocationListResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.SummaryEventResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.SummaryInstitutionResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.SummaryOskResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.SummarySalesResponse;
import com.androiddeveloper.pens.namiapp.data.prefs.PreferencesHelper;
import com.androiddeveloper.pens.namiapp.di.ApplicationContext;
import com.androiddeveloper.pens.namiapp.ui.soobackcheckinglocationlist.Location;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created by miftahun on 6/11/18.
 * <p>
 * Author Miftahun Najat
 * Email miftahunajat@gmail.com
 * Github https://github.com/miftahunajat
 */

@Singleton
public class AppDataManager implements DataManager {

    private static final String TAG = "AppDataManager";

    private final Context mContext;
    private final PreferencesHelper mPreferencesHelper;
    private final ApiHelper mApiHelper;
    private final FirebaseHelper mFirebaseHelper;

    @Inject
    AppDataManager(@ApplicationContext Context context,
                          PreferencesHelper preferencesHelper,
                          ApiHelper apiHelper,
                          FirebaseHelper firebaseHelper) {
        mContext = context;
        mPreferencesHelper = preferencesHelper;
        mApiHelper = apiHelper;
        mFirebaseHelper = firebaseHelper;
    }

    @Override
    public int getCurrentUserLoggedInMode() {
        return mPreferencesHelper.getCurrentUserLoggedInMode();
    }

    @Override
    public void setCurrentUserLoggedInMode(LoggedInMode mode) {
        mPreferencesHelper.setCurrentUserLoggedInMode(mode);
    }

    @Override
    public String getCurrentUserId() {
        return mPreferencesHelper.getCurrentUserId();
    }

    @Override
    public void setCurrentUserId(String userId) {
        mPreferencesHelper.setCurrentUserId(userId);
    }

    @Override
    public String getAccessToken() {
        return mPreferencesHelper.getAccessToken();
    }

    @Override
    public void setAccessToken(String accessToken) {
        mPreferencesHelper.setAccessToken(accessToken);
    }

    @Override
    public String getCurrentUserName() {
        return mPreferencesHelper.getCurrentUserName();
    }

    @Override
    public void setCurrentUserName(String userName) {
        mPreferencesHelper.setCurrentUserName(userName);
    }

    @Override
    public String getCurrentUserType() {
        return mPreferencesHelper.getCurrentUserType();
    }

    @Override
    public void setCurrentUserType(String userType) {
        mPreferencesHelper.setCurrentUserType(userType);
    }

    @Override
    public String getCurrentDeviceId() {
        return mPreferencesHelper.getCurrentDeviceId();
    }

    @Override
    public void setCurrentDeviceId(String deviceId) {
        mPreferencesHelper.setCurrentDeviceId(deviceId);
    }

    @Override
    public void setCUrrentUserTitle(String currentUserTitle) {
        mPreferencesHelper.setCUrrentUserTitle(currentUserTitle);
    }

    @Override
    public String getCurrentUserTitle() {
        return mPreferencesHelper.getCurrentUserTitle();
    }


    @Override
    public void setUserAsLoggedOut() {
        updateUserInfo(
                null,
                null,
                DataManager.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT,
                null,
                null,
                null,
                null,
                null,
                null);
    }

    @Override
    public void updateUserInfo(String accessToken, String userId, LoggedInMode loggedInMode, String deviceId, String userName, String userType, String userTitle,String pathImage, String appType) {
        setCurrentUserId(userId);
        setCurrentUserLoggedInMode(loggedInMode);
        setCurrentDeviceId(deviceId);
        setCurrentUserName(userName);
        setCurrentUserType(userType);
        setCUrrentUserTitle(userTitle);
        setCurrentUserPathImage(pathImage);
        setCurrentApplicationType(appType);
    }

    public void setCurrentUserPathImage(String pathImage) {
        mPreferencesHelper.setCurrentUserPathImage(pathImage);
    }

    @Override
    public String getCurrentUserPathImage() {
        return mPreferencesHelper.getCurrentUserPathImage();
    }

    @Override
    public void setCurrentApplicationType(String AppType) {
        mPreferencesHelper.setCurrentApplicationType(AppType);
    }

    @Override
    public String getCurrentApplicationType() {
        return mPreferencesHelper.getCurrentApplicationType();
    }

    @Override
    public Observable<List<ActualCallListResponse>> getActualCallList() {
        return mApiHelper.getActualCallList();
    }

    @Override
    public Single<List<BackCheckingLocationListResponse>> getBackCheckingLocationList(String latitude, String longitude, String distinstitution, String distOssOsk) {
        return mApiHelper.getBackCheckingLocationList(latitude,longitude,distinstitution,distOssOsk);
    }

    @Override
    public Single<List<SooBackCheckingLocationListResponse>> getSooBackchekingLocationList(String userid, String latitude, String longitude, String distinstitution, String distOssOsk, String lac, String ci) {
        return mApiHelper.getSooBackchekingLocationList(userid, latitude, longitude, distinstitution, distOssOsk, lac, ci);
    }

    @Override
    public Observable<List<NotificationResponse>> getNotificationList() {
        return mApiHelper.getNotificationList();
    }

    @Override
    public Single<LoginResponse> sentLoginUser(String deviceId, String phoneNumber) {
        return mApiHelper.sentLoginUser(deviceId,phoneNumber);
    }

    @Override
    public Single<LoginResponse> sentLoginRajawali(String deviceId, String phoneNumber) {
        return mApiHelper.sentLoginRajawali(deviceId, phoneNumber);
    }

    @Override
    public Single<BasicResponse> sentLogoutUser(String phoneNumber) {
        return mApiHelper.sentLogoutUser(phoneNumber);
    }

    @Override
    public Single<BasicResponse> sentLogoutUserRajawali(String phoneNumber) {
        return mApiHelper.sentLogoutUserRajawali(phoneNumber);
    }

    @Override
    public Single<List<BasicConfigResponse>> getServerConfig() {
        return mApiHelper.getServerConfig();
    }

    @Override
    public Single<List<BasicConfigResponse>> getServerConfigRajawali() {
        return mApiHelper.getServerConfigRajawali();
    }

    @Override
    public Single<ConfigPermitResponse> getPermissionMenu() {
        return mApiHelper.getPermissionMenu();
    }

    @Override
    public Single<BasicResponse> insertCheckinStatus(Map<String, String> params) {
        return mApiHelper.insertCheckinStatus(params);
    }

    @Override
    public Single<BasicResponse> insertCheckoutStatus(String userid, String latitude, String longitude) {
        return mApiHelper.insertCheckoutStatus(userid, latitude, longitude);
    }

    @Override
    public Single<BackcheckingStatusResponse> getCheckoutStatus(String userid, String latitude, String longitude) {
        return mApiHelper.getCheckoutStatus(userid, latitude, longitude);
    }

    @Override
    public Single<BackcheckingResponse> getBackcheckingInstitutionTotalStudent(String locationId) {
        return mApiHelper.getBackcheckingInstitutionTotalStudent(locationId);
    }

    @Override
    public Single<BackcheckingResponse> getBackcheckingInstitutionVisit(String locationId) {
        return mApiHelper.getBackcheckingInstitutionVisit(locationId);
    }

    @Override
    public Single<BackcheckingResponse> getBackcheckingOssOskVisit(String locationId) {
        return mApiHelper.getBackcheckingOssOskVisit(locationId);
    }

    @Override
    public Single<BackcheckingResponse> getBackcheckingInstitutionBranding(String locationId) {
        return mApiHelper.getBackcheckingInstitutionBranding(locationId);
    }

    @Override
    public Single<BackcheckingResponse> getBackcheckingInstitutionMerchandising(String locationId) {
        return mApiHelper.getBackcheckingInstitutionMerchandising(locationId);
    }

    @Override
    public Single<BackcheckingResponse> getBackcheckingInstitutionMarketShare(String locationId) {
        return mApiHelper.getBackcheckingInstitutionMarketShare(locationId);
    }

    @Override
    public Single<BackcheckingResponse> getBackcheckingInstitutionSalesThrough(String locationId) {
        return mApiHelper.getBackcheckingInstitutionSalesThrough(locationId);
    }

    @Override
    public Single<BackcheckingResponse> getBackcheckingInstitutionReadSignal(String locationId) {
        return mApiHelper.getBackcheckingInstitutionReadSignal(locationId);
    }

    @Override
    public Single<BasicResponse> submitBackcheckingForm(Map<String, String> params) {
        return mApiHelper.submitBackcheckingForm(params);
    }

    @Override
    public Single<BackcheckingResponse> getBackcheckingOssOskBranding(String locationId) {
        return mApiHelper.getBackcheckingOssOskBranding(locationId);
    }

    @Override
    public Single<BackcheckingResponse> getBackcheckingOssOskBsDsRs(String locationId) {
        return mApiHelper.getBackcheckingOssOskBsDsRs(locationId);
    }

    @Override
    public Single<BackcheckingResponse> getBackcheckingOssOskTopSelling(String locationId) {
        return mApiHelper.getBackcheckingOssOskTopSelling(locationId);
    }

    @Override
    public Single<BackcheckingResponse> getBackcheckingOssOskSalesThrough(String locationId) {
        return mApiHelper.getBackcheckingOssOskSalesThrough(locationId);
    }

    @Override
    public Single<BackcheckingResponse> getBackcheckingOssOskReadSignal(String locationId) {
        return mApiHelper.getBackcheckingOssOskReadSignal(locationId);
    }

    @Override
    public Single<BasicResponse> submitBackcheckingFormOssOsk(Map<String, String> params) {
        return mApiHelper.submitBackcheckingFormOssOsk(params);
    }

    @Override
    public Single<BasicResponse> getBackcheckingStatus(String userId, String locationId, String locationType) {
        return mApiHelper.getBackcheckingStatus(userId, locationId, locationType);
    }

    @Override
    public Single<BasicResponse> getSooBackcheckingStatus(String userId, String locationId, String locationType) {
        return mApiHelper.getSooBackcheckingStatus(userId, locationId, locationType);
    }

    @Override
    public Single<BackcheckingMainResponse> getBackcheckingMain(String userId) {
        return mApiHelper.getBackcheckingMain(userId);
    }

    @Override
    public Single<PerformancePoiOskResponse> getSummaryPerformancePoiOsk(String userId, String teritory, String userId_aoc, String username_aoc, String period_type) {
        return mApiHelper.getSummaryPerformancePoiOsk(userId, teritory, userId_aoc, username_aoc, period_type);
    }

    @Override
    public Single<PerformanceEventResponse> getSummaryPerformanceEvent(String userId, String teritory, String username_aoc) {
        return mApiHelper.getSummaryPerformanceEvent(userId, teritory, username_aoc);
    }

    @Override
    public Single<PerformanceBackcheckingResponse> getSummaryPerformanceBackchecking(String userId, String teritory) {
        return mApiHelper.getSummaryPerformanceBackchecking(userId,teritory);
    }

    @Override
    public Single<PerformanceShareResponse> getSummaryPerformanceShare(String userId, String teritory) {
        return mApiHelper.getSummaryPerformanceShare(userId, teritory);
    }

    @Override
    public Single<SummaryOskResponse> getSummaryOsk(String userId,String teritory) {
        return mApiHelper.getSummaryOsk(userId,teritory);
    }

    @Override
    public Single<SummaryInstitutionResponse> getSummaryInstitution(String userId,String teritory) {
        return mApiHelper.getSummaryInstitution(userId,teritory);
    }

    @Override
    public Single<SummaryEventResponse> getSummaryEvent(String userId, String teritory) {
        return mApiHelper.getSummaryEvent(userId,teritory);
    }

    @Override
    public Single<SummarySalesResponse> getSummarySales(String userId, String teritory) {
        return mApiHelper.getSummarySales(userId,teritory);
    }

    @Override
    public Single<List<String>> getBrandingOperator() {
        return mApiHelper.getBrandingOperator();
    }

    @Override
    public Single<List<Event>> getEventLocations(String latitude, String longitude) {
        return mApiHelper.getEventLocations(latitude, longitude);
    }

    @Override
    public Single<List<BackcheckingMain>> getFeedbackMain(String userid) {
        return mApiHelper.getFeedbackMain(userid);
    }

    @Override
    public Single<FeedbackLeaderNote> getFeedbackLeaderNote(String userid, String userIdSendTo, String locationId, String lastupdate, String typeBackcheck) {
        return mApiHelper.getFeedbackLeaderNote(userid,userIdSendTo,locationId,lastupdate,typeBackcheck);
    }

    @Override
    public Single<List<FeedbackLocationResponse>> getFeedbackLocation(String userid) {
        return mApiHelper.getFeedbackLocation(userid);
    }

    @Override
    public Single<BasicResponse> uploadProfilePhoto(String userId, String imageData) {
        return mApiHelper.uploadProfilePhoto(userId,imageData);
    }

    @Override
    public Single<BasicResponse> feedbackInstitutionInsert(Map<String, String> map) {
        return mApiHelper.feedbackInstitutionInsert(map);
    }

    @Override
    public Single<BasicResponse> feedbackOssOskInsert(Map<String, String> map) {
        return mApiHelper.feedbackOssOskInsert(map);
    }

    @Override
    public Completable validatePhoneNumber(String phoneNumber, Activity activity, PhoneAuthProvider.OnVerificationStateChangedCallbacks callbacks) {
        return mFirebaseHelper.validatePhoneNumber(phoneNumber,
                activity,
                callbacks);
    }

    @Override
    public Completable signInWithPhoneAuthCredential(PhoneAuthCredential credential, Activity activity, OnCompleteListener<AuthResult> callback) {
        return mFirebaseHelper.signInWithPhoneAuthCredential(credential, activity, callback);
    }
}
