package com.androiddeveloper.pens.namiapp.data.firebase;

import android.app.Activity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Completable;

/**
 * Created by miftahun on 7/8/18.
 * <p>
 * Author Miftahun Najat
 * Email miftahunajat@gmail.com
 * Github https://github.com/miftahunajat
 */

@Singleton
public class AppFirebaseHelper implements FirebaseHelper {

    private static final String TAG = "AppFirebaseHelper";
    private FirebaseAuth mFirebaseAuth;

    @Inject
    public AppFirebaseHelper(){
        mFirebaseAuth = FirebaseAuth.getInstance();
    }


    @Override
    public Completable validatePhoneNumber(String phoneNumber, Activity activity,
                                     PhoneAuthProvider.OnVerificationStateChangedCallbacks callbacks) {
        mFirebaseAuth.useAppLanguage();
        return Completable.fromAction(() -> PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,
                60,
                TimeUnit.SECONDS,
                activity,
                callbacks
        ));
    }

    public Completable signInWithPhoneAuthCredential(PhoneAuthCredential credential,
                                               Activity activity,
                                               OnCompleteListener<AuthResult> callback) {
        return Completable.fromAction(()->{
            mFirebaseAuth.signInWithCredential(credential)
                    .addOnCompleteListener(activity, callback);
        });

    }
}
