package com.androiddeveloper.pens.namiapp.data.firebase;

import android.app.Activity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Single;

/**
 * Created by miftahun on 7/8/18.
 * <p>
 * Author Miftahun Najat
 * Email miftahunajat@gmail.com
 * Github https://github.com/miftahunajat
 */

public interface FirebaseHelper {

    Completable validatePhoneNumber(String phoneNumber,
                              Activity activity,
                              PhoneAuthProvider.OnVerificationStateChangedCallbacks callbacks);

    Completable signInWithPhoneAuthCredential(PhoneAuthCredential credential,
                                              Activity activity,
                                              OnCompleteListener<AuthResult> callback);
}
