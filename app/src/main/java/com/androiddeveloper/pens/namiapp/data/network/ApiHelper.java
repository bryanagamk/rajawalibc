package com.androiddeveloper.pens.namiapp.data.network;

import com.androiddeveloper.pens.namiapp.data.network.model.ActualCallListResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.BackCheckingLocationListResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.BackcheckingMain;
import com.androiddeveloper.pens.namiapp.data.network.model.BackcheckingMainResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.BackcheckingResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.BackcheckingStatusResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.BasicConfigResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.BasicResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.ConfigPermitResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.Event;
import com.androiddeveloper.pens.namiapp.data.network.model.Feedback;
import com.androiddeveloper.pens.namiapp.data.network.model.FeedbackLeaderNote;
import com.androiddeveloper.pens.namiapp.data.network.model.FeedbackLocationResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.LoginResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.NotificationResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceBackcheckingResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceEventResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformancePoiOsk;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformancePoiOskResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceShareResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceSummary;
import com.androiddeveloper.pens.namiapp.data.network.model.SooBackCheckingLocationListResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.SummaryEventResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.SummaryInstitutionResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.SummaryOskResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.SummarySalesResponse;
import com.androiddeveloper.pens.namiapp.ui.soobackcheckinglocationlist.Location;
import com.androiddeveloper.pens.namiapp.ui.summary.fragment.SummaryType;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.GET;


/**
 * Created by miftahun on 6/17/18.
 * <p>
 * Author Miftahun Najat
 * Email miftahunajat@gmail.com
 * Github https://github.com/miftahunajat
 */

public interface ApiHelper {

    Observable<List<ActualCallListResponse>> getActualCallList();

    Single<List<BackCheckingLocationListResponse>> getBackCheckingLocationList(String latitude,
                                                                                    String longitude,
                                                                                   String distinstitution,
                                                                                   String distOssOsk);


    Single<List<SooBackCheckingLocationListResponse>> getSooBackchekingLocationList(String userid,
                                                                                        String latitude,
                                                                                        String longitude,
                                                                                        String distinstitution,
                                                                                        String distOssOsk,
                                                                                        String lac,
                                                                                        String ci
    );

    Observable<List<NotificationResponse>> getNotificationList();

    Single<LoginResponse> sentLoginUser(String deviceId, String phoneNumber);

    Single<LoginResponse> sentLoginRajawali(String deviceId, String phoneNumber);

    Single<BasicResponse> sentLogoutUser(String phoneNumber);

    Single<BasicResponse> sentLogoutUserRajawali(String phoneNumber);

    Single<List<BasicConfigResponse>> getServerConfig();

    Single<List<BasicConfigResponse>> getServerConfigRajawali();

    Single<ConfigPermitResponse> getPermissionMenu();

    Single<BasicResponse> insertCheckinStatus(Map<String, String> params);

    Single<BasicResponse> insertCheckoutStatus(String userid,
                                               String latitude,
                                               String longitude);

    Single<BackcheckingStatusResponse> getCheckoutStatus(String userid,
                                                         String latitude,
                                                         String longitude);

    Single<BackcheckingResponse> getBackcheckingInstitutionTotalStudent(String locationId);

    Single<BackcheckingResponse> getBackcheckingInstitutionVisit(String locationId);

    Single<BackcheckingResponse> getBackcheckingOssOskVisit(String locationId);

    Single<BackcheckingResponse> getBackcheckingInstitutionBranding(String locationId);

    Single<BackcheckingResponse> getBackcheckingInstitutionMerchandising(String locationId);

    Single<BackcheckingResponse> getBackcheckingInstitutionMarketShare(String locationId);

    Single<BackcheckingResponse> getBackcheckingInstitutionSalesThrough(String locationId);

    Single<BackcheckingResponse> getBackcheckingInstitutionReadSignal(String locationId);

    Single<BasicResponse> submitBackcheckingForm(Map<String, String> params);

    Single<BackcheckingResponse> getBackcheckingOssOskBranding(String locationId);

    Single<BackcheckingResponse> getBackcheckingOssOskBsDsRs(String locationId);

    Single<BackcheckingResponse> getBackcheckingOssOskTopSelling(String locationId);

    Single<BackcheckingResponse> getBackcheckingOssOskSalesThrough(String locationId);

    Single<BackcheckingResponse> getBackcheckingOssOskReadSignal(String locationId);

    Single<BasicResponse> submitBackcheckingFormOssOsk(Map<String, String> params);

    Single<BasicResponse> getBackcheckingStatus(String userId,
                                                String locationId,
                                                String locationType);

    Single<BasicResponse> getSooBackcheckingStatus(String userId,
                                                String locationId,
                                                String locationType);

    Single<BackcheckingMainResponse> getBackcheckingMain(String userId);

    Single<PerformancePoiOskResponse> getSummaryPerformancePoiOsk(String userId,
                                                                  String teritory,
                                                                  String userId_aoc,
                                                                  String username_aoc,
                                                                  String period_type);

    Single<PerformanceEventResponse> getSummaryPerformanceEvent(String userId,
                                                                String teritory,
                                                                String username_aoc);

    Single<PerformanceBackcheckingResponse> getSummaryPerformanceBackchecking(String userId,String teritory);

    Single<PerformanceShareResponse> getSummaryPerformanceShare(String userId, String teritory);

    Single<SummaryOskResponse> getSummaryOsk(String userId,String teritory);

    Single<SummaryInstitutionResponse> getSummaryInstitution(String userId,String teritory);

    Single<SummaryEventResponse> getSummaryEvent(String userId, String teritory);

    Single<SummarySalesResponse> getSummarySales(String userId, String teritory);

    Single<List<String>> getBrandingOperator();

    Single<List<Event>> getEventLocations(String latitude,
                                          String longitude);

    Single<List<BackcheckingMain>> getFeedbackMain(String userid);

    Single<FeedbackLeaderNote> getFeedbackLeaderNote(String userid,
                                                           String userIdSendTo,
                                                           String locationId,
                                                           String lastupdate,
                                                           String typeBackcheck);

    Single<List<FeedbackLocationResponse>> getFeedbackLocation(String userid);

    Single<BasicResponse> uploadProfilePhoto(String userId, String imageData);

    Single<BasicResponse> feedbackInstitutionInsert(Map<String,String> map);

    Single<BasicResponse> feedbackOssOskInsert(Map<String,String> map);

}
