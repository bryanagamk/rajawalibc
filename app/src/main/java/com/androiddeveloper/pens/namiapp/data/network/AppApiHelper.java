package com.androiddeveloper.pens.namiapp.data.network;

import android.util.Log;

import com.androiddeveloper.pens.namiapp.data.network.model.ActualCallListResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.BackCheckingLocationListResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.BackcheckingMain;
import com.androiddeveloper.pens.namiapp.data.network.model.BackcheckingMainResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.BackcheckingResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.BackcheckingStatusResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.BasicConfigResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.BasicResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.ConfigPermitResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.Event;
import com.androiddeveloper.pens.namiapp.data.network.model.Feedback;
import com.androiddeveloper.pens.namiapp.data.network.model.FeedbackLeaderNote;
import com.androiddeveloper.pens.namiapp.data.network.model.FeedbackLocationResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.LoginResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.NotificationResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceBackcheckingResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceEventResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformancePoiOsk;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformancePoiOskResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceShareResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceSummary;
import com.androiddeveloper.pens.namiapp.data.network.model.SooBackCheckingLocationListResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.SummaryEventResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.SummaryInstitutionResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.SummaryOskResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.SummarySalesResponse;
import com.androiddeveloper.pens.namiapp.data.network.rest.ApiEndPoint;
import com.androiddeveloper.pens.namiapp.ui.soobackcheckinglocationlist.Location;
import com.androiddeveloper.pens.namiapp.ui.summary.fragment.SummaryType;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.interceptors.HttpLoggingInterceptor;
import com.rx2androidnetworking.Rx2AndroidNetworking;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.Single;
import okhttp3.OkHttpClient;
import retrofit2.http.GET;

/**
 * Created by miftahun on 6/17/18.
 * <p>
 * Author Miftahun Najat
 * Email miftahunajat@gmail.com
 * Github https://github.com/miftahunajat
 */

@Singleton

public class AppApiHelper implements ApiHelper {

    public static final String TYPE_BACKCHECK_INSTITUTION = "INSTITUTION";

    public static final String TYPE_BACKCHECK_OSS_OSK = "OSS_OSK";

    OkHttpClient client;

    @Inject
    AppApiHelper(){
        client = new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.MINUTES)
                .readTimeout(15, TimeUnit.MINUTES)
                .build();
        AndroidNetworking.enableLogging(HttpLoggingInterceptor.Level.BODY);
    }

    @Override
    public Observable<List<ActualCallListResponse>> getActualCallList() {
        ActualCallListResponse actualCallListResponse = new ActualCallListResponse();
        List<ActualCallListResponse> actualCallListResponseList = new ArrayList<>();
        actualCallListResponseList.add(actualCallListResponse);
        return Observable.fromArray(actualCallListResponseList);
    }

    @Override
    public Single<List<BackCheckingLocationListResponse>> getBackCheckingLocationList(String latitude,
                                                                                          String longitude,
                                                                                          String distinstitution,
                                                                                          String distOssOsk) {
        Map<String,String> map = new MapBuilder()
                .add("latitude",latitude)
                .add("longitude",longitude)
                .add("distinstitution",distinstitution)
                .add("distossosk",distOssOsk)
                .build();

        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SP_LEADER_BACKCHECKING_LOCATION)
                .addBodyParameter(map)
                .build()
                .getObjectListSingle(BackCheckingLocationListResponse.class);
    }

    @Override
    public Single<List<SooBackCheckingLocationListResponse>> getSooBackchekingLocationList(String userid, String latitude, String longitude, String distinstitution, String distOssOsk, String lac, String ci) {

        Map<String, String> map = new MapBuilder()
                .add("userid", userid)
                .add("latitude",latitude)
                .add("longitude",longitude)
                .add("disoutlet",distinstitution)
                .add("dissite",distOssOsk)
                .add("lac", lac)
                .add("ci", ci)
                .build();

        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SP_RAJAWALI_BACKCHECKING)
                .addBodyParameter(map)
                .build()
                .getObjectListSingle(SooBackCheckingLocationListResponse.class);
    }

    @Override
    public Observable<List<NotificationResponse>> getNotificationList() {
        NotificationResponse notificationResponse = new NotificationResponse();
        List<NotificationResponse> notificationResponseList = new ArrayList<>();
        notificationResponseList.add(notificationResponse);
        return Observable.fromArray(notificationResponseList);
    }

    @Override
    public Single<LoginResponse> sentLoginUser(String deviceId, String phoneNumber) {
        Map<String,String> map = new MapBuilder()
                .add("userid",phoneNumber.replace("+",""))
                .add("deviceid",deviceId)
                .build();
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SP_USER_LOGIN)
                .addBodyParameter(map)
                .build()
                .getObjectSingle(LoginResponse.class);
    }

    @Override
    public Single<LoginResponse> sentLoginRajawali(String deviceId, String phoneNumber) {
        Map<String,String> map = new MapBuilder()
                .add("userid",phoneNumber.replace("+",""))
                .add("deviceid",deviceId)
                .build();
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SP_RAJAWALI_LOGIN)
                .addBodyParameter(map)
                .build()
                .getObjectSingle(LoginResponse.class);
    }

    @Override
    public Single<BasicResponse> sentLogoutUser(String phoneNumber) {
        Log.d("Debug",phoneNumber);
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SP_USER_LOGOUT)
                .addBodyParameter("userid",phoneNumber.replace("+",""))
                .build()
                .getObjectSingle(BasicResponse.class);
    }

    @Override
    public Single<BasicResponse> sentLogoutUserRajawali(String phoneNumber) {
        Log.d("Debug",phoneNumber);
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SP_USER_LOGOUT_RAJAWALI)
                .addBodyParameter("userid",phoneNumber.replace("+",""))
                .build()
                .getObjectSingle(BasicResponse.class);
    }

    @Override
    public Single<List<BasicConfigResponse>> getServerConfig() {
        return  Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SP_CONFIG_STARTUP)
                .build()
                .getObjectListSingle(BasicConfigResponse.class);
    }

    @Override
    public Single<List<BasicConfigResponse>> getServerConfigRajawali() {
        return  Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SP_CONFIG_STARTUP_RAJAWALI)
                .build()
                .getObjectListSingle(BasicConfigResponse.class);
    }

    @Override
    public Single<ConfigPermitResponse> getPermissionMenu() {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SP_CONFIG_PERMIT)
                .build()
                .getObjectSingle(ConfigPermitResponse.class);
    }

    @Override
    public Single<BasicResponse> insertCheckinStatus(Map<String, String> params) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SP_LEADER_CHECKIN_INSERT)
                .addBodyParameter(params)
                .build()
                .getObjectSingle(BasicResponse.class);
    }

    @Override
    public Single<BasicResponse> insertCheckoutStatus(String userid, String latitude, String longitude) {
        Map<String,String> map = new MapBuilder()
                .add("userid",userid)
                .add("latitude",latitude)
                .add("longitude",longitude)
                .build();
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SP_LEADER_CHECKOUT_INSERT)
                .addBodyParameter(map)
                .build()
                .getObjectSingle(BasicResponse.class);
    }

    @Override
    public Single<BackcheckingStatusResponse> getCheckoutStatus(String userid, String latitude, String longitude) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SP_LEADER_CHECKOUT_STATUS)
                .addBodyParameter("userid",userid)
                .addBodyParameter("latitude",latitude)
                .addBodyParameter("longitude",longitude)
                .build()
                .getObjectSingle(BackcheckingStatusResponse.class);
    }

    @Override
    public Single<BackcheckingResponse> getBackcheckingInstitutionTotalStudent(String locationId) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SP_BACKCHECKING_INSTITUTION_TOTAL_STUDENT)
                .addBodyParameter("location_id", locationId)
                .build()
                .getObjectSingle(BackcheckingResponse.class);
    }

    @Override
    public Single<BackcheckingResponse> getBackcheckingInstitutionVisit(String locationId) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SP_BACKCHECKING_INSTITUTION_READ_VISIT)
                .addBodyParameter("location_id",locationId)
                .build()
                .getObjectSingle(BackcheckingResponse.class);
    }

    @Override
    public Single<BackcheckingResponse> getBackcheckingOssOskVisit(String locationId) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SP_BACKCHECKING_OSS_OSK_READ_VISIT)
                .addBodyParameter("location_id",locationId)
                .build()
                .getObjectSingle(BackcheckingResponse.class);
    }

    @Override
    public Single<BackcheckingResponse> getBackcheckingInstitutionBranding(String locationId) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SP_BACKCHECKING_INSTITUTION_READ_BRANDING)
                .addBodyParameter("location_id",locationId)
                .build()
                .getObjectSingle(BackcheckingResponse.class);
    }

    @Override
    public Single<BackcheckingResponse> getBackcheckingInstitutionMerchandising(String locationId) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SP_BACKCHECKING_INSTITUTION_MERCHANDISING)
                .addBodyParameter("location_id",locationId)
                .build()
                .getObjectSingle(BackcheckingResponse.class);
    }

    @Override
    public Single<BackcheckingResponse> getBackcheckingInstitutionMarketShare(String locationId) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SP_BACKCHECKING_INSTITUTION_READ_MS)
                .addBodyParameter("location_id",locationId)
                .build()
                .getObjectSingle(BackcheckingResponse.class);
    }

    @Override
    public Single<BackcheckingResponse> getBackcheckingInstitutionSalesThrough(String locationId) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SP_BACKCHECKING_INSTITUTION_READ_SALES_THROUGH)
                .addBodyParameter("location_id",locationId)
                .build()
                .getObjectSingle(BackcheckingResponse.class);
    }

    @Override
    public Single<BackcheckingResponse> getBackcheckingInstitutionReadSignal(String locationId) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SP_BACKCHECKING_INSTITUTION_READ_SIGNAL)
                .addBodyParameter("location_id",locationId)
                .build()
                .getObjectSingle(BackcheckingResponse.class);
    }

    @Override
    public Single<BasicResponse> submitBackcheckingForm(Map<String, String> params) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SP_BACKCHECKING_INSTITUTION_INSERT)
                .addBodyParameter(params)
                .build()
                .getObjectSingle(BasicResponse.class);
    }

    @Override
    public Single<BackcheckingResponse> getBackcheckingOssOskBranding(String locationId) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SP_BACKCHECKING_OSS_OSK_READ_BRANDING)
                .addBodyParameter("location_id",locationId)
                .build()
                .getObjectSingle(BackcheckingResponse.class);
    }

    @Override
    public Single<BackcheckingResponse> getBackcheckingOssOskBsDsRs(String locationId) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SP_BACKCHECKING_OSS_OSK_READ_BS_DS_RS)
                .addBodyParameter("location_id",locationId)
                .build()
                .getObjectSingle(BackcheckingResponse.class);
    }

    @Override
    public Single<BackcheckingResponse> getBackcheckingOssOskTopSelling(String locationId) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SP_BACKCHECKING_OSS_OSK_READ_TOP_SELLING)
                .addBodyParameter("location_id",locationId)
                .build()
                .getObjectSingle(BackcheckingResponse.class);
    }

    @Override
    public Single<BackcheckingResponse> getBackcheckingOssOskSalesThrough(String locationId) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SP_BACKCHECKING_OSS_OSK_READ_SALES_THROUGH)
                .addBodyParameter("location_id",locationId)
                .build()
                .getObjectSingle(BackcheckingResponse.class);
    }

    @Override
    public Single<BackcheckingResponse> getBackcheckingOssOskReadSignal(String locationId) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SP_BACKCHECKING_OSS_OSK_READ_SIGNAL)
                .addBodyParameter("location_id",locationId)
                .build()
                .getObjectSingle(BackcheckingResponse.class);
    }

    @Override
    public Single<BasicResponse> submitBackcheckingFormOssOsk(Map<String, String> params) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SP_BACKCHECKING_OSS_OSK_INSERT)
                .addBodyParameter(params)
                .build()
                .getObjectSingle(BasicResponse.class);
    }

    @Override
    public Single<BasicResponse> getBackcheckingStatus(String userId, String locationId, String locationType) {
        Map<String,String> map = new MapBuilder()
                .add("userid",userId)
                .add("location_id",locationId)
                .add("location_type",locationType)
                .build();
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SP_BACKCHECKING_STATUS)
                .addBodyParameter(map)
                .build()
                .getObjectSingle(BasicResponse.class);
    }

    @Override
    public Single<BasicResponse> getSooBackcheckingStatus(String userId, String locationId, String locationType) {
        Map<String,String> map = new MapBuilder()
                .add("userid",userId)
                .add("location_id",locationId)
                .add("location_type",locationType)
                .build();
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SP_BACKCHECKING_STATUS)
                .addBodyParameter(map)
                .build()
                .getObjectSingle(BasicResponse.class);
    }

    @Override
    public Single<BackcheckingMainResponse> getBackcheckingMain(String userId) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SP_BACKCHECKING_MAIN)
                .addBodyParameter("userid",userId)
                .build()
                .getObjectSingle(BackcheckingMainResponse.class);
    }

    @Override
    public Single<PerformancePoiOskResponse> getSummaryPerformancePoiOsk(String userId, String teritory, String userId_aoc, String username_aoc, String period_type) {
        Map<String,String> map = new MapBuilder()
                .add("userid",userId)
                .add("teritory",teritory)
                .add("userid_aoc", userId_aoc)
                .add("username_aoc", username_aoc)
                .add("period_type", period_type)
                .build();
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SUMMARY_PERFORMANCE_POI_OSK)
                .addBodyParameter(map)
                .build()
                .getObjectSingle(PerformancePoiOskResponse.class);
    }

    @Override
    public Single<PerformanceEventResponse> getSummaryPerformanceEvent(String userId, String teritory, String username_aoc) {
        Map<String,String> map = new MapBuilder()
                .add("userid",userId)
                .add("teritory",teritory)
                .add("username_aoc", username_aoc)
                .build();
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SUMMARY_PERFORMANCE_EVENT)
                .addBodyParameter(map)
                .build()
                .getObjectSingle(PerformanceEventResponse.class);
    }

    @Override
    public Single<PerformanceBackcheckingResponse> getSummaryPerformanceBackchecking(String userId, String teritory) {
        Map<String,String> map = new MapBuilder()
                .add("userid",userId)
                .add("teritory",teritory)
                .build();
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SUMMARY_PERFORMANCE_BACKCHECKING)
                .addBodyParameter(map)
                .build()
                .getObjectSingle(PerformanceBackcheckingResponse.class);
    }

    @Override
    public Single<PerformanceShareResponse> getSummaryPerformanceShare(String userId, String teritory) {
        Map<String,String> map = new MapBuilder()
                .add("userid",userId)
                .add("teritory",teritory)
                .build();
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SUMMARY_PERFORMANCE_SHARE)
                .addBodyParameter(map)
                .build()
                .getObjectSingle(PerformanceShareResponse.class);
    }

    @Override
    public Single<SummaryOskResponse> getSummaryOsk(String userId,String teritory) {
        Map<String,String> map = new MapBuilder()
                .add("userid",userId)
                .add("teritory",teritory)
                .build();
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SUMMARY_OSK)
                .addBodyParameter(map)
                .build()
                .getObjectSingle(SummaryOskResponse.class);
    }

    @Override
    public Single<SummaryInstitutionResponse> getSummaryInstitution(String userId,String teritory) {
        Map<String,String> map = new MapBuilder()
                .add("userid",userId)
                .add("teritory",teritory)
                .build();
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SUMMARY_INSTITUTION)
                .addBodyParameter(map)
                .build()
                .getObjectSingle(SummaryInstitutionResponse.class);
    }

    @Override
    public Single<SummaryEventResponse> getSummaryEvent(String userId, String teritory) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SUMMARY_EVENT)
                .addBodyParameter("userid",userId)
                .addBodyParameter("teritory",teritory)
                .build()
                .getObjectSingle(SummaryEventResponse.class);
    }

    @Override
    public Single<SummarySalesResponse> getSummarySales(String userId, String teritory) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SUMMARY_SALES)
                .addBodyParameter("userid",userId)
                .addBodyParameter("teritory",teritory)
                .build()
                .getObjectSingle(SummarySalesResponse.class);
    }

    @Override
    public Single<List<String>> getBrandingOperator() {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SP_CONFIG_BRANDING)
                .build()
                .getObjectListSingle(String.class);
    }

    @Override
    public Single<List<Event>> getEventLocations(String latitude,String longitude) {
        Map<String,String> map = new MapBuilder()
                .add("latitude",latitude)
                .add("longitude",longitude)
                .build();
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SP_EVENT_LOCATION_READ)
                .addBodyParameter(map)
                .build()
                .getObjectListSingle(Event.class);
    }

    @Override
    public Single<List<BackcheckingMain>> getFeedbackMain(String userid) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SP_FEEDBACK_MAIN)
                .addBodyParameter("userid",userid)
                .build()
                .getObjectListSingle(BackcheckingMain.class);
    }

    @Override
    public Single<FeedbackLeaderNote> getFeedbackLeaderNote(String userid, String userIdSendTo, String locationId, String lastupdate, String typeBackcheck) {
                Log.d("userid",userid);
                Log.d("userid_sendto",userIdSendTo);
                Log.d("location_id",locationId);
                Log.d("lastupdate",lastupdate);
                Log.d("type_backcheck",typeBackcheck);
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SP_FEEDBACK_LEADERNOTE)
                .addBodyParameter("userid",userid)
                .addBodyParameter("userid_sendto",userIdSendTo)
                .addBodyParameter("location_id",locationId)
                .addBodyParameter("lastupdate",lastupdate)
                .addBodyParameter("type_backcheck",typeBackcheck)
                .build()
                .getObjectSingle(FeedbackLeaderNote.class);
    }

    @Override
    public Single<List<FeedbackLocationResponse>> getFeedbackLocation(String userid) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SP_FEEDBACK_LOCATION)
                .addBodyParameter("userid",userid)
                .build()
                .getObjectListSingle(FeedbackLocationResponse.class);
    }

    @Override
    public Single<BasicResponse> uploadProfilePhoto(String userId, String imageData) {
        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(2,TimeUnit.MINUTES)
                .connectTimeout(2,TimeUnit.MINUTES)
                .writeTimeout(2,TimeUnit.MINUTES)
                .build();
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SP_USER_PROFILE)
                .addBodyParameter("userid",userId)
                .addBodyParameter("imageData",imageData)
                .setOkHttpClient(client)
                .build()
                .getObjectSingle(BasicResponse.class);
    }

    @Override
    public Single<BasicResponse> feedbackInstitutionInsert(Map<String, String> map) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SP_FEEDBACK_INSERT_INSTITUTION)
                .addBodyParameter(map)
                .build()
                .getObjectSingle(BasicResponse.class);
    }

    @Override
    public Single<BasicResponse> feedbackOssOskInsert(Map<String, String> map) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SP_FEEDBACK_INSERT_OSS_OSK)
                .addBodyParameter(map)
                .build()
                .getObjectSingle(BasicResponse.class);
    }

    private class MapBuilder extends HashMap<String, String>{

        public MapBuilder add(String key,String value){
            put(key,value);
            return this;
        }

        public Map<String,String> build(){
            return this;
        }
    }

}
