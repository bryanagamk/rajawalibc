package com.androiddeveloper.pens.namiapp.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BackCheckingLocationListResponse implements Serializable {

    @SerializedName("AREA")
    @Expose
    private String area;
    @SerializedName("regional")
    @Expose
    private String regional;
    @SerializedName("branch")
    @Expose
    private String branch;
    @SerializedName("cluster")
    @Expose
    private String cluster;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName(value="location_id", alternate={"npsn","id_outlet_sefiia"})
    @Expose
    private String locationId;
    @SerializedName("location_type")
    @Expose
    private String locationType;
    @SerializedName(value="institution_name", alternate={"outlet_name"})
    @Expose
    private String institutionName;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("distance")
    @Expose
    private String distance;
    @SerializedName(value="user_id", alternate={"useridto"})
    @Expose
    private String userId;
    @SerializedName(value="user_name", alternate={"usernameto"})
    @Expose
    private String userName;
    @SerializedName("message")
    @Expose
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAREA() {
        return area;
    }

    public void setAREA(String area) {
        this.area = area;
    }

    public String getRegional() {
        return regional;
    }

    public void setRegional(String regional) {
        this.regional = regional;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getCluster() {
        return cluster;
    }

    public void setCluster(String cluster) {
        this.cluster = cluster;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    @Override
    public String toString() {
        return "BackCheckingLocationListResponse{" +
                "area='" + area + '\'' +
                ", regional='" + regional + '\'' +
                ", branch='" + branch + '\'' +
                ", cluster='" + cluster + '\'' +
                ", city='" + city + '\'' +
                ", locationId='" + locationId + '\'' +
                ", locationType='" + locationType + '\'' +
                ", institutionName='" + institutionName + '\'' +
                ", address='" + address + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", distance='" + distance + '\'' +
                '}';
    }
}
