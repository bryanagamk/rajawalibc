package com.androiddeveloper.pens.namiapp.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by miftahun on 7/23/18.
 * <p>
 * Author Miftahun Najat
 * Email miftahunajat@gmail.com
 * Github https://github.com/miftahunajat
 */

public class Backchecking {

    @SerializedName(value="npsn", alternate={"id_outlet_digipos"})
    @Expose
    private String npsn;
    @SerializedName(value="institution_name", alternate={"outlet_name"})
    @Expose
    private String institutionName;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("user_name")
    @Expose
    private String userName;
    @SerializedName("max_visit")
    @Expose
    private String maxVisit;
    @SerializedName("shopsign_branding")
    @Expose
    private String shopsignBranding;
    @SerializedName("tablecloth_branding")
    @Expose
    private String tableclothBranding;
    @SerializedName("sachet")
    @Expose
    private String sachet;
    @SerializedName("canteen_path")
    @Expose
    private String canteenPath;
    @SerializedName("total_poster")
    @Expose
    private String totalPoster;
    @SerializedName("total_banner")
    @Expose
    private String totalBanner;
    @SerializedName("total_shopsign")
    @Expose
    private String totalShopsign;
    @SerializedName("total_tablecloth")
    @Expose
    private String totalTablecloth;

    @SerializedName("ms_telkomsel")
    @Expose
    private Object msTelkomsel;
    @SerializedName("ms_ooredoo")
    @Expose
    private Object msOoredoo;
    @SerializedName("ms_xl")
    @Expose
    private Object msXl;
    @SerializedName("ms_axis")
    @Expose
    private Object msAxis;
    @SerializedName("ms_3")
    @Expose
    private Object ms3;
    @SerializedName("ms_others")
    @Expose
    private Object msOthers;
    @SerializedName("user_telkomsel")
    @Expose
    private String userTelkomsel;
    @SerializedName("user_ooredoo")
    @Expose
    private String userOoredoo;
    @SerializedName("user_xl")
    @Expose
    private String userXl;
    @SerializedName("user_axis")
    @Expose
    private String userAxis;
    @SerializedName("user_3")
    @Expose
    private String user3;
    @SerializedName("user_others")
    @Expose
    private String userOthers;
    @SerializedName("total_respondent")
    @Expose
    private String totalRespondent;
    @SerializedName("dominant_branding")
    @Expose
    private String dominantBranding;
    @SerializedName("outlet_path")
    @Expose
    private String outletPath;

    @SerializedName("bs_telkomsel")
    @Expose
    private String bsTelkomsel;
    @SerializedName("bs_ooredoo")
    @Expose
    private String bsOoredoo;
    @SerializedName("bs_xl")
    @Expose
    private String bsXl;
    @SerializedName("bs_axis")
    @Expose
    private String bsAxis;
    @SerializedName("bs_3")
    @Expose
    private String bs3;
    @SerializedName("bs_others")
    @Expose
    private String bsOthers;
    @SerializedName("ds_telkomsel")
    @Expose
    private String dsTelkomsel;
    @SerializedName("ds_ooredoo")
    @Expose
    private String dsOoredoo;
    @SerializedName("ds_xl")
    @Expose
    private String dsXl;
    @SerializedName("ds_axis")
    @Expose
    private String dsAxis;
    @SerializedName("ds_3")
    @Expose
    private String ds3;
    @SerializedName("ds_others")
    @Expose
    private String dsOthers;
    @SerializedName("rs_telkomsel")
    @Expose
    private String rsTelkomsel;
    @SerializedName("rs_ooredoo")
    @Expose
    private String rsOoredoo;
    @SerializedName("rs_xl")
    @Expose
    private String rsXl;
    @SerializedName("rs_axis")
    @Expose
    private String rsAxis;
    @SerializedName("rs_3")
    @Expose
    private String rs3;
    @SerializedName("rs_others")
    @Expose
    private String rsOthers;
    @SerializedName("quota_inet_telkomsel")
    @Expose
    private String quotaInetTelkomsel;
    @SerializedName("quota_dpi_telkomsel")
    @Expose
    private String quotaDpiTelkomsel;
    @SerializedName("quota_others_telkomsel")
    @Expose
    private String quotaOthersTelkomsel;
    @SerializedName("tp_telkomsel")
    @Expose
    private String tpTelkomsel;
    @SerializedName("eup_telkomsel")
    @Expose
    private String eupTelkomsel;
    @SerializedName("quota_inet_ooredoo")
    @Expose
    private String quotaInetOoredoo;
    @SerializedName("quota_dpi_ooredoo")
    @Expose
    private String quotaDpiOoredoo;
    @SerializedName("quota_others_ooredoo")
    @Expose
    private String quotaOthersOoredoo;
    @SerializedName("tp_ooredoo")
    @Expose
    private String tpOoredoo;
    @SerializedName("eup_ooredoo")
    @Expose
    private String eupOoredoo;
    @SerializedName("quota_inet_xl")
    @Expose
    private String quotaInetXl;
    @SerializedName("quota_dpi_xl")
    @Expose
    private String quotaDpiXl;
    @SerializedName("quota_others_xl")
    @Expose
    private String quotaOthersXl;
    @SerializedName("tp_xl")
    @Expose
    private String tpXl;
    @SerializedName("eup_xl")
    @Expose
    private String eupXl;
    @SerializedName("quota_inet_axis")
    @Expose
    private String quotaInetAxis;
    @SerializedName("quota_dpi_axis")
    @Expose
    private String quotaDpiAxis;
    @SerializedName("quota_others_axis")
    @Expose
    private String quotaOthersAxis;
    @SerializedName("tp_axis")
    @Expose
    private String tpAxis;
    @SerializedName("eup_axis")
    @Expose
    private String eupAxis;
    @SerializedName("quota_inet_3")
    @Expose
    private String quotaInet3;
    @SerializedName("quota_dpi_3")
    @Expose
    private String quotaDpi3;
    @SerializedName("quota_others_3")
    @Expose
    private String quotaOthers3;
    @SerializedName("tp_3")
    @Expose
    private String tp3;
    @SerializedName("eup_3")
    @Expose
    private String eup3;
    @SerializedName("quota_inet_others")
    @Expose
    private String quotaInetOthers;
    @SerializedName("quota_dpi_others")
    @Expose
    private String quotaDpiOthers;
    @SerializedName("quota_others_others")
    @Expose
    private String quotaOthersOthers;
    @SerializedName("tp_others")
    @Expose
    private String tpOthers;
    @SerializedName("eup_others")
    @Expose
    private String eupOthers;

    @SerializedName("text_mtd")
    @Expose
    private String textMtd;
    @SerializedName("text_ptd")
    @Expose
    private String textPtd;

    @SerializedName("total_student")
    @Expose
    private String jumlahSiswa;

    @SerializedName("broadband_sales")
    @Expose
    private String broadbandSales;

    @SerializedName("sales_ptd")
    @Expose
    private String salesPtd;

    @SerializedName("sales_mtd")
    @Expose
    private String salesMtd;

    @SerializedName("3g_xl")
    private String jsonMember3gXl;

    @SerializedName("voice_axis")
    private String voiceAxis;

    @SerializedName("4g_axis")
    private String jsonMember4gAxis;

    @SerializedName("4g_others")
    private String jsonMember4gOthers;

    @SerializedName("3g_axis")
    private String jsonMember3gAxis;

    @SerializedName("voice_telkomsel")
    private String voiceTelkomsel;

    @SerializedName("voice_ooredoo")
    private String voiceOoredoo;

    @SerializedName("3g_others")
    private String jsonMember3gOthers;

    @SerializedName("3g_telkomsel")
    private String jsonMember3gTelkomsel;

    @SerializedName("3g_ooredoo")
    private String jsonMember3gOoredoo;

    @SerializedName("voice_xl")
    private String voiceXl;

    @SerializedName("4g_xl")
    private String jsonMember4gXl;

    @SerializedName("3g_3")
    private String jsonMember3g3;

    @SerializedName("4g_ooredoo")
    private String jsonMember4gOoredoo;

    @SerializedName("voice_others")
    private String voiceOthers;

    @SerializedName("voice_3")
    private String voice3;

    @SerializedName("4g_telkomsel")
    private String jsonMember4gTelkomsel;

    @SerializedName("4g_3")
    private String jsonMember4g3;

    public String getSalesPtd() {
        return salesPtd;
    }

    public void setSalesPtd(String salesPtd) {
        this.salesPtd = salesPtd;
    }

    public String getSalesMtd() {
        return salesMtd;
    }

    public void setSalesMtd(String salesMtd) {
        this.salesMtd = salesMtd;
    }

    public void setJsonMember3gXl(String jsonMember3gXl){
        this.jsonMember3gXl = jsonMember3gXl;
    }

    public String getJsonMember3gXl(){
        return jsonMember3gXl;
    }

    public void setVoiceAxis(String voiceAxis){
        this.voiceAxis = voiceAxis;
    }

    public String getVoiceAxis(){
        return voiceAxis;
    }

    public void setJsonMember4gAxis(String jsonMember4gAxis){
        this.jsonMember4gAxis = jsonMember4gAxis;
    }

    public String getJsonMember4gAxis(){
        return jsonMember4gAxis;
    }

    public void setJsonMember4gOthers(String jsonMember4gOthers){
        this.jsonMember4gOthers = jsonMember4gOthers;
    }

    public String getJsonMember4gOthers(){
        return jsonMember4gOthers;
    }

    public void setJsonMember3gAxis(String jsonMember3gAxis){
        this.jsonMember3gAxis = jsonMember3gAxis;
    }

    public String getJsonMember3gAxis(){
        return jsonMember3gAxis;
    }

    public void setVoiceTelkomsel(String voiceTelkomsel){
        this.voiceTelkomsel = voiceTelkomsel;
    }

    public String getVoiceTelkomsel(){
        return voiceTelkomsel;
    }

    public void setVoiceOoredoo(String voiceOoredoo){
        this.voiceOoredoo = voiceOoredoo;
    }

    public String getVoiceOoredoo(){
        return voiceOoredoo;
    }

    public void setJsonMember3gOthers(String jsonMember3gOthers){
        this.jsonMember3gOthers = jsonMember3gOthers;
    }

    public String getJsonMember3gOthers(){
        return jsonMember3gOthers;
    }

    public void setJsonMember3gTelkomsel(String jsonMember3gTelkomsel){
        this.jsonMember3gTelkomsel = jsonMember3gTelkomsel;
    }

    public String getJsonMember3gTelkomsel(){
        return jsonMember3gTelkomsel;
    }

    public void setJsonMember3gOoredoo(String jsonMember3gOoredoo){
        this.jsonMember3gOoredoo = jsonMember3gOoredoo;
    }

    public String getJsonMember3gOoredoo(){
        return jsonMember3gOoredoo;
    }

    public void setVoiceXl(String voiceXl){
        this.voiceXl = voiceXl;
    }

    public String getVoiceXl(){
        return voiceXl;
    }

    public void setJsonMember4gXl(String jsonMember4gXl){
        this.jsonMember4gXl = jsonMember4gXl;
    }

    public String getJsonMember4gXl(){
        return jsonMember4gXl;
    }

    public void setJsonMember3g3(String jsonMember3g3){
        this.jsonMember3g3 = jsonMember3g3;
    }

    public String getJsonMember3g3(){
        return jsonMember3g3;
    }

    public void setJsonMember4gOoredoo(String jsonMember4gOoredoo){
        this.jsonMember4gOoredoo = jsonMember4gOoredoo;
    }

    public String getJsonMember4gOoredoo(){
        return jsonMember4gOoredoo;
    }

    public void setVoiceOthers(String voiceOthers){
        this.voiceOthers = voiceOthers;
    }

    public String getVoiceOthers(){
        return voiceOthers;
    }

    public void setVoice3(String voice3){
        this.voice3 = voice3;
    }

    public String getVoice3(){
        return voice3;
    }

    public void setJsonMember4gTelkomsel(String jsonMember4gTelkomsel){
        this.jsonMember4gTelkomsel = jsonMember4gTelkomsel;
    }

    public String getJsonMember4gTelkomsel(){
        return jsonMember4gTelkomsel;
    }

    public void setJsonMember4g3(String jsonMember4g3){
        this.jsonMember4g3 = jsonMember4g3;
    }

    public String getJsonMember4g3(){
        return jsonMember4g3;
    }

    public String getBroadbandSales() {
        return broadbandSales;
    }

    public void setBroadbandSales(String broadbandSales) {
        this.broadbandSales = broadbandSales;
    }

    public String getJumlahSiswa() {
        return jumlahSiswa;
    }

    public void setJumlahSiswa(String jumlahSiswa) {
        this.jumlahSiswa = jumlahSiswa;
    }

    public String getTextMtd() {
        return textMtd;
    }

    public void setTextMtd(String textMtd) {
        this.textMtd = textMtd;
    }

    public String getTextPtd() {
        return textPtd;
    }

    public void setTextPtd(String textPtd) {
        this.textPtd = textPtd;
    }

    public String getBsTelkomsel() {
        return bsTelkomsel;
    }

    public void setBsTelkomsel(String bsTelkomsel) {
        this.bsTelkomsel = bsTelkomsel;
    }

    public String getBsOoredoo() {
        return bsOoredoo;
    }

    public void setBsOoredoo(String bsOoredoo) {
        this.bsOoredoo = bsOoredoo;
    }

    public String getBsXl() {
        return bsXl;
    }

    public void setBsXl(String bsXl) {
        this.bsXl = bsXl;
    }

    public String getBsAxis() {
        return bsAxis;
    }

    public void setBsAxis(String bsAxis) {
        this.bsAxis = bsAxis;
    }

    public String getBs3() {
        return bs3;
    }

    public void setBs3(String bs3) {
        this.bs3 = bs3;
    }

    public String getBsOthers() {
        return bsOthers;
    }

    public void setBsOthers(String bsOthers) {
        this.bsOthers = bsOthers;
    }

    public String getDsTelkomsel() {
        return dsTelkomsel;
    }

    public void setDsTelkomsel(String dsTelkomsel) {
        this.dsTelkomsel = dsTelkomsel;
    }

    public String getDsOoredoo() {
        return dsOoredoo;
    }

    public void setDsOoredoo(String dsOoredoo) {
        this.dsOoredoo = dsOoredoo;
    }

    public String getDsXl() {
        return dsXl;
    }

    public void setDsXl(String dsXl) {
        this.dsXl = dsXl;
    }

    public String getDsAxis() {
        return dsAxis;
    }

    public void setDsAxis(String dsAxis) {
        this.dsAxis = dsAxis;
    }

    public String getDs3() {
        return ds3;
    }

    public void setDs3(String ds3) {
        this.ds3 = ds3;
    }

    public String getDsOthers() {
        return dsOthers;
    }

    public void setDsOthers(String dsOthers) {
        this.dsOthers = dsOthers;
    }

    public String getRsTelkomsel() {
        return rsTelkomsel;
    }

    public void setRsTelkomsel(String rsTelkomsel) {
        this.rsTelkomsel = rsTelkomsel;
    }

    public String getRsOoredoo() {
        return rsOoredoo;
    }

    public void setRsOoredoo(String rsOoredoo) {
        this.rsOoredoo = rsOoredoo;
    }

    public String getRsXl() {
        return rsXl;
    }

    public void setRsXl(String rsXl) {
        this.rsXl = rsXl;
    }

    public String getRsAxis() {
        return rsAxis;
    }

    public void setRsAxis(String rsAxis) {
        this.rsAxis = rsAxis;
    }

    public String getRs3() {
        return rs3;
    }

    public void setRs3(String rs3) {
        this.rs3 = rs3;
    }

    public String getRsOthers() {
        return rsOthers;
    }

    public void setRsOthers(String rsOthers) {
        this.rsOthers = rsOthers;
    }

    public Object getMsTelkomsel() {
        return msTelkomsel;
    }

    public void setMsTelkomsel(Object msTelkomsel) {
        this.msTelkomsel = msTelkomsel;
    }

    public Object getMsOoredoo() {
        return msOoredoo;
    }

    public void setMsOoredoo(Object msOoredoo) {
        this.msOoredoo = msOoredoo;
    }

    public Object getMsXl() {
        return msXl;
    }

    public void setMsXl(Object msXl) {
        this.msXl = msXl;
    }

    public Object getMsAxis() {
        return msAxis;
    }

    public void setMsAxis(Object msAxis) {
        this.msAxis = msAxis;
    }

    public Object getMs3() {
        return ms3;
    }

    public void setMs3(Object ms3) {
        this.ms3 = ms3;
    }

    public Object getMsOthers() {
        return msOthers;
    }

    public void setMsOthers(Object msOthers) {
        this.msOthers = msOthers;
    }

    public String getUserTelkomsel() {
        return userTelkomsel;
    }

    public void setUserTelkomsel(String userTelkomsel) {
        this.userTelkomsel = userTelkomsel;
    }

    public String getUserOoredoo() {
        return userOoredoo;
    }

    public void setUserOoredoo(String userOoredoo) {
        this.userOoredoo = userOoredoo;
    }

    public String getUserXl() {
        return userXl;
    }

    public void setUserXl(String userXl) {
        this.userXl = userXl;
    }

    public String getUserAxis() {
        return userAxis;
    }

    public void setUserAxis(String userAxis) {
        this.userAxis = userAxis;
    }

    public String getUser3() {
        return user3;
    }

    public void setUser3(String user3) {
        this.user3 = user3;
    }

    public String getUserOthers() {
        return userOthers;
    }

    public void setUserOthers(String userOthers) {
        this.userOthers = userOthers;
    }

    public String getTotalRespondent() {
        return totalRespondent;
    }

    public void setTotalRespondent(String totalRespondent) {
        this.totalRespondent = totalRespondent;
    }

    public String getNpsn() {
        return npsn;
    }

    public void setNpsn(String npsn) {
        this.npsn = npsn;
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMaxVisit() {
        return maxVisit;
    }

    public void setMaxVisit(String maxVisit) {
        this.maxVisit = maxVisit;
    }

    public String getShopsignBranding() {
        return shopsignBranding;
    }

    public void setShopsignBranding(String shopsignBranding) {
        this.shopsignBranding = shopsignBranding;
    }

    public String getTableclothBranding() {
        return tableclothBranding;
    }

    public void setTableclothBranding(String tableclothBranding) {
        this.tableclothBranding = tableclothBranding;
    }

    public String getSachet() {
        return sachet;
    }

    public void setSachet(String sachet) {
        this.sachet = sachet;
    }

    public String getCanteenPath() {
        return canteenPath;
    }

    public void setCanteenPath(String canteenPath) {
        this.canteenPath = canteenPath;
    }

    public String getTotalPoster() {
        return totalPoster;
    }

    public void setTotalPoster(String totalPoster) {
        this.totalPoster = totalPoster;
    }

    public String getTotalBanner() {
        return totalBanner;
    }

    public void setTotalBanner(String totalBanner) {
        this.totalBanner = totalBanner;
    }

    public String getTotalShopsign() {
        return totalShopsign;
    }

    public void setTotalShopsign(String totalShopsign) {
        this.totalShopsign = totalShopsign;
    }

    public String getTotalTablecloth() {
        return totalTablecloth;
    }

    public void setTotalTablecloth(String totalTablecloth) {
        this.totalTablecloth = totalTablecloth;
    }

    public String getDominantBranding() {
        return dominantBranding;
    }

    public void setDominantBranding(String dominantBranding) {
        this.dominantBranding = dominantBranding;
    }

    public String getOutletPath() {
        return outletPath;
    }

    public void setOutletPath(String outletPath) {
        this.outletPath = outletPath;
    }

    public String getQuotaInetTelkomsel() {
        return quotaInetTelkomsel;
    }

    public void setQuotaInetTelkomsel(String quotaInetTelkomsel) {
        this.quotaInetTelkomsel = quotaInetTelkomsel;
    }

    public String getQuotaDpiTelkomsel() {
        return quotaDpiTelkomsel;
    }

    public void setQuotaDpiTelkomsel(String quotaDpiTelkomsel) {
        this.quotaDpiTelkomsel = quotaDpiTelkomsel;
    }

    public String getQuotaOthersTelkomsel() {
        return quotaOthersTelkomsel;
    }

    public void setQuotaOthersTelkomsel(String quotaOthersTelkomsel) {
        this.quotaOthersTelkomsel = quotaOthersTelkomsel;
    }

    public String getTpTelkomsel() {
        return tpTelkomsel;
    }

    public void setTpTelkomsel(String tpTelkomsel) {
        this.tpTelkomsel = tpTelkomsel;
    }

    public String getEupTelkomsel() {
        return eupTelkomsel;
    }

    public void setEupTelkomsel(String eupTelkomsel) {
        this.eupTelkomsel = eupTelkomsel;
    }

    public String getQuotaInetOoredoo() {
        return quotaInetOoredoo;
    }

    public void setQuotaInetOoredoo(String quotaInetOoredoo) {
        this.quotaInetOoredoo = quotaInetOoredoo;
    }

    public String getQuotaDpiOoredoo() {
        return quotaDpiOoredoo;
    }

    public void setQuotaDpiOoredoo(String quotaDpiOoredoo) {
        this.quotaDpiOoredoo = quotaDpiOoredoo;
    }

    public String getQuotaOthersOoredoo() {
        return quotaOthersOoredoo;
    }

    public void setQuotaOthersOoredoo(String quotaOthersOoredoo) {
        this.quotaOthersOoredoo = quotaOthersOoredoo;
    }

    public String getTpOoredoo() {
        return tpOoredoo;
    }

    public void setTpOoredoo(String tpOoredoo) {
        this.tpOoredoo = tpOoredoo;
    }

    public String getEupOoredoo() {
        return eupOoredoo;
    }

    public void setEupOoredoo(String eupOoredoo) {
        this.eupOoredoo = eupOoredoo;
    }

    public String getQuotaInetXl() {
        return quotaInetXl;
    }

    public void setQuotaInetXl(String quotaInetXl) {
        this.quotaInetXl = quotaInetXl;
    }

    public String getQuotaDpiXl() {
        return quotaDpiXl;
    }

    public void setQuotaDpiXl(String quotaDpiXl) {
        this.quotaDpiXl = quotaDpiXl;
    }

    public String getQuotaOthersXl() {
        return quotaOthersXl;
    }

    public void setQuotaOthersXl(String quotaOthersXl) {
        this.quotaOthersXl = quotaOthersXl;
    }

    public String getTpXl() {
        return tpXl;
    }

    public void setTpXl(String tpXl) {
        this.tpXl = tpXl;
    }

    public String getEupXl() {
        return eupXl;
    }

    public void setEupXl(String eupXl) {
        this.eupXl = eupXl;
    }

    public String getQuotaInetAxis() {
        return quotaInetAxis;
    }

    public void setQuotaInetAxis(String quotaInetAxis) {
        this.quotaInetAxis = quotaInetAxis;
    }

    public String getQuotaDpiAxis() {
        return quotaDpiAxis;
    }

    public void setQuotaDpiAxis(String quotaDpiAxis) {
        this.quotaDpiAxis = quotaDpiAxis;
    }

    public String getQuotaOthersAxis() {
        return quotaOthersAxis;
    }

    public void setQuotaOthersAxis(String quotaOthersAxis) {
        this.quotaOthersAxis = quotaOthersAxis;
    }

    public String getTpAxis() {
        return tpAxis;
    }

    public void setTpAxis(String tpAxis) {
        this.tpAxis = tpAxis;
    }

    public String getEupAxis() {
        return eupAxis;
    }

    public void setEupAxis(String eupAxis) {
        this.eupAxis = eupAxis;
    }

    public String getQuotaInet3() {
        return quotaInet3;
    }

    public void setQuotaInet3(String quotaInet3) {
        this.quotaInet3 = quotaInet3;
    }

    public String getQuotaDpi3() {
        return quotaDpi3;
    }

    public void setQuotaDpi3(String quotaDpi3) {
        this.quotaDpi3 = quotaDpi3;
    }

    public String getQuotaOthers3() {
        return quotaOthers3;
    }

    public void setQuotaOthers3(String quotaOthers3) {
        this.quotaOthers3 = quotaOthers3;
    }

    public String getTp3() {
        return tp3;
    }

    public void setTp3(String tp3) {
        this.tp3 = tp3;
    }

    public String getEup3() {
        return eup3;
    }

    public void setEup3(String eup3) {
        this.eup3 = eup3;
    }

    public String getQuotaInetOthers() {
        return quotaInetOthers;
    }

    public void setQuotaInetOthers(String quotaInetOthers) {
        this.quotaInetOthers = quotaInetOthers;
    }

    public String getQuotaDpiOthers() {
        return quotaDpiOthers;
    }

    public void setQuotaDpiOthers(String quotaDpiOthers) {
        this.quotaDpiOthers = quotaDpiOthers;
    }

    public String getQuotaOthersOthers() {
        return quotaOthersOthers;
    }

    public void setQuotaOthersOthers(String quotaOthersOthers) {
        this.quotaOthersOthers = quotaOthersOthers;
    }

    public String getTpOthers() {
        return tpOthers;
    }

    public void setTpOthers(String tpOthers) {
        this.tpOthers = tpOthers;
    }

    public String getEupOthers() {
        return eupOthers;
    }

    public void setEupOthers(String eupOthers) {
        this.eupOthers = eupOthers;
    }

    @Override
    public String toString() {
        return "Backchecking{" +
                "npsn='" + npsn + '\'' +
                ", institutionName='" + institutionName + '\'' +
                ", userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                ", maxVisit='" + maxVisit + '\'' +
                ", shopsignBranding='" + shopsignBranding + '\'' +
                ", tableclothBranding='" + tableclothBranding + '\'' +
                ", sachet='" + sachet + '\'' +
                ", canteenPath='" + canteenPath + '\'' +
                ", totalPoster='" + totalPoster + '\'' +
                ", totalBanner='" + totalBanner + '\'' +
                ", totalShopsign='" + totalShopsign + '\'' +
                ", totalTablecloth='" + totalTablecloth + '\'' +
                ", msTelkomsel=" + msTelkomsel +
                ", msOoredoo=" + msOoredoo +
                ", msXl=" + msXl +
                ", msAxis=" + msAxis +
                ", ms3=" + ms3 +
                ", msOthers=" + msOthers +
                ", userTelkomsel='" + userTelkomsel + '\'' +
                ", userOoredoo='" + userOoredoo + '\'' +
                ", userXl='" + userXl + '\'' +
                ", userAxis='" + userAxis + '\'' +
                ", user3='" + user3 + '\'' +
                ", userOthers='" + userOthers + '\'' +
                ", totalRespondent='" + totalRespondent + '\'' +
                ", dominantBranding='" + dominantBranding + '\'' +
                ", outletPath='" + outletPath + '\'' +
                ", bsTelkomsel='" + bsTelkomsel + '\'' +
                ", bsOoredoo='" + bsOoredoo + '\'' +
                ", bsXl='" + bsXl + '\'' +
                ", bsAxis='" + bsAxis + '\'' +
                ", bs3='" + bs3 + '\'' +
                ", bsOthers='" + bsOthers + '\'' +
                ", dsTelkomsel='" + dsTelkomsel + '\'' +
                ", dsOoredoo='" + dsOoredoo + '\'' +
                ", dsXl='" + dsXl + '\'' +
                ", dsAxis='" + dsAxis + '\'' +
                ", ds3='" + ds3 + '\'' +
                ", dsOthers='" + dsOthers + '\'' +
                ", rsTelkomsel='" + rsTelkomsel + '\'' +
                ", rsOoredoo='" + rsOoredoo + '\'' +
                ", rsXl='" + rsXl + '\'' +
                ", rsAxis='" + rsAxis + '\'' +
                ", rs3='" + rs3 + '\'' +
                ", rsOthers='" + rsOthers + '\'' +
                ", quotaInetTelkomsel='" + quotaInetTelkomsel + '\'' +
                ", quotaDpiTelkomsel='" + quotaDpiTelkomsel + '\'' +
                ", quotaOthersTelkomsel='" + quotaOthersTelkomsel + '\'' +
                ", tpTelkomsel='" + tpTelkomsel + '\'' +
                ", eupTelkomsel='" + eupTelkomsel + '\'' +
                ", quotaInetOoredoo='" + quotaInetOoredoo + '\'' +
                ", quotaDpiOoredoo='" + quotaDpiOoredoo + '\'' +
                ", quotaOthersOoredoo='" + quotaOthersOoredoo + '\'' +
                ", tpOoredoo='" + tpOoredoo + '\'' +
                ", eupOoredoo='" + eupOoredoo + '\'' +
                ", quotaInetXl='" + quotaInetXl + '\'' +
                ", quotaDpiXl='" + quotaDpiXl + '\'' +
                ", quotaOthersXl='" + quotaOthersXl + '\'' +
                ", tpXl='" + tpXl + '\'' +
                ", eupXl='" + eupXl + '\'' +
                ", quotaInetAxis='" + quotaInetAxis + '\'' +
                ", quotaDpiAxis='" + quotaDpiAxis + '\'' +
                ", quotaOthersAxis='" + quotaOthersAxis + '\'' +
                ", tpAxis='" + tpAxis + '\'' +
                ", eupAxis='" + eupAxis + '\'' +
                ", quotaInet3='" + quotaInet3 + '\'' +
                ", quotaDpi3='" + quotaDpi3 + '\'' +
                ", quotaOthers3='" + quotaOthers3 + '\'' +
                ", tp3='" + tp3 + '\'' +
                ", eup3='" + eup3 + '\'' +
                ", quotaInetOthers='" + quotaInetOthers + '\'' +
                ", quotaDpiOthers='" + quotaDpiOthers + '\'' +
                ", quotaOthersOthers='" + quotaOthersOthers + '\'' +
                ", tpOthers='" + tpOthers + '\'' +
                ", eupOthers='" + eupOthers + '\'' +
                ", textMtd='" + textMtd + '\'' +
                ", textPtd='" + textPtd + '\'' +
                ", jumlahSiswa='" + jumlahSiswa + '\'' +
                ", broadbandSales='" + broadbandSales + '\'' +
                ", jsonMember3gXl='" + jsonMember3gXl + '\'' +
                ", voiceAxis='" + voiceAxis + '\'' +
                ", jsonMember4gAxis='" + jsonMember4gAxis + '\'' +
                ", jsonMember4gOthers='" + jsonMember4gOthers + '\'' +
                ", jsonMember3gAxis='" + jsonMember3gAxis + '\'' +
                ", voiceTelkomsel='" + voiceTelkomsel + '\'' +
                ", voiceOoredoo='" + voiceOoredoo + '\'' +
                ", jsonMember3gOthers='" + jsonMember3gOthers + '\'' +
                ", jsonMember3gTelkomsel='" + jsonMember3gTelkomsel + '\'' +
                ", jsonMember3gOoredoo='" + jsonMember3gOoredoo + '\'' +
                ", voiceXl='" + voiceXl + '\'' +
                ", jsonMember4gXl='" + jsonMember4gXl + '\'' +
                ", jsonMember3g3='" + jsonMember3g3 + '\'' +
                ", jsonMember4gOoredoo='" + jsonMember4gOoredoo + '\'' +
                ", voiceOthers='" + voiceOthers + '\'' +
                ", voice3='" + voice3 + '\'' +
                ", jsonMember4gTelkomsel='" + jsonMember4gTelkomsel + '\'' +
                ", jsonMember4g3='" + jsonMember4g3 + '\'' +
                '}';
    }
}