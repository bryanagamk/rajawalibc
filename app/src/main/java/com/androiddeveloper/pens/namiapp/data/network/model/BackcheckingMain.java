package com.androiddeveloper.pens.namiapp.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class BackcheckingMain{

	@SerializedName("area")
	@Expose
	private String area;

	@SerializedName("cluster")
	@Expose
	private String cluster;

	@SerializedName("regional")
	@Expose
	private String regional;

	@SerializedName("city")
	@Expose
	private String city;

	@SerializedName("user_name")
	@Expose
	private String userName;

	@SerializedName("branch")
	@Expose
	private String branch;

	@SerializedName("total_backchecking_osk_ptd")
	@Expose
	private String totalBackcheckingOskPtd;

	@SerializedName("user_id")
	@Expose
	private String userId;

	@SerializedName("total_backchecking_osk_mtd")
	@Expose
	private String totalBackcheckingOskMtd;

	@SerializedName("total_backchecking_institution_ptd")
	@Expose
	private String totalBackcheckingInstitutionPtd;

	@SerializedName("total_feedback_osk_mtd")
	@Expose
	private String totalFeedbackOskMtd;

	@SerializedName("total_feedback_institution_mtd")
	@Expose
	private String totalFeedbackInstitutionMtd;

	@SerializedName("total_backchecking_institution_mtd")
	@Expose
	private String totalBackcheckingInstitutionMtd;

	@SerializedName("total_feedback_osk_ptd")
	@Expose
	private String totalFeedbackOskPtd;

	@SerializedName("total_feedback_institution_ptd")
	@Expose
	private String totalFeedbackInstitutionPtd;

	public String getTotalFeedbackOskPtd() {
		return totalFeedbackOskPtd;
	}

	public void setTotalFeedbackOskPtd(String totalFeedbackOskPtd) {
		this.totalFeedbackOskPtd = totalFeedbackOskPtd;
	}

	public String getTotalFeedbackInstitutionPtd() {
		return totalFeedbackInstitutionPtd;
	}

	public void setTotalFeedbackInstitutionPtd(String totalFeedbackInstitutionPtd) {
		this.totalFeedbackInstitutionPtd = totalFeedbackInstitutionPtd;
	}

	public void setArea(String area){
		this.area = area;
	}

	public String getArea(){
		return area;
	}

	public void setCluster(String cluster){
		this.cluster = cluster;
	}

	public String getCluster(){
		return cluster;
	}

	public void setRegional(String regional){
		this.regional = regional;
	}

	public String getRegional(){
		return regional;
	}

	public void setCity(String city){
		this.city = city;
	}

	public String getCity(){
		return city;
	}

	public void setUserName(String userName){
		this.userName = userName;
	}

	public String getUserName(){
		return userName;
	}

	public void setBranch(String branch){
		this.branch = branch;
	}

	public String getBranch(){
		return branch;
	}

	public void setTotalBackcheckingOskPtd(String totalBackcheckingOskPtd){
		this.totalBackcheckingOskPtd = totalBackcheckingOskPtd;
	}

	public String getTotalBackcheckingOskPtd(){
		return totalBackcheckingOskPtd;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setTotalBackcheckingOskMtd(String totalBackcheckingOskMtd){
		this.totalBackcheckingOskMtd = totalBackcheckingOskMtd;
	}

	public String getTotalBackcheckingOskMtd(){
		return totalBackcheckingOskMtd;
	}

	public void setTotalBackcheckingInstitutionPtd(String totalBackcheckingInstitutionPtd){
		this.totalBackcheckingInstitutionPtd = totalBackcheckingInstitutionPtd;
	}

	public String getTotalBackcheckingInstitutionPtd(){
		return totalBackcheckingInstitutionPtd;
	}

	public void setTotalFeedbackOskMtd(String totalFeedbackOskMtd){
		this.totalFeedbackOskMtd = totalFeedbackOskMtd;
	}

	public String getTotalFeedbackOskMtd(){
		return totalFeedbackOskMtd;
	}

	public void setTotalFeedbackInstitutionMtd(String totalFeedbackInstitutionMtd){
		this.totalFeedbackInstitutionMtd = totalFeedbackInstitutionMtd;
	}

	public String getTotalFeedbackInstitutionMtd(){
		return totalFeedbackInstitutionMtd;
	}

	public void setTotalBackcheckingInstitutionMtd(String totalBackcheckingInstitutionMtd){
		this.totalBackcheckingInstitutionMtd = totalBackcheckingInstitutionMtd;
	}

	public String getTotalBackcheckingInstitutionMtd(){
		return totalBackcheckingInstitutionMtd;
	}

	@Override
 	public String toString(){
		return 
			"BackcheckingMain{" + 
			"area = '" + area + '\'' + 
			",cluster = '" + cluster + '\'' + 
			",regional = '" + regional + '\'' + 
			",city = '" + city + '\'' + 
			",user_name = '" + userName + '\'' + 
			",branch = '" + branch + '\'' + 
			",total_backchecking_osk_ptd = '" + totalBackcheckingOskPtd + '\'' + 
			",user_id = '" + userId + '\'' + 
			",total_backchecking_osk_mtd = '" + totalBackcheckingOskMtd + '\'' + 
			",total_backchecking_institution_ptd = '" + totalBackcheckingInstitutionPtd + '\'' + 
			",total_feedback_osk_mtd = '" + totalFeedbackOskMtd + '\'' + 
			",total_feedback_institution_mtd = '" + totalFeedbackInstitutionMtd + '\'' + 
			",total_backchecking_institution_mtd = '" + totalBackcheckingInstitutionMtd + '\'' + 
			"}";
		}
}