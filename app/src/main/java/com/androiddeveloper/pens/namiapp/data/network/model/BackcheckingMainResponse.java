package com.androiddeveloper.pens.namiapp.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class BackcheckingMainResponse{

	@SerializedName("backchecking_main")
	@Expose
	private BackcheckingMain backcheckingMain;

	@SerializedName("success")
	@Expose
	private boolean success;

	@SerializedName("message")
	@Expose
	private String message;

	public void setBackcheckingMain(BackcheckingMain backcheckingMain){
		this.backcheckingMain = backcheckingMain;
	}

	public BackcheckingMain getBackcheckingMain(){
		return backcheckingMain;
	}

	public void setSuccess(boolean success){
		this.success = success;
	}

	public boolean isSuccess(){
		return success;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	@Override
 	public String toString(){
		return 
			"BackcheckingMainResponse{" + 
			"backchecking_main = '" + backcheckingMain + '\'' + 
			",success = '" + success + '\'' + 
			",message = '" + message + '\'' + 
			"}";
		}
}