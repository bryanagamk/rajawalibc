package com.androiddeveloper.pens.namiapp.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by miftahun on 7/23/18.
 * <p>
 * Author Miftahun Najat
 * Email miftahunajat@gmail.com
 * Github https://github.com/miftahunajat
 */

public class BackcheckingResponse {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("backchecking")
    @Expose
    private Backchecking backchecking;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Backchecking getBackchecking() {
        return backchecking;
    }

    public void setBackchecking(Backchecking backchecking) {
        this.backchecking = backchecking;
    }

    @Override
    public String toString() {
        return "BackcheckingResponse{" +
                "success=" + success +
                ", message='" + message + '\'' +
                ", backchecking=" + backchecking +
                '}';
    }
}
