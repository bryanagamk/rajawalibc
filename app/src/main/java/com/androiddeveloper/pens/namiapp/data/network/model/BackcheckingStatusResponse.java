package com.androiddeveloper.pens.namiapp.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by miftahun on 8/11/18.
 */

public class BackcheckingStatusResponse {
    @SerializedName("backchecking")
    @Expose
    private BackCheckingLocationListResponse backcheckingMain;

    @SerializedName("success")
    @Expose
    private boolean success;

    @SerializedName("message")
    @Expose
    private String message;

    public void BackCheckingLocationListResponse(BackCheckingLocationListResponse backcheckingMain){
        this.backcheckingMain = backcheckingMain;
    }

    public BackCheckingLocationListResponse getBackcheckingMain() {
        return backcheckingMain;
    }

    public void setBackcheckingMain(BackCheckingLocationListResponse backcheckingMain) {
        this.backcheckingMain = backcheckingMain;
    }

    public BackCheckingLocationListResponse BackCheckingLocationListResponse(){
        return backcheckingMain;
    }

    public void setSuccess(boolean success){
        this.success = success;
    }

    public boolean isSuccess(){
        return success;
    }

    public void setMessage(String message){
        this.message = message;
    }

    public String getMessage(){
        return message;
    }

    @Override
    public String toString(){
        return
                "BackCheckingLocationListResponseResponse{" +
                        "backchecking_main = '" + backcheckingMain + '\'' +
                        ",success = '" + success + '\'' +
                        ",message = '" + message + '\'' +
                        "}";
    }
}
