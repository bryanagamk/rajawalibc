package com.androiddeveloper.pens.namiapp.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by miftahun on 7/18/18.
 * <p>
 * Author Miftahun Najat
 * Email miftahunajat@gmail.com
 * Github https://github.com/miftahunajat
 */

public class BasicConfigResponse {

    @SerializedName("param_variable")
    @Expose
    private String paramVariable;
    @SerializedName("param_value")
    @Expose
    private String paramValue;



    public String getParamVariable() {
        return paramVariable;
    }

    public void setParamVariable(String paramVariable) {
        this.paramVariable = paramVariable;
    }

    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    @Override
    public String toString() {
        return "BasicConfigResponse{" +
                "paramVariable='" + paramVariable + '\'' +
                ", paramValue='" + paramValue + '\'' +
                '}';
    }
}