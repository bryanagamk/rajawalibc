package com.androiddeveloper.pens.namiapp.data.network.model;

import com.androiddeveloper.pens.namiapp.utils.MenuEnum;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by miftahun on 7/20/18.
 * <p>
 * Email miftahunajat@gmail.com
 * Github https://github.com/miftahunajat
 */

public class ConfigPermitResponse {

    @SerializedName("pjp_menu")
    @Expose
    private String pjpMenu;

    @SerializedName("oss_osk_menu")
    @Expose
    private String ossOskMenu;
    @SerializedName("institution_menu")
    @Expose
    private String institutionMenu;
    @SerializedName("event_menu")
    @Expose
    private String eventMenu;
    @SerializedName("backchecking_menu")
    @Expose
    private String backcheckingMenu;
    @SerializedName("feedback_menu")
    @Expose
    private String feedbackMenu;

    public List<MenuEnum> getAvailableMenu(String userType){
        List<MenuEnum> result = new ArrayList<>();
        if (containString(getPjpMenu().split(", "), userType)){
            result.add(MenuEnum.PJP_MENU);
        }
        if (containString(getEventMenu().split(", "), userType)){
            result.add(MenuEnum.EVENT_MENU);
        }
        if (containString(getBackcheckingMenu().split(", "), userType)){
            result.add(MenuEnum.BACKCHECKING_MENU);
        }
        if (containString(getFeedbackMenu().split(", "), userType)){
            result.add(MenuEnum.FEEDBACK_MENU);
        }

        return result;
    }

    private boolean containString(String[] arrayString, String target){
        for (String s : arrayString){
            if (s.equals(target)){
                return true;
            }
        }

        return false;
    }


    public String getPjpMenu() {
        return pjpMenu;
    }

    public void setPjpMenu(String pjpMenu) {
        this.pjpMenu = pjpMenu;
    }

    public String getOssOskMenu() {
        return ossOskMenu;
    }

    public void setOssOskMenu(String ossOskMenu) {
        this.ossOskMenu = ossOskMenu;
    }

    public String getInstitutionMenu() {
        return institutionMenu;
    }

    public void setInstitutionMenu(String institutionMenu) {
        this.institutionMenu = institutionMenu;
    }

    public String getEventMenu() {
        return eventMenu;
    }

    public void setEventMenu(String eventMenu) {
        this.eventMenu = eventMenu;
    }

    public String getBackcheckingMenu() {
        return backcheckingMenu;
    }

    public void setBackcheckingMenu(String backcheckingMenu) {
        this.backcheckingMenu = backcheckingMenu;
    }

    public String getFeedbackMenu() {
        return feedbackMenu;
    }

    public void setFeedbackMenu(String feedbackMenu) {
        this.feedbackMenu = feedbackMenu;
    }

}