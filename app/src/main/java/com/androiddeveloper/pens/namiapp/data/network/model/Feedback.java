package com.androiddeveloper.pens.namiapp.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Feedback {

    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("nametag")
    @Expose
    private String nametag;
    @SerializedName("locationtag")
    @Expose
    private String locationtag;
    @SerializedName("backchecking_oss_osk")
    @Expose
    private String backcheckingOssOsk;
    @SerializedName("not_responded_oss_osk")
    @Expose
    private String notRespondedOssOsk;
    @SerializedName("responded_oss_osk")
    @Expose
    private String respondedOssOsk;
    @SerializedName("backchecking_institution")
    @Expose
    private String backcheckingInstitution;
    @SerializedName("not_responded_institution")
    @Expose
    private String notRespondedInstitution;
    @SerializedName("responded_institution")
    @Expose
    private String respondedInstitution;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getNametag() {
        return nametag;
    }

    public void setNametag(String nametag) {
        this.nametag = nametag;
    }

    public String getLocationtag() {
        return locationtag;
    }

    public void setLocationtag(String locationtag) {
        this.locationtag = locationtag;
    }

    public String getBackcheckingOssOsk() {
        return backcheckingOssOsk;
    }

    public void setBackcheckingOssOsk(String backcheckingOssOsk) {
        this.backcheckingOssOsk = backcheckingOssOsk;
    }

    public String getNotRespondedOssOsk() {
        return notRespondedOssOsk;
    }

    public void setNotRespondedOssOsk(String notRespondedOssOsk) {
        this.notRespondedOssOsk = notRespondedOssOsk;
    }

    public String getRespondedOssOsk() {
        return respondedOssOsk;
    }

    public void setRespondedOssOsk(String respondedOssOsk) {
        this.respondedOssOsk = respondedOssOsk;
    }

    public String getBackcheckingInstitution() {
        return backcheckingInstitution;
    }

    public void setBackcheckingInstitution(String backcheckingInstitution) {
        this.backcheckingInstitution = backcheckingInstitution;
    }

    public String getNotRespondedInstitution() {
        return notRespondedInstitution;
    }

    public void setNotRespondedInstitution(String notRespondedInstitution) {
        this.notRespondedInstitution = notRespondedInstitution;
    }

    public String getRespondedInstitution() {
        return respondedInstitution;
    }

    public void setRespondedInstitution(String respondedInstitution) {
        this.respondedInstitution = respondedInstitution;
    }

}