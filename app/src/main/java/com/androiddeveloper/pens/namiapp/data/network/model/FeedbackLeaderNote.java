package com.androiddeveloper.pens.namiapp.data.network.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by miftahun on 8/26/18.
 */

public class FeedbackLeaderNote {

    @SerializedName("npsn")
    private String npsn;

    @SerializedName(value="institution_name", alternate={"outlet_name"})
    private String institutionName;

    @SerializedName("visit_note")
    private String visitNote;

    @SerializedName("sales_through_note")
    private String salesThroughNote;

    @SerializedName("branding_note")
    private String brandingNote;

    @SerializedName("merchandising_note")
    private String merchandisingNote;

    @SerializedName("market_share_note")
    private String marketShareNote;

    @SerializedName("visit_feedback")
    private String visitFeedback;

    @SerializedName("sales_through_feedback")
    private String salesThroughFeedback;

    @SerializedName("branding_feedback")
    private String brandingFeedback;

    @SerializedName("merchandising_feedback")
    private String merchandisingFeedback;

    @SerializedName("market_share_feedback")
    private String marketShareFeedback;

    @SerializedName("purchasing_note")
    private String purchasingNote;

    @SerializedName("share_note")
    private String shareNote;

    @SerializedName("matpro_note")
    private String matproNote;

    @SerializedName("matpro_feedback")
    private String matproFeedback;

    @SerializedName("share_feedback")
    private String shareFeedback;

    @SerializedName("mostselling_feedback")
    private String mostSellingFeedback;

    @SerializedName("mostselling_note")
    private String mostSellingNote;



    @SerializedName("purchasing_feedback")
    private String purchasingFeedback;

    @SerializedName("telkomsel_product_knowledge_note")
    private String productKnowledge;

    @SerializedName("product_knowledge_feedback")
    private String productKnowledgeFeedback;

    @SerializedName("message")
    private String message;

    public String getProductKnowledgeFeedback() {
        return productKnowledgeFeedback;
    }

    public void setProductKnowledgeFeedback(String productKnowledgeFeedback) {
        this.productKnowledgeFeedback = productKnowledgeFeedback;
    }

    public String getPurchasingNote() {
        return purchasingNote;
    }

    public void setPurchasingNote(String purchasingNote) {
        this.purchasingNote = purchasingNote;
    }

    public String getShareNote() {
        return shareNote;
    }

    public void setShareNote(String shareNote) {
        this.shareNote = shareNote;
    }

    public String getMatproNote() {
        return matproNote;
    }

    public void setMatproNote(String matproNote) {
        this.matproNote = matproNote;
    }

    public String getProductKnowledge() {
        return productKnowledge;
    }

    public void setProductKnowledge(String productKnowledge) {
        this.productKnowledge = productKnowledge;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public FeedbackLeaderNote() {
    }

    public String getNpsn() {
        return npsn;
    }

    public void setNpsn(String npsn) {
        this.npsn = npsn;
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }

    public String getVisitNote() {
        return visitNote;
    }

    public void setVisitNote(String visitNote) {
        this.visitNote = visitNote;
    }

    public String getSalesThroughNote() {
        return salesThroughNote;
    }

    public void setSalesThroughNote(String salesThroughNote) {
        this.salesThroughNote = salesThroughNote;
    }

    public String getBrandingNote() {
        return brandingNote;
    }

    public void setBrandingNote(String brandingNote) {
        this.brandingNote = brandingNote;
    }

    public String getMerchandisingNote() {
        return merchandisingNote;
    }

    public void setMerchandisingNote(String merchandisingNote) {
        this.merchandisingNote = merchandisingNote;
    }

    public String getMarketShareNote() {
        return marketShareNote;
    }

    public void setMarketShareNote(String marketShareNote) {
        this.marketShareNote = marketShareNote;
    }

    public String getVisitFeedback() {
        return visitFeedback;
    }

    public void setVisitFeedback(String visitFeedback) {
        this.visitFeedback = visitFeedback;
    }

    public String getSalesThroughFeedback() {
        return salesThroughFeedback;
    }

    public void setSalesThroughFeedback(String salesThroughFeedback) {
        this.salesThroughFeedback = salesThroughFeedback;
    }

    public String getBrandingFeedback() {
        return brandingFeedback;
    }

    public void setBrandingFeedback(String brandingFeedback) {
        this.brandingFeedback = brandingFeedback;
    }

    public String getMerchandisingFeedback() {
        return merchandisingFeedback;
    }

    public void setMerchandisingFeedback(String merchandisingFeedback) {
        this.merchandisingFeedback = merchandisingFeedback;
    }

    public String getMarketShareFeedback() {
        return marketShareFeedback;
    }

    public void setMarketShareFeedback(String marketShareFeedback) {
        this.marketShareFeedback = marketShareFeedback;
    }

    public String getMatproFeedback() {
        return matproFeedback;
    }

    public void setMatproFeedback(String matproFeedback) {
        this.matproFeedback = matproFeedback;
    }

    public String getShareFeedback() {
        return shareFeedback;
    }

    public void setShareFeedback(String shareFeedback) {
        this.shareFeedback = shareFeedback;
    }

    public String getMostSellingFeedback() {
        return mostSellingFeedback;
    }

    public void setMostSellingFeedback(String mostSellingFeedback) {
        this.mostSellingFeedback = mostSellingFeedback;
    }

    public String getPurchasingFeedback() {
        return purchasingFeedback;
    }

    public void setPurchasingFeedback(String purchasingFeedback) {
        this.purchasingFeedback = purchasingFeedback;
    }

    public String getMostSellingNote() {
        return mostSellingNote;
    }

    public void setMostSellingNote(String mostSellingNote) {
        this.mostSellingNote = mostSellingNote;
    }

    @Override

    public String toString() {
        return "FeedbackLeaderNote{" +
                "npsn='" + npsn + '\'' +
                ", institutionName='" + institutionName + '\'' +
                ", visitNote='" + visitNote + '\'' +
                ", salesThroughNote='" + salesThroughNote + '\'' +
                ", brandingNote='" + brandingNote + '\'' +
                ", merchandisingNote='" + merchandisingNote + '\'' +
                ", marketShareNote='" + marketShareNote + '\'' +
                ", visitFeedback='" + visitFeedback + '\'' +
                ", salesThroughFeedback='" + salesThroughFeedback + '\'' +
                ", brandingFeedback='" + brandingFeedback + '\'' +
                ", merchandisingFeedback='" + merchandisingFeedback + '\'' +
                ", marketShareFeedback='" + marketShareFeedback + '\'' +
                ", message='" + message + '\'' +
                '}';
    }


}
