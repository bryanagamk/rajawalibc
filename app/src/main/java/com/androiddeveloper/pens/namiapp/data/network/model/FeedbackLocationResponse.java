package com.androiddeveloper.pens.namiapp.data.network.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class FeedbackLocationResponse {

	@SerializedName("user_id_send_to")
	private String userIdSendTo;

	@SerializedName("id_outlet_digipos")
	private String idOutletDigipos;

	@SerializedName("user_name_send_to")
	private String userNameSendTo;

	@SerializedName("confirmation_note")
	private String confirmationNote;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("user_name")
	private String userName;

	@SerializedName("last_update")
	private String lastUpdate;

	@SerializedName("outlet_name")
	private String outletName;

	@SerializedName("location_type")
	private String locationType;

	public void setUserIdSendTo(String userIdSendTo){
		this.userIdSendTo = userIdSendTo;
	}

	public String getUserIdSendTo(){
		return userIdSendTo;
	}

	public void setIdOutletDigipos(String idOutletDigipos){
		this.idOutletDigipos = idOutletDigipos;
	}

	public String getIdOutletDigipos(){
		return idOutletDigipos;
	}

	public void setUserNameSendTo(String userNameSendTo){
		this.userNameSendTo = userNameSendTo;
	}

	public String getUserNameSendTo(){
		return userNameSendTo;
	}

	public void setConfirmationNote(String confirmationNote){
		this.confirmationNote = confirmationNote;
	}

	public String getConfirmationNote(){
		return confirmationNote;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setUserName(String userName){
		this.userName = userName;
	}

	public String getUserName(){
		return userName;
	}

	public void setLastUpdate(String lastUpdate){
		this.lastUpdate = lastUpdate;
	}

	public String getLastUpdate(){
		return lastUpdate;
	}

	public void setOutletName(String outletName){
		this.outletName = outletName;
	}

	public String getOutletName(){
		return outletName;
	}

	public void setLocationType(String locationType){
		this.locationType = locationType;
	}

	public String getLocationType(){
		return locationType;
	}

	@Override
 	public String toString(){
		return 
			"FeedbackLocationResponse{" +
			"user_id_send_to = '" + userIdSendTo + '\'' + 
			",id_outlet_digipos = '" + idOutletDigipos + '\'' + 
			",user_name_send_to = '" + userNameSendTo + '\'' + 
			",confirmation_note = '" + confirmationNote + '\'' + 
			",user_id = '" + userId + '\'' + 
			",user_name = '" + userName + '\'' + 
			",last_update = '" + lastUpdate + '\'' + 
			",outlet_name = '" + outletName + '\'' + 
			",location_type = '" + locationType + '\'' + 
			"}";
		}
}