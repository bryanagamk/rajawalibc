package com.androiddeveloper.pens.namiapp.data.network.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

public class PerformanceBackchecking{

	@SerializedName("area")
	private String area;

	@SerializedName("osk_actual_bc")
	private String oskActualBc;

	@SerializedName("city")
	private String city;

	@SerializedName("cluster")
	private String cluster;

	@SerializedName("regional")
	private String regional;

	@SerializedName("user_name")
	private String userName;

	@SerializedName("branch")
	private String branch;

	@SerializedName("poi_ach_bc")
	private String poiAchBc;

	@SerializedName("osk_target_bc")
	private String oskTargetBc;

	@SerializedName("poi_actual_bc")
	private String poiActualBc;

	@SerializedName("poi_target_bc")
	private String poiTargetBc;

	@SerializedName("user_type")
	private String userType;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("osk_ach_bc")
	private String oskAchBc;

	private String title;

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setArea(String area){
		this.area = area;
	}

	public String getArea(){
		return area;
	}

	public void setOskActualBc(String oskActualBc){
		this.oskActualBc = oskActualBc;
	}

	public String getOskActualBc(){
		return oskActualBc;
	}

	public void setCluster(String cluster){
		this.cluster = cluster;
	}

	public String getCluster(){
		return cluster;
	}

	public void setRegional(String regional){
		this.regional = regional;
	}

	public String getRegional(){
		return regional;
	}

	public void setUserName(String userName){
		this.userName = userName;
	}

	public String getUserName(){
		return userName;
	}

	public void setBranch(String branch){
		this.branch = branch;
	}

	public String getBranch(){
		return branch;
	}

	public void setPoiAchBc(String poiAchBc){
		this.poiAchBc = poiAchBc;
	}

	public String getPoiAchBc(){
		return poiAchBc;
	}

	public void setOskTargetBc(String oskTargetBc){
		this.oskTargetBc = oskTargetBc;
	}

	public String getOskTargetBc(){
		return oskTargetBc;
	}

	public void setPoiActualBc(String poiActualBc){
		this.poiActualBc = poiActualBc;
	}

	public String getPoiActualBc(){
		return poiActualBc;
	}

	public void setPoiTargetBc(String poiTargetBc){
		this.poiTargetBc = poiTargetBc;
	}

	public String getPoiTargetBc(){
		return poiTargetBc;
	}

	public void setUserType(String userType){
		this.userType = userType;
	}

	public String getUserType(){
		return userType;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setOskAchBc(String oskAchBc){
		this.oskAchBc = oskAchBc;
	}

	public String getOskAchBc(){
		return oskAchBc;
	}

	@Override
 	public String toString(){
		return 
			"PerformanceBackchecking{" + 
			"area = '" + area + '\'' + 
			",osk_actual_bc = '" + oskActualBc + '\'' + 
			",cluster = '" + cluster + '\'' + 
			",regional = '" + regional + '\'' + 
			",user_name = '" + userName + '\'' + 
			",branch = '" + branch + '\'' + 
			",poi_ach_bc = '" + poiAchBc + '\'' + 
			",osk_target_bc = '" + oskTargetBc + '\'' + 
			",poi_actual_bc = '" + poiActualBc + '\'' + 
			",poi_target_bc = '" + poiTargetBc + '\'' + 
			",user_type = '" + userType + '\'' + 
			",user_id = '" + userId + '\'' + 
			",osk_ach_bc = '" + oskAchBc + '\'' + 
			"}";
		}

	public PerformanceSummary getSummaryPerfomanceBc(){
		PerformanceSummary performanceBc = new PerformanceSummary();
		performanceBc.setTvPoiAchBc(getPoiAchBc());
		performanceBc.setTvPoiTargetBc(getPoiTargetBc());
		performanceBc.setTvPoiActualBc(getPoiActualBc());
		performanceBc.setTvOskAchBc(getOskAchBc());
		performanceBc.setTvOskTargetBc(getOskTargetBc());
		performanceBc.setTvOskActualBc(getOskActualBc());
		performanceBc.setUserId(getUserId());
		performanceBc.setUserName(getUserName());
		performanceBc.setTitle(getTitle());
		return performanceBc;
	}

	public String getTitle(){
		if (userName != null){
			return cluster + "\t\t" + "( " + userName + " )" + "\t\t" + "( " + userType + " )";
		}else if (cluster != null )
			return cluster;
		else if (branch != null)
			return branch;
		else return regional;
	}
}