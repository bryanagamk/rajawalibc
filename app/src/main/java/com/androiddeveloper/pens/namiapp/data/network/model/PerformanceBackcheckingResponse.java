package com.androiddeveloper.pens.namiapp.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PerformanceBackcheckingResponse {

    @SerializedName("summary_backchecking")
    @Expose
    private List<PerformanceBackchecking> summaryBackchecking;

    @SerializedName("success")
    @Expose
    private boolean success;

    @SerializedName("message")
    @Expose
    private String message;

    public List<PerformanceBackchecking> getSummaryBackchecking() {
        return summaryBackchecking;
    }

    public void setSummaryBackchecking(List<PerformanceBackchecking> summaryBackchecking) {
        this.summaryBackchecking = summaryBackchecking;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "PerformanceBackcheckingResponse{" +
                "summaryBackchecking=" + summaryBackchecking +
                ", success=" + success +
                ", message='" + message + '\'' +
                '}';
    }
}
