package com.androiddeveloper.pens.namiapp.data.network.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

public class PerformanceEvent{

	@SerializedName("area")
	private String area;

	@SerializedName("cluster")
	private String cluster;

	@SerializedName("actual_sales_mtd")
	private String actualSalesMtd;

	@SerializedName("actual_sales_ptd")
	private String actualSalesPtd;

	@SerializedName("regional")
	private String regional;

	@SerializedName("city")
	private String city;

	@SerializedName("event_actual_call_ptd")
	private String eventActualCallPtd;

	@SerializedName("user_name")
	private String userName;

	@SerializedName("event_actual_call_mtd")
	private String eventActualCallMtd;

	@SerializedName("branch")
	private String branch;

	@SerializedName("event_plan_call_ptd")
	private String eventPlanCallPtd;

	@SerializedName("event_plan_call_mtd")
	private String eventPlanCallMtd;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("event_ach_call_ptd")
	private String eventAchCallPtd;

	@SerializedName("event_ach_call_mtd")
	private String eventAchCallMtd;

	private String title;

	public void setTitle(String title) {
		this.title = title;
	}

	public void setArea(String area){
		this.area = area;
	}

	public String getArea(){
		return area;
	}

	public void setCluster(String cluster){
		this.cluster = cluster;
	}

	public String getCluster(){
		return cluster;
	}

	public void setActualSalesMtd(String actualSalesMtd){
		this.actualSalesMtd = actualSalesMtd;
	}

	public String getActualSalesMtd(){
		return actualSalesMtd;
	}

	public void setActualSalesPtd(String actualSalesPtd){
		this.actualSalesPtd = actualSalesPtd;
	}

	public String getActualSalesPtd(){
		return actualSalesPtd;
	}

	public void setRegional(String regional){
		this.regional = regional;
	}

	public String getRegional(){
		return regional;
	}

	public void setCity(String city){
		this.city = city;
	}

	public String getCity(){
		return city;
	}

	public void setEventActualCallPtd(String eventActualCallPtd){
		this.eventActualCallPtd = eventActualCallPtd;
	}

	public String getEventActualCallPtd(){
		return eventActualCallPtd;
	}

	public void setUserName(String userName){
		this.userName = userName;
	}

	public String getUserName(){
		return userName;
	}

	public void setEventActualCallMtd(String eventActualCallMtd){
		this.eventActualCallMtd = eventActualCallMtd;
	}

	public String getEventActualCallMtd(){
		return eventActualCallMtd;
	}

	public void setBranch(String branch){
		this.branch = branch;
	}

	public String getBranch(){
		return branch;
	}

	public void setEventPlanCallPtd(String eventPlanCallPtd){
		this.eventPlanCallPtd = eventPlanCallPtd;
	}

	public String getEventPlanCallPtd(){
		return eventPlanCallPtd;
	}

	public void setEventPlanCallMtd(String eventPlanCallMtd){
		this.eventPlanCallMtd = eventPlanCallMtd;
	}

	public String getEventPlanCallMtd(){
		return eventPlanCallMtd;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setEventAchCallPtd(String eventAchCallPtd){
		this.eventAchCallPtd = eventAchCallPtd;
	}

	public String getEventAchCallPtd(){
		return eventAchCallPtd;
	}

	public void setEventAchCallMtd(String eventAchCallMtd){
		this.eventAchCallMtd = eventAchCallMtd;
	}

	public String getEventAchCallMtd(){
		return eventAchCallMtd;
	}

	@Override
	public String toString() {
		return "PerformanceEvent{" +
				"area='" + area + '\'' +
				", cluster='" + cluster + '\'' +
				", actualSalesMtd='" + actualSalesMtd + '\'' +
				", actualSalesPtd='" + actualSalesPtd + '\'' +
				", regional='" + regional + '\'' +
				", city='" + city + '\'' +
				", eventActualCallPtd='" + eventActualCallPtd + '\'' +
				", userName='" + userName + '\'' +
				", eventActualCallMtd='" + eventActualCallMtd + '\'' +
				", branch='" + branch + '\'' +
				", eventPlanCallPtd='" + eventPlanCallPtd + '\'' +
				", eventPlanCallMtd='" + eventPlanCallMtd + '\'' +
				", userId='" + userId + '\'' +
				", eventAchCallPtd='" + eventAchCallPtd + '\'' +
				", eventAchCallMtd='" + eventAchCallMtd + '\'' +
				", title='" + title + '\'' +
				'}';
	}

	public PerformanceSummary getSummaryPerfomanceEvent(){
		PerformanceSummary performanceEvent = new PerformanceSummary();
		performanceEvent.setTvAchEventCallMtd(getEventAchCallMtd());
		performanceEvent.setTvAchEventCallPtd(getEventAchCallPtd());
		performanceEvent.setTvActualEventCallMtd(getEventActualCallMtd());
		performanceEvent.setTvActualEventCallPtd(getEventActualCallPtd());
		performanceEvent.setTvPlanEventCallMtd(getEventPlanCallMtd());
		performanceEvent.setTvPlanEventCallPtd(getEventPlanCallPtd());
		performanceEvent.setTvSalesEventCallMtd(getActualSalesMtd());
		performanceEvent.setTvSalesEventCallPtd(getActualSalesPtd());
		performanceEvent.setTitle(getTitle());
		return performanceEvent;
	}

	public String getTitle(){
		if (userName != null){
			return city + "\t\t" + "( " + userName + " )";
		}else if (city != null )
			return city;
		else if (cluster != null )
			return cluster;
		else if (branch != null)
			return branch;
		else return regional;
	}
}