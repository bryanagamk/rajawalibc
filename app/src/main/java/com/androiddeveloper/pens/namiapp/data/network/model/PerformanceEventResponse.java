package com.androiddeveloper.pens.namiapp.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PerformanceEventResponse {
    @SerializedName("summary_event")
    @Expose
    private List<PerformanceEvent> summaryPerformanceEvent;

    @SerializedName("success")
    @Expose
    private boolean success;

    @SerializedName("message")
    @Expose
    private String message;

    public List<PerformanceEvent> getSummaryPerformanceEvent() {
        return summaryPerformanceEvent;
    }

    public void setSummaryPerformanceEvent(List<PerformanceEvent> summaryPerformanceEvent) {
        this.summaryPerformanceEvent = summaryPerformanceEvent;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "PerformanceEventResponse{" +
                "summaryPerformanceEvent=" + summaryPerformanceEvent +
                ", success=" + success +
                ", message='" + message + '\'' +
                '}';
    }
}
