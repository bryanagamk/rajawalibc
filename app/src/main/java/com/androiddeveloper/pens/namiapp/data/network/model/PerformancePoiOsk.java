package com.androiddeveloper.pens.namiapp.data.network.model;

import javax.annotation.Generated;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class PerformancePoiOsk implements Serializable {

	@SerializedName("cluster")
	private String cluster;

	@SerializedName("osk_outlet_productive_ptd")
	private String oskOutletProductivePtd;

	@SerializedName("regional")
	private String regional;

	@SerializedName("city")
	private String city;

	@SerializedName("user_name")
	private String userName;

	@SerializedName("location_id")
	private String locationId;

	@SerializedName("location_name")
	private String locationName;

	@SerializedName("location_type")
	private String locationType;

	@SerializedName("osk_outlet_productive_mtd")
	private String oskOutletProductiveMtd;

	@SerializedName("poi_effective_call_mtd")
	private String poiEffectiveCallMtd;

	@SerializedName("osk_ach_call_ptd")
	private String oskAchCallPtd;

	@SerializedName("poi_effective_call_ptd")
	private String poiEffectiveCallPtd;

	@SerializedName("poi_actual_call_mtd")
	private String poiActualCallMtd;

	@SerializedName("branch")
	private String branch;

	@SerializedName("osk_effective_call_ptd")
	private String oskEffectiveCallPtd;

	@SerializedName("osk_effective_call_mtd")
	private String oskEffectiveCallMtd;

	@SerializedName("poi_actual_call_ptd")
	private String poiActualCallPtd;

	@SerializedName("poi_plan_mtd")
	private String poiPlanMtd;

	@SerializedName("osk_actual_call_mtd")
	private String oskActualCallMtd;

	@SerializedName("osk_plan_call_ptd")
	private String oskPlanCallPtd;

	@SerializedName("osk_ach_call_mtd")
	private String oskAchCallMtd;

	@SerializedName("osk_plan_call_mtd")
	private String oskPlanCallMtd;

	@SerializedName("poi_plan_call_ptd")
	private String poiPlanCallPtd;

	@SerializedName("osk_actual_call_ptd")
	private String oskActualCallPtd;

	@SerializedName("area")
	private String area;

	@SerializedName("osk_actual_sales_mtd")
	private String oskActualSalesMtd;

	@SerializedName("poi_market_survey_mtd")
	private String poiMarketSurveyMtd;

	@SerializedName("osk_actual_sales_ptd")
	private String oskActualSalesPtd;

	@SerializedName("poi_ach_call_mtd")
	private String poiAchCallMtd;

	@SerializedName("poi_plan_call_mtd")
	private String poiPlanCallMtd;

	@SerializedName("poi_actual_sales_mtd")
	private String poiActualSalesMtd;

	@SerializedName("poi_market_survey_ptd")
	private String poiMarketSurveyPtd;

	@SerializedName("poi_plan_channel_ptd")
	private String poiPlanChannelPtd;

	@SerializedName("poi_ach_call_ptd")
	private String poiAchCallPtd;

	@SerializedName("poi_actual_sales_ptd")
	private String poiActualSalesPtd;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("osk_plan_channel_ptd")
	private String oskPlanChannelPtd;

	@SerializedName("osk_plan_mtd")
	private String oskPlanMtd;

	@SerializedName("actual_sales_mtd")
	private String actualSalesMtd;

	@SerializedName("ach_call_mtd")
	private String achCallMtd;

	@SerializedName("effective_call_mtd")
	private String effectiveCallMtd;

	@SerializedName("actual_sales_ptd")
	private String actualSalesPtd;

	@SerializedName("effective_call_ptd")
	private String effectiveCallPtd;

	@SerializedName("plan_channel")
	private String planChannel;

	@SerializedName("plan_call_mtd")
	private String planCallMtd;

	@SerializedName("actual_call_ptd")
	private String actualCallPtd;

	@SerializedName("plan_call_ptd")
	private String planCallPtd;

	@SerializedName("ach_call_ptd")
	private String achCallPtd;

	@SerializedName("market_survey_mtd")
	private String marketSurveyMtd;

	@SerializedName("actual_call_mtd")
	private String actualCallMtd;

	@SerializedName("market_survey_ptd")
	private String marketSurveyPtd;

	@SerializedName("outlet_productive_ptd")
	private String outletProductivePtd;

	@SerializedName("outlet_productive_mtd")
	private String outletProductiveMtd;

	@SerializedName("period_type")
	private String periodType;

	@SerializedName("summary_poi_osk")
	@Expose
	private List<PerformancePoiOsk> summaryPerformancePoiOsk;


	public String getPeriodType() {
		return periodType;
	}

	public void setPeriodType(String periodType) {
		this.periodType = periodType;
	}

	public void setActualSalesMtd(String actualSalesMtd){
		this.actualSalesMtd = actualSalesMtd;
	}

	public String getActualSalesMtd(){
		return actualSalesMtd;
	}

	public void setAchCallMtd(String achCallMtd){
		this.achCallMtd = achCallMtd;
	}

	public String getAchCallMtd(){
		return achCallMtd;
	}

	public void setEffectiveCallMtd(String effectiveCallMtd){
		this.effectiveCallMtd = effectiveCallMtd;
	}

	public String getEffectiveCallMtd(){
		return effectiveCallMtd;
	}

	public void setActualSalesPtd(String actualSalesPtd){
		this.actualSalesPtd = actualSalesPtd;
	}

	public String getActualSalesPtd(){
		return actualSalesPtd;
	}

	public void setEffectiveCallPtd(String effectiveCallPtd){
		this.effectiveCallPtd = effectiveCallPtd;
	}

	public String getEffectiveCallPtd(){
		return effectiveCallPtd;
	}

	public void setPlanChannel(String planChannel){
		this.planChannel = planChannel;
	}

	public String getPlanChannel(){
		return planChannel;
	}

	public void setPlanCallMtd(String planCallMtd){
		this.planCallMtd = planCallMtd;
	}

	public String getPlanCallMtd(){
		return planCallMtd;
	}

	public void setActualCallPtd(String actualCallPtd){
		this.actualCallPtd = actualCallPtd;
	}

	public String getActualCallPtd(){
		return actualCallPtd;
	}

	public void setPlanCallPtd(String planCallPtd){
		this.planCallPtd = planCallPtd;
	}

	public String getPlanCallPtd(){
		return planCallPtd;
	}

	public void setAchCallPtd(String achCallPtd){
		this.achCallPtd = achCallPtd;
	}

	public String getAchCallPtd(){
		return achCallPtd;
	}

	public void setMarketSurveyMtd(String marketSurveyMtd){
		this.marketSurveyMtd = marketSurveyMtd;
	}

	public String getMarketSurveyMtd(){
		return marketSurveyMtd;
	}

	public void setActualCallMtd(String actualCallMtd){
		this.actualCallMtd = actualCallMtd;
	}

	public String getActualCallMtd(){
		return actualCallMtd;
	}

	public void setMarketSurveyPtd(String marketSurveyPtd){
		this.marketSurveyPtd = marketSurveyPtd;
	}

	public String getMarketSurveyPtd(){
		return marketSurveyPtd;
	}

	public void setOutletProductivePtd(String outletProductivePtd){
		this.outletProductivePtd = outletProductivePtd;
	}

	public String getOutletProductivePtd(){
		return outletProductivePtd;
	}

	public void setOutletProductiveMtd(String outletProductiveMtd){
		this.outletProductiveMtd = outletProductiveMtd;
	}

	public String getOutletProductiveMtd(){
		return outletProductiveMtd;
	}

	public List<PerformancePoiOsk> getSummaryPerformancePoiOsk() {
		return summaryPerformancePoiOsk;
	}

	public void setSummaryPerformancePoiOsk(List<PerformancePoiOsk> summaryPerformancePoiOsk) {
		this.summaryPerformancePoiOsk = summaryPerformancePoiOsk;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getLocationType() {
		return locationType;
	}

	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}

	public void setCluster(String cluster){
		this.cluster = cluster;
	}

	public String getCluster(){
		return cluster;
	}

	public void setOskOutletProductivePtd(String oskOutletProductivePtd){
		this.oskOutletProductivePtd = oskOutletProductivePtd;
	}

	public String getOskOutletProductivePtd(){
		return oskOutletProductivePtd;
	}

	public void setRegional(String regional){
		this.regional = regional;
	}

	public String getRegional(){
		return regional;
	}

	public void setCity(String city){
		this.city = city;
	}

	public String getCity(){
		return city;
	}

	public void setUserName(String userName){
		this.userName = userName;
	}

	public String getUserName(){
		return userName;
	}

	public void setOskOutletProductiveMtd(String oskOutletProductiveMtd){
		this.oskOutletProductiveMtd = oskOutletProductiveMtd;
	}

	public String getOskOutletProductiveMtd(){
		return oskOutletProductiveMtd;
	}

	public void setPoiEffectiveCallMtd(String poiEffectiveCallMtd){
		this.poiEffectiveCallMtd = poiEffectiveCallMtd;
	}

	public String getPoiEffectiveCallMtd(){
		return poiEffectiveCallMtd;
	}

	public void setOskAchCallPtd(String oskAchCallPtd){
		this.oskAchCallPtd = oskAchCallPtd;
	}

	public String getOskAchCallPtd(){
		return oskAchCallPtd;
	}

	public void setPoiEffectiveCallPtd(String poiEffectiveCallPtd){
		this.poiEffectiveCallPtd = poiEffectiveCallPtd;
	}

	public String getPoiEffectiveCallPtd(){
		return poiEffectiveCallPtd;
	}

	public void setPoiActualCallMtd(String poiActualCallMtd){
		this.poiActualCallMtd = poiActualCallMtd;
	}

	public String getPoiActualCallMtd(){
		return poiActualCallMtd;
	}

	public void setBranch(String branch){
		this.branch = branch;
	}

	public String getBranch(){
		return branch;
	}

	public void setOskEffectiveCallPtd(String oskEffectiveCallPtd){
		this.oskEffectiveCallPtd = oskEffectiveCallPtd;
	}

	public String getOskEffectiveCallPtd(){
		return oskEffectiveCallPtd;
	}

	public void setOskEffectiveCallMtd(String oskEffectiveCallMtd){
		this.oskEffectiveCallMtd = oskEffectiveCallMtd;
	}

	public String getOskEffectiveCallMtd(){
		return oskEffectiveCallMtd;
	}

	public void setPoiActualCallPtd(String poiActualCallPtd){
		this.poiActualCallPtd = poiActualCallPtd;
	}

	public String getPoiActualCallPtd(){
		return poiActualCallPtd;
	}

	public void setPoiPlanMtd(String poiPlanMtd){
		this.poiPlanMtd = poiPlanMtd;
	}

	public String getPoiPlanMtd(){
		return poiPlanMtd;
	}

	public void setOskActualCallMtd(String oskActualCallMtd){
		this.oskActualCallMtd = oskActualCallMtd;
	}

	public String getOskActualCallMtd(){
		return oskActualCallMtd;
	}

	public void setOskPlanCallPtd(String oskPlanCallPtd){
		this.oskPlanCallPtd = oskPlanCallPtd;
	}

	public String getOskPlanCallPtd(){
		return oskPlanCallPtd;
	}

	public void setOskAchCallMtd(String oskAchCallMtd){
		this.oskAchCallMtd = oskAchCallMtd;
	}

	public String getOskAchCallMtd(){
		return oskAchCallMtd;
	}

	public void setOskPlanCallMtd(String oskPlanCallMtd){
		this.oskPlanCallMtd = oskPlanCallMtd;
	}

	public String getOskPlanCallMtd(){
		return oskPlanCallMtd;
	}

	public void setPoiPlanCallPtd(String poiPlanCallPtd){
		this.poiPlanCallPtd = poiPlanCallPtd;
	}

	public String getPoiPlanCallPtd(){
		return poiPlanCallPtd;
	}

	public void setOskActualCallPtd(String oskActualCallPtd){
		this.oskActualCallPtd = oskActualCallPtd;
	}

	public String getOskActualCallPtd(){
		return oskActualCallPtd;
	}

	public void setArea(String area){
		this.area = area;
	}

	public String getArea(){
		return area;
	}

	public void setOskActualSalesMtd(String oskActualSalesMtd){
		this.oskActualSalesMtd = oskActualSalesMtd;
	}

	public String getOskActualSalesMtd(){
		return oskActualSalesMtd;
	}

	public void setPoiMarketSurveyMtd(String poiMarketSurveyMtd){
		this.poiMarketSurveyMtd = poiMarketSurveyMtd;
	}

	public String getPoiMarketSurveyMtd(){
		return poiMarketSurveyMtd;
	}

	public void setOskActualSalesPtd(String oskActualSalesPtd){
		this.oskActualSalesPtd = oskActualSalesPtd;
	}

	public String getOskActualSalesPtd(){
		return oskActualSalesPtd;
	}

	public void setPoiAchCallMtd(String poiAchCallMtd){
		this.poiAchCallMtd = poiAchCallMtd;
	}

	public String getPoiAchCallMtd(){
		return poiAchCallMtd;
	}

	public void setPoiPlanCallMtd(String poiPlanCallMtd){
		this.poiPlanCallMtd = poiPlanCallMtd;
	}

	public String getPoiPlanCallMtd(){
		return poiPlanCallMtd;
	}

	public void setPoiActualSalesMtd(String poiActualSalesMtd){
		this.poiActualSalesMtd = poiActualSalesMtd;
	}

	public String getPoiActualSalesMtd(){
		return poiActualSalesMtd;
	}

	public void setPoiMarketSurveyPtd(String poiMarketSurveyPtd){
		this.poiMarketSurveyPtd = poiMarketSurveyPtd;
	}

	public String getPoiMarketSurveyPtd(){
		return poiMarketSurveyPtd;
	}

	public void setPoiPlanChannelPtd(String poiPlanChannelPtd){
		this.poiPlanChannelPtd = poiPlanChannelPtd;
	}

	public String getPoiPlanChannelPtd(){
		return poiPlanChannelPtd;
	}

	public void setPoiAchCallPtd(String poiAchCallPtd){
		this.poiAchCallPtd = poiAchCallPtd;
	}

	public String getPoiAchCallPtd(){
		return poiAchCallPtd;
	}

	public void setPoiActualSalesPtd(String poiActualSalesPtd){
		this.poiActualSalesPtd = poiActualSalesPtd;
	}

	public String getPoiActualSalesPtd(){
		return poiActualSalesPtd;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setOskPlanChannelPtd(String oskPlanChannelPtd){
		this.oskPlanChannelPtd = oskPlanChannelPtd;
	}

	public String getOskPlanChannelPtd(){
		return oskPlanChannelPtd;
	}

	public void setOskPlanMtd(String oskPlanMtd){
		this.oskPlanMtd = oskPlanMtd;
	}

	public String getOskPlanMtd(){
		return oskPlanMtd;
	}

	@Override
	public String toString() {
		return "PerformancePoiOsk{" +
				"cluster='" + cluster + '\'' +
				", oskOutletProductivePtd='" + oskOutletProductivePtd + '\'' +
				", regional='" + regional + '\'' +
				", city='" + city + '\'' +
				", userName='" + userName + '\'' +
				", locationId='" + locationId + '\'' +
				", locationName='" + locationName + '\'' +
				", locationType='" + locationType + '\'' +
				", oskOutletProductiveMtd='" + oskOutletProductiveMtd + '\'' +
				", poiEffectiveCallMtd='" + poiEffectiveCallMtd + '\'' +
				", oskAchCallPtd='" + oskAchCallPtd + '\'' +
				", poiEffectiveCallPtd='" + poiEffectiveCallPtd + '\'' +
				", poiActualCallMtd='" + poiActualCallMtd + '\'' +
				", branch='" + branch + '\'' +
				", oskEffectiveCallPtd='" + oskEffectiveCallPtd + '\'' +
				", oskEffectiveCallMtd='" + oskEffectiveCallMtd + '\'' +
				", poiActualCallPtd='" + poiActualCallPtd + '\'' +
				", poiPlanMtd='" + poiPlanMtd + '\'' +
				", oskActualCallMtd='" + oskActualCallMtd + '\'' +
				", oskPlanCallPtd='" + oskPlanCallPtd + '\'' +
				", oskAchCallMtd='" + oskAchCallMtd + '\'' +
				", oskPlanCallMtd='" + oskPlanCallMtd + '\'' +
				", poiPlanCallPtd='" + poiPlanCallPtd + '\'' +
				", oskActualCallPtd='" + oskActualCallPtd + '\'' +
				", area='" + area + '\'' +
				", oskActualSalesMtd='" + oskActualSalesMtd + '\'' +
				", poiMarketSurveyMtd='" + poiMarketSurveyMtd + '\'' +
				", oskActualSalesPtd='" + oskActualSalesPtd + '\'' +
				", poiAchCallMtd='" + poiAchCallMtd + '\'' +
				", poiPlanCallMtd='" + poiPlanCallMtd + '\'' +
				", poiActualSalesMtd='" + poiActualSalesMtd + '\'' +
				", poiMarketSurveyPtd='" + poiMarketSurveyPtd + '\'' +
				", poiPlanChannelPtd='" + poiPlanChannelPtd + '\'' +
				", poiAchCallPtd='" + poiAchCallPtd + '\'' +
				", poiActualSalesPtd='" + poiActualSalesPtd + '\'' +
				", userId='" + userId + '\'' +
				", oskPlanChannelPtd='" + oskPlanChannelPtd + '\'' +
				", oskPlanMtd='" + oskPlanMtd + '\'' +
				", actualSalesMtd='" + actualSalesMtd + '\'' +
				", achCallMtd='" + achCallMtd + '\'' +
				", effectiveCallMtd='" + effectiveCallMtd + '\'' +
				", actualSalesPtd='" + actualSalesPtd + '\'' +
				", effectiveCallPtd='" + effectiveCallPtd + '\'' +
				", planChannel='" + planChannel + '\'' +
				", planCallMtd='" + planCallMtd + '\'' +
				", actualCallPtd='" + actualCallPtd + '\'' +
				", planCallPtd='" + planCallPtd + '\'' +
				", achCallPtd='" + achCallPtd + '\'' +
				", marketSurveyMtd='" + marketSurveyMtd + '\'' +
				", actualCallMtd='" + actualCallMtd + '\'' +
				", marketSurveyPtd='" + marketSurveyPtd + '\'' +
				", outletProductivePtd='" + outletProductivePtd + '\'' +
				", outletProductiveMtd='" + outletProductiveMtd + '\'' +
				", summaryPerformancePoiOsk=" + summaryPerformancePoiOsk +
				'}';
	}

	public PerformanceSummary getPjpPtdPoiOsk(){
        PerformanceSummary performanceSummary = new PerformanceSummary();
        performanceSummary.setTvPoiAchCall(getPoiAchCallPtd());
        performanceSummary.setTvPoiPlanCall(getPoiPlanCallPtd());
        performanceSummary.setTvPoiActualCall(getPoiActualCallPtd());
        performanceSummary.setTvPoiEffectiveCall(getPoiEffectiveCallPtd());
        performanceSummary.setTvPoiPlanMarketSurvey(getPoiPlanChannelPtd());
        performanceSummary.setTvPoiActualMarketSurvey(getPoiMarketSurveyPtd());
        performanceSummary.setTvPoiActualSales(getPoiActualSalesPtd());
        performanceSummary.setTvOskAchCall(getOskAchCallPtd());
        performanceSummary.setTvOskPlanCall(getOskPlanCallPtd());
        performanceSummary.setTvOskActualCall(getOskActualCallPtd());
        performanceSummary.setTvOskEffectiveCall(getOskEffectiveCallPtd());
        performanceSummary.setTvOskPlanOutletProd(getOskPlanChannelPtd());
        performanceSummary.setTvOskOutletProd(getOskOutletProductivePtd());
        performanceSummary.setTvOskActualSales(getOskActualSalesPtd());
        performanceSummary.setUserId(getUserId());
        performanceSummary.setUserName(getUserName());
        performanceSummary.setLocationId(getLocationId());
        performanceSummary.setLocationName(getLocationName());
        performanceSummary.setTitle(getTitle());
        performanceSummary.setPeriodType(getPeriodType());
        performanceSummary.setTitlePoiOsk(getTitlePoiOsk());
        return performanceSummary;
    }

    public PerformanceSummary getPjpMtdPoiOsk(){
        PerformanceSummary performanceSummary = new PerformanceSummary();
        performanceSummary.setTvPoiAchCall(getPoiAchCallMtd());
        performanceSummary.setTvPoiPlanCall(getPoiPlanCallMtd());
        performanceSummary.setTvPoiActualCall(getPoiActualCallMtd());
        performanceSummary.setTvPoiEffectiveCall(getPoiEffectiveCallMtd());
        performanceSummary.setTvPoiPlanMarketSurvey(getPoiPlanMtd());
        performanceSummary.setTvPoiActualMarketSurvey(getPoiMarketSurveyMtd());
        performanceSummary.setTvPoiActualSales(getPoiActualSalesMtd());
        performanceSummary.setTvOskAchCall(getOskAchCallMtd());
        performanceSummary.setTvOskPlanCall(getOskPlanCallMtd());
        performanceSummary.setTvOskActualCall(getOskActualCallMtd());
        performanceSummary.setTvOskEffectiveCall(getOskEffectiveCallMtd());
        performanceSummary.setTvOskPlanOutletProd(getOskPlanMtd());
        performanceSummary.setTvOskOutletProd(getOskOutletProductiveMtd());
        performanceSummary.setTvOskActualSales(getOskActualSalesMtd());
		performanceSummary.setLocationId(getLocationId());
		performanceSummary.setLocationName(getLocationName());
		performanceSummary.setUserId(getUserId());
		performanceSummary.setUserName(getUserName());
		performanceSummary.setTitle(getTitle());
		performanceSummary.setPeriodType(getPeriodType());
		performanceSummary.setTitlePoiOsk(getTitlePoiOsk());
        return performanceSummary;
    }

	public PerformanceSummary getPjpPtdPoiOskDetail(){
		PerformanceSummary performanceSummary = new PerformanceSummary();
		performanceSummary.setAchCallPtd(getAchCallPtd());
		performanceSummary.setPlanCallPtd(getPlanCallPtd());
		performanceSummary.setActualCallPtd(getActualCallPtd());
		performanceSummary.setEffectiveCallPtd(getEffectiveCallPtd());
		performanceSummary.setOutletProdPtd(getOutletProductivePtd());
		performanceSummary.setActualMarketSurveyPtd(getMarketSurveyPtd());
		performanceSummary.setActualSalesPtd(getActualSalesPtd());
		performanceSummary.setAchCallMtd(getAchCallMtd());
		performanceSummary.setPlanCallMtd(getPlanCallMtd());
		performanceSummary.setActualCallMtd(getActualCallMtd());
		performanceSummary.setEffectiveCallMtd(getEffectiveCallMtd());
		performanceSummary.setOutletProdMtd(getOutletProductiveMtd());
		performanceSummary.setActualMarketSurveyMtd(getMarketSurveyMtd());
		performanceSummary.setActualSalesMtd(getActualSalesMtd());
		performanceSummary.setUserId(getUserId());
		performanceSummary.setUserName(getUserName());
		performanceSummary.setLocationId(getLocationId());
		performanceSummary.setLocationName(getLocationName());
		performanceSummary.setTitle(getTitle());
		performanceSummary.setLocationType(getLocationType());
		performanceSummary.setPeriodType(getPeriodType());
		performanceSummary.setTitlePoiOsk(getTitlePoiOsk());
		return performanceSummary;
	}

	public PerformanceSummary getPjpMtdPoiOskDetail(){
		PerformanceSummary performanceSummary = new PerformanceSummary();
		performanceSummary.setAchCallPtd(getAchCallPtd());
		performanceSummary.setPlanCallPtd(getPlanCallPtd());
		performanceSummary.setActualCallPtd(getActualCallPtd());
		performanceSummary.setEffectiveCallPtd(getEffectiveCallPtd());
		performanceSummary.setOutletProdPtd(getOutletProductivePtd());
		performanceSummary.setActualMarketSurveyPtd(getMarketSurveyPtd());
		performanceSummary.setActualSalesPtd(getActualSalesPtd());
		performanceSummary.setAchCallMtd(getAchCallMtd());
		performanceSummary.setPlanCallMtd(getPlanCallMtd());
		performanceSummary.setActualCallMtd(getActualCallMtd());
		performanceSummary.setEffectiveCallMtd(getEffectiveCallMtd());
		performanceSummary.setOutletProdMtd(getOutletProductiveMtd());
		performanceSummary.setActualMarketSurveyMtd(getMarketSurveyMtd());
		performanceSummary.setActualSalesMtd(getActualSalesMtd());
		performanceSummary.setLocationId(getLocationId());
		performanceSummary.setLocationName(getLocationName());
		performanceSummary.setUserId(getUserId());
		performanceSummary.setUserName(getUserName());
		performanceSummary.setTitle(getTitle());
		performanceSummary.setLocationType(getLocationType());
		performanceSummary.setPeriodType(getPeriodType());
		performanceSummary.setTitlePoiOsk(getTitlePoiOsk());
		return performanceSummary;
	}

	public String getTitle(){
		if (userName != null)
			return city + "\t\t" + "( " + userName + " )";
		else if (city != null )
			return city;
		else if (cluster != null )
			return cluster;
		else if (branch != null)
			return branch;
		else return regional;
	}

	public String getTitlePoiOsk(){
		if (locationId != null)
			return locationName + "\t\t" + "( " + locationId + " )";
		else
			return null;
	}
}