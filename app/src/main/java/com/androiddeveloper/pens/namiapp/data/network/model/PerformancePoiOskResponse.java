package com.androiddeveloper.pens.namiapp.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PerformancePoiOskResponse {

    @SerializedName("summary_poi_osk")
    @Expose
    private List<PerformancePoiOsk> summaryPerformancePoiOsk;

    @SerializedName("success")
    @Expose
    private boolean success;

    @SerializedName("message")
    @Expose
    private String message;

    public List<PerformancePoiOsk> getSummaryPerformancePoiOsk() {
        return summaryPerformancePoiOsk;
    }

    public void setSummaryPerformancePoiOsk(List<PerformancePoiOsk> summaryPerformancePoiOsk) {
        this.summaryPerformancePoiOsk = summaryPerformancePoiOsk;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "PerformancePoiOskResponse{" +
                "summaryPerformancePoiOsk=" + summaryPerformancePoiOsk +
                ", success=" + success +
                ", message='" + message + '\'' +
                '}';
    }
}
