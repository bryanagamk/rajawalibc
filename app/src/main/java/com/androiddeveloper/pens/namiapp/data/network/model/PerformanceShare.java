package com.androiddeveloper.pens.namiapp.data.network.model;

import javax.annotation.Generated;

import com.dinuscxj.progressbar.CircleProgressBar;
import com.google.gson.annotations.SerializedName;

public class PerformanceShare{

	@SerializedName("marketshare_telkomsel")
	private String marketshareTelkomsel;

	@SerializedName("cluster")
	private String cluster;

	@SerializedName("broadbandshare_telkomsel")
	private String broadbandshareTelkomsel;

	@SerializedName("displayshare_xl")
	private String displayshareXl;

	@SerializedName("regional")
	private String regional;

	@SerializedName("city")
	private String city;

	@SerializedName("marketshare_3")
	private String marketshare3;

	@SerializedName("rechargeshare_others")
	private String rechargeshareOthers;

	@SerializedName("branch")
	private String branch;

	@SerializedName("marketshare_ooredoo")
	private String marketshareOoredoo;

	@SerializedName("broadbandshare_3")
	private String broadbandshare3;

	@SerializedName("displayshare_3")
	private String displayshare3;

	@SerializedName("rechargeshare_telkomsel")
	private String rechargeshareTelkomsel;

	@SerializedName("broadbandshare_axis")
	private String broadbandshareAxis;

	@SerializedName("displayshare_others")
	private String displayshareOthers;

	@SerializedName("displayshare_axis")
	private String displayshareAxis;

	@SerializedName("marketshare_axis")
	private String marketshareAxis;

	@SerializedName("rechargeshare_xl")
	private String rechargeshareXl;

	@SerializedName("area")
	private String area;

	@SerializedName("broadbandshare_others")
	private String broadbandshareOthers;

	@SerializedName("rechargeshare_axis")
	private String rechargeshareAxis;

	@SerializedName("displayshare_telkomsel")
	private String displayshareTelkomsel;

	@SerializedName("marketshare_others")
	private String marketshareOthers;

	@SerializedName("rechargeshare_ooredoo")
	private String rechargeshareOoredoo;

	@SerializedName("rechargeshare_3")
	private String rechargeshare3;

	@SerializedName("displayshare_ooredoo")
	private String displayshareOoredoo;

	@SerializedName("broadbandshare_ooredoo")
	private String broadbandshareOoredoo;

	@SerializedName("marketshare_xl")
	private String marketshareXl;

	@SerializedName("broadbandshare_xl")
	private String broadbandshareXl;

	@SerializedName("location_name")
	private String locationName;

	@SerializedName("location_type")
	private String locationType;

	@SerializedName("location_id")
	private String locationId;

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getLocationType() {
		return locationType;
	}

	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public void setMarketshareTelkomsel(String marketshareTelkomsel){
		this.marketshareTelkomsel = marketshareTelkomsel;
	}

	public String getMarketshareTelkomsel(){
		return marketshareTelkomsel;
	}

	public void setCluster(String cluster){
		this.cluster = cluster;
	}

	public String getCluster(){
		return cluster;
	}

	public void setBroadbandshareTelkomsel(String broadbandshareTelkomsel){
		this.broadbandshareTelkomsel = broadbandshareTelkomsel;
	}

	public String getBroadbandshareTelkomsel(){
		return broadbandshareTelkomsel;
	}

	public void setDisplayshareXl(String displayshareXl){
		this.displayshareXl = displayshareXl;
	}

	public String getDisplayshareXl(){
		return displayshareXl;
	}

	public void setRegional(String regional){
		this.regional = regional;
	}

	public String getRegional(){
		return regional;
	}

	public void setCity(String city){
		this.city = city;
	}

	public String getCity(){
		return city;
	}

	public void setMarketshare3(String marketshare3){
		this.marketshare3 = marketshare3;
	}

	public String getMarketshare3(){
		return marketshare3;
	}

	public void setRechargeshareOthers(String rechargeshareOthers){
		this.rechargeshareOthers = rechargeshareOthers;
	}

	public String getRechargeshareOthers(){
		return rechargeshareOthers;
	}

	public void setBranch(String branch){
		this.branch = branch;
	}

	public String getBranch(){
		return branch;
	}

	public void setMarketshareOoredoo(String marketshareOoredoo){
		this.marketshareOoredoo = marketshareOoredoo;
	}

	public String getMarketshareOoredoo(){
		return marketshareOoredoo;
	}

	public void setBroadbandshare3(String broadbandshare3){
		this.broadbandshare3 = broadbandshare3;
	}

	public String getBroadbandshare3(){
		return broadbandshare3;
	}

	public void setDisplayshare3(String displayshare3){
		this.displayshare3 = displayshare3;
	}

	public String getDisplayshare3(){
		return displayshare3;
	}

	public void setRechargeshareTelkomsel(String rechargeshareTelkomsel){
		this.rechargeshareTelkomsel = rechargeshareTelkomsel;
	}

	public String getRechargeshareTelkomsel(){
		return rechargeshareTelkomsel;
	}

	public void setBroadbandshareAxis(String broadbandshareAxis){
		this.broadbandshareAxis = broadbandshareAxis;
	}

	public String getBroadbandshareAxis(){
		return broadbandshareAxis;
	}

	public void setDisplayshareOthers(String displayshareOthers){
		this.displayshareOthers = displayshareOthers;
	}

	public String getDisplayshareOthers(){
		return displayshareOthers;
	}

	public void setDisplayshareAxis(String displayshareAxis){
		this.displayshareAxis = displayshareAxis;
	}

	public String getDisplayshareAxis(){
		return displayshareAxis;
	}

	public void setMarketshareAxis(String marketshareAxis){
		this.marketshareAxis = marketshareAxis;
	}

	public String getMarketshareAxis(){
		return marketshareAxis;
	}

	public void setRechargeshareXl(String rechargeshareXl){
		this.rechargeshareXl = rechargeshareXl;
	}

	public String getRechargeshareXl(){
		return rechargeshareXl;
	}

	public void setArea(String area){
		this.area = area;
	}

	public String getArea(){
		return area;
	}

	public void setBroadbandshareOthers(String broadbandshareOthers){
		this.broadbandshareOthers = broadbandshareOthers;
	}

	public String getBroadbandshareOthers(){
		return broadbandshareOthers;
	}

	public void setRechargeshareAxis(String rechargeshareAxis){
		this.rechargeshareAxis = rechargeshareAxis;
	}

	public String getRechargeshareAxis(){
		return rechargeshareAxis;
	}

	public void setDisplayshareTelkomsel(String displayshareTelkomsel){
		this.displayshareTelkomsel = displayshareTelkomsel;
	}

	public String getDisplayshareTelkomsel(){
		return displayshareTelkomsel;
	}

	public void setMarketshareOthers(String marketshareOthers){
		this.marketshareOthers = marketshareOthers;
	}

	public String getMarketshareOthers(){
		return marketshareOthers;
	}

	public void setRechargeshareOoredoo(String rechargeshareOoredoo){
		this.rechargeshareOoredoo = rechargeshareOoredoo;
	}

	public String getRechargeshareOoredoo(){
		return rechargeshareOoredoo;
	}

	public void setRechargeshare3(String rechargeshare3){
		this.rechargeshare3 = rechargeshare3;
	}

	public String getRechargeshare3(){
		return rechargeshare3;
	}

	public void setDisplayshareOoredoo(String displayshareOoredoo){
		this.displayshareOoredoo = displayshareOoredoo;
	}

	public String getDisplayshareOoredoo(){
		return displayshareOoredoo;
	}

	public void setBroadbandshareOoredoo(String broadbandshareOoredoo){
		this.broadbandshareOoredoo = broadbandshareOoredoo;
	}

	public String getBroadbandshareOoredoo(){
		return broadbandshareOoredoo;
	}

	public void setMarketshareXl(String marketshareXl){
		this.marketshareXl = marketshareXl;
	}

	public String getMarketshareXl(){
		return marketshareXl;
	}

	public void setBroadbandshareXl(String broadbandshareXl){
		this.broadbandshareXl = broadbandshareXl;
	}

	public String getBroadbandshareXl(){
		return broadbandshareXl;
	}

	@Override
	public String toString() {
		return "PerformanceShare{" +
				"marketshareTelkomsel='" + marketshareTelkomsel + '\'' +
				", cluster='" + cluster + '\'' +
				", broadbandshareTelkomsel='" + broadbandshareTelkomsel + '\'' +
				", displayshareXl='" + displayshareXl + '\'' +
				", regional='" + regional + '\'' +
				", city='" + city + '\'' +
				", marketshare3='" + marketshare3 + '\'' +
				", rechargeshareOthers='" + rechargeshareOthers + '\'' +
				", branch='" + branch + '\'' +
				", marketshareOoredoo='" + marketshareOoredoo + '\'' +
				", broadbandshare3='" + broadbandshare3 + '\'' +
				", displayshare3='" + displayshare3 + '\'' +
				", rechargeshareTelkomsel='" + rechargeshareTelkomsel + '\'' +
				", broadbandshareAxis='" + broadbandshareAxis + '\'' +
				", displayshareOthers='" + displayshareOthers + '\'' +
				", displayshareAxis='" + displayshareAxis + '\'' +
				", marketshareAxis='" + marketshareAxis + '\'' +
				", rechargeshareXl='" + rechargeshareXl + '\'' +
				", area='" + area + '\'' +
				", broadbandshareOthers='" + broadbandshareOthers + '\'' +
				", rechargeshareAxis='" + rechargeshareAxis + '\'' +
				", displayshareTelkomsel='" + displayshareTelkomsel + '\'' +
				", marketshareOthers='" + marketshareOthers + '\'' +
				", rechargeshareOoredoo='" + rechargeshareOoredoo + '\'' +
				", rechargeshare3='" + rechargeshare3 + '\'' +
				", displayshareOoredoo='" + displayshareOoredoo + '\'' +
				", broadbandshareOoredoo='" + broadbandshareOoredoo + '\'' +
				", marketshareXl='" + marketshareXl + '\'' +
				", broadbandshareXl='" + broadbandshareXl + '\'' +
				", locationName='" + locationName + '\'' +
				", locationType='" + locationType + '\'' +
				", locationId='" + locationId + '\'' +
				'}';
	}

	public PerformanceShareSummary getSummaryPerfomanceShare(){
		PerformanceShareSummary performanceShare = new PerformanceShareSummary();
		performanceShare.setCpTselBs(getBroadbandshareTelkomsel());
		performanceShare.setCpOoredooBs(getBroadbandshareOoredoo());
		performanceShare.setCpXlBs(getBroadbandshareXl());
		performanceShare.setCpAxisBs(getBroadbandshareAxis());
		performanceShare.setCp3Bs(getBroadbandshare3());
		performanceShare.setCpOthersBs(getBroadbandshareOthers());
		performanceShare.setCpTselDs(getDisplayshareTelkomsel());
		performanceShare.setCpOoredooDs(getDisplayshareOoredoo());
		performanceShare.setCpXlDs(getDisplayshareXl());
		performanceShare.setCpAxisDs(getDisplayshareAxis());
		performanceShare.setCp3Ds(getDisplayshare3());
		performanceShare.setCpOthersDs(getDisplayshareOthers());
		performanceShare.setCpTselRs(getRechargeshareTelkomsel());
		performanceShare.setCpOoredooRs(getRechargeshareOoredoo());
		performanceShare.setCpXlRs(getRechargeshareXl());
		performanceShare.setCpAxisRs(getRechargeshareAxis());
		performanceShare.setCp3Rs(getRechargeshare3());
		performanceShare.setCpOthersRs(getRechargeshareOthers());
		performanceShare.setCpTselMs(getMarketshareTelkomsel());
		performanceShare.setCpOoredooMs(getMarketshareOoredoo());
		performanceShare.setCpXlMs(getMarketshareXl());
		performanceShare.setCpAxisMs(getMarketshareAxis());
		performanceShare.setCp3Ms(getMarketshare3());
		performanceShare.setCpOthersMs(getMarketshareOthers());
		performanceShare.setLocationType(getLocationType());
		performanceShare.setTitle(getTitle());
		return performanceShare;
	}

	public String getTitle(){

		if (locationName != null)
			return city + " - " + locationName + " (" + locationId + ") ";
		else if (city != null )
			return city;
		else if (cluster != null )
			return cluster;
		else if (branch != null)
			return branch;
		else return regional;
	}
}