package com.androiddeveloper.pens.namiapp.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PerformanceShareResponse {
    @SerializedName("summary_share")
    @Expose
    private List<PerformanceShare> summaryPerformanceShare;

    @SerializedName("success")
    @Expose
    private boolean success;

    @SerializedName("message")
    @Expose
    private String message;

    public List<PerformanceShare> getSummaryPerformanceShare() {
        return summaryPerformanceShare;
    }

    public void setSummaryPerformanceShare(List<PerformanceShare> summaryPerformanceShare) {
        this.summaryPerformanceShare = summaryPerformanceShare;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "PerformanceShareResponse{" +
                "summaryPerformanceShare=" + summaryPerformanceShare +
                ", success=" + success +
                ", message='" + message + '\'' +
                '}';
    }
}
