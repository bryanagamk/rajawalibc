package com.androiddeveloper.pens.namiapp.data.network.model;

import com.dinuscxj.progressbar.CircleProgressBar;
import com.google.gson.annotations.SerializedName;

public class PerformanceShareSummary {
    private String cpTselBs;
    private String cpOoredooBs;
    private String cpXlBs;
    private String cpAxisBs;
    private String cp3Bs;
    private String cpOthersBs;

    private String cpTselDs;
    private String cpOoredooDs;
    private String cpXlDs;
    private String cpAxisDs;
    private String cp3Ds;
    private String cpOthersDs;

    private String cpTselRs;
    private String cpOoredooRs;
    private String cpXlRs;
    private String cpAxisRs;
    private String cp3Rs;
    private String cpOthersRs;

    private String cpTselMs;
    private String cpOoredooMs;
    private String cpXlMs;
    private String cpAxisMs;
    private String cp3Ms;
    private String cpOthersMs;

    private String locationType;

    private String title;

    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCpTselBs() {
        return cpTselBs;
    }

    public void setCpTselBs(String cpTselBs) {
        this.cpTselBs = cpTselBs;
    }

    public String getCpOoredooBs() {
        return cpOoredooBs;
    }

    public void setCpOoredooBs(String cpOoredooBs) {
        this.cpOoredooBs = cpOoredooBs;
    }

    public String getCpXlBs() {
        return cpXlBs;
    }

    public void setCpXlBs(String cpXlBs) {
        this.cpXlBs = cpXlBs;
    }

    public String getCpAxisBs() {
        return cpAxisBs;
    }

    public void setCpAxisBs(String cpAxisBs) {
        this.cpAxisBs = cpAxisBs;
    }

    public String getCp3Bs() {
        return cp3Bs;
    }

    public void setCp3Bs(String cp3Bs) {
        this.cp3Bs = cp3Bs;
    }

    public String getCpOthersBs() {
        return cpOthersBs;
    }

    public void setCpOthersBs(String cpOthersBs) {
        this.cpOthersBs = cpOthersBs;
    }

    public String getCpTselDs() {
        return cpTselDs;
    }

    public void setCpTselDs(String cpTselDs) {
        this.cpTselDs = cpTselDs;
    }

    public String getCpOoredooDs() {
        return cpOoredooDs;
    }

    public void setCpOoredooDs(String cpOoredooDs) {
        this.cpOoredooDs = cpOoredooDs;
    }

    public String getCpXlDs() {
        return cpXlDs;
    }

    public void setCpXlDs(String cpXlDs) {
        this.cpXlDs = cpXlDs;
    }

    public String getCpAxisDs() {
        return cpAxisDs;
    }

    public void setCpAxisDs(String cpAxisDs) {
        this.cpAxisDs = cpAxisDs;
    }

    public String getCp3Ds() {
        return cp3Ds;
    }

    public void setCp3Ds(String cp3Ds) {
        this.cp3Ds = cp3Ds;
    }

    public String getCpOthersDs() {
        return cpOthersDs;
    }

    public void setCpOthersDs(String cpOthersDs) {
        this.cpOthersDs = cpOthersDs;
    }

    public String getCpTselRs() {
        return cpTselRs;
    }

    public void setCpTselRs(String cpTselRs) {
        this.cpTselRs = cpTselRs;
    }

    public String getCpOoredooRs() {
        return cpOoredooRs;
    }

    public void setCpOoredooRs(String cpOoredooRs) {
        this.cpOoredooRs = cpOoredooRs;
    }

    public String getCpXlRs() {
        return cpXlRs;
    }

    public void setCpXlRs(String cpXlRs) {
        this.cpXlRs = cpXlRs;
    }

    public String getCpAxisRs() {
        return cpAxisRs;
    }

    public void setCpAxisRs(String cpAxisRs) {
        this.cpAxisRs = cpAxisRs;
    }

    public String getCp3Rs() {
        return cp3Rs;
    }

    public void setCp3Rs(String cp3Rs) {
        this.cp3Rs = cp3Rs;
    }

    public String getCpOthersRs() {
        return cpOthersRs;
    }

    public void setCpOthersRs(String cpOthersRs) {
        this.cpOthersRs = cpOthersRs;
    }

    public String getCpTselMs() {
        return cpTselMs;
    }

    public void setCpTselMs(String cpTselMs) {
        this.cpTselMs = cpTselMs;
    }

    public String getCpOoredooMs() {
        return cpOoredooMs;
    }

    public void setCpOoredooMs(String cpOoredooMs) {
        this.cpOoredooMs = cpOoredooMs;
    }

    public String getCpXlMs() {
        return cpXlMs;
    }

    public void setCpXlMs(String cpXlMs) {
        this.cpXlMs = cpXlMs;
    }

    public String getCpAxisMs() {
        return cpAxisMs;
    }

    public void setCpAxisMs(String cpAxisMs) {
        this.cpAxisMs = cpAxisMs;
    }

    public String getCp3Ms() {
        return cp3Ms;
    }

    public void setCp3Ms(String cp3Ms) {
        this.cp3Ms = cp3Ms;
    }

    public String getCpOthersMs() {
        return cpOthersMs;
    }

    public void setCpOthersMs(String cpOthersMs) {
        this.cpOthersMs = cpOthersMs;
    }

    @Override
    public String toString() {
        return "PerformanceShareSummary{" +
                "cpTselBs='" + cpTselBs + '\'' +
                ", cpOoredooBs='" + cpOoredooBs + '\'' +
                ", cpXlBs='" + cpXlBs + '\'' +
                ", cpAxisBs='" + cpAxisBs + '\'' +
                ", cp3Bs='" + cp3Bs + '\'' +
                ", cpOthersBs='" + cpOthersBs + '\'' +
                ", cpTselDs='" + cpTselDs + '\'' +
                ", cpOoredooDs='" + cpOoredooDs + '\'' +
                ", cpXlDs='" + cpXlDs + '\'' +
                ", cpAxisDs='" + cpAxisDs + '\'' +
                ", cp3Ds='" + cp3Ds + '\'' +
                ", cpOthersDs='" + cpOthersDs + '\'' +
                ", cpTselRs='" + cpTselRs + '\'' +
                ", cpOoredooRs='" + cpOoredooRs + '\'' +
                ", cpXlRs='" + cpXlRs + '\'' +
                ", cpAxisRs='" + cpAxisRs + '\'' +
                ", cp3Rs='" + cp3Rs + '\'' +
                ", cpOthersRs='" + cpOthersRs + '\'' +
                ", cpTselMs='" + cpTselMs + '\'' +
                ", cpOoredooMs='" + cpOoredooMs + '\'' +
                ", cpXlMs='" + cpXlMs + '\'' +
                ", cpAxisMs='" + cpAxisMs + '\'' +
                ", cp3Ms='" + cp3Ms + '\'' +
                ", cpOthersMs='" + cpOthersMs + '\'' +
                ", locationType='" + locationType + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}
