package com.androiddeveloper.pens.namiapp.data.network.model;

import android.widget.TextView;

import java.io.Serializable;
import java.util.List;

public class PerformanceSummary implements Serializable {

    private String tvPoiAchCall;
    private String tvPoiPlanCall;
    private String tvPoiActualCall;
    private String tvPoiEffectiveCall;
    private String tvPoiPlanMarketSurvey;
    private String tvPoiActualMarketSurvey;
    private String tvPoiActualSales;
    private String tvOskAchCall;
    private String tvOskPlanCall;
    private String tvOskActualCall;
    private String tvOskEffectiveCall;
    private String tvOskPlanOutletProd;
    private String tvOskOutletProd;
    private String tvOskActualSales;

    private String tvAchEventCallMtd;
    private String tvAchEventCallPtd;
    private String tvActualEventCallMtd;
    private String tvActualEventCallPtd;
    private String tvPlanEventCallPtd;
    private String tvPlanEventCallMtd;
    private String tvSalesEventCallPtd;
    private String tvSalesEventCallMtd;

    private String tvPoiAchBc;
    private String tvPoiTargetBc;
    private String tvPoiActualBc;
    private String tvOskAchBc;
    private String tvOskTargetBc;
    private String tvOskActualBc;

    private String AchCallPtd;
    private String PlanCallPtd;
    private String ActualCallPtd;
    private String EffectiveCallPtd;
    private String ActualMarketSurveyPtd;
    private String ActualSalesPtd;
    private String OutletProdPtd;
    private String AchCallMtd;
    private String PlanCallMtd;
    private String ActualCallMtd;
    private String EffectiveCallMtd;
    private String ActualMarketSurveyMtd;
    private String ActualSalesMtd;
    private String OutletProdMtd;

    private String userId;
    private String userName;
    private String locationId;
    private String locationName;
    private String locationType;
    private String periodType;
    private String title;
    private String titlePoiOsk;

    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }

    public String getPeriodType() {
        return periodType;
    }

    public void setPeriodType(String periodType) {
        this.periodType = periodType;
    }

    public String getAchCallPtd() {
        return AchCallPtd;
    }

    public void setAchCallPtd(String achCallPtd) {
        AchCallPtd = achCallPtd;
    }

    public String getPlanCallPtd() {
        return PlanCallPtd;
    }

    public void setPlanCallPtd(String planCallPtd) {
        PlanCallPtd = planCallPtd;
    }

    public String getActualCallPtd() {
        return ActualCallPtd;
    }

    public void setActualCallPtd(String actualCallPtd) {
        ActualCallPtd = actualCallPtd;
    }

    public String getEffectiveCallPtd() {
        return EffectiveCallPtd;
    }

    public void setEffectiveCallPtd(String effectiveCallPtd) {
        EffectiveCallPtd = effectiveCallPtd;
    }

    public String getActualMarketSurveyPtd() {
        return ActualMarketSurveyPtd;
    }

    public void setActualMarketSurveyPtd(String actualMarketSurveyPtd) {
        ActualMarketSurveyPtd = actualMarketSurveyPtd;
    }

    public String getActualSalesPtd() {
        return ActualSalesPtd;
    }

    public void setActualSalesPtd(String actualSalesPtd) {
        ActualSalesPtd = actualSalesPtd;
    }

    public String getOutletProdPtd() {
        return OutletProdPtd;
    }

    public void setOutletProdPtd(String outletProdPtd) {
        OutletProdPtd = outletProdPtd;
    }

    public String getAchCallMtd() {
        return AchCallMtd;
    }

    public void setAchCallMtd(String achCallMtd) {
        AchCallMtd = achCallMtd;
    }

    public String getPlanCallMtd() {
        return PlanCallMtd;
    }

    public void setPlanCallMtd(String planCallMtd) {
        PlanCallMtd = planCallMtd;
    }

    public String getActualCallMtd() {
        return ActualCallMtd;
    }

    public void setActualCallMtd(String actualCallMtd) {
        ActualCallMtd = actualCallMtd;
    }

    public String getEffectiveCallMtd() {
        return EffectiveCallMtd;
    }

    public void setEffectiveCallMtd(String effectiveCallMtd) {
        EffectiveCallMtd = effectiveCallMtd;
    }

    public String getActualMarketSurveyMtd() {
        return ActualMarketSurveyMtd;
    }

    public void setActualMarketSurveyMtd(String actualMarketSurveyMtd) {
        ActualMarketSurveyMtd = actualMarketSurveyMtd;
    }

    public String getActualSalesMtd() {
        return ActualSalesMtd;
    }

    public void setActualSalesMtd(String actualSalesMtd) {
        ActualSalesMtd = actualSalesMtd;
    }

    public String getOutletProdMtd() {
        return OutletProdMtd;
    }

    public void setOutletProdMtd(String outletProdMtd) {
        OutletProdMtd = outletProdMtd;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getTitlePoiOsk() {
        return titlePoiOsk;
    }

    public void setTitlePoiOsk(String titlePoiOsk) {
        this.titlePoiOsk = titlePoiOsk;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTvPoiPlanMarketSurvey() {
        return tvPoiPlanMarketSurvey;
    }

    public void setTvPoiPlanMarketSurvey(String tvPoiPlanMarketSurvey) {
        this.tvPoiPlanMarketSurvey = tvPoiPlanMarketSurvey;
    }

    public String getTvOskPlanOutletProd() {
        return tvOskPlanOutletProd;
    }

    public void setTvOskPlanOutletProd(String tvOskPlanOutletProd) {
        this.tvOskPlanOutletProd = tvOskPlanOutletProd;
    }

    public String getTvPoiAchBc() {
        return tvPoiAchBc;
    }

    public void setTvPoiAchBc(String tvPoiAchBc) {
        this.tvPoiAchBc = tvPoiAchBc;
    }

    public String getTvPoiTargetBc() {
        return tvPoiTargetBc;
    }

    public void setTvPoiTargetBc(String tvPoiTargetBc) {
        this.tvPoiTargetBc = tvPoiTargetBc;
    }

    public String getTvPoiActualBc() {
        return tvPoiActualBc;
    }

    public void setTvPoiActualBc(String tvPoiActualBc) {
        this.tvPoiActualBc = tvPoiActualBc;
    }

    public String getTvOskAchBc() {
        return tvOskAchBc;
    }

    public void setTvOskAchBc(String tvOskAchBc) {
        this.tvOskAchBc = tvOskAchBc;
    }

    public String getTvOskTargetBc() {
        return tvOskTargetBc;
    }

    public void setTvOskTargetBc(String tvOskTargetBc) {
        this.tvOskTargetBc = tvOskTargetBc;
    }

    public String getTvOskActualBc() {
        return tvOskActualBc;
    }

    public void setTvOskActualBc(String tvOskActualBc) {
        this.tvOskActualBc = tvOskActualBc;
    }

    public String getTvAchEventCallMtd() {
        return tvAchEventCallMtd;
    }

    public void setTvAchEventCallMtd(String tvAchEventCallMtd) {
        this.tvAchEventCallMtd = tvAchEventCallMtd;
    }

    public String getTvAchEventCallPtd() {
        return tvAchEventCallPtd;
    }

    public void setTvAchEventCallPtd(String tvAchEventCallPtd) {
        this.tvAchEventCallPtd = tvAchEventCallPtd;
    }

    public String getTvActualEventCallMtd() {
        return tvActualEventCallMtd;
    }

    public void setTvActualEventCallMtd(String tvActualEventCallMtd) {
        this.tvActualEventCallMtd = tvActualEventCallMtd;
    }

    public String getTvActualEventCallPtd() {
        return tvActualEventCallPtd;
    }

    public void setTvActualEventCallPtd(String tvActualEventCallPtd) {
        this.tvActualEventCallPtd = tvActualEventCallPtd;
    }

    public String getTvPlanEventCallPtd() {
        return tvPlanEventCallPtd;
    }

    public void setTvPlanEventCallPtd(String tvPlanEventCallPtd) {
        this.tvPlanEventCallPtd = tvPlanEventCallPtd;
    }

    public String getTvPlanEventCallMtd() {
        return tvPlanEventCallMtd;
    }

    public void setTvPlanEventCallMtd(String tvPlanEventCallMtd) {
        this.tvPlanEventCallMtd = tvPlanEventCallMtd;
    }

    public String getTvSalesEventCallPtd() {
        return tvSalesEventCallPtd;
    }

    public void setTvSalesEventCallPtd(String tvSalesEventCallPtd) {
        this.tvSalesEventCallPtd = tvSalesEventCallPtd;
    }

    public String getTvSalesEventCallMtd() {
        return tvSalesEventCallMtd;
    }

    public void setTvSalesEventCallMtd(String tvSalesEventCallMtd) {
        this.tvSalesEventCallMtd = tvSalesEventCallMtd;
    }

    public String getTvPoiAchCall() {
        return tvPoiAchCall;
    }

    public void setTvPoiAchCall(String tvPoiAchCall) {
        this.tvPoiAchCall = tvPoiAchCall;
    }

    public String getTvPoiPlanCall() {
        return tvPoiPlanCall;
    }

    public void setTvPoiPlanCall(String tvPoiPlanCall) {
        this.tvPoiPlanCall = tvPoiPlanCall;
    }

    public String getTvPoiActualCall() {
        return tvPoiActualCall;
    }

    public void setTvPoiActualCall(String tvPoiActualCall) {
        this.tvPoiActualCall = tvPoiActualCall;
    }

    public String getTvPoiEffectiveCall() {
        return tvPoiEffectiveCall;
    }

    public void setTvPoiEffectiveCall(String tvPoiEffectiveCall) {
        this.tvPoiEffectiveCall = tvPoiEffectiveCall;
    }

    public String getTvPoiActualMarketSurvey() {
        return tvPoiActualMarketSurvey;
    }

    public void setTvPoiActualMarketSurvey(String tvPoiActualMarketSurvey) {
        this.tvPoiActualMarketSurvey = tvPoiActualMarketSurvey;
    }

    public String getTvPoiActualSales() {
        return tvPoiActualSales;
    }

    public void setTvPoiActualSales(String tvPoiActualSales) {
        this.tvPoiActualSales = tvPoiActualSales;
    }

    public String getTvOskAchCall() {
        return tvOskAchCall;
    }

    public void setTvOskAchCall(String tvOskAchCall) {
        this.tvOskAchCall = tvOskAchCall;
    }

    public String getTvOskPlanCall() {
        return tvOskPlanCall;
    }

    public void setTvOskPlanCall(String tvOskPlanCall) {
        this.tvOskPlanCall = tvOskPlanCall;
    }

    public String getTvOskActualCall() {
        return tvOskActualCall;
    }

    public void setTvOskActualCall(String tvOskActualCall) {
        this.tvOskActualCall = tvOskActualCall;
    }

    public String getTvOskEffectiveCall() {
        return tvOskEffectiveCall;
    }

    public void setTvOskEffectiveCall(String tvOskEffectiveCall) {
        this.tvOskEffectiveCall = tvOskEffectiveCall;
    }

    public String getTvOskOutletProd() {
        return tvOskOutletProd;
    }

    public void setTvOskOutletProd(String tvOskOutletProd) {
        this.tvOskOutletProd = tvOskOutletProd;
    }

    public String getTvOskActualSales() {
        return tvOskActualSales;
    }

    public void setTvOskActualSales(String tvOskActualSales) {
        this.tvOskActualSales = tvOskActualSales;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "PerformanceSummary{" +
                "tvPoiAchCall='" + tvPoiAchCall + '\'' +
                ", tvPoiPlanCall='" + tvPoiPlanCall + '\'' +
                ", tvPoiActualCall='" + tvPoiActualCall + '\'' +
                ", tvPoiEffectiveCall='" + tvPoiEffectiveCall + '\'' +
                ", tvPoiPlanMarketSurvey='" + tvPoiPlanMarketSurvey + '\'' +
                ", tvPoiActualMarketSurvey='" + tvPoiActualMarketSurvey + '\'' +
                ", tvPoiActualSales='" + tvPoiActualSales + '\'' +
                ", tvOskAchCall='" + tvOskAchCall + '\'' +
                ", tvOskPlanCall='" + tvOskPlanCall + '\'' +
                ", tvOskActualCall='" + tvOskActualCall + '\'' +
                ", tvOskEffectiveCall='" + tvOskEffectiveCall + '\'' +
                ", tvOskPlanOutletProd='" + tvOskPlanOutletProd + '\'' +
                ", tvOskOutletProd='" + tvOskOutletProd + '\'' +
                ", tvOskActualSales='" + tvOskActualSales + '\'' +
                ", tvAchEventCallMtd='" + tvAchEventCallMtd + '\'' +
                ", tvAchEventCallPtd='" + tvAchEventCallPtd + '\'' +
                ", tvActualEventCallMtd='" + tvActualEventCallMtd + '\'' +
                ", tvActualEventCallPtd='" + tvActualEventCallPtd + '\'' +
                ", tvPlanEventCallPtd='" + tvPlanEventCallPtd + '\'' +
                ", tvPlanEventCallMtd='" + tvPlanEventCallMtd + '\'' +
                ", tvSalesEventCallPtd='" + tvSalesEventCallPtd + '\'' +
                ", tvSalesEventCallMtd='" + tvSalesEventCallMtd + '\'' +
                ", tvPoiAchBc='" + tvPoiAchBc + '\'' +
                ", tvPoiTargetBc='" + tvPoiTargetBc + '\'' +
                ", tvPoiActualBc='" + tvPoiActualBc + '\'' +
                ", tvOskAchBc='" + tvOskAchBc + '\'' +
                ", tvOskTargetBc='" + tvOskTargetBc + '\'' +
                ", tvOskActualBc='" + tvOskActualBc + '\'' +
                ", userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                ", locationId='" + locationId + '\'' +
                ", locationName='" + locationName + '\'' +
                ", title='" + title + '\'' +
                ", titlePoiOsk='" + titlePoiOsk + '\'' +
                '}';
    }
}
