package com.androiddeveloper.pens.namiapp.data.network.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

public class Profile{

	@SerializedName("chairman_contact")
	private String chairmanContact;

	@SerializedName("principal_name")
	private String principalName;

	@SerializedName("influencer_title")
	private String influencerTitle;

	@SerializedName("ambassador_religion")
	private Object ambassadorReligion;

	@SerializedName("network_telkomsel")
	private Object networkTelkomsel;

	@SerializedName("wallmagazine_branding")
	private Object wallmagazineBranding;

	@SerializedName("network_3")
	private Object network3;

	@SerializedName("institution_instagram")
	private Object institutionInstagram;

	@SerializedName("total_cooperative")
	private String totalCooperative;

	@SerializedName("principal_religion")
	private String principalReligion;

	@SerializedName("last_update")
	private String lastUpdate;

	@SerializedName("total_students")
	private String totalStudents;

	@SerializedName("total_teacher")
	private String totalTeacher;

	@SerializedName("ambassador_birthday")
	private Object ambassadorBirthday;

	@SerializedName("field_branding")
	private Object fieldBranding;

	@SerializedName("total_sportroom")
	private String totalSportroom;

	@SerializedName("influencer_hobby")
	private String influencerHobby;

	@SerializedName("ambassador_fb")
	private Object ambassadorFb;

	@SerializedName("npsn")
	private String npsn;

	@SerializedName("principal_contact")
	private String principalContact;

	@SerializedName("institution_fb")
	private Object institutionFb;

	@SerializedName("total_canteen")
	private String totalCanteen;

	@SerializedName("chairman_hobby")
	private String chairmanHobby;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("network_ooredoo")
	private Object networkOoredoo;

	@SerializedName("network_others")
	private Object networkOthers;

	@SerializedName("chairman_religion")
	private String chairmanReligion;

	@SerializedName("wall_branding")
	private Object wallBranding;

	@SerializedName("total_laboratorium")
	private String totalLaboratorium;

	@SerializedName("chairman_name")
	private String chairmanName;

	@SerializedName("week")
	private String week;

	@SerializedName("user_name")
	private String userName;

	@SerializedName("ambassador_name")
	private Object ambassadorName;

	@SerializedName("principal_hobby")
	private String principalHobby;

	@SerializedName("ambassador_line")
	private Object ambassadorLine;

	@SerializedName("influencer_name")
	private String influencerName;

	@SerializedName("influencer_religion")
	private String influencerReligion;

	@SerializedName("network_xl")
	private Object networkXl;

	@SerializedName("network_axis")
	private Object networkAxis;

	@SerializedName("total_classroom")
	private String totalClassroom;

	@SerializedName("chairman_birthday")
	private String chairmanBirthday;

	@SerializedName("ambassador_hobby")
	private Object ambassadorHobby;

	@SerializedName("influencer_contact")
	private String influencerContact;

	@SerializedName("institution_birthday")
	private String institutionBirthday;

	@SerializedName("institution_url")
	private Object institutionUrl;

	@SerializedName("interested_activities")
	private String interestedActivities;

	@SerializedName("principal_birthday")
	private String principalBirthday;

	@SerializedName("total_ambassador")
	private String totalAmbassador;

	@SerializedName("influencer_birthday")
	private String influencerBirthday;

	@SerializedName("ambassador_contact")
	private Object ambassadorContact;

	@SerializedName("ambassador_instagram")
	private Object ambassadorInstagram;

	public void setChairmanContact(String chairmanContact){
		this.chairmanContact = chairmanContact;
	}

	public String getChairmanContact(){
		return chairmanContact;
	}

	public void setPrincipalName(String principalName){
		this.principalName = principalName;
	}

	public String getPrincipalName(){
		return principalName;
	}

	public void setInfluencerTitle(String influencerTitle){
		this.influencerTitle = influencerTitle;
	}

	public String getInfluencerTitle(){
		return influencerTitle;
	}

	public void setAmbassadorReligion(Object ambassadorReligion){
		this.ambassadorReligion = ambassadorReligion;
	}

	public Object getAmbassadorReligion(){
		return ambassadorReligion;
	}

	public void setNetworkTelkomsel(Object networkTelkomsel){
		this.networkTelkomsel = networkTelkomsel;
	}

	public Object getNetworkTelkomsel(){
		return networkTelkomsel;
	}

	public void setWallmagazineBranding(Object wallmagazineBranding){
		this.wallmagazineBranding = wallmagazineBranding;
	}

	public Object getWallmagazineBranding(){
		return wallmagazineBranding;
	}

	public void setNetwork3(Object network3){
		this.network3 = network3;
	}

	public Object getNetwork3(){
		return network3;
	}

	public void setInstitutionInstagram(Object institutionInstagram){
		this.institutionInstagram = institutionInstagram;
	}

	public Object getInstitutionInstagram(){
		return institutionInstagram;
	}

	public void setTotalCooperative(String totalCooperative){
		this.totalCooperative = totalCooperative;
	}

	public String getTotalCooperative(){
		return totalCooperative;
	}

	public void setPrincipalReligion(String principalReligion){
		this.principalReligion = principalReligion;
	}

	public String getPrincipalReligion(){
		return principalReligion;
	}

	public void setLastUpdate(String lastUpdate){
		this.lastUpdate = lastUpdate;
	}

	public String getLastUpdate(){
		return lastUpdate;
	}

	public void setTotalStudents(String totalStudents){
		this.totalStudents = totalStudents;
	}

	public String getTotalStudents(){
		return totalStudents;
	}

	public void setTotalTeacher(String totalTeacher){
		this.totalTeacher = totalTeacher;
	}

	public String getTotalTeacher(){
		return totalTeacher;
	}

	public void setAmbassadorBirthday(Object ambassadorBirthday){
		this.ambassadorBirthday = ambassadorBirthday;
	}

	public Object getAmbassadorBirthday(){
		return ambassadorBirthday;
	}

	public void setFieldBranding(Object fieldBranding){
		this.fieldBranding = fieldBranding;
	}

	public Object getFieldBranding(){
		return fieldBranding;
	}

	public void setTotalSportroom(String totalSportroom){
		this.totalSportroom = totalSportroom;
	}

	public String getTotalSportroom(){
		return totalSportroom;
	}

	public void setInfluencerHobby(String influencerHobby){
		this.influencerHobby = influencerHobby;
	}

	public String getInfluencerHobby(){
		return influencerHobby;
	}

	public void setAmbassadorFb(Object ambassadorFb){
		this.ambassadorFb = ambassadorFb;
	}

	public Object getAmbassadorFb(){
		return ambassadorFb;
	}

	public void setNpsn(String npsn){
		this.npsn = npsn;
	}

	public String getNpsn(){
		return npsn;
	}

	public void setPrincipalContact(String principalContact){
		this.principalContact = principalContact;
	}

	public String getPrincipalContact(){
		return principalContact;
	}

	public void setInstitutionFb(Object institutionFb){
		this.institutionFb = institutionFb;
	}

	public Object getInstitutionFb(){
		return institutionFb;
	}

	public void setTotalCanteen(String totalCanteen){
		this.totalCanteen = totalCanteen;
	}

	public String getTotalCanteen(){
		return totalCanteen;
	}

	public void setChairmanHobby(String chairmanHobby){
		this.chairmanHobby = chairmanHobby;
	}

	public String getChairmanHobby(){
		return chairmanHobby;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setNetworkOoredoo(Object networkOoredoo){
		this.networkOoredoo = networkOoredoo;
	}

	public Object getNetworkOoredoo(){
		return networkOoredoo;
	}

	public void setNetworkOthers(Object networkOthers){
		this.networkOthers = networkOthers;
	}

	public Object getNetworkOthers(){
		return networkOthers;
	}

	public void setChairmanReligion(String chairmanReligion){
		this.chairmanReligion = chairmanReligion;
	}

	public String getChairmanReligion(){
		return chairmanReligion;
	}

	public void setWallBranding(Object wallBranding){
		this.wallBranding = wallBranding;
	}

	public Object getWallBranding(){
		return wallBranding;
	}

	public void setTotalLaboratorium(String totalLaboratorium){
		this.totalLaboratorium = totalLaboratorium;
	}

	public String getTotalLaboratorium(){
		return totalLaboratorium;
	}

	public void setChairmanName(String chairmanName){
		this.chairmanName = chairmanName;
	}

	public String getChairmanName(){
		return chairmanName;
	}

	public void setWeek(String week){
		this.week = week;
	}

	public String getWeek(){
		return week;
	}

	public void setUserName(String userName){
		this.userName = userName;
	}

	public String getUserName(){
		return userName;
	}

	public void setAmbassadorName(Object ambassadorName){
		this.ambassadorName = ambassadorName;
	}

	public Object getAmbassadorName(){
		return ambassadorName;
	}

	public void setPrincipalHobby(String principalHobby){
		this.principalHobby = principalHobby;
	}

	public String getPrincipalHobby(){
		return principalHobby;
	}

	public void setAmbassadorLine(Object ambassadorLine){
		this.ambassadorLine = ambassadorLine;
	}

	public Object getAmbassadorLine(){
		return ambassadorLine;
	}

	public void setInfluencerName(String influencerName){
		this.influencerName = influencerName;
	}

	public String getInfluencerName(){
		return influencerName;
	}

	public void setInfluencerReligion(String influencerReligion){
		this.influencerReligion = influencerReligion;
	}

	public String getInfluencerReligion(){
		return influencerReligion;
	}

	public void setNetworkXl(Object networkXl){
		this.networkXl = networkXl;
	}

	public Object getNetworkXl(){
		return networkXl;
	}

	public void setNetworkAxis(Object networkAxis){
		this.networkAxis = networkAxis;
	}

	public Object getNetworkAxis(){
		return networkAxis;
	}

	public void setTotalClassroom(String totalClassroom){
		this.totalClassroom = totalClassroom;
	}

	public String getTotalClassroom(){
		return totalClassroom;
	}

	public void setChairmanBirthday(String chairmanBirthday){
		this.chairmanBirthday = chairmanBirthday;
	}

	public String getChairmanBirthday(){
		return chairmanBirthday;
	}

	public void setAmbassadorHobby(Object ambassadorHobby){
		this.ambassadorHobby = ambassadorHobby;
	}

	public Object getAmbassadorHobby(){
		return ambassadorHobby;
	}

	public void setInfluencerContact(String influencerContact){
		this.influencerContact = influencerContact;
	}

	public String getInfluencerContact(){
		return influencerContact;
	}

	public void setInstitutionBirthday(String institutionBirthday){
		this.institutionBirthday = institutionBirthday;
	}

	public String getInstitutionBirthday(){
		return institutionBirthday;
	}

	public void setInstitutionUrl(Object institutionUrl){
		this.institutionUrl = institutionUrl;
	}

	public Object getInstitutionUrl(){
		return institutionUrl;
	}

	public void setInterestedActivities(String interestedActivities){
		this.interestedActivities = interestedActivities;
	}

	public String getInterestedActivities(){
		return interestedActivities;
	}

	public void setPrincipalBirthday(String principalBirthday){
		this.principalBirthday = principalBirthday;
	}

	public String getPrincipalBirthday(){
		return principalBirthday;
	}

	public void setTotalAmbassador(String totalAmbassador){
		this.totalAmbassador = totalAmbassador;
	}

	public String getTotalAmbassador(){
		return totalAmbassador;
	}

	public void setInfluencerBirthday(String influencerBirthday){
		this.influencerBirthday = influencerBirthday;
	}

	public String getInfluencerBirthday(){
		return influencerBirthday;
	}

	public void setAmbassadorContact(Object ambassadorContact){
		this.ambassadorContact = ambassadorContact;
	}

	public Object getAmbassadorContact(){
		return ambassadorContact;
	}

	public void setAmbassadorInstagram(Object ambassadorInstagram){
		this.ambassadorInstagram = ambassadorInstagram;
	}

	public Object getAmbassadorInstagram(){
		return ambassadorInstagram;
	}

	@Override
 	public String toString(){
		return 
			"Profile{" + 
			"chairman_contact = '" + chairmanContact + '\'' + 
			",principal_name = '" + principalName + '\'' + 
			",influencer_title = '" + influencerTitle + '\'' + 
			",ambassador_religion = '" + ambassadorReligion + '\'' + 
			",network_telkomsel = '" + networkTelkomsel + '\'' + 
			",wallmagazine_branding = '" + wallmagazineBranding + '\'' + 
			",network_3 = '" + network3 + '\'' + 
			",institution_instagram = '" + institutionInstagram + '\'' + 
			",total_cooperative = '" + totalCooperative + '\'' + 
			",principal_religion = '" + principalReligion + '\'' + 
			",last_update = '" + lastUpdate + '\'' + 
			",total_students = '" + totalStudents + '\'' + 
			",total_teacher = '" + totalTeacher + '\'' + 
			",ambassador_birthday = '" + ambassadorBirthday + '\'' + 
			",field_branding = '" + fieldBranding + '\'' + 
			",total_sportroom = '" + totalSportroom + '\'' + 
			",influencer_hobby = '" + influencerHobby + '\'' + 
			",ambassador_fb = '" + ambassadorFb + '\'' + 
			",npsn = '" + npsn + '\'' + 
			",principal_contact = '" + principalContact + '\'' + 
			",institution_fb = '" + institutionFb + '\'' + 
			",total_canteen = '" + totalCanteen + '\'' + 
			",chairman_hobby = '" + chairmanHobby + '\'' + 
			",user_id = '" + userId + '\'' + 
			",network_ooredoo = '" + networkOoredoo + '\'' + 
			",network_others = '" + networkOthers + '\'' + 
			",chairman_religion = '" + chairmanReligion + '\'' + 
			",wall_branding = '" + wallBranding + '\'' + 
			",total_laboratorium = '" + totalLaboratorium + '\'' + 
			",chairman_name = '" + chairmanName + '\'' + 
			",week = '" + week + '\'' + 
			",user_name = '" + userName + '\'' + 
			",ambassador_name = '" + ambassadorName + '\'' + 
			",principal_hobby = '" + principalHobby + '\'' + 
			",ambassador_line = '" + ambassadorLine + '\'' + 
			",influencer_name = '" + influencerName + '\'' + 
			",influencer_religion = '" + influencerReligion + '\'' + 
			",network_xl = '" + networkXl + '\'' + 
			",network_axis = '" + networkAxis + '\'' + 
			",total_classroom = '" + totalClassroom + '\'' + 
			",chairman_birthday = '" + chairmanBirthday + '\'' + 
			",ambassador_hobby = '" + ambassadorHobby + '\'' + 
			",influencer_contact = '" + influencerContact + '\'' + 
			",institution_birthday = '" + institutionBirthday + '\'' + 
			",institution_url = '" + institutionUrl + '\'' + 
			",interested_activities = '" + interestedActivities + '\'' + 
			",principal_birthday = '" + principalBirthday + '\'' + 
			",total_ambassador = '" + totalAmbassador + '\'' + 
			",influencer_birthday = '" + influencerBirthday + '\'' + 
			",ambassador_contact = '" + ambassadorContact + '\'' + 
			",ambassador_instagram = '" + ambassadorInstagram + '\'' + 
			"}";
		}
}