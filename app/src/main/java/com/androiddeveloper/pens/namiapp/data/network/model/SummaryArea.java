package com.androiddeveloper.pens.namiapp.data.network.model;

/**
 * Created by miftahun on 8/23/18.
 */

public class SummaryArea {

    private String achPtd;
    private String achMtd;
    private String actualPtd;
    private String planPtd;
    private String actualMtd;
    private String planMtd;
    private String title;

    private String salesOskPtd;
    private String salesOskMtd;
    private String salesInstitutionPtd;
    private String salesInstitutionMtd;
    private String salesEventPtd;
    private String salesEventMtd;

    public SummaryArea() {

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAchPtd() {
        return achPtd;
    }

    public void setAchPtd(String achPtd) {
        this.achPtd = achPtd;
    }

    public String getAchMtd() {
        return achMtd;
    }

    public void setAchMtd(String achMtd) {
        this.achMtd = achMtd;
    }

    public String getActualPtd() {
        return actualPtd;
    }

    public void setActualPtd(String actualPtd) {
        this.actualPtd = actualPtd;
    }

    public String getPlanPtd() {
        return planPtd;
    }

    public void setPlanPtd(String planPtd) {
        this.planPtd = planPtd;
    }

    public String getActualMtd() {
        return actualMtd;
    }

    public void setActualMtd(String actualMtd) {
        this.actualMtd = actualMtd;
    }

    public String getPlanMtd() {
        return planMtd;
    }

    public void setPlanMtd(String planMtd) {
        this.planMtd = planMtd;
    }

    public String getSalesOskPtd() {
        return salesOskPtd;
    }

    public void setSalesOskPtd(String salesOskPtd) {
        this.salesOskPtd = salesOskPtd;
    }

    public String getSalesOskMtd() {
        return salesOskMtd;
    }

    public void setSalesOskMtd(String salesOskMtd) {
        this.salesOskMtd = salesOskMtd;
    }

    public String getSalesInstitutionPtd() {
        return salesInstitutionPtd;
    }

    public void setSalesInstitutionPtd(String salesInstitutionPtd) {
        this.salesInstitutionPtd = salesInstitutionPtd;
    }

    public String getSalesInstitutionMtd() {
        return salesInstitutionMtd;
    }

    public void setSalesInstitutionMtd(String salesInstitutionMtd) {
        this.salesInstitutionMtd = salesInstitutionMtd;
    }

    public String getSalesEventPtd() {
        return salesEventPtd;
    }

    public void setSalesEventPtd(String salesEventPtd) {
        this.salesEventPtd = salesEventPtd;
    }

    public String getSalesEventMtd() {
        return salesEventMtd;
    }

    public void setSalesEventMtd(String salesEventMtd) {
        this.salesEventMtd = salesEventMtd;
    }

    @Override
    public String toString() {
        return "SummaryArea{" +
                "achPtd='" + achPtd + '\'' +
                ", achMtd='" + achMtd + '\'' +
                ", actualPtd='" + actualPtd + '\'' +
                ", planPtd='" + planPtd + '\'' +
                ", actualMtd='" + actualMtd + '\'' +
                ", planMtd='" + planMtd + '\'' +
                ", title='" + title + '\'' +
                ", salesOskPtd='" + salesOskPtd + '\'' +
                ", salesOskMtd='" + salesOskMtd + '\'' +
                ", salesInstitutionPtd='" + salesInstitutionPtd + '\'' +
                ", salesInstitutionMtd='" + salesInstitutionMtd + '\'' +
                ", salesEventPtd='" + salesEventPtd + '\'' +
                ", salesEventMtd='" + salesEventMtd + '\'' +
                '}';
    }
}
