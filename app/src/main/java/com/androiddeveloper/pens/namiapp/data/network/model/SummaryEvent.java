package com.androiddeveloper.pens.namiapp.data.network.model;

import javax.annotation.Generated;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class SummaryEvent{

	@SerializedName("user_name")
	@Expose
	private  String aoc;

	@SerializedName("area")
	private String area;

	@SerializedName("cluster")
	private String cluster;

	@SerializedName("plan_event_call_mtd")
	private String planEventCallMtd;

	@SerializedName("actual_event_call_ptd")
	private String actualEventCallPtd;

	@SerializedName("ach_event_call_ptd")
	private String achEventCallPtd;

	@SerializedName("actual_event_call_mtd")
	private String actualEventCallMtd;

	@SerializedName("ach_event_call_mtd")
	private String achEventCallMtd;

	@SerializedName("regional")
	private String regional;

	@SerializedName("city")
	private String city;

	@SerializedName("branch")
	private String branch;

	@SerializedName("plan_event_call_ptd")
	private String planEventCallPtd;

	public String getAoc() {
		return aoc;
	}

	public void setAoc(String aoc) {
		this.aoc = aoc;
	}

	public void setArea(String area){
		this.area = area;
	}

	public String getArea(){
		return area;
	}

	public void setCluster(String cluster){
		this.cluster = cluster;
	}

	public String getCluster(){
		return cluster;
	}

	public void setPlanEventCallMtd(String planEventCallMtd){
		this.planEventCallMtd = planEventCallMtd;
	}

	public String getPlanEventCallMtd(){
		return planEventCallMtd;
	}

	public void setActualEventCallPtd(String actualEventCallPtd){
		this.actualEventCallPtd = actualEventCallPtd;
	}

	public String getActualEventCallPtd(){
		return actualEventCallPtd;
	}

	public void setAchEventCallPtd(String achEventCallPtd){
		this.achEventCallPtd = achEventCallPtd;
	}

	public String getAchEventCallPtd(){
		return achEventCallPtd;
	}

	public void setActualEventCallMtd(String actualEventCallMtd){
		this.actualEventCallMtd = actualEventCallMtd;
	}

	public String getActualEventCallMtd(){
		return actualEventCallMtd;
	}

	public void setAchEventCallMtd(String achEventCallMtd){
		this.achEventCallMtd = achEventCallMtd;
	}

	public String getAchEventCallMtd(){
		return achEventCallMtd;
	}

	public void setRegional(String regional){
		this.regional = regional;
	}

	public String getRegional(){
		return regional;
	}

	public void setCity(String city){
		this.city = city;
	}

	public String getCity(){
		return city;
	}

	public void setBranch(String branch){
		this.branch = branch;
	}

	public String getBranch(){
		return branch;
	}

	public void setPlanEventCallPtd(String planEventCallPtd){
		this.planEventCallPtd = planEventCallPtd;
	}

	public String getPlanEventCallPtd(){
		return planEventCallPtd;
	}

	@Override
 	public String toString(){
		return 
			"SummaryEvent{" + 
			"area = '" + area + '\'' + 
			",cluster = '" + cluster + '\'' + 
			",plan_event_call_mtd = '" + planEventCallMtd + '\'' + 
			",actual_event_call_ptd = '" + actualEventCallPtd + '\'' + 
			",ach_event_call_ptd = '" + achEventCallPtd + '\'' + 
			",actual_event_call_mtd = '" + actualEventCallMtd + '\'' + 
			",ach_event_call_mtd = '" + achEventCallMtd + '\'' + 
			",regional = '" + regional + '\'' + 
			",city = '" + city + '\'' + 
			",branch = '" + branch + '\'' + 
			",plan_event_call_ptd = '" + planEventCallPtd + '\'' + 
			"}";
		}

	public SummaryArea getSummaryEvent(){
		SummaryArea summaryArea = new SummaryArea();
		summaryArea.setAchMtd(getAchEventCallMtd());
		summaryArea.setAchPtd(getAchEventCallPtd());
		summaryArea.setActualPtd(getActualEventCallPtd());
		summaryArea.setActualMtd(getActualEventCallMtd());
		summaryArea.setPlanPtd(getPlanEventCallPtd());
		summaryArea.setPlanMtd(getPlanEventCallMtd());
		summaryArea.setTitle(getTitle());
		return summaryArea;
	}

	public String getTitle(){
		if (aoc != null){
			return city + "\t\t" + "( " + aoc + " )";
		}else if (city != null )
			return city;
		else if (cluster != null )
			return cluster;
		else if (branch != null)
			return branch;
		else return regional;
	}
}