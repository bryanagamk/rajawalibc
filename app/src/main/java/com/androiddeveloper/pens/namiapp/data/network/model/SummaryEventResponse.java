package com.androiddeveloper.pens.namiapp.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by miftahun on 8/29/18.
 */

public class SummaryEventResponse{

    @SerializedName("success")
    @Expose
    private boolean success;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("summary_event")
    @Expose
    private List<SummaryEvent> summaryEvents;

    public void setSuccess(boolean success){
        this.success = success;
    }

    public boolean isSuccess(){
        return success;
    }

    public void setMessage(String message){
        this.message = message;
    }

    public String getMessage(){
        return message;
    }

    public List<SummaryEvent> getSummaryEvents() {
        return summaryEvents;
    }

    public void setSummaryEvents(List<SummaryEvent> summaryEvents) {
        this.summaryEvents = summaryEvents;
    }

    @Override
    public String toString() {
        return "SummaryEventResponse{" +
                "success=" + success +
                ", message='" + message + '\'' +
                ", summaryEvents=" + summaryEvents +
                '}';
    }
}