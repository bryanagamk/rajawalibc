package com.androiddeveloper.pens.namiapp.data.network.model;

import javax.annotation.Generated;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class SummaryInstitution{



	@SerializedName("area")
	@Expose
	private String area;

	@SerializedName("ach_institution_survey")
	@Expose
	private String achInstitutionSurvey;

	@SerializedName("regional")
	@Expose
	private String regional;

	@SerializedName("actual_institution_call_ptd")
	@Expose
	private String actualInstitutionCallPtd;

	@SerializedName("effective_institution_call_ptd")
	@Expose
	private String effectiveInstitutionCallPtd;

	@SerializedName("effective_institution_call_mtd")
	@Expose
	private String effectiveInstitutionCallMtd;

	@SerializedName("ms_3")
	@Expose
	private String ms3;

	@SerializedName("actual_institution_call_mtd")
	@Expose
	private String actualInstitutionCallMtd;

	@SerializedName("plan_institution_call_ptd")
	@Expose
	private String planInstitutionCallPtd;

	@SerializedName("ms_others")
	@Expose
	private String msOthers;

	@SerializedName("branch")
	@Expose
	private String branch;

	@SerializedName("plan_institution_call_mtd")
	@Expose
	private String planInstitutionCallMtd;

	@SerializedName("actual_institution_survey")
	@Expose
	private String actualInstitutionSurvey;

	@SerializedName("plan_institution_survey")
	@Expose
	private String planInstitutionSurvey;

	@SerializedName("ms_telkomsel")
	@Expose
	private String msTelkomsel;

	@SerializedName("ms_axis")
	@Expose
	private String msAxis;

	@SerializedName("ms_xl")
	@Expose
	private String msXl;

	@SerializedName("visit_institution_mtd")
	@Expose
	private String planEffectivePoiCallMtd;

	@SerializedName("visit_institution_ptd")
	@Expose
	private String PlanEffectivePoiCallPtd;

	@SerializedName("ach_institution_call_mtd")
	@Expose
	private String achInstitutionCallMtd;

	@SerializedName("ach_effective_institution_call_ptd")
	@Expose
	private String achEffectiveInstitutionCallPtd;

	@SerializedName("ach_effective_institution_call_mtd")
	@Expose
	private String achEffectiveInstitutionCallMtd;

	@SerializedName("ach_institution_call_ptd")
	@Expose
	private String achInstitutionCallPtd;

	@SerializedName("ms_ooredoo")
	@Expose
	private String msOoredoo;

	@SerializedName("cluster")
	@Expose
	private String cluster;

	@SerializedName("city")
	@Expose
	private String city;

	@SerializedName("user_name")
	@Expose
	private String aoc;

	public String getPlanEffectivePoiCallMtd() {
		return planEffectivePoiCallMtd;
	}

	public void setPlanEffectivePoiCallMtd(String planEffectivePoiCallMtd) {
		this.planEffectivePoiCallMtd = planEffectivePoiCallMtd;
	}

	public String getPlanEffectivePoiCallPtd() {
		return PlanEffectivePoiCallPtd;
	}

	public void setPlanEffectivePoiCallPtd(String planEffectivePoiCallPtd) {
		PlanEffectivePoiCallPtd = planEffectivePoiCallPtd;
	}

	public String getEffectiveInstitutionCallPtd() {
		return effectiveInstitutionCallPtd;
	}

	public void setEffectiveInstitutionCallPtd(String effectiveInstitutionCallPtd) {
		this.effectiveInstitutionCallPtd = effectiveInstitutionCallPtd;
	}

	public String getEffectiveInstitutionCallMtd() {
		return effectiveInstitutionCallMtd;
	}

	public void setEffectiveInstitutionCallMtd(String effectiveInstitutionCallMtd) {
		this.effectiveInstitutionCallMtd = effectiveInstitutionCallMtd;
	}

	public String getAchEffectiveInstitutionCallPtd() {
		return achEffectiveInstitutionCallPtd;
	}

	public void setAchEffectiveInstitutionCallPtd(String achEffectiveInstitutionCallPtd) {
		this.achEffectiveInstitutionCallPtd = achEffectiveInstitutionCallPtd;
	}

	public String getAchEffectiveInstitutionCallMtd() {
		return achEffectiveInstitutionCallMtd;
	}

	public void setAchEffectiveInstitutionCallMtd(String achEffectiveInstitutionCallMtd) {
		this.achEffectiveInstitutionCallMtd = achEffectiveInstitutionCallMtd;
	}

	public void setArea(String area){
		this.area = area;
	}

	public String getArea(){
		return area;
	}

	public void setAchInstitutionSurvey(String achInstitutionSurvey){
		this.achInstitutionSurvey = achInstitutionSurvey;
	}

	public String getAchInstitutionSurvey(){
		return achInstitutionSurvey;
	}

	public void setRegional(String regional){
		this.regional = regional;
	}

	public String getRegional(){
		return regional;
	}

	public void setActualInstitutionCallPtd(String actualInstitutionCallPtd){
		this.actualInstitutionCallPtd = actualInstitutionCallPtd;
	}

	public String getActualInstitutionCallPtd(){
		return actualInstitutionCallPtd;
	}

	public void setMs3(String ms3){
		this.ms3 = ms3;
	}

	public String getMs3(){
		return ms3;
	}

	public void setActualInstitutionCallMtd(String actualInstitutionCallMtd){
		this.actualInstitutionCallMtd = actualInstitutionCallMtd;
	}

	public String getActualInstitutionCallMtd(){
		return actualInstitutionCallMtd;
	}

	public void setPlanInstitutionCallPtd(String planInstitutionCallPtd){
		this.planInstitutionCallPtd = planInstitutionCallPtd;
	}

	public String getPlanInstitutionCallPtd(){
		return planInstitutionCallPtd;
	}

	public void setMsOthers(String msOthers){
		this.msOthers = msOthers;
	}

	public String getMsOthers(){
		return msOthers;
	}

	public void setBranch(String branch){
		this.branch = branch;
	}

	public String getBranch(){
		return branch;
	}

	public void setPlanInstitutionCallMtd(String planInstitutionCallMtd){
		this.planInstitutionCallMtd = planInstitutionCallMtd;
	}

	public String getPlanInstitutionCallMtd(){
		return planInstitutionCallMtd;
	}

	public void setActualInstitutionSurvey(String actualInstitutionSurvey){
		this.actualInstitutionSurvey = actualInstitutionSurvey;
	}

	public String getActualInstitutionSurvey(){
		return actualInstitutionSurvey;
	}

	public void setPlanInstitutionSurvey(String planInstitutionSurvey){
		this.planInstitutionSurvey = planInstitutionSurvey;
	}

	public String getPlanInstitutionSurvey(){
		return planInstitutionSurvey;
	}

	public void setMsTelkomsel(String msTelkomsel){
		this.msTelkomsel = msTelkomsel;
	}

	public String getMsTelkomsel(){
		return msTelkomsel;
	}

	public void setMsAxis(String msAxis){
		this.msAxis = msAxis;
	}

	public String getMsAxis(){
		return msAxis;
	}

	public void setMsXl(String msXl){
		this.msXl = msXl;
	}

	public String getMsXl(){
		return msXl;
	}

	public void setAchInstitutionCallMtd(String achInstitutionCallMtd){
		this.achInstitutionCallMtd = achInstitutionCallMtd;
	}

	public String getAchInstitutionCallMtd(){
		return achInstitutionCallMtd;
	}

	public void setAchInstitutionCallPtd(String achInstitutionCallPtd){
		this.achInstitutionCallPtd = achInstitutionCallPtd;
	}

	public String getAchInstitutionCallPtd(){
		return achInstitutionCallPtd;
	}

	public void setMsOoredoo(String msOoredoo){
		this.msOoredoo = msOoredoo;
	}

	public String getMsOoredoo(){
		return msOoredoo;
	}

	public SummaryArea getActualCallPoi(){
		SummaryArea summaryArea = new SummaryArea();
		summaryArea.setTitle(getTitle());
		summaryArea.setAchMtd(getAchInstitutionCallMtd());
		summaryArea.setAchPtd(getAchInstitutionCallPtd());
		summaryArea.setActualPtd(getActualInstitutionCallPtd());
		summaryArea.setActualMtd(getActualInstitutionCallMtd());
		summaryArea.setPlanPtd(getPlanInstitutionCallPtd());
		summaryArea.setPlanMtd(getPlanInstitutionCallMtd());
		return summaryArea;
	}

	public SummaryArea getEffectiveCallPoi(){
		SummaryArea summaryArea = new SummaryArea();
		summaryArea.setTitle(getTitle());
		summaryArea.setAchMtd(getAchEffectiveInstitutionCallMtd());
		summaryArea.setAchPtd(getAchEffectiveInstitutionCallPtd());
		summaryArea.setActualPtd(getEffectiveInstitutionCallPtd());
		summaryArea.setActualMtd(getEffectiveInstitutionCallMtd());
		summaryArea.setPlanPtd(getPlanEffectivePoiCallPtd());
		summaryArea.setPlanMtd(getPlanEffectivePoiCallMtd());
		return summaryArea;
	}


	@Override
	public String toString() {
		return "SummaryInstitution{" +
				"area='" + area + '\'' +
				", achInstitutionSurvey='" + achInstitutionSurvey + '\'' +
				", regional='" + regional + '\'' +
				", actualInstitutionCallPtd='" + actualInstitutionCallPtd + '\'' +
				", effectiveInstitutionCallPtd='" + effectiveInstitutionCallPtd + '\'' +
				", effectiveInstitutionCallMtd='" + effectiveInstitutionCallMtd + '\'' +
				", ms3='" + ms3 + '\'' +
				", actualInstitutionCallMtd='" + actualInstitutionCallMtd + '\'' +
				", planInstitutionCallPtd='" + planInstitutionCallPtd + '\'' +
				", msOthers='" + msOthers + '\'' +
				", branch='" + branch + '\'' +
				", planInstitutionCallMtd='" + planInstitutionCallMtd + '\'' +
				", actualInstitutionSurvey='" + actualInstitutionSurvey + '\'' +
				", planInstitutionSurvey='" + planInstitutionSurvey + '\'' +
				", msTelkomsel='" + msTelkomsel + '\'' +
				", msAxis='" + msAxis + '\'' +
				", msXl='" + msXl + '\'' +
				", planEffectivePoiCallMtd='" + planEffectivePoiCallMtd + '\'' +
				", PlanEffectivePoiCallPtd='" + PlanEffectivePoiCallPtd + '\'' +
				", achInstitutionCallMtd='" + achInstitutionCallMtd + '\'' +
				", achEffectiveInstitutionCallPtd='" + achEffectiveInstitutionCallPtd + '\'' +
				", achEffectiveInstitutionCallMtd='" + achEffectiveInstitutionCallMtd + '\'' +
				", achInstitutionCallPtd='" + achInstitutionCallPtd + '\'' +
				", msOoredoo='" + msOoredoo + '\'' +
				", cluster='" + cluster + '\'' +
				", city='" + city + '\'' +
				", aoc='" + aoc + '\'' +
				'}';
	}

	public String getTitle(){
		if (aoc != null)
			return city + "\t\t" + "( " + aoc + " )";
		else if (city != null )
			return city;
		else if (cluster != null )
			return cluster;
		else if (branch != null)
			return branch;
		else return regional;
	}

}