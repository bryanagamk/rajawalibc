package com.androiddeveloper.pens.namiapp.data.network.model;

import javax.annotation.Generated;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@Generated("com.robohorse.robopojogenerator")
public class SummaryInstitutionResponse{

	@SerializedName("success")
	@Expose
	private boolean success;

	@SerializedName("message")
	@Expose
	private String message;

	@SerializedName("summary_institution")
	@Expose
	private List<SummaryInstitution> summaryInstitution;

	public void setSuccess(boolean success){
		this.success = success;
	}

	public boolean isSuccess(){
		return success;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public List<SummaryInstitution> getSummaryInstitution() {
		return summaryInstitution;
	}

	public void setSummaryInstitution(List<SummaryInstitution> summaryInstitution) {
		this.summaryInstitution = summaryInstitution;
	}

	@Override
 	public String toString(){
		return 
			"SummaryInstitutionResponse{" + 
			"success = '" + success + '\'' + 
			",message = '" + message + '\'' + 
			",summary_institution = '" + summaryInstitution + '\'' + 
			"}";
		}
}