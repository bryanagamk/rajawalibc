package com.androiddeveloper.pens.namiapp.data.network.model;

import javax.annotation.Generated;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class SummaryOsk{

	@SerializedName("actual_osk_call_mtd")
	@Expose
	private String actualOskCallMtd;

	@SerializedName("bs_telkomsel")
	@Expose
	private String bsTelkomsel;

	@SerializedName("rs_3")
	@Expose
	private String rs3;

	@SerializedName("ds_axis")
	@Expose
	private String dsAxis;

	@SerializedName("regional")
	@Expose
	private String regional;

	@SerializedName("bs_axis")
	@Expose
	private String bsAxis;

	@SerializedName("branch")
	@Expose
	private String branch;

	@SerializedName("cluster")
	@Expose
	private String cluster;

	@SerializedName("city")
	@Expose
	private String city;

	@SerializedName("user_name")
	@Expose
	private String aoc;

	@SerializedName("actual_osk_call_ptd")
	@Expose
	private String actualOskCallPtd;

	@SerializedName("rs_xl")
	@Expose
	private String rsXl;

	@SerializedName("ds_ooredoo")
	@Expose
	private String dsOoredoo;

	@SerializedName("ds_xl")
	@Expose
	private String dsXl;

	@SerializedName("bs_others")
	@Expose
	private String bsOthers;

	@SerializedName("ds_others")
	@Expose
	private String dsOthers;

	@SerializedName("ach_osk_productive")
	@Expose
	private String achOskProductive;

	@SerializedName("ds_telkomsel")
	@Expose
	private String dsTelkomsel;

	@SerializedName("area")
	@Expose
	private String area;

	@SerializedName("ach_osk_call_mtd")
	@Expose
	private String achOskCallMtd;

	@SerializedName("rs_ooredoo")
	@Expose
	private String rsOoredoo;

	@SerializedName("bs_3")
	@Expose
	private String bs3;

	@SerializedName("ds_3")
	@Expose
	private String ds3;

	@SerializedName("rs_others")
	@Expose
	private String rsOthers;

	@SerializedName("plan_osk_call_ptd")
	@Expose
	private String planOskCallPtd;

	@SerializedName("actual_osk_productive")
	@Expose
	private String actualOskProductive;

	@SerializedName("effective_osk_call_mtd")
	@Expose
	private String effectiveOskCallMtd;

	@SerializedName("effective_osk_call_ptd")
	@Expose
	private String effectiveOskCallPtd;

	@SerializedName("rs_axis")
	@Expose
	private String rsAxis;

	@SerializedName("plan_osk_call_mtd")
	@Expose
	private String planOskCallMtd;

	@SerializedName("rs_telkomsel")
	@Expose
	private String rsTelkomsel;

	@SerializedName("bs_xl")
	@Expose
	private String bsXl;

	@SerializedName("plan_osk_productive")
	@Expose
	private String planOskProductive;

	@SerializedName("ach_osk_call_ptd")
	@Expose
	private String achOskCallPtd;

	@SerializedName("visit_osk_mtd")
	@Expose
	private String planEffectiveOskCallMtd;

	@SerializedName("visit_osk_ptd")
	@Expose
	private String PlanEffectiveOskCallPtd;

	@SerializedName("ach_effective_osk_call_mtd")
	@Expose
	private String achEffectiveOskCallMtd;

	@SerializedName("ach_effective_osk_call_ptd")
	@Expose
	private String achEffectiveOskCallPtd;

	@SerializedName("bs_ooredoo")
	@Expose
	private String bsOoredoo;

	public String getPlanEffectiveOskCallMtd() {
		return planEffectiveOskCallMtd;
	}

	public void setPlanEffectiveOskCallMtd(String planEffectiveOskCallMtd) {
		this.planEffectiveOskCallMtd = planEffectiveOskCallMtd;
	}

	public String getPlanEffectiveOskCallPtd() {
		return PlanEffectiveOskCallPtd;
	}

	public void setPlanEffectiveOskCallPtd(String planEffectiveOskCallPtd) {
		PlanEffectiveOskCallPtd = planEffectiveOskCallPtd;
	}

	public void setActualOskCallMtd(String actualOskCallMtd){
		this.actualOskCallMtd = actualOskCallMtd;
	}

	public String getActualOskCallMtd(){
		return actualOskCallMtd;
	}

	public void setBsTelkomsel(String bsTelkomsel){
		this.bsTelkomsel = bsTelkomsel;
	}

	public String getBsTelkomsel(){
		return bsTelkomsel;
	}

	public void setRs3(String rs3){
		this.rs3 = rs3;
	}

	public String getRs3(){
		return rs3;
	}

	public void setDsAxis(String dsAxis){
		this.dsAxis = dsAxis;
	}

	public String getDsAxis(){
		return dsAxis;
	}

	public void setRegional(String regional){
		this.regional = regional;
	}

	public String getRegional(){
		return regional;
	}

	public void setBsAxis(String bsAxis){
		this.bsAxis = bsAxis;
	}

	public String getBsAxis(){
		return bsAxis;
	}

	public void setBranch(String branch){
		this.branch = branch;
	}

	public String getBranch(){
		return branch;
	}

	public void setActualOskCallPtd(String actualOskCallPtd){
		this.actualOskCallPtd = actualOskCallPtd;
	}

	public String getActualOskCallPtd(){
		return actualOskCallPtd;
	}

	public void setRsXl(String rsXl){
		this.rsXl = rsXl;
	}

	public String getRsXl(){
		return rsXl;
	}

	public void setDsOoredoo(String dsOoredoo){
		this.dsOoredoo = dsOoredoo;
	}

	public String getDsOoredoo(){
		return dsOoredoo;
	}

	public void setDsXl(String dsXl){
		this.dsXl = dsXl;
	}

	public String getDsXl(){
		return dsXl;
	}

	public void setBsOthers(String bsOthers){
		this.bsOthers = bsOthers;
	}

	public String getBsOthers(){
		return bsOthers;
	}

	public void setDsOthers(String dsOthers){
		this.dsOthers = dsOthers;
	}

	public String getDsOthers(){
		return dsOthers;
	}

	public void setAchOskProductive(String achOskProductive){
		this.achOskProductive = achOskProductive;
	}

	public String getAchOskProductive(){
		return achOskProductive;
	}

	public void setDsTelkomsel(String dsTelkomsel){
		this.dsTelkomsel = dsTelkomsel;
	}

	public String getDsTelkomsel(){
		return dsTelkomsel;
	}

	public void setArea(String area){
		this.area = area;
	}

	public String getArea(){
		return area;
	}

	public void setAchOskCallMtd(String achOskCallMtd){
		this.achOskCallMtd = achOskCallMtd;
	}

	public String getAchOskCallMtd(){
		return achOskCallMtd;
	}

	public void setRsOoredoo(String rsOoredoo){
		this.rsOoredoo = rsOoredoo;
	}

	public String getRsOoredoo(){
		return rsOoredoo;
	}

	public void setBs3(String bs3){
		this.bs3 = bs3;
	}

	public String getBs3(){
		return bs3;
	}

	public void setDs3(String ds3){
		this.ds3 = ds3;
	}

	public String getDs3(){
		return ds3;
	}

	public void setRsOthers(String rsOthers){
		this.rsOthers = rsOthers;
	}

	public String getRsOthers(){
		return rsOthers;
	}

	public void setPlanOskCallPtd(String planOskCallPtd){
		this.planOskCallPtd = planOskCallPtd;
	}

	public String getPlanOskCallPtd(){
		return planOskCallPtd;
	}

	public void setActualOskProductive(String actualOskProductive){
		this.actualOskProductive = actualOskProductive;
	}

	public String getActualOskProductive(){
		return actualOskProductive;
	}

	public void setRsAxis(String rsAxis){
		this.rsAxis = rsAxis;
	}

	public String getRsAxis(){
		return rsAxis;
	}

	public void setPlanOskCallMtd(String planOskCallMtd){
		this.planOskCallMtd = planOskCallMtd;
	}

	public String getPlanOskCallMtd(){
		return planOskCallMtd;
	}

	public void setRsTelkomsel(String rsTelkomsel){
		this.rsTelkomsel = rsTelkomsel;
	}

	public String getRsTelkomsel(){
		return rsTelkomsel;
	}

	public void setBsXl(String bsXl){
		this.bsXl = bsXl;
	}

	public String getBsXl(){
		return bsXl;
	}

	public void setPlanOskProductive(String planOskProductive){
		this.planOskProductive = planOskProductive;
	}

	public String getPlanOskProductive(){
		return planOskProductive;
	}

	public void setAchOskCallPtd(String achOskCallPtd){
		this.achOskCallPtd = achOskCallPtd;
	}

	public String getAchOskCallPtd(){
		return achOskCallPtd;
	}

	public void setBsOoredoo(String bsOoredoo){
		this.bsOoredoo = bsOoredoo;
	}

	public String getBsOoredoo(){
		return bsOoredoo;
	}

	@Override
 	public String toString(){
		return 
			"SummaryOsk{" + 
			"actual_osk_call_mtd = '" + actualOskCallMtd + '\'' + 
			",bs_telkomsel = '" + bsTelkomsel + '\'' + 
			",rs_3 = '" + rs3 + '\'' + 
			",ds_axis = '" + dsAxis + '\'' + 
			",regional = '" + regional + '\'' + 
			",bs_axis = '" + bsAxis + '\'' + 
			",branch = '" + branch + '\'' + 
			",actual_osk_call_ptd = '" + actualOskCallPtd + '\'' + 
			",rs_xl = '" + rsXl + '\'' + 
			",ds_ooredoo = '" + dsOoredoo + '\'' + 
			",ds_xl = '" + dsXl + '\'' + 
			",bs_others = '" + bsOthers + '\'' + 
			",ds_others = '" + dsOthers + '\'' + 
			",ach_osk_productive = '" + achOskProductive + '\'' + 
			",ds_telkomsel = '" + dsTelkomsel + '\'' + 
			",area = '" + area + '\'' + 
			",ach_osk_call_mtd = '" + achOskCallMtd + '\'' + 
			",rs_ooredoo = '" + rsOoredoo + '\'' + 
			",bs_3 = '" + bs3 + '\'' + 
			",ds_3 = '" + ds3 + '\'' + 
			",rs_others = '" + rsOthers + '\'' + 
			",plan_osk_call_ptd = '" + planOskCallPtd + '\'' + 
			",actual_osk_productive = '" + actualOskProductive + '\'' + 
			",rs_axis = '" + rsAxis + '\'' + 
			",plan_osk_call_mtd = '" + planOskCallMtd + '\'' + 
			",rs_telkomsel = '" + rsTelkomsel + '\'' + 
			",bs_xl = '" + bsXl + '\'' + 
			",plan_osk_productive = '" + planOskProductive + '\'' + 
			",ach_osk_call_ptd = '" + achOskCallPtd + '\'' + 
			",bs_ooredoo = '" + bsOoredoo + '\'' + 
			"}";
		}

	public String getEffectiveOskCallMtd() {
		return effectiveOskCallMtd;
	}

	public void setEffectiveOskCallMtd(String effectiveOskCallMtd) {
		this.effectiveOskCallMtd = effectiveOskCallMtd;
	}

	public String getEffectiveOskCallPtd() {
		return effectiveOskCallPtd;
	}

	public void setEffectiveOskCallPtd(String effectiveOskCallPtd) {
		this.effectiveOskCallPtd = effectiveOskCallPtd;
	}

	public String getAchEffectiveOskCallMtd() {
		return achEffectiveOskCallMtd;
	}

	public void setAchEffectiveOskCallMtd(String achEffectiveOskCallMtd) {
		this.achEffectiveOskCallMtd = achEffectiveOskCallMtd;
	}

	public String getAchEffectiveOskCallPtd() {
		return achEffectiveOskCallPtd;
	}

	public void setAchEffectiveOskCallPtd(String achEffectiveOskCallPtd) {
		this.achEffectiveOskCallPtd = achEffectiveOskCallPtd;
	}

	public SummaryArea getActualCallOsk(){
		SummaryArea summaryArea = new SummaryArea();
		summaryArea.setAchMtd(getAchOskCallMtd());
		summaryArea.setAchPtd(getAchOskCallPtd());
		summaryArea.setActualPtd(getActualOskCallPtd());
		summaryArea.setActualMtd(getActualOskCallMtd());
		summaryArea.setPlanPtd(getPlanOskCallPtd());
		summaryArea.setPlanMtd(getPlanOskCallMtd());
		summaryArea.setTitle(getTitle());
		return summaryArea;
	}

	public SummaryArea getEffectiveCallOsk(){
		SummaryArea summaryArea = new SummaryArea();
		summaryArea.setAchMtd(getAchOskCallMtd());
		summaryArea.setAchPtd(getAchOskCallPtd());
		summaryArea.setActualPtd(getEffectiveOskCallPtd());
		summaryArea.setActualMtd(getEffectiveOskCallMtd());
		summaryArea.setPlanPtd(getPlanEffectiveOskCallPtd());
		summaryArea.setPlanMtd(getPlanEffectiveOskCallMtd());
		summaryArea.setTitle(getTitle());
		return summaryArea;
	}

	public String getTitle(){
		if (aoc != null)
			return city + "\t\t" + "( " + aoc + " )";
		else if (city != null )
			return city;
		else if (cluster != null )
			return cluster;
		else if (branch != null)
			return branch;
		else return regional;
	}
}