package com.androiddeveloper.pens.namiapp.data.network.model;

import javax.annotation.Generated;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@Generated("com.robohorse.robopojogenerator")
public class SummaryOskResponse{

	@SerializedName("summary_osk")
	@Expose
	private List<SummaryOsk> summaryOsk;

	@SerializedName("success")
	@Expose
	private boolean success;

	@SerializedName("message")
	@Expose
	private String message;

	public List<SummaryOsk> getSummaryOsk() {
		return summaryOsk;
	}

	public void setSummaryOsk(List<SummaryOsk> summaryOsk) {
		this.summaryOsk = summaryOsk;
	}

	public void setSuccess(boolean success){
		this.success = success;
	}

	public boolean isSuccess(){
		return success;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	@Override
 	public String toString(){
		return 
			"SummaryOskResponse{" + 
			"summary_osk = '" + summaryOsk + '\'' + 
			",success = '" + success + '\'' + 
			",message = '" + message + '\'' + 
			"}";
		}
}