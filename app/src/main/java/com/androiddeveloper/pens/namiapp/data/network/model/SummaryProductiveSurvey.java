package com.androiddeveloper.pens.namiapp.data.network.model;

/**
 * Created by miftahun on 9/2/18.
 */

public class SummaryProductiveSurvey {
    private String achOskProductive;

    private String achMarketSurvey;

    private String actualOskProductive;

    private String planOskProductive;

    private String actualMarketSurvey;

    private String planMarketSurvey;

    private String title;


    public SummaryProductiveSurvey() {
    }

    public String getAchOskProductive() {
        return achOskProductive;
    }

    public void setAchOskProductive(String achOskProductive) {
        this.achOskProductive = achOskProductive;
    }

    public String getAchMarketSurvey() {
        return achMarketSurvey;
    }

    public void setAchMarketSurvey(String achMarketSurvey) {
        this.achMarketSurvey = achMarketSurvey;
    }

    public String getActualOskProductive() {
        return actualOskProductive;
    }

    public void setActualOskProductive(String actualOskProductive) {
        this.actualOskProductive = actualOskProductive;
    }

    public String getPlanOskProductive() {
        return planOskProductive;
    }

    public void setPlanOskProductive(String planOskProductive) {
        this.planOskProductive = planOskProductive;
    }

    public String getActualMarketSurvey() {
        return actualMarketSurvey;
    }

    public void setActualMarketSurvey(String actualMarketSurvey) {
        this.actualMarketSurvey = actualMarketSurvey;
    }

    public String getPlanMarketSurvey() {
        return planMarketSurvey;
    }

    public void setPlanMarketSurvey(String planMarketSurvey) {
        this.planMarketSurvey = planMarketSurvey;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
