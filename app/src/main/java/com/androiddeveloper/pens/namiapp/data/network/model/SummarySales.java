package com.androiddeveloper.pens.namiapp.data.network.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class SummarySales{

	@SerializedName("area")
	private String area;

	@SerializedName("cluster")
	private String cluster;

	@SerializedName("regional")
	private String regional;

	@SerializedName("city")
	private String city;

	@SerializedName("user_name")
	private String aoc;

	@SerializedName("actual_event_sales_mtd")
	private String actualEventSalesMtd;

	@SerializedName("branch")
	private String branch;

	@SerializedName("actual_event_sales_ptd")
	private String actualEventSalesPtd;

	@SerializedName("actual_osk_sales_ptd")
	private String actualOskSalesPtd;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("actual_osk_sales_mtd")
	private String actualOskSalesMtd;

	@SerializedName("actual_institution_sales_mtd")
	private String actualInstitutionSalesMtd;

	@SerializedName("actual_institution_sales_ptd")
	private String actualInstitutionSalesPtd;

	public void setArea(String area){
		this.area = area;
	}

	public String getArea(){
		return area;
	}

	public void setCluster(String cluster){
		this.cluster = cluster;
	}

	public String getCluster(){
		return cluster;
	}

	public void setRegional(String regional){
		this.regional = regional;
	}

	public String getRegional(){
		return regional;
	}

	public void setCity(String city){
		this.city = city;
	}

	public String getCity(){
		return city;
	}

	public void setAoc(String aoc){
		this.aoc = aoc;
	}

	public String getAoc(){
		return aoc;
	}

	public void setActualEventSalesMtd(String actualEventSalesMtd){
		this.actualEventSalesMtd = actualEventSalesMtd;
	}

	public String getActualEventSalesMtd(){
		return actualEventSalesMtd;
	}

	public void setBranch(String branch){
		this.branch = branch;
	}

	public String getBranch(){
		return branch;
	}

	public void setActualEventSalesPtd(String actualEventSalesPtd){
		this.actualEventSalesPtd = actualEventSalesPtd;
	}

	public String getActualEventSalesPtd(){
		return actualEventSalesPtd;
	}

	public void setActualOskSalesPtd(String actualOskSalesPtd){
		this.actualOskSalesPtd = actualOskSalesPtd;
	}

	public String getActualOskSalesPtd(){
		return actualOskSalesPtd;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setActualOskSalesMtd(String actualOskSalesMtd){
		this.actualOskSalesMtd = actualOskSalesMtd;
	}

	public String getActualOskSalesMtd(){
		return actualOskSalesMtd;
	}

	public void setActualInstitutionSalesMtd(String actualInstitutionSalesMtd){
		this.actualInstitutionSalesMtd = actualInstitutionSalesMtd;
	}

	public String getActualInstitutionSalesMtd(){
		return actualInstitutionSalesMtd;
	}

	public void setActualInstitutionSalesPtd(String actualInstitutionSalesPtd){
		this.actualInstitutionSalesPtd = actualInstitutionSalesPtd;
	}

	public String getActualInstitutionSalesPtd(){
		return actualInstitutionSalesPtd;
	}

	@Override
 	public String toString(){
		return 
			"SummarySales{" + 
			"area = '" + area + '\'' + 
			",cluster = '" + cluster + '\'' + 
			",regional = '" + regional + '\'' + 
			",city = '" + city + '\'' + 
			",user_name = '" + aoc + '\'' +
			",actual_event_sales_mtd = '" + actualEventSalesMtd + '\'' + 
			",branch = '" + branch + '\'' + 
			",actual_event_sales_ptd = '" + actualEventSalesPtd + '\'' + 
			",actual_osk_sales_ptd = '" + actualOskSalesPtd + '\'' + 
			",user_id = '" + userId + '\'' + 
			",actual_osk_sales_mtd = '" + actualOskSalesMtd + '\'' + 
			",actual_institution_sales_mtd = '" + actualInstitutionSalesMtd + '\'' + 
			",actual_institution_sales_ptd = '" + actualInstitutionSalesPtd + '\'' + 
			"}";
		}

	public SummaryArea getSalesPerformace(){
		SummaryArea summaryArea = new SummaryArea();
		summaryArea.setTitle(getTitle());
		summaryArea.setSalesInstitutionPtd(actualInstitutionSalesPtd);
		summaryArea.setSalesInstitutionMtd(actualInstitutionSalesMtd);
		summaryArea.setSalesOskMtd(actualOskSalesMtd);
		summaryArea.setSalesOskPtd(actualOskSalesPtd);
		summaryArea.setSalesEventMtd(actualEventSalesMtd);
		summaryArea.setSalesEventPtd(actualEventSalesPtd);
		 return summaryArea;
	}

	public String getTitle(){
		if (aoc != null)
			return city + "\t\t" + "( " + aoc + " )";
		else if (city != null )
			return city;
		else if (cluster != null )
			return cluster;
		else if (branch != null)
			return branch;
		else return regional;
	}
}