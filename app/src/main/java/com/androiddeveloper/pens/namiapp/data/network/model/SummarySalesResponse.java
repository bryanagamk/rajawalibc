package com.androiddeveloper.pens.namiapp.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by miftahun on 8/29/18.
 */

public class SummarySalesResponse {
    @SerializedName("summary_sales")
    @Expose
    private List<SummarySales> summarySales;

    @SerializedName("success")
    @Expose
    private boolean success;

    @SerializedName("message")
    @Expose
    private String message;

    public List<SummarySales> getSummarySales() {
        return summarySales;
    }

    public void setSummarySales(List<SummarySales> summarySales) {
        this.summarySales = summarySales;
    }

    public void setSuccess(boolean success){
        this.success = success;
    }

    public boolean isSuccess(){
        return success;
    }

    public void setMessage(String message){
        this.message = message;
    }

    public String getMessage(){
        return message;
    }


}
