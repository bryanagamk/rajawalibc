package com.androiddeveloper.pens.namiapp.data.network.rest;

import android.util.Log;

import com.androidnetworking.interceptors.HttpLoggingInterceptor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.sql.Time;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.crashlytics.android.Crashlytics.TAG;

/**
 * Created by miftahun on 7/11/18.
 * <p>
 * Author Miftahun Najat
 * Email miftahunajat@gmail.com
 * Github https://github.com/miftahunajat
 */

public class ApiClient {

    public static final String BASE_URL_NAMI = "http://163.53.193.92/nami4.0prod/";
    public static final String BASE_URL_RAJAWALI = "http://163.53.193.92/rajawaliapp/";
    public static final String BASE_URL_NAMI_IMAGE = "http://163.53.193.92/nami2018/";
    private static Retrofit retrofit;

    public Retrofit getClient() {
        OkHttpClient.Builder httpclient = new OkHttpClient.Builder();
        httpclient.connectTimeout(60*15
                ,TimeUnit.SECONDS);
        httpclient.readTimeout(60*15,TimeUnit.SECONDS);
        httpclient.writeTimeout(60*15, TimeUnit.SECONDS);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpclient.interceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                return onOnIntercept(chain);
            }
        });


        if (retrofit == null) {
            Gson gson = new GsonBuilder()
                    .setDateFormat("yyyy-MM-dd")
                    .setLenient()
                    .excludeFieldsWithoutExposeAnnotation()
                    .create();
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL_NAMI)
                    .client(httpclient.build())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
        }
        return retrofit;
    }

    private Response onOnIntercept(Interceptor.Chain chain) throws IOException {
        try {
            Response response = chain.proceed(chain.request());
            try {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                    JSONArray obj=new JSONArray(response);
                    String content = obj.toString();
                    Log.d(TAG, "OBJECT" + " - " + content);
                    return response.newBuilder().body(ResponseBody.create(response.body().contentType(), content)).build();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }catch (ConnectTimeoutException exception) {
            exception.printStackTrace();
        }catch (SocketTimeoutException e){
            e.printStackTrace();
        }

        return chain.proceed(chain.request());
    }



}
