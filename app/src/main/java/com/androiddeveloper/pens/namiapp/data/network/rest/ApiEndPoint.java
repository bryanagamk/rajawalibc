/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.androiddeveloper.pens.namiapp.data.network.rest;


import com.androiddeveloper.pens.namiapp.BuildConfig;

/**
 * Created by amitshekhar on 01/02/17.
 */

public final class ApiEndPoint {


    //SUMMARY
    public static final String ENDPOINT_SUMMARY_PERFORMANCE_POI_OSK = ApiClient.BASE_URL_NAMI
            + "sp_performance_poi_osk.php";

    public static final String ENDPOINT_SUMMARY_PERFORMANCE_EVENT = ApiClient.BASE_URL_NAMI
            + "sp_performance_event.php";

    public static final String ENDPOINT_SUMMARY_PERFORMANCE_BACKCHECKING = ApiClient.BASE_URL_NAMI
            + "sp_performance_backchecking.php";

    public static final String ENDPOINT_SUMMARY_PERFORMANCE_SHARE = ApiClient.BASE_URL_NAMI
            + "sp_performance_share.php";

    public static final String ENDPOINT_SUMMARY_OSK = ApiClient.BASE_URL_NAMI
            + "sp_performance_osk.php";

    public static final String ENDPOINT_SUMMARY_INSTITUTION = ApiClient.BASE_URL_NAMI
            + "sp_performance_institution.php";

    public static final String ENDPOINT_SUMMARY_EVENT = ApiClient.BASE_URL_NAMI
            + "sp_performance_event.php";

    public static final String ENDPOINT_SUMMARY_SALES = ApiClient.BASE_URL_NAMI +
            "sp_performance_sales.php";

    public static final String ENDPOINT_SP_CONFIG_STARTUP = ApiClient.BASE_URL_NAMI
            + "sp_config_startup.php";

    public static final String ENDPOINT_SP_CONFIG_STARTUP_RAJAWALI = ApiClient.BASE_URL_RAJAWALI
            + "sp_config_startup.php";

    public static final String ENDPOINT_SP_CONFIG_PERMIT= ApiClient.BASE_URL_NAMI
            + "sp_config_permit.php";

    public static final String ENDPOINT_SP_USER_LOGIN = ApiClient.BASE_URL_NAMI
            + "sp_user_login.php";

    public static final String ENDPOINT_SP_RAJAWALI_LOGIN = ApiClient.BASE_URL_RAJAWALI
            + "sp_user_login.php";

    public static final String ENDPOINT_SP_USER_LOGOUT = ApiClient.BASE_URL_NAMI
            + "sp_user_logout.php";

    public static final String ENDPOINT_SP_USER_LOGOUT_RAJAWALI = ApiClient.BASE_URL_RAJAWALI
            + "sp_user_logout.php";

    public static final String ENDPOINT_SP_RAJAWALI_BACKCHECKING = ApiClient.BASE_URL_RAJAWALI
            + "sp_backchecking_location.php";

    public static final String ENDPOINT_SP_LEADER_BACKCHECKING_LOCATION = ApiClient.BASE_URL_NAMI
            + "sp_leader_backchecking_location.php";

    public static final String ENDPOINT_SP_LEADER_CHECKIN_INSERT = ApiClient.BASE_URL_NAMI
            + "sp_leader_checkin_insert.php";

    public static final String ENDPOINT_SP_LEADER_CHECKOUT_INSERT = ApiClient.BASE_URL_NAMI
            + "sp_leader_checkout_insert.php";

    public static final String ENDPOINT_SP_LEADER_CHECKOUT_STATUS = ApiClient.BASE_URL_NAMI
            + "sp_leader_checkout_status.php";

    public static final String ENDPOINT_SP_BACKCHECKING_INSTITUTION_TOTAL_STUDENT = ApiClient.BASE_URL_NAMI
            + "sp_backchecking_institution_total_student.php";

    public static final String ENDPOINT_SP_BACKCHECKING_INSTITUTION_READ_VISIT = ApiClient.BASE_URL_NAMI
            + "sp_backchecking_institution_read_visit.php";

    public static final String ENDPOINT_SP_BACKCHECKING_INSTITUTION_READ_BRANDING= ApiClient.BASE_URL_NAMI
            + "sp_backchecking_institution_read_branding.php";

    public static final String ENDPOINT_SP_BACKCHECKING_INSTITUTION_READ_SALES_THROUGH = ApiClient.BASE_URL_NAMI
            + "sp_backchecking_institution_read_sales_through.php";

    public static final String ENDPOINT_SP_BACKCHECKING_INSTITUTION_MERCHANDISING = ApiClient.BASE_URL_NAMI +
            "sp_backchecking_institution_read_merchandising.php";

    public static final String ENDPOINT_SP_BACKCHECKING_INSTITUTION_READ_MS = ApiClient.BASE_URL_NAMI +
            "sp_backchecking_institution_read_ms.php";

    public static final String ENDPOINT_SP_BACKCHECKING_INSTITUTION_INSERT = ApiClient.BASE_URL_NAMI +
            "sp_leader_backchecking_institution_insert.php";

    public static final String ENDPOINT_SP_BACKCHECKING_INSTITUTION_READ_SIGNAL = ApiClient.BASE_URL_NAMI
            + "sp_backchecking_institution_read_signal.php";

    public static final String ENDPOINT_SP_BACKCHECKING_OSS_OSK_READ_BS_DS_RS = ApiClient.BASE_URL_NAMI +
            "sp_backchecking_oss_osk_read_bs_ds_rs.php";

    public static final String ENDPOINT_SP_BACKCHECKING_OSS_OSK_READ_TOP_SELLING = ApiClient.BASE_URL_NAMI +
            "sp_backchecking_oss_osk_read_top_selling.php";

    public static final String ENDPOINT_SP_BACKCHECKING_OSS_OSK_READ_SIGNAL = ApiClient.BASE_URL_NAMI
            + "sp_backchecking_oss_osk_read_signal.php";

    public static final String ENDPOINT_SP_BACKCHECKING_OSS_OSK_INSERT = ApiClient.BASE_URL_NAMI +
            "sp_leader_backchecking_oss_osk_insert.php";

    public static final String ENDPOINT_SP_BACKCHECKING_STATUS = ApiClient.BASE_URL_NAMI +
            "sp_backchecking_status.php";

    public static final String ENDPOINT_SP_BACKCHECKING_MAIN = ApiClient.BASE_URL_NAMI +
            "sp_backchecking_main.php";

    public static final String ENDPOINT_SP_CONFIG_BRANDING = ApiClient.BASE_URL_NAMI +
            "sp_config_branding.php";

    public static final String ENDPOINT_SP_EVENT_LOCATION_READ = ApiClient.BASE_URL_NAMI +
            "sp_event_location_read.php";

    public static final String ENDPOINT_SP_BACKCHECKING_OSS_OSK_READ_VISIT = ApiClient.BASE_URL_NAMI +
            "sp_backchecking_oss_osk_read_visit.php";

    public static final String ENDPOINT_SP_BACKCHECKING_OSS_OSK_READ_BRANDING = ApiClient.BASE_URL_NAMI +
            "sp_backchecking_oss_osk_read_branding.php";

    public static final String ENDPOINT_SP_BACKCHECKING_OSS_OSK_READ_SALES_THROUGH = ApiClient.BASE_URL_NAMI +
            "sp_backchecking_oss_osk_read_sales_through.php";


    //Feedback
    public static final String ENDPOINT_SP_FEEDBACK_MAIN = ApiClient.BASE_URL_NAMI +
            "sp_feedback_main.php";

    public static final String ENDPOINT_SP_FEEDBACK_LOCATION = ApiClient.BASE_URL_NAMI +
            "sp_feedback_location.php";

    public static final String ENDPOINT_SP_FEEDBACK_LEADERNOTE = ApiClient.BASE_URL_NAMI +
            "sp_feedback_leader_note.php";

    public static final String ENDPOINT_SP_FEEDBACK_INSERT_INSTITUTION = ApiClient.BASE_URL_NAMI +
            "sp_feedback_institution_insert.php";

    public static final String ENDPOINT_SP_FEEDBACK_INSERT_OSS_OSK = ApiClient.BASE_URL_NAMI +
            "sp_feedback_oss_osk_insert.php";

    //UPload profile photo
    public static final String ENDPOINT_SP_USER_PROFILE = ApiClient.BASE_URL_NAMI +
            "sp_user_profile.php";


    // AOC
    public static final String ENDPOINT_SP_INSTITUTION_PROFILING_READ = ApiClient.BASE_URL_NAMI +
            "sp_institution_profiling_read.php";

}
