package com.androiddeveloper.pens.namiapp.data.network.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by miftahun on 8/8/18.
 */

public class FirebaseApiClient {
    public static final String BASE_URL_NAMI = "https://fcm.googleapis.com/fcm/";

    private static Retrofit retrofit;


    public static Retrofit getClient() {
        OkHttpClient.Builder httpclient = new OkHttpClient.Builder();

        if (retrofit == null) {
            Gson gson = new GsonBuilder()
                    .setDateFormat("yyyy-MM-dd")
                    .setLenient()
                    .excludeFieldsWithoutExposeAnnotation()
                    .create();
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL_NAMI)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(httpclient.build())
                    .build();
        }
        return retrofit;
    }
}
