package com.androiddeveloper.pens.namiapp.data.network.rest;

import com.androiddeveloper.pens.namiapp.data.network.model.LoginResponse;

import io.reactivex.Single;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by miftahun on 8/8/18.
 */

public interface FirebaseApiService {

    @FormUrlEncoded
    @POST("send")
    Single<LoginResponse> login(@Field("userid") String userId,
                                @Field("deviceid") String deviceid);
}
