

package com.androiddeveloper.pens.namiapp.data.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.data.network.model.BackCheckingLocationListResponse;
import com.androiddeveloper.pens.namiapp.di.ApplicationContext;
import com.androiddeveloper.pens.namiapp.di.PreferenceInfo;
import com.androiddeveloper.pens.namiapp.utils.AppConstants;
import com.google.gson.Gson;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by miftahun on 6/11/18.
 * <p>
 * Author Miftahun Najat
 * Email miftahunajat@gmail.com
 * Github https://github.com/miftahunajat
 */

@Singleton
public class AppPreferencesHelper implements PreferencesHelper {

    private static final String PREF_KEY_USER_LOGGED_IN_MODE = "PREF_KEY_USER_LOGGED_IN_MODE";
    private static final String PREF_KEY_CURRENT_USER_ID = "PREF_KEY_CURRENT_USER_ID";
    private static final String PREF_KEY_CURRENT_USER_NAME = "PREF_KEY_CURRENT_USER_NAME";
    private static final String PREF_KEY_CURRENT_USER_TYPE = "PREF_KEY_CURRENT_USER_TYPE";
    private static final String PREF_KEY_CURRENT_USER_DEVICE_ID = "PREF_KEY_CURRENT_USER_DEVICE_ID";
    private static final String PREF_KEY_ACCESS_TOKEN = "PREF_KEY_ACCESS_TOKEN";
    private static final String PREF_KEY_CURRENT_USER_TITLE = "PREF_KEY_USER_TITLE";
    private static final String PREF_KEY_CURRENT_USER_IMAGE = "PREF_KEY_USER_IMAGE";
    private static final String PREF_KEY_CURRENT_APPLICATION_TYPE = "PREF_KEY_APPLICATION_TYPE";

    private final SharedPreferences mPrefs;

    @Inject
    public AppPreferencesHelper(@ApplicationContext Context context,
                                @PreferenceInfo String prefFileName) {
        mPrefs = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
    }

    @Override
    public String getCurrentUserId() {
        String userId = mPrefs.getString(PREF_KEY_CURRENT_USER_ID, AppConstants.NULL_INDEX);
        return userId == AppConstants.NULL_INDEX ? null : userId;
    }

    @Override
    public void setCurrentUserId(String userId) {
        String id = userId == null ? AppConstants.NULL_INDEX : userId;
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_ID, id).apply();
    }


    @Override
    public int getCurrentUserLoggedInMode() {
        return mPrefs.getInt(PREF_KEY_USER_LOGGED_IN_MODE,
                DataManager.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT.getType());
    }

    @Override
    public void setCurrentUserLoggedInMode(DataManager.LoggedInMode mode) {
        mPrefs.edit().putInt(PREF_KEY_USER_LOGGED_IN_MODE, mode.getType()).apply();
    }

    @Override
    public String getAccessToken() {
        return mPrefs.getString(PREF_KEY_ACCESS_TOKEN, null);
    }

    @Override
    public void setAccessToken(String accessToken) {
        mPrefs.edit().putString(PREF_KEY_ACCESS_TOKEN, accessToken).apply();
    }

    @Override
    public String getCurrentUserName() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_NAME, null);
    }

    @Override
    public void setCurrentUserName(String userName) {
        String mUserName = userName == null ? AppConstants.NULL_INDEX : userName;
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_NAME, mUserName).apply();
    }

    @Override
    public String getCurrentUserType() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_TYPE, null);
    }

    @Override
    public void setCurrentUserType(String userType) {
        String mUserType = userType == null ? AppConstants.NULL_INDEX : userType;
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_TYPE, mUserType).apply();
    }

    @Override
    public String getCurrentDeviceId() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_DEVICE_ID, null);
    }

    @Override
    public void setCurrentDeviceId(String deviceId) {
        String mDeviceId = deviceId == null ? AppConstants.NULL_INDEX : deviceId;
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_DEVICE_ID, mDeviceId).apply();
    }

    @Override
    public void setCUrrentUserTitle(String currentUserTitle) {
        String mTitle = currentUserTitle == null ? AppConstants.NULL_INDEX : currentUserTitle;
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_TITLE, mTitle).apply();
    }

    @Override
    public String getCurrentUserTitle() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_TITLE, null);
    }

    @Override
    public void setCurrentUserPathImage(String pathImage) {
        String mPathImage = pathImage == null ? AppConstants.NULL_INDEX : pathImage;
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_IMAGE, mPathImage).apply();
    }

    @Override
    public String getCurrentUserPathImage() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_IMAGE, null);
    }

    @Override
    public void setCurrentApplicationType(String AppType) {
        String mAppType = AppType == null ? AppConstants.NULL_INDEX : AppType;
        mPrefs.edit().putString(PREF_KEY_CURRENT_APPLICATION_TYPE, mAppType).apply();
    }

    @Override
    public String getCurrentApplicationType() {
        return mPrefs.getString(PREF_KEY_CURRENT_APPLICATION_TYPE, null);
    }


}
