
package com.androiddeveloper.pens.namiapp.data.prefs;

import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.data.network.model.BackCheckingLocationListResponse;

/**
 * Created by miftahun on 6/11/18.
 * <p>
 * Author Miftahun Najat
 * Email miftahunajat@gmail.com
 * Github https://github.com/miftahunajat
 */

public interface PreferencesHelper {

    int getCurrentUserLoggedInMode();

    void setCurrentUserLoggedInMode(DataManager.LoggedInMode mode);

    String getCurrentUserId();

    void setCurrentUserId(String userId);

    String getAccessToken();

    void setAccessToken(String accessToken);

    String getCurrentUserName();

    void setCurrentUserName(String userName);

    String getCurrentUserType();

    void setCurrentUserType(String userType);

    String getCurrentDeviceId();

    void setCurrentDeviceId(String deviceId);

    void setCUrrentUserTitle(String currentUserTitle);

    String getCurrentUserTitle();

    void setCurrentUserPathImage(String pathImage);

    String getCurrentUserPathImage();

    void setCurrentApplicationType(String AppType);

    String getCurrentApplicationType();
}
