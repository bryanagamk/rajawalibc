package com.androiddeveloper.pens.namiapp.di.componen;

import com.androiddeveloper.pens.namiapp.di.PerActivity;
import com.androiddeveloper.pens.namiapp.di.module.ActivityModule;
import com.androiddeveloper.pens.namiapp.ui.aoc.eventaoc.EventAocActivity;
import com.androiddeveloper.pens.namiapp.ui.aoc.home.HomeActivity;
import com.androiddeveloper.pens.namiapp.ui.backcheckinglocation.BackCheckingLocationActivity;
import com.androiddeveloper.pens.namiapp.ui.backcheckinglocationlist.BackCheckingLocationListActivity;
import com.androiddeveloper.pens.namiapp.ui.backcheckingossosk.BackCheckingOssOskActivity;
import com.androiddeveloper.pens.namiapp.ui.backcheckingsekolahkampus.BackCheckingSekolahKampusActivity;
import com.androiddeveloper.pens.namiapp.ui.crashreport.CrashReportActivity;
import com.androiddeveloper.pens.namiapp.ui.dasboard.DasboardActivity;
import com.androiddeveloper.pens.namiapp.ui.dashboardbc.ActivityDashboardBC;
import com.androiddeveloper.pens.namiapp.ui.detailSooBc.DetailBackCheckActivity;
import com.androiddeveloper.pens.namiapp.ui.detailbackcheckinglocation.DetailBackCheckingLocationActivity;
import com.androiddeveloper.pens.namiapp.ui.feedbacklocation.FeedbackLocationActivity;
import com.androiddeveloper.pens.namiapp.ui.feedbackossosk.FeedbackOssOskActivity;
import com.androiddeveloper.pens.namiapp.ui.event.EventActivity;
import com.androiddeveloper.pens.namiapp.ui.faq.FaqActivity;
import com.androiddeveloper.pens.namiapp.ui.feddbacklist.FeedbackListActivity;
import com.androiddeveloper.pens.namiapp.ui.feedback.FeedbackActivity;
import com.androiddeveloper.pens.namiapp.ui.feedbackossoskinsert.FeedbackOssOskInsertActivity;
import com.androiddeveloper.pens.namiapp.ui.feedbacksekolahkampus.FeedbackSekolahKampusActivity;
import com.androiddeveloper.pens.namiapp.ui.feedbacksekolahkampusinsert.FeedbackSekolahKampusInsertActivity;
import com.androiddeveloper.pens.namiapp.ui.main.MainActivity;
import com.androiddeveloper.pens.namiapp.ui.notification.NotificationActivity;
import com.androiddeveloper.pens.namiapp.ui.aoc.ossosk.ossoskactualcall.OssOskActualCallActivity;
import com.androiddeveloper.pens.namiapp.ui.aoc.ossosk.ossoskmenu.OssOskMenuActivity;
import com.androiddeveloper.pens.namiapp.ui.aoc.ossosk.ossosksales.OssOskSalesActivity;
import com.androiddeveloper.pens.namiapp.ui.aoc.ossosk.ossoskzonamarking.OssOskZonaMarkingActivity;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusmenu.SekolahKampusMenuActivity;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofile.SekolahKampusProfileActivity;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofilekantin.SekolahKampusProfileKantinActivity;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofilemarketshare.SekolahKampusProfileMarketShareActivity;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofilemarketsharequickcount.SekolahKampusMarketShareQuickCountActivity;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofilemerchandising.SekolahKampusProfileMerchandisingActivity;
import com.androiddeveloper.pens.namiapp.ui.sharepercentage.ShareAreaActivity;
import com.androiddeveloper.pens.namiapp.ui.sharepercentage.fragmentshare.ShareAreaFragment;
import com.androiddeveloper.pens.namiapp.ui.soobackcheckinglocationlist.SooBackCheckingLocationListActivity;
import com.androiddeveloper.pens.namiapp.ui.summary.SummaryAreaActivity;
import com.androiddeveloper.pens.namiapp.ui.summary.fragment.AreaFragment;
import com.androiddeveloper.pens.namiapp.ui.performancepoiosklist.PerformancePoiOskListActivity;
import com.androiddeveloper.pens.namiapp.ui.summarybackchecking.SummaryBackcheckingActivity;
import com.androiddeveloper.pens.namiapp.ui.summarybackchecking.fragment.SummaryBackcheckingFragment;
import com.androiddeveloper.pens.namiapp.ui.verifiynumber.VerifyNumberActivity;

import dagger.Component;

/**
 * Created by miftahun on 6/11/18.
 * <p>
 * Author Miftahun Najat
 * Email miftahunajat@gmail.com
 * Github https://github.com/miftahunajat
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(MainActivity mainActivity);

    void inject(DasboardActivity dasboardActivity);

    void inject(ActivityDashboardBC activityDashboardBC);

    void inject(BackCheckingOssOskActivity backCheckingOssOskActivity);

    void inject(FeedbackActivity feedbackActivity);

    void inject(FeedbackListActivity feedbackListActivity);

    void inject(FeedbackOssOskActivity feedbackOssOskActivity);

    void inject(BackCheckingSekolahKampusActivity backCheckingSekolahKampusActivity);

    void inject(BackCheckingLocationActivity backCheckingLocationActivity);

    void inject(BackCheckingLocationListActivity backCheckingLocationListActivity);

    void inject(DetailBackCheckingLocationActivity detailBackCheckingLocationActivity);

    void inject(FaqActivity faqActivity);

    void inject(VerifyNumberActivity verifyNumberActivity);

    void inject(EventActivity eventActivity);

    void inject(NotificationActivity notificationActivity);

    void inject(SummaryAreaActivity summaryAreaActivity);

    void inject(FeedbackSekolahKampusActivity feedbackSekolahKampusActivity);

    void inject(AreaFragment areaFragment);

    void inject(FeedbackLocationActivity feedbackLocationActivity);

    void inject(FeedbackSekolahKampusInsertActivity feedbackSekolahKampusInsertActivity);

    void inject(FeedbackOssOskInsertActivity feedbackOssOskInsertActivity);

    void inject(SekolahKampusProfileActivity sekolahKampusProfileActivity);

    void inject(SekolahKampusMenuActivity sekolahKampusMenuActivity);

    void inject(SekolahKampusProfileKantinActivity sekolahKampusProfileKantinActivity);

    void inject(SekolahKampusProfileMarketShareActivity sekolahKampusProfileMarketShareActivity);

    void inject(SekolahKampusMarketShareQuickCountActivity sekolahKampusMarketShareQuickCountActivity);

    void inject(SekolahKampusProfileMerchandisingActivity sekolahKampusProfileMerchandisingActivity);

    void inject(OssOskActualCallActivity ossOskActualCallActivity);

    void inject(OssOskMenuActivity ossOskMenuActivity);

    void inject(OssOskSalesActivity ossOskSalesActivity);

    void inject(OssOskZonaMarkingActivity ossOskZonaMarkingActivity);

    void inject(ShareAreaActivity shareAreaActivity);

    void inject(ShareAreaFragment shareAreaFragment);

    void inject(SummaryBackcheckingFragment summaryBackcheckingFragment);

    void inject(SummaryBackcheckingActivity summaryBackcheckingActivity);

    void inject(PerformancePoiOskListActivity performancePoiOskListActivity);

    void inject(CrashReportActivity crashReportActivity);

    void inject(HomeActivity homeActivity);

    void inject(EventAocActivity eventAocActivity);

    void inject(SooBackCheckingLocationListActivity sooBackCheckingLocationListActivity);

    void inject(DetailBackCheckActivity detailBackCheckActivity);
}