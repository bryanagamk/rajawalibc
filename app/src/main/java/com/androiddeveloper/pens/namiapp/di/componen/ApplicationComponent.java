package com.androiddeveloper.pens.namiapp.di.componen;

import android.app.Application;
import android.content.Context;

import com.androiddeveloper.pens.namiapp.App;
import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.di.ApplicationContext;
import com.androiddeveloper.pens.namiapp.di.module.ApplicationModule;
import com.androiddeveloper.pens.namiapp.service.NotificationService;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by miftahun on 6/11/18.
 * <p>
 * Author Miftahun Najat
 * Email miftahunajat@gmail.com
 * Github https://github.com/miftahunajat
 */

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(App app);

    @ApplicationContext
    Context context();

    Application application();

    DataManager getDataManager();
}