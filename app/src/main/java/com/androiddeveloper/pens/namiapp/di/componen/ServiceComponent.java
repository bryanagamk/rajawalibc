package com.androiddeveloper.pens.namiapp.di.componen;

import com.androiddeveloper.pens.namiapp.di.PerService;
import com.androiddeveloper.pens.namiapp.di.module.ServiceModule;

import dagger.Component;

/**
 * Created by miftahun on 6/11/18.
 * <p>
 * Author Miftahun Najat
 * Email miftahunajat@gmail.com
 * Github https://github.com/miftahunajat
 */

@PerService
@Component(dependencies = ApplicationComponent.class, modules = ServiceModule.class)
public interface ServiceComponent {

}