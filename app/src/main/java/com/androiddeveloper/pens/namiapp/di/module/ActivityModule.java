package com.androiddeveloper.pens.namiapp.di.module;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;

import com.androiddeveloper.pens.namiapp.data.network.model.BackCheckingLocationListResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.FeedbackLocationResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.NotificationResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceSummary;
import com.androiddeveloper.pens.namiapp.data.network.model.SooBackCheckingLocationListResponse;
import com.androiddeveloper.pens.namiapp.di.ActivityContext;
import com.androiddeveloper.pens.namiapp.di.PerActivity;
import com.androiddeveloper.pens.namiapp.ui.aoc.eventaoc.EventAocMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.aoc.eventaoc.EventAocMvpView;
import com.androiddeveloper.pens.namiapp.ui.aoc.eventaoc.EventAocPresenter;
import com.androiddeveloper.pens.namiapp.ui.aoc.home.HomeMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.aoc.home.HomeMvpView;
import com.androiddeveloper.pens.namiapp.ui.aoc.home.HomePresenter;
import com.androiddeveloper.pens.namiapp.ui.backcheckinglocation.BackCheckingLocationMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.backcheckinglocation.BackCheckingLocationMvpView;
import com.androiddeveloper.pens.namiapp.ui.backcheckinglocation.BackCheckingLocationPresenter;
import com.androiddeveloper.pens.namiapp.ui.backcheckinglocationlist.BackCheckingLocationListAdapter;
import com.androiddeveloper.pens.namiapp.ui.backcheckinglocationlist.BackCheckingLocationListMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.backcheckinglocationlist.BackCheckingLocationListMvpView;
import com.androiddeveloper.pens.namiapp.ui.backcheckinglocationlist.BackCheckingLocationListPresenter;
import com.androiddeveloper.pens.namiapp.ui.backcheckingossosk.BackCheckingOssOskMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.backcheckingossosk.BackCheckingOssOskMvpView;
import com.androiddeveloper.pens.namiapp.ui.backcheckingossosk.BackCheckingOssOskPresenter;
import com.androiddeveloper.pens.namiapp.ui.backcheckingsekolahkampus.BackCheckingSekolahKampusMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.backcheckingsekolahkampus.BackCheckingSekolahKampusMvpView;
import com.androiddeveloper.pens.namiapp.ui.backcheckingsekolahkampus.BackCheckingSekolahKampusPresenter;
import com.androiddeveloper.pens.namiapp.ui.crashreport.CrashReportMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.crashreport.CrashReportMvpView;
import com.androiddeveloper.pens.namiapp.ui.crashreport.CrashReportPresenter;
import com.androiddeveloper.pens.namiapp.ui.dasboard.DasboardMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.dasboard.DasboardMvpView;
import com.androiddeveloper.pens.namiapp.ui.dasboard.DasboardPresenter;
import com.androiddeveloper.pens.namiapp.ui.dashboardbc.ActivityDashboardBC;
import com.androiddeveloper.pens.namiapp.ui.dashboardbc.DashboardBCMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.dashboardbc.DashboardBCMvpView;
import com.androiddeveloper.pens.namiapp.ui.dashboardbc.DashboardBCPresenter;
import com.androiddeveloper.pens.namiapp.ui.detailSooBc.DetailBackCheckMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.detailSooBc.DetailBackCheckMvpView;
import com.androiddeveloper.pens.namiapp.ui.detailSooBc.DetailBackCheckPresenter;
import com.androiddeveloper.pens.namiapp.ui.detailbackcheckinglocation.DetailBackCheckingLocationMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.detailbackcheckinglocation.DetailBackCheckingLocationMvpView;
import com.androiddeveloper.pens.namiapp.ui.detailbackcheckinglocation.DetailBackCheckingLocationPresenter;
import com.androiddeveloper.pens.namiapp.ui.feedbacklocation.FeedbackLocationMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.feedbacklocation.FeedbackLocationMvpView;
import com.androiddeveloper.pens.namiapp.ui.feedbacklocation.FeedbackLocationPresenter;
import com.androiddeveloper.pens.namiapp.ui.feedbackossosk.FeedbackOssOskMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.feedbackossosk.FeedbackOssOskMvpView;
import com.androiddeveloper.pens.namiapp.ui.feedbackossosk.FeedbackOssOskPresenter;
import com.androiddeveloper.pens.namiapp.ui.event.EventMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.event.EventMvpView;
import com.androiddeveloper.pens.namiapp.ui.event.EventPresenter;
import com.androiddeveloper.pens.namiapp.ui.feddbacklist.FeedbackListAdapter;
import com.androiddeveloper.pens.namiapp.ui.feddbacklist.FeedbackListMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.feddbacklist.FeedbackListMvpView;
import com.androiddeveloper.pens.namiapp.ui.feddbacklist.FeedbackListPresenter;
import com.androiddeveloper.pens.namiapp.ui.feedback.FeedbackAdapter;
import com.androiddeveloper.pens.namiapp.ui.feedback.FeedbackMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.feedback.FeedbackMvpView;
import com.androiddeveloper.pens.namiapp.ui.feedback.FeedbackPresenter;
import com.androiddeveloper.pens.namiapp.ui.feedbackossoskinsert.FeedbackOssOskInsertMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.feedbackossoskinsert.FeedbackOssOskInsertMvpView;
import com.androiddeveloper.pens.namiapp.ui.feedbackossoskinsert.FeedbackOssOskInsertPresenter;
import com.androiddeveloper.pens.namiapp.ui.feedbacksekolahkampus.FeedbackSekolahKampusMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.feedbacksekolahkampus.FeedbackSekolahKampusMvpView;
import com.androiddeveloper.pens.namiapp.ui.feedbacksekolahkampus.FeedbackSekolahKampusPresenter;
import com.androiddeveloper.pens.namiapp.ui.feedbacksekolahkampusinsert.FeedbackSekolahKampusInsertMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.feedbacksekolahkampusinsert.FeedbackSekolahKampusInsertMvpView;
import com.androiddeveloper.pens.namiapp.ui.feedbacksekolahkampusinsert.FeedbackSekolahKampusInsertPresenter;
import com.androiddeveloper.pens.namiapp.ui.main.MainMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.main.MainMvpView;
import com.androiddeveloper.pens.namiapp.ui.main.MainPresenter;
import com.androiddeveloper.pens.namiapp.ui.notification.NotificationAdapter;
import com.androiddeveloper.pens.namiapp.ui.notification.NotificationMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.notification.NotificationMvpView;
import com.androiddeveloper.pens.namiapp.ui.notification.NotificationPresenter;
import com.androiddeveloper.pens.namiapp.ui.aoc.ossosk.ossoskactualcall.OssOskActualCallMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.aoc.ossosk.ossoskactualcall.OssOskActualCallMvpView;
import com.androiddeveloper.pens.namiapp.ui.aoc.ossosk.ossoskactualcall.OssOskActualCallPresenter;
import com.androiddeveloper.pens.namiapp.ui.aoc.ossosk.ossoskmenu.OssOskMenuMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.aoc.ossosk.ossoskmenu.OssOskMenuMvpView;
import com.androiddeveloper.pens.namiapp.ui.aoc.ossosk.ossoskmenu.OssOskMenuPresenter;
import com.androiddeveloper.pens.namiapp.ui.aoc.ossosk.ossosksales.OssOskSalesMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.aoc.ossosk.ossosksales.OssOskSalesMvpView;
import com.androiddeveloper.pens.namiapp.ui.aoc.ossosk.ossosksales.OssOskSalesPresenter;
import com.androiddeveloper.pens.namiapp.ui.aoc.ossosk.ossoskzonamarking.OssOskZonaMarkingMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.aoc.ossosk.ossoskzonamarking.OssOskZonaMarkingMvpView;
import com.androiddeveloper.pens.namiapp.ui.aoc.ossosk.ossoskzonamarking.OssOskZonaMarkingPresenter;
import com.androiddeveloper.pens.namiapp.ui.performancepoiosklist.PerformancePoiOskListAdapter;
import com.androiddeveloper.pens.namiapp.ui.performancepoiosklist.PerformancePoiOskListMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.performancepoiosklist.PerformancePoiOskListMvpView;
import com.androiddeveloper.pens.namiapp.ui.performancepoiosklist.PerformancePoiOskListPresenter;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusmenu.SekolahKampusMenuMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusmenu.SekolahKampusMenuMvpView;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusmenu.SekolahKampusMenuPresenter;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofile.SekolahKampusProfileMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofile.SekolahKampusProfileMvpView;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofile.SekolahKampusProfilePresenter;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofilekantin.SekolahKampusProfileKantinMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofilekantin.SekolahKampusProfileKantinMvpView;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofilekantin.SekolahKampusProfileKantinPresenter;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofilemarketshare.SekolahKampusProfileMarketShareMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofilemarketshare.SekolahKampusProfileMarketShareMvpView;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofilemarketshare.SekolahKampusProfileMarketSharePresenter;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofilemarketsharequickcount.SekolahKampusMarketShareQuickCountMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofilemarketsharequickcount.SekolahKampusMarketShareQuickCountMvpView;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofilemarketsharequickcount.SekolahKampusMarketShareQuickCountPresenter;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofilemerchandising.SekolahKampusProfileMerchandisingMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofilemerchandising.SekolahKampusProfileMerchandisingMvpView;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofilemerchandising.SekolahKampusProfileMerchandisingPresenter;
import com.androiddeveloper.pens.namiapp.ui.sharepercentage.ShareAreaMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.sharepercentage.ShareAreaMvpView;
import com.androiddeveloper.pens.namiapp.ui.sharepercentage.ShareAreaPresenter;
import com.androiddeveloper.pens.namiapp.ui.sharepercentage.fragmentshare.ShareAreaFragmentMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.sharepercentage.fragmentshare.ShareAreaFragmentMvpView;
import com.androiddeveloper.pens.namiapp.ui.sharepercentage.fragmentshare.ShareAreaFragmentPresenter;
import com.androiddeveloper.pens.namiapp.ui.soobackcheckinglocationlist.SooBackCheckingLocationListAdapter;
import com.androiddeveloper.pens.namiapp.ui.soobackcheckinglocationlist.SooBackCheckingLocationListMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.soobackcheckinglocationlist.SooBackCheckingLocationListMvpView;
import com.androiddeveloper.pens.namiapp.ui.soobackcheckinglocationlist.SooBackCheckingLocationListPresenter;
import com.androiddeveloper.pens.namiapp.ui.summary.SummaryAreaMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.summary.SummaryAreaMvpView;
import com.androiddeveloper.pens.namiapp.ui.summary.SummaryAreaPresenter;
import com.androiddeveloper.pens.namiapp.ui.summary.fragment.AreaMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.summary.fragment.AreaMvpView;
import com.androiddeveloper.pens.namiapp.ui.summary.fragment.AreaPresenter;
import com.androiddeveloper.pens.namiapp.ui.summary.fragment.SummaryType;
import com.androiddeveloper.pens.namiapp.ui.summarybackchecking.SummaryBackcheckingMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.summarybackchecking.SummaryBackcheckingMvpView;
import com.androiddeveloper.pens.namiapp.ui.summarybackchecking.SummaryBackcheckingPresenter;
import com.androiddeveloper.pens.namiapp.ui.summarybackchecking.fragment.SummaryBackcheckingFragmentMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.summarybackchecking.fragment.SummaryBackcheckingFragmentMvpView;
import com.androiddeveloper.pens.namiapp.ui.summarybackchecking.fragment.SummaryBackcheckingFragmentPresenter;
import com.androiddeveloper.pens.namiapp.ui.verifiynumber.VerifyNumberMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.verifiynumber.VerifyNumberMvpView;
import com.androiddeveloper.pens.namiapp.ui.verifiynumber.VerifyNumberPresenter;
import com.androiddeveloper.pens.namiapp.ui.faq.FaqMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.faq.FaqMvpView;
import com.androiddeveloper.pens.namiapp.ui.faq.FaqPresenter;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by miftahun on 6/11/18.
 * <p>
 * Author Miftahun Najat
 * Email miftahunajat@gmail.com
 * Github https://github.com/miftahunajat
 */

@Module
public class ActivityModule {

    private AppCompatActivity mActivity;
    SummaryType SummaryType;
    private String types;

    public ActivityModule(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return mActivity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return mActivity;
    }

    @Provides
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    @PerActivity
    MainMvpPresenter<MainMvpView> provideMainPresenter(MainPresenter<MainMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    DasboardMvpPresenter<DasboardMvpView> provideDashboardPresenter(DasboardPresenter<DasboardMvpView> presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    DashboardBCMvpPresenter<DashboardBCMvpView> provideDashboardBCPresenter(DashboardBCPresenter<DashboardBCMvpView> presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    FeedbackMvpPresenter<FeedbackMvpView> provideFeedbackPresenter(FeedbackPresenter<FeedbackMvpView> presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    BackCheckingOssOskMvpPresenter<BackCheckingOssOskMvpView> provideBackCheckingOssOskPresenter(BackCheckingOssOskPresenter<BackCheckingOssOskMvpView> presenter){
        return presenter;
    }

    @Provides
    FeedbackAdapter provideFeedbackAdapter(){
        return new FeedbackAdapter(new ArrayList<FeedbackLocationResponse>());
    }

    @Provides
    FeedbackListAdapter provideFeedbackListAdapter(){
        return new FeedbackListAdapter(new ArrayList<FeedbackLocationResponse>());
    }

    @Provides
    BackCheckingLocationListAdapter provideBackCheckingListAdapter(){
        return new BackCheckingLocationListAdapter(new ArrayList<BackCheckingLocationListResponse>());
    }

    @Provides
    SooBackCheckingLocationListAdapter provideSooBackCheckingLocationListAdapter(){
        return new SooBackCheckingLocationListAdapter(new ArrayList<SooBackCheckingLocationListResponse>());
    }

    @Provides
    PerformancePoiOskListAdapter providePerformancePoiOskListAdapter(){
        return new PerformancePoiOskListAdapter(new ArrayList<PerformanceSummary>(), this.SummaryType, types);
    }

    @Provides
    NotificationAdapter provideNotificationAdapter(){
        return new NotificationAdapter(new ArrayList<NotificationResponse>());
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager(AppCompatActivity activity) {
        return new LinearLayoutManager(activity);
    }

    @Provides
    @PerActivity
    FeedbackOssOskMvpPresenter<FeedbackOssOskMvpView> provideDetailFeedbackPresenter(FeedbackOssOskPresenter<FeedbackOssOskMvpView> presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    FeedbackListMvpPresenter<FeedbackListMvpView> provideFeedbackListPresenter(FeedbackListPresenter<FeedbackListMvpView> presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    BackCheckingSekolahKampusMvpPresenter<BackCheckingSekolahKampusMvpView> provideBackCheckingSekolahKampusPresenter(BackCheckingSekolahKampusPresenter<BackCheckingSekolahKampusMvpView> presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    PerformancePoiOskListMvpPresenter<PerformancePoiOskListMvpView> providePerformancePoiOskListPresenter(PerformancePoiOskListPresenter<PerformancePoiOskListMvpView> presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    BackCheckingLocationMvpPresenter<BackCheckingLocationMvpView> provideBackCheckingLocationPresenter(BackCheckingLocationPresenter<BackCheckingLocationMvpView> presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    BackCheckingLocationListMvpPresenter<BackCheckingLocationListMvpView> provideBackCheckingLocationListPresenter(BackCheckingLocationListPresenter<BackCheckingLocationListMvpView> presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    SooBackCheckingLocationListMvpPresenter<SooBackCheckingLocationListMvpView> provideSooBackCheckingLocationListMvpViewSooBackCheckingLocationListPresenter(SooBackCheckingLocationListPresenter<SooBackCheckingLocationListMvpView> presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    DetailBackCheckingLocationMvpPresenter<DetailBackCheckingLocationMvpView> provideDetailBackCheckingLocationPresenter(DetailBackCheckingLocationPresenter<DetailBackCheckingLocationMvpView> presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    FaqMvpPresenter<FaqMvpView> provideFaqPresenter(FaqPresenter<FaqMvpView> presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    VerifyNumberMvpPresenter<VerifyNumberMvpView> provideVerifyNumberPresenter(VerifyNumberPresenter<VerifyNumberMvpView> presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    NotificationMvpPresenter<NotificationMvpView> provideNotificationPresenter(NotificationPresenter<NotificationMvpView> presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    EventMvpPresenter<EventMvpView> provideEventPresenter(EventPresenter<EventMvpView> presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    SummaryAreaMvpPresenter<SummaryAreaMvpView> provideSummaryAreaPresenter(SummaryAreaPresenter<SummaryAreaMvpView> presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    ShareAreaMvpPresenter<ShareAreaMvpView> provideShareAreaPresenter(ShareAreaPresenter<ShareAreaMvpView> presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    SummaryBackcheckingMvpPresenter<SummaryBackcheckingMvpView> provideSummaryBackcheckingPresenter(SummaryBackcheckingPresenter<SummaryBackcheckingMvpView> presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    ShareAreaFragmentMvpPresenter<ShareAreaFragmentMvpView> provideShareAreaFragmentPresenter(ShareAreaFragmentPresenter<ShareAreaFragmentMvpView> presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    FeedbackSekolahKampusMvpPresenter<FeedbackSekolahKampusMvpView> provideFeedbackSekolahKampusPresenter(FeedbackSekolahKampusPresenter<FeedbackSekolahKampusMvpView> presenter) {
     return presenter;
    }

    @Provides
    @PerActivity
    AreaMvpPresenter<AreaMvpView> provideAreaPresenter(AreaPresenter<AreaMvpView> presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    SummaryBackcheckingFragmentMvpPresenter<SummaryBackcheckingFragmentMvpView> provideSummaryBackcheckingFragmentPresenter(SummaryBackcheckingFragmentPresenter<SummaryBackcheckingFragmentMvpView> presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    FeedbackLocationMvpPresenter<FeedbackLocationMvpView> provideFeedbackLocationPresenter(FeedbackLocationPresenter<FeedbackLocationMvpView> presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    FeedbackSekolahKampusInsertMvpPresenter<FeedbackSekolahKampusInsertMvpView> provideFeedbackSekolahKampusInsertPresenter(FeedbackSekolahKampusInsertPresenter<FeedbackSekolahKampusInsertMvpView> presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    FeedbackOssOskInsertMvpPresenter<FeedbackOssOskInsertMvpView> provideFeedbackOssOskInsertPresenter(FeedbackOssOskInsertPresenter<FeedbackOssOskInsertMvpView> presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    SekolahKampusProfileMvpPresenter<SekolahKampusProfileMvpView> provideSekolahKampusProfilePresenter(SekolahKampusProfilePresenter<SekolahKampusProfileMvpView> presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    SekolahKampusMenuMvpPresenter<SekolahKampusMenuMvpView> provideSekolahKampusMenuPresenter(SekolahKampusMenuPresenter<SekolahKampusMenuMvpView> presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    SekolahKampusProfileKantinMvpPresenter<SekolahKampusProfileKantinMvpView> provideSekolahKampusProfileKantinPresenter(SekolahKampusProfileKantinPresenter<SekolahKampusProfileKantinMvpView> presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    SekolahKampusProfileMarketShareMvpPresenter<SekolahKampusProfileMarketShareMvpView> provideSekolahKampusProfileMarketSharePresenter(SekolahKampusProfileMarketSharePresenter<SekolahKampusProfileMarketShareMvpView> presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    SekolahKampusMarketShareQuickCountMvpPresenter<SekolahKampusMarketShareQuickCountMvpView> provideSekolahKampusMarketShareQuickCountPresenter(SekolahKampusMarketShareQuickCountPresenter<SekolahKampusMarketShareQuickCountMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    SekolahKampusProfileMerchandisingMvpPresenter<SekolahKampusProfileMerchandisingMvpView> provideSekolahKampusProfileMerchandisingPresenter(SekolahKampusProfileMerchandisingPresenter<SekolahKampusProfileMerchandisingMvpView> presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    OssOskActualCallMvpPresenter<OssOskActualCallMvpView> provideOssOskActualCallPresenter(OssOskActualCallPresenter<OssOskActualCallMvpView> presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    OssOskMenuMvpPresenter<OssOskMenuMvpView> provideOssOskMenuPresenter(OssOskMenuPresenter<OssOskMenuMvpView> presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    OssOskSalesMvpPresenter<OssOskSalesMvpView> ProvideOssOskSalesPresenter(OssOskSalesPresenter<OssOskSalesMvpView> presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    OssOskZonaMarkingMvpPresenter<OssOskZonaMarkingMvpView> provideOssOskZonaMarkingPresenter(OssOskZonaMarkingPresenter<OssOskZonaMarkingMvpView> presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    CrashReportMvpPresenter<CrashReportMvpView> provideCrashReportPresenter(CrashReportPresenter<CrashReportMvpView> presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    HomeMvpPresenter<HomeMvpView> provideHomePresenter(HomePresenter<HomeMvpView> presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    EventAocMvpPresenter<EventAocMvpView> provideEventAocPresenter(EventAocPresenter<EventAocMvpView> presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    DetailBackCheckMvpPresenter<DetailBackCheckMvpView> provideDetailBackCheckMvpViewDetailBackCheckMvpPresenter(DetailBackCheckPresenter<DetailBackCheckMvpView> presenter){
        return presenter;
    }

}
