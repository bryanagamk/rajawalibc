package com.androiddeveloper.pens.namiapp.di.module;

import android.app.Application;
import android.content.Context;

import com.androiddeveloper.pens.namiapp.data.AppDataManager;
import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.data.firebase.AppFirebaseHelper;
import com.androiddeveloper.pens.namiapp.data.firebase.FirebaseHelper;
import com.androiddeveloper.pens.namiapp.data.network.ApiHelper;
import com.androiddeveloper.pens.namiapp.data.network.AppApiHelper;
import com.androiddeveloper.pens.namiapp.data.prefs.AppPreferencesHelper;
import com.androiddeveloper.pens.namiapp.data.prefs.PreferencesHelper;
import com.androiddeveloper.pens.namiapp.di.ApplicationContext;
import com.androiddeveloper.pens.namiapp.di.DatabaseInfo;
import com.androiddeveloper.pens.namiapp.di.PreferenceInfo;
import com.androiddeveloper.pens.namiapp.service.NotificationService;
import com.androiddeveloper.pens.namiapp.utils.AppConstants;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by miftahun on 6/11/18.
 * <p>
 * Author Miftahun Najat
 * Email miftahunajat@gmail.com
 * Github https://github.com/miftahunajat
 */

@Module
public class ApplicationModule {

    private final Application mApplication;

    public ApplicationModule(Application mApplication) {
        this.mApplication = mApplication;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @DatabaseInfo
    String provideDatabaseName() {
        return "database";
    }

    @Provides
    @PreferenceInfo
    String providePreferenceName() {
        return AppConstants.PREF_NAME;
    }

    @Provides
    @Singleton
    DataManager provideDataManager(AppDataManager dataManager) {
        return dataManager;
    }

    @Provides
    @Singleton
    ApiHelper provideApiHelpter(AppApiHelper apiHelper){
        return apiHelper;
    }

    @Provides
    @Singleton
    PreferencesHelper providePreferencesHelper(AppPreferencesHelper appPreferencesHelper) {
        return appPreferencesHelper;
    }

    @Provides
    @Singleton
    FirebaseHelper provideFirebaseHelper(AppFirebaseHelper appFirebaseHelper){
        return appFirebaseHelper;
    }
}
