package com.androiddeveloper.pens.namiapp.rajawali.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.rajawali.model.BackcheckLocation;
import com.androiddeveloper.pens.namiapp.ui.detailSooBc.DetailBackCheckActivity;

import java.util.ArrayList;

/**
 * Created by kmdr7 on 10/01/19
 * <p>
 * Author Andika Ahmad Ramadhan
 * Email Andikahmadr@gmail.com
 * Github https://github.com/kmdrn7
 **/
public class BackcheckLocationAdapter extends RecyclerView.Adapter<BackcheckLocationAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<BackcheckLocation> dataList;

    public BackcheckLocationAdapter(Context context, ArrayList<BackcheckLocation> list){
        this.mContext = context;
        this.dataList = list;
    }

    @Override
    public BackcheckLocationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_soo_back_checking_location_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(BackcheckLocationAdapter.ViewHolder holder, int position) {
        holder.tv_location.setText((dataList.get(position).getLocation_name() + " " +dataList.get(position).getLast_visit()));
        holder.tv_lat.setText(dataList.get(position).getLatitude());
        holder.tv_long.setText(dataList.get(position).getLongitude());
        holder.tv_radius.setText(dataList.get(position).getDistance() + " Meter");
        holder.tv_last.setText(dataList.get(position).getUser_name());
        holder.tv_status.setText(dataList.get(position).getMessage_allowed_chekin());

        if ( dataList.get(position).getLocation_type().equals("SITE") ){
            holder.iv_logo.setImageResource(R.drawable.tower);
        } else {
            holder.iv_logo.setImageResource(R.drawable.aoc_osk);
        }

        holder.ll.setOnClickListener(view -> {
            Intent intent = new Intent(mContext, DetailBackCheckActivity.class);
            intent.putExtra("nama", dataList.get(position).getLocation_name());
            intent.putExtra("radius", dataList.get(position).getDistance() + " Meter");
            intent.putExtra("long", dataList.get(position).getLongitude());
            intent.putExtra("lat", dataList.get(position).getLatitude());
            intent.putExtra("alamat", dataList.get(position).getAlamat());
            intent.putExtra("regional", dataList.get(position).getRegional());
            intent.putExtra("cluster", dataList.get(position).getCluster());
            intent.putExtra("city", dataList.get(position).getCity());
            intent.putExtra("last_visit", dataList.get(position).getLast_visit());

            intent.putExtra("display_telkomsel", dataList.get(position).getDisplay_telkomsel());
            intent.putExtra("display_ooredoo", dataList.get(position).getDisplay_ooredoo());
            intent.putExtra("display_xl", dataList.get(position).getDisplay_xl());
            intent.putExtra("display_axis", dataList.get(position).getDisplay_axis());
            intent.putExtra("display_three", dataList.get(position).getDisplay_three());
            intent.putExtra("display_others", dataList.get(position).getDisplay_others());

            intent.putExtra("recharge_telkomsel", dataList.get(position).getRecharge_share_telkomsel());
            intent.putExtra("recharge_ooredoo", dataList.get(position).getRecharge_share_ooredoo());
            intent.putExtra("recharge_xl", dataList.get(position).getRecharge_share_xl());
            intent.putExtra("recharge_axis", dataList.get(position).getRecharge_share_axis());
            intent.putExtra("recharge_three", dataList.get(position).getRecharge_share_three());
            intent.putExtra("recharge_others", dataList.get(position).getDisplay_others());

            intent.putExtra("type", dataList.get(position).getLocation_type());
            mContext.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_location, tv_lat, tv_long, tv_radius, tv_last, tv_status;
        public ImageView iv_logo;
        public LinearLayout ll;

        public ViewHolder(View view){
            super(view);
            tv_location = view.findViewById(R.id.tv_backcheck_location);
            tv_lat = view.findViewById(R.id.tv_backcheck_lat);
            tv_long = view.findViewById(R.id.tv_backcheck_long);
            tv_radius = view.findViewById(R.id.tv_backcheck_distance);
            tv_last = view.findViewById(R.id.tv_backcheck_last_visi);
            tv_status = view.findViewById(R.id.tv_backcheck_status);
            iv_logo = view.findViewById(R.id.iv_backcheck_logo);
            ll = view.findViewById(R.id.linear_backcheck_wrapper);
        }
    }
}
