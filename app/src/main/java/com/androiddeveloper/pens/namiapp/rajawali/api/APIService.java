package com.androiddeveloper.pens.namiapp.rajawali.api;

import com.androiddeveloper.pens.namiapp.rajawali.model.BackcheckLocation;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by kmdr7 on 10/01/19
 * <p>
 * Author Andika Ahmad Ramadhan
 * Email Andikahmadr@gmail.com
 * Github https://github.com/kmdrn7
 **/
public interface APIService {

    @FormUrlEncoded
    @POST("sp_backchecking_location.php")
    Call<ArrayList<BackcheckLocation>> getBackcheckLocationResponseCall(
            @Field("userid") String userid,
            @Field("latitude") String lat_,
            @Field("longitude") String long_,
            @Field("disoutlet") String disoutlet,
            @Field("dissite") String dissite,
            @Field("lac") String lac_,
            @Field("ci") String ci_);

}
