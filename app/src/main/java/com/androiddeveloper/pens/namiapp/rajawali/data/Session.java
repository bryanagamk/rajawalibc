package com.androiddeveloper.pens.namiapp.rajawali.data;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by kmdr7 on 12/01/19
 * <p>
 * Author Andika Ahmad Ramadhan
 * Email Andikahmadr@gmail.com
 * Github https://github.com/kmdrn7
 **/
public class Session {

    SharedPreferences sp;
    SharedPreferences.Editor spEditor;
    Context ctx;

    public Session( Context ctx ){
        this.ctx = ctx;
        sp = ctx.getSharedPreferences("RAJAWALI_SESSION", Context.MODE_PRIVATE);
        spEditor = sp.edit();
    }

    public void setUserid( String userid ){
        spEditor.putString("userid", userid);
        spEditor.commit();
    }

    public String getUserid(){
        return sp.getString("userid", "Kosongan");
    }

}
