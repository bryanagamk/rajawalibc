package com.androiddeveloper.pens.namiapp.rajawali.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kmdr7 on 10/01/19
 * <p>
 * Author Andika Ahmad Ramadhan
 * Email Andikahmadr@gmail.com
 * Github https://github.com/kmdrn7
 **/
public class BackcheckLocation {

    @SerializedName("AREA")
    @Expose
    private String AREA;

    @SerializedName("regional")
    @Expose
    private String regional;

    @SerializedName("branch")
    @Expose
    private String branch;

    @SerializedName("city")
    @Expose
    private String city;

    @SerializedName("location_name")
    @Expose
    private String location_name;

    @SerializedName("message_allowed_chekin")
    @Expose
    private String message_allowed_chekin;

    @SerializedName("latitude")
    @Expose
    private String latitude;

    @SerializedName("longitude")
    @Expose
    private String longitude;

    @SerializedName("distance")
    @Expose
    private String distance;

    @SerializedName("username")
    @Expose
    private String user_name;

    @SerializedName("last_visit")
    @Expose
    private String last_visit;

    @SerializedName("location_type")
    @Expose
    private String location_type;

    @SerializedName("alamat")
    @Expose
    private String alamat;

    @SerializedName("cluster")
    @Expose
    private String cluster;

    @SerializedName("broadband_share_telkomsel")
    @Expose
    private String sales_telkomsel;

    @SerializedName("broadband_share_ooredoo")
    @Expose
    private String sales_ooredoo;

    @SerializedName("broadband_share_xl")
    @Expose
    private String sales_xl;

    @SerializedName("broadband_share_axis")
    @Expose
    private String sales_axis;

    @SerializedName("broadband_share_three")
    @Expose
    private String sales_othree;

    @SerializedName("broadband_share_others")
    @Expose
    private String sales_others;

    @SerializedName("display_telkomsel")
    @Expose
    private String display_telkomsel;

    @SerializedName("display_ooredoo")
    @Expose
    private String display_ooredoo;

    @SerializedName("display_xl")
    @Expose
    private String display_xl;

    @SerializedName("display_axis")
    @Expose
    private String display_axis;

    @SerializedName("display_three")
    @Expose
    private String display_three;

    @SerializedName("display_others")
    @Expose
    private String display_others;

    @SerializedName("recharge_share_telkomsel")
    @Expose
    private String recharge_share_telkomsel;

    @SerializedName("recharge_share_ooredoo")
    @Expose
    private String recharge_share_ooredoo;

    @SerializedName("recharge_share_xl")
    @Expose
    private String recharge_share_xl;

    @SerializedName("recharge_share_axis")
    @Expose
    private String recharge_share_axis;

    @SerializedName("recharge_share_three")
    @Expose
    private String recharge_share_three;

    @SerializedName("recharge_share_others")
    @Expose
    private String recharge_share_others;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getCluster() {
        return cluster;
    }

    public void setCluster(String cluster) {
        this.cluster = cluster;
    }

    public String getSales_telkomsel() {
        return sales_telkomsel;
    }

    public void setSales_telkomsel(String sales_telkomsel) {
        this.sales_telkomsel = sales_telkomsel;
    }

    public String getSales_ooredoo() {
        return sales_ooredoo;
    }

    public void setSales_ooredoo(String sales_ooredoo) {
        this.sales_ooredoo = sales_ooredoo;
    }

    public String getSales_xl() {
        return sales_xl;
    }

    public void setSales_xl(String sales_xl) {
        this.sales_xl = sales_xl;
    }

    public String getSales_axis() {
        return sales_axis;
    }

    public void setSales_axis(String sales_axis) {
        this.sales_axis = sales_axis;
    }

    public String getSales_othree() {
        return sales_othree;
    }

    public void setSales_othree(String sales_othree) {
        this.sales_othree = sales_othree;
    }

    public String getSales_others() {
        return sales_others;
    }

    public void setSales_others(String sales_others) {
        this.sales_others = sales_others;
    }

    public String getDisplay_telkomsel() {
        return display_telkomsel;
    }

    public void setDisplay_telkomsel(String display_telkomsel) {
        this.display_telkomsel = display_telkomsel;
    }

    public String getDisplay_ooredoo() {
        return display_ooredoo;
    }

    public void setDisplay_ooredoo(String display_ooredoo) {
        this.display_ooredoo = display_ooredoo;
    }

    public String getDisplay_xl() {
        return display_xl;
    }

    public void setDisplay_xl(String display_xl) {
        this.display_xl = display_xl;
    }

    public String getDisplay_axis() {
        return display_axis;
    }

    public void setDisplay_axis(String display_axis) {
        this.display_axis = display_axis;
    }

    public String getDisplay_three() {
        return display_three;
    }

    public void setDisplay_three(String display_three) {
        this.display_three = display_three;
    }

    public String getDisplay_others() {
        return display_others;
    }

    public void setDisplay_others(String display_others) {
        this.display_others = display_others;
    }

    public String getRecharge_share_telkomsel() {
        return recharge_share_telkomsel;
    }

    public void setRecharge_share_telkomsel(String recharge_share_telkomsel) {
        this.recharge_share_telkomsel = recharge_share_telkomsel;
    }

    public String getRecharge_share_ooredoo() {
        return recharge_share_ooredoo;
    }

    public void setRecharge_share_ooredoo(String recharge_share_ooredoo) {
        this.recharge_share_ooredoo = recharge_share_ooredoo;
    }

    public String getRecharge_share_xl() {
        return recharge_share_xl;
    }

    public void setRecharge_share_xl(String recharge_share_xl) {
        this.recharge_share_xl = recharge_share_xl;
    }

    public String getRecharge_share_axis() {
        return recharge_share_axis;
    }

    public void setRecharge_share_axis(String recharge_share_axis) {
        this.recharge_share_axis = recharge_share_axis;
    }

    public String getRecharge_share_three() {
        return recharge_share_three;
    }

    public void setRecharge_share_three(String recharge_share_three) {
        this.recharge_share_three = recharge_share_three;
    }

    public String getRecharge_share_others() {
        return recharge_share_others;
    }

    public void setRecharge_share_others(String recharge_share_others) {
        this.recharge_share_others = recharge_share_others;
    }

    public String getLast_visit() {
        return last_visit;
    }

    public void setLast_visit(String last_visit) {
        this.last_visit = last_visit;
    }

    public String getLocation_type() {
        return location_type;
    }

    public void setLocation_type(String location_type) {
        this.location_type = location_type;
    }

    public String getLocation_name() {
        return location_name;
    }

    public void setLocation_name(String location_name) {
        this.location_name = location_name;
    }

    public String getMessage_allowed_chekin() {
        return message_allowed_chekin;
    }

    public void setMessage_allowed_chekin(String message_allowed_chekin) {
        this.message_allowed_chekin = message_allowed_chekin;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getAREA() {
        return AREA;
    }

    public void setAREA(String AREA) {
        this.AREA = AREA;
    }

    public String getRegional() {
        return regional;
    }

    public void setRegional(String regional) {
        this.regional = regional;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }
}
