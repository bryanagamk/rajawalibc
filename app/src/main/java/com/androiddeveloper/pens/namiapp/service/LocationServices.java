package com.androiddeveloper.pens.namiapp.service;

import android.app.Activity;
import android.location.LocationManager;

import com.androiddeveloper.pens.namiapp.di.ActivityContext;

import javax.inject.Inject;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by miftahun on 7/18/18.
 * <p>
 * Author Miftahun Najat
 * Email miftahunajat@gmail.com
 * Github https://github.com/miftahunajat
 */

public class LocationServices {

    private LocationManager locationManager;

    @Inject
    LocationServices(@ActivityContext Activity activity){
        if(locationManager == null)
            locationManager = (LocationManager)activity.getSystemService(LOCATION_SERVICE);
    }
}
