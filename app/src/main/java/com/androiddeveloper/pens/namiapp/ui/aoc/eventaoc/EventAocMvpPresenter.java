package com.androiddeveloper.pens.namiapp.ui.aoc.eventaoc;

import com.androiddeveloper.pens.namiapp.di.PerActivity;
import com.androiddeveloper.pens.namiapp.ui.base.MvpPresenter;

@PerActivity
public interface EventAocMvpPresenter<V extends EventAocMvpView> extends MvpPresenter<V> {
}
