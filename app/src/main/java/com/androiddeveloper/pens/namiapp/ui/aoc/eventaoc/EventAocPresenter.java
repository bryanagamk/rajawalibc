package com.androiddeveloper.pens.namiapp.ui.aoc.eventaoc;

import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.di.PerActivity;
import com.androiddeveloper.pens.namiapp.ui.base.BasePresenter;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class EventAocPresenter<V extends EventAocMvpView> extends BasePresenter<V>
        implements EventAocMvpPresenter<V> {

    @Inject
    public EventAocPresenter(DataManager dataManager, CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);
    }
}
