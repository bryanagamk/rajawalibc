package com.androiddeveloper.pens.namiapp.ui.aoc.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.ui.aoc.eventaoc.EventAocActivity;
import com.androiddeveloper.pens.namiapp.ui.aoc.ossosk.ossoskmenu.OssOskMenuActivity;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusmenu.SekolahKampusMenuActivity;
import com.androiddeveloper.pens.namiapp.ui.base.BaseActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends BaseActivity implements HomeMvpView {

    @BindView(R.id.button_aochome_pjposk)
    Button pjpOsk;
    @BindView(R.id.button_aochome_sekolahhome_sekolah)
    Button pjpSekolah;
    @BindView(R.id.button_aochome_event)
    Button eventAoc;


    @Inject
    HomePresenter<HomeMvpView> mPresenter;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, HomeActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aoc_home);
        getActivityComponent().inject(this);

        if (isNetworkConnected()) {
            setUnBinder(ButterKnife.bind(this));

            mPresenter.onAttach(this);

            setUp();
        }
    }

    @Override
    protected void setUp() {
        getSupportActionBar().setTitle("Menu AOC");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        pjpOsk.setOnClickListener(v -> {
            Intent intent = OssOskMenuActivity.getStartIntent(getApplicationContext());
            startActivity(intent);
        });

        pjpSekolah.setOnClickListener(v -> {
            Intent intent = SekolahKampusMenuActivity.getStartIntent(getApplicationContext());
            startActivity(intent);
        });

        eventAoc.setOnClickListener(v -> {
            Intent intent = EventAocActivity.getStartIntent(getApplicationContext());
            startActivity(intent);
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
