package com.androiddeveloper.pens.namiapp.ui.aoc.home;

import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.ui.aoc.ossosk.ossoskmenu.OssOskMenuMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.aoc.ossosk.ossoskmenu.OssOskMenuMvpView;
import com.androiddeveloper.pens.namiapp.ui.base.BasePresenter;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class HomePresenter<V extends HomeMvpView> extends BasePresenter<V>
        implements HomeMvpPresenter<V> {

    @Inject
    public HomePresenter(DataManager dataManager, CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);
    }
}
