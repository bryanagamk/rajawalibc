package com.androiddeveloper.pens.namiapp.ui.aoc.ossosk.ossoskactualcall;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.ui.base.BaseActivity;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class OssOskActualCallActivity extends BaseActivity implements OssOskActualCallMvpView {

    @Inject
    OssOskActualCallPresenter<OssOskActualCallMvpView> mPresenter;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, OssOskActualCallActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oss_osk_actual_call);
        getActivityComponent().inject(this);

        if (isNetworkConnected()) {
            setUnBinder(ButterKnife.bind(this));

            mPresenter.onAttach(this);

            setUp();
        }
    }

    @Override
    protected void setUp() {
        getSupportActionBar().setTitle("Actual Call");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
