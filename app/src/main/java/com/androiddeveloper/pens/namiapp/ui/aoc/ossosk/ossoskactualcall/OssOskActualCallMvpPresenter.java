package com.androiddeveloper.pens.namiapp.ui.aoc.ossosk.ossoskactualcall;

import com.androiddeveloper.pens.namiapp.di.PerActivity;
import com.androiddeveloper.pens.namiapp.ui.base.MvpPresenter;

/**
 * Created by ASUS on 10/12/2018.
 */

@PerActivity
public interface OssOskActualCallMvpPresenter<V extends OssOskActualCallMvpView> extends MvpPresenter<V> {
}
