package com.androiddeveloper.pens.namiapp.ui.aoc.ossosk.ossoskactualcall;

import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.ui.base.BasePresenter;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by ASUS on 10/12/2018.
 */

public class OssOskActualCallPresenter<V extends OssOskActualCallMvpView> extends BasePresenter<V>
        implements OssOskActualCallMvpPresenter<V> {

    @Inject
    public OssOskActualCallPresenter(DataManager dataManager, CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);
    }
}
