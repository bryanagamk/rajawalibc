package com.androiddeveloper.pens.namiapp.ui.aoc.ossosk.ossoskmenu;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.LinearLayout;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.ui.base.BaseActivity;
import com.androiddeveloper.pens.namiapp.ui.aoc.ossosk.ossoskactualcall.OssOskActualCallActivity;
import com.androiddeveloper.pens.namiapp.ui.aoc.ossosk.ossosksales.OssOskSalesActivity;
import com.androiddeveloper.pens.namiapp.ui.aoc.ossosk.ossoskzonamarking.OssOskZonaMarkingActivity;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusmenu.SekolahKampusMenuActivity;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusmenu.SekolahKampusMenuMvpView;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusmenu.SekolahKampusMenuPresenter;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofile.SekolahKampusProfileActivity;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofilekantin.SekolahKampusProfileKantinActivity;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofilemarketshare.SekolahKampusProfileMarketShareActivity;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofilemarketsharequickcount.SekolahKampusMarketShareQuickCountActivity;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofilemerchandising.SekolahKampusProfileMerchandisingActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OssOskMenuActivity extends BaseActivity implements OssOskMenuMvpView {

    @BindView(R.id.line_menu_oss_osk_zona_marking)
    LinearLayout lineMenuOssOskZonaMarking;

    @BindView(R.id.line_menu_oss_osk_sales)
    LinearLayout lineMenuOssOskSales;

    @BindView(R.id.line_menu_oss_osk_actual_call)
    LinearLayout lineMenuOssOskActualCall;

    @Inject
    OssOskMenuPresenter<OssOskMenuMvpView> mPresenter;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, OssOskMenuActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oss_osk_menu);
        getActivityComponent().inject(this);

        if (isNetworkConnected()) {
            setUnBinder(ButterKnife.bind(this));

            mPresenter.onAttach(this);

            setUp();
        }
    }

    @Override
    protected void setUp() {
        getSupportActionBar().setTitle("Menu OSK");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        lineMenuOssOskZonaMarking.setOnClickListener(v -> {
            Intent i = new Intent(getApplicationContext(), OssOskZonaMarkingActivity.class);
            startActivity(i);
        });

        lineMenuOssOskSales.setOnClickListener(v -> {
            Intent i = new Intent(getApplicationContext(), OssOskSalesActivity.class);
            startActivity(i);
        });

        lineMenuOssOskActualCall.setOnClickListener(v -> {
            Intent i = new Intent(getApplicationContext(), OssOskActualCallActivity.class);
            startActivity(i);
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
