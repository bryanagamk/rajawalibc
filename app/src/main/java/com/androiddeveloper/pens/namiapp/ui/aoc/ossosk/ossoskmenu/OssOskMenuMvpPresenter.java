package com.androiddeveloper.pens.namiapp.ui.aoc.ossosk.ossoskmenu;

import com.androiddeveloper.pens.namiapp.di.PerActivity;
import com.androiddeveloper.pens.namiapp.ui.base.MvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.base.MvpView;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusmenu.SekolahKampusMenuMvpView;

/**
 * Created by ASUS on 10/9/2018.
 */

@PerActivity
public interface OssOskMenuMvpPresenter<V extends OssOskMenuMvpView> extends MvpPresenter<V> {

}
