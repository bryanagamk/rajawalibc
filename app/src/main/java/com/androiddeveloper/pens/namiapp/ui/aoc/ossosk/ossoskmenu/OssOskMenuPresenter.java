package com.androiddeveloper.pens.namiapp.ui.aoc.ossosk.ossoskmenu;

import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.ui.base.BasePresenter;
import com.androiddeveloper.pens.namiapp.ui.base.MvpView;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by ASUS on 10/9/2018.
 */

public class OssOskMenuPresenter<V extends OssOskMenuMvpView> extends BasePresenter<V>
        implements OssOskMenuMvpPresenter<V> {

    @Inject
    public OssOskMenuPresenter(DataManager dataManager, CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);
    }
}
