package com.androiddeveloper.pens.namiapp.ui.aoc.ossosk.ossosksales;

import com.androiddeveloper.pens.namiapp.di.PerActivity;
import com.androiddeveloper.pens.namiapp.ui.base.MvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.aoc.ossosk.ossoskmenu.OssOskMenuMvpView;

/**
 * Created by ASUS on 10/12/2018.
 */

@PerActivity
public interface OssOskSalesMvpPresenter<V extends OssOskSalesMvpView> extends MvpPresenter<V> {
}
