package com.androiddeveloper.pens.namiapp.ui.aoc.ossosk.ossosksales;

import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.ui.base.BasePresenter;
import com.androiddeveloper.pens.namiapp.ui.aoc.ossosk.ossoskmenu.OssOskMenuMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.aoc.ossosk.ossoskmenu.OssOskMenuMvpView;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by ASUS on 10/12/2018.
 */

public class OssOskSalesPresenter<V extends OssOskSalesMvpView> extends BasePresenter<V>
        implements OssOskSalesMvpPresenter<V> {

    @Inject
    public OssOskSalesPresenter(DataManager dataManager, CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);
    }
}
