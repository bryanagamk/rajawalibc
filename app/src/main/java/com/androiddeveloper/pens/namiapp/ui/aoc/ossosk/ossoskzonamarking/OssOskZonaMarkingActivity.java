package com.androiddeveloper.pens.namiapp.ui.aoc.ossosk.ossoskzonamarking;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.ui.base.BaseActivity;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class OssOskZonaMarkingActivity extends BaseActivity implements OssOskZonaMarkingMvpView {

    @Inject
    OssOskZonaMarkingPresenter<OssOskZonaMarkingMvpView> mPresenter;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, OssOskZonaMarkingActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oss_osk_zona_marking);
        getActivityComponent().inject(this);

        if (isNetworkConnected()) {
            setUnBinder(ButterKnife.bind(this));

            mPresenter.onAttach(this);

            setUp();
        }
    }

    @Override
    protected void setUp() {
        getSupportActionBar().setTitle("Zona Marking");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
