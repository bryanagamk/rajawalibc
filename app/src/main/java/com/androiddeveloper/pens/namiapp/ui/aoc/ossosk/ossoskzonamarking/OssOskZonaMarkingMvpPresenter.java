package com.androiddeveloper.pens.namiapp.ui.aoc.ossosk.ossoskzonamarking;

import com.androiddeveloper.pens.namiapp.di.PerActivity;
import com.androiddeveloper.pens.namiapp.ui.base.MvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.aoc.ossosk.ossosksales.OssOskSalesMvpView;

/**
 * Created by ASUS on 10/12/2018.
 */
@PerActivity
public interface OssOskZonaMarkingMvpPresenter<V extends OssOskZonaMarkingMvpView> extends MvpPresenter<V> {
}
