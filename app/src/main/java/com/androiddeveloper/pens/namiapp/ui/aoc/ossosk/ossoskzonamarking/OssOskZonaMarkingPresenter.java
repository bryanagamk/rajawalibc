package com.androiddeveloper.pens.namiapp.ui.aoc.ossosk.ossoskzonamarking;

import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.ui.base.BasePresenter;
import com.androiddeveloper.pens.namiapp.ui.aoc.ossosk.ossosksales.OssOskSalesMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.aoc.ossosk.ossosksales.OssOskSalesMvpView;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by ASUS on 10/12/2018.
 */

public class OssOskZonaMarkingPresenter<V extends OssOskZonaMarkingMvpView> extends BasePresenter<V>
        implements OssOskZonaMarkingMvpPresenter<V> {
    @Inject
    public OssOskZonaMarkingPresenter(DataManager dataManager, CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);
    }
}
