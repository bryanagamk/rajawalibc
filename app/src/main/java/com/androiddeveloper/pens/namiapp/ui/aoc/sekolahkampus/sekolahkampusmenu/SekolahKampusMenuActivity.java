package com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusmenu;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.LinearLayout;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.ui.base.BaseActivity;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofile.SekolahKampusProfileActivity;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofilekantin.SekolahKampusProfileKantinActivity;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofilemarketshare.SekolahKampusProfileMarketShareActivity;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofilemarketsharequickcount.SekolahKampusMarketShareQuickCountActivity;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofilemerchandising.SekolahKampusProfileMerchandisingActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SekolahKampusMenuActivity extends BaseActivity implements SekolahKampusMenuMvpView {

    @BindView(R.id.line_menu_profile_sekolah_kampus)
    LinearLayout lineMenuProfileSekolahKampus;

    @BindView(R.id.line_menu_profile_kantin_sekolah_kampus)
    LinearLayout lineMenuProfileKantinSekolahKampus;

    @BindView(R.id.line_menu_profile_market_share_sekolah_kampus)
    LinearLayout lineMenuProfileMarketShareSekolahKampus;

    @BindView(R.id.line_menu_profile_marketshare_quickcount_sekolah_kampus)
    LinearLayout lineMenuProfileMarketShareQuicCountSekolahKampus;

    @BindView(R.id.line_menu_profile_merchandising_sekolah_kampus)
    LinearLayout lineMenuProfileMerchandisingSekolahKampus;


    @Inject
    SekolahKampusMenuPresenter<SekolahKampusMenuMvpView> mPresenter;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, SekolahKampusMenuActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sekolah_kampus_menu);
        getActivityComponent().inject(this);

        if (isNetworkConnected()) {
            setUnBinder(ButterKnife.bind(this));

            mPresenter.onAttach(this);

            setUp();
        }
    }

    @Override
    protected void setUp() {
        getSupportActionBar().setTitle("Menu POI");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        lineMenuProfileSekolahKampus.setOnClickListener(v -> {
            Intent i = new Intent(getApplicationContext(), SekolahKampusProfileActivity.class);
            startActivity(i);
        });

        lineMenuProfileKantinSekolahKampus.setOnClickListener(v -> {
            Intent i = new Intent(getApplicationContext(), SekolahKampusProfileKantinActivity.class);
            startActivity(i);
        });

        lineMenuProfileMarketShareSekolahKampus.setOnClickListener(v -> {
            Intent i = new Intent(getApplicationContext(), SekolahKampusProfileMarketShareActivity.class);
            startActivity(i);
        });

        lineMenuProfileMarketShareQuicCountSekolahKampus.setOnClickListener(v -> {
            Intent i = new Intent(getApplicationContext(), SekolahKampusMarketShareQuickCountActivity.class);
            startActivity(i);
        });

        lineMenuProfileMerchandisingSekolahKampus.setOnClickListener(v -> {
            Intent i = new Intent(getApplicationContext(), SekolahKampusProfileMerchandisingActivity.class);
            startActivity(i);
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
