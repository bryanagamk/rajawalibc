package com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusmenu;

import com.androiddeveloper.pens.namiapp.di.PerActivity;
import com.androiddeveloper.pens.namiapp.ui.base.MvpPresenter;

@PerActivity
public interface SekolahKampusMenuMvpPresenter<V extends SekolahKampusMenuMvpView> extends MvpPresenter<V> {
}
