package com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusmenu;

import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.ui.base.BasePresenter;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class SekolahKampusMenuPresenter<V extends SekolahKampusMenuMvpView> extends BasePresenter<V>
        implements SekolahKampusMenuMvpPresenter<V> {

    @Inject
    public SekolahKampusMenuPresenter(DataManager dataManager, CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);
    }
}
