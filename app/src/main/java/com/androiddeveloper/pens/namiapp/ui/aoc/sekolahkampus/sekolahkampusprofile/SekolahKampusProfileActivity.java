package com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.ui.base.BaseActivity;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class SekolahKampusProfileActivity extends BaseActivity implements SekolahKampusProfileMvpView {

    @Inject
    SekolahKampusProfilePresenter<SekolahKampusProfileMvpView> mPresenter;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, SekolahKampusProfileActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sekolah_kampus_profile);
        getActivityComponent().inject(this);

        if (isNetworkConnected()) {
            setUnBinder(ButterKnife.bind(this));

            mPresenter.onAttach(this);

            setUp();
        }
    }

    @Override
    protected void setUp() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Profile POI");
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
