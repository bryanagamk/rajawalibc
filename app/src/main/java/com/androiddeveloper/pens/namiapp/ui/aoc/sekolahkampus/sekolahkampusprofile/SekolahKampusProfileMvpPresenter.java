package com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofile;

import com.androiddeveloper.pens.namiapp.di.PerActivity;
import com.androiddeveloper.pens.namiapp.ui.base.MvpPresenter;

@PerActivity
public interface SekolahKampusProfileMvpPresenter<V extends SekolahKampusProfileMvpView> extends MvpPresenter<V> {
    void getSekolahKampusProfile();
}
