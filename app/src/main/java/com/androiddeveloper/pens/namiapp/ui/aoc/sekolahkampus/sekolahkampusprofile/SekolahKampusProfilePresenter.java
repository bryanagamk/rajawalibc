package com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofile;

import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.ui.base.BasePresenter;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class SekolahKampusProfilePresenter<V extends SekolahKampusProfileMvpView> extends BasePresenter<V>
        implements SekolahKampusProfileMvpPresenter<V> {

    @Inject
    public SekolahKampusProfilePresenter(DataManager dataManager, CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);
    }

    @Override
    public void getSekolahKampusProfile() {

    }
}
