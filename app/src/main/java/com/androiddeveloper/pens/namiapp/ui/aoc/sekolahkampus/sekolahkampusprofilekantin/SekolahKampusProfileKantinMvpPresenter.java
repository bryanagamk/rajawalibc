package com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofilekantin;

import com.androiddeveloper.pens.namiapp.di.PerActivity;
import com.androiddeveloper.pens.namiapp.ui.base.MvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofile.SekolahKampusProfileMvpView;

@PerActivity
public interface SekolahKampusProfileKantinMvpPresenter<V extends SekolahKampusProfileKantinMvpView> extends MvpPresenter<V> {
}
