package com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofilekantin;

import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.ui.base.BasePresenter;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofile.SekolahKampusProfileMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofile.SekolahKampusProfileMvpView;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class SekolahKampusProfileKantinPresenter<V extends SekolahKampusProfileKantinMvpView> extends BasePresenter<V>
        implements SekolahKampusProfileKantinMvpPresenter<V> {

    @Inject
    public SekolahKampusProfileKantinPresenter(DataManager dataManager, CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);
    }
}
