package com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofilemarketshare;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.ui.base.BaseActivity;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofile.SekolahKampusProfileActivity;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofile.SekolahKampusProfileMvpView;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofile.SekolahKampusProfilePresenter;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class SekolahKampusProfileMarketShareActivity extends BaseActivity implements SekolahKampusProfileMarketShareMvpView {

    @Inject
    SekolahKampusProfileMarketSharePresenter<SekolahKampusProfileMarketShareMvpView> mPresenter;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, SekolahKampusProfileMarketShareActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sekolah_kampus_profile_market_share);
        getActivityComponent().inject(this);

        if (isNetworkConnected()) {
            setUnBinder(ButterKnife.bind(this));

            mPresenter.onAttach(this);

            setUp();
        }
    }

    @Override
    protected void setUp() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Profile Market Share");
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
