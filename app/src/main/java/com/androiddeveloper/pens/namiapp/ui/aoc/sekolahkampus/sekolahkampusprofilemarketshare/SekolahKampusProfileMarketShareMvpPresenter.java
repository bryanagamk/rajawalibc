package com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofilemarketshare;

import com.androiddeveloper.pens.namiapp.di.PerActivity;
import com.androiddeveloper.pens.namiapp.ui.base.MvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofilekantin.SekolahKampusProfileKantinMvpView;

@PerActivity
public interface SekolahKampusProfileMarketShareMvpPresenter<V extends SekolahKampusProfileMarketShareMvpView> extends MvpPresenter<V> {
}
