package com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofilemarketshare;

import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.ui.base.BasePresenter;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofilekantin.SekolahKampusProfileKantinMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofilekantin.SekolahKampusProfileKantinMvpView;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class SekolahKampusProfileMarketSharePresenter<V extends SekolahKampusProfileMarketShareMvpView> extends BasePresenter<V>
        implements SekolahKampusProfileMarketShareMvpPresenter<V> {

    @Inject
    public SekolahKampusProfileMarketSharePresenter(DataManager dataManager, CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);
    }
}
