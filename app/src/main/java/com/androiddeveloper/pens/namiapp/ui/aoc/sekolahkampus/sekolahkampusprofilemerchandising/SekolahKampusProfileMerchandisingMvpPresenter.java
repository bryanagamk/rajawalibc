package com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofilemerchandising;

import com.androiddeveloper.pens.namiapp.di.PerActivity;
import com.androiddeveloper.pens.namiapp.ui.base.MvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofilemarketshare.SekolahKampusProfileMarketShareMvpView;

@PerActivity
public interface SekolahKampusProfileMerchandisingMvpPresenter<V extends SekolahKampusProfileMerchandisingMvpView> extends MvpPresenter<V> {
}
