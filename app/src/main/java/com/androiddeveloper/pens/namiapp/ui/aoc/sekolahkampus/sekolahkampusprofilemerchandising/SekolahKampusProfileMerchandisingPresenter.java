package com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofilemerchandising;

import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.ui.base.BasePresenter;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofilemarketshare.SekolahKampusProfileMarketShareMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusprofilemarketshare.SekolahKampusProfileMarketShareMvpView;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class SekolahKampusProfileMerchandisingPresenter<V extends SekolahKampusProfileMerchandisingMvpView> extends BasePresenter<V>
        implements SekolahKampusProfileMerchandisingMvpPresenter<V> {

    @Inject
    public SekolahKampusProfileMerchandisingPresenter(DataManager dataManager, CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);
    }
}
