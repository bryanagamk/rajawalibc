package com.androiddeveloper.pens.namiapp.ui.backchecking;

import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.ui.base.BaseActivity;

import java.util.List;
import java.util.Locale;

/**
 * Created by miftahun on 8/14/18.
 */

public abstract class BaseBackcheckingActivity extends BaseActivity implements BaseBackcheckingMvpView {

    public void generatePersentaseShare(EditText[] editTexts, TextView[] textViews){
        float totalSum = 0f;
        Float[] countPerProduct = {0f,0f,0f,0f,0f,0f};
        for (int i = 0 ; i <editTexts.length ; i++){
            if (editTexts[i].getText().toString().trim().equals("")){
                countPerProduct[i] = 0f;
            }else{
                countPerProduct[i] = Float.valueOf(editTexts[i].getText().toString());
            }

            totalSum += countPerProduct[i];
        }

        if (totalSum != 0 ){
            for (int i = 0 ; i <textViews.length ; i++){
                String result = String.format(Locale.getDefault(),"%.2f%%",100*countPerProduct[i] / totalSum);
                textViews[i].setText(result);
            }
        }else{
            for (int i = 0 ; i <textViews.length ; i++){
                textViews[i].setText("0%");
            }
        }
    }

    @Override
    public void setupSpinnerMenu(List<String> strings, Spinner spinner) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item,strings);
        // muncul pilihan brand
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // set adapter
        spinner.setAdapter(adapter);
    }
}