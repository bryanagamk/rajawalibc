package com.androiddeveloper.pens.namiapp.ui.backchecking;

import android.widget.Spinner;

import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.ui.base.BasePresenter;
import com.androiddeveloper.pens.namiapp.ui.base.MvpPresenter;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by miftahun on 8/14/18.
 */

public interface BaseBackcheckingMvpPresenter<V extends BaseBackcheckingMvpView> extends MvpPresenter<V> {

    void getSpinnerMenu(Spinner spinner);

}
