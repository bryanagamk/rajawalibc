package com.androiddeveloper.pens.namiapp.ui.backchecking;

import android.widget.Spinner;

import com.androiddeveloper.pens.namiapp.ui.base.MvpView;

import java.util.List;

/**
 * Created by miftahun on 8/14/18.
 */

public interface BaseBackcheckingMvpView extends MvpView {

    void setupSpinnerMenu(List<String> strings,Spinner spinner);

}
