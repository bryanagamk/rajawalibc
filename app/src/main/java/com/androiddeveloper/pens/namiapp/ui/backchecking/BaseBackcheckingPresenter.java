package com.androiddeveloper.pens.namiapp.ui.backchecking;

import android.widget.Spinner;

import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.ui.base.BasePresenter;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by miftahun on 8/14/18.
 */

public abstract class BaseBackcheckingPresenter<V extends BaseBackcheckingMvpView> extends BasePresenter<V>
        implements BaseBackcheckingMvpPresenter<V>{

    @Override
    public void getSpinnerMenu(Spinner spinner) {
        getCompositeDisposable().add(getDataManager().getBrandingOperator()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(strings -> {
            if (strings != null && !strings.isEmpty()){
                getMvpView().setupSpinnerMenu(strings,spinner);
            }else{
                getMvpView().showMessage("Gagal mendapatkan data branding");
            }
        },throwable -> {
            getSpinnerMenu(spinner);
            getMvpView().showMessage("Gagal mendapatkan data branding");
        }));
    }

    public BaseBackcheckingPresenter(DataManager dataManager, CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);
    }
}
