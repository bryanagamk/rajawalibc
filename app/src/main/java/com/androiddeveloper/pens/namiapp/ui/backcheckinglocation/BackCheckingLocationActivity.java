package com.androiddeveloper.pens.namiapp.ui.backcheckinglocation;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.data.network.model.BackCheckingLocationListResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.BackcheckingMain;
import com.androiddeveloper.pens.namiapp.ui.backcheckinglocationlist.BackCheckingLocationListActivity;
import com.androiddeveloper.pens.namiapp.ui.base.BaseActivity;
import com.androiddeveloper.pens.namiapp.ui.detailbackcheckinglocation.DetailBackCheckingLocationActivity;
import com.androiddeveloper.pens.namiapp.utils.LocationUtils;
import com.androiddeveloper.pens.namiapp.utils.LocationValidatorUtil;
import com.androiddeveloper.pens.namiapp.utils.SingleShotLocationProvider;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.Target;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BackCheckingLocationActivity extends BaseActivity implements BackCheckingLocationMvpView {

    @BindView(R.id.btn_go_backchecking)
    Button btnGoBackchecking;

    @Inject
    BackCheckingLocationMvpPresenter<BackCheckingLocationMvpView> mPresenter;

    boolean canGoBackchecking = false;

    Location mLocation;

    public static Intent getStartIntent(Context context) {
        return new Intent(context, BackCheckingLocationActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_back_checking_location);

        getActivityComponent().inject(this);

        if (isNetworkAvailable()) {
            setUnBinder(ButterKnife.bind(this));

            mPresenter.onAttach(this);

            setUp();
        }
    }

    @Override
    protected void setUp() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Back Checking");

        Target viewTarget = new ViewTarget(R.id.tv_bulan_jumlah_poi_outlet_backchecking_location_oss_osk, this);
        new ShowcaseView.Builder(this)
                .setTarget(viewTarget)
                .withMaterialShowcase()
                .setContentTitle("Back Checking & Feedback")
                .setContentText("Back Checking untuk total ter-backchecking & Feedback untuk total ter-Feedback dari AOC")
                .setStyle(R.style.CustomShowcaseTheme2)
                .singleShot(60)
                .build();

        mProgressDialog = new ProgressDialog(this);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean isMockingLocation() {
        if (LocationValidatorUtil.isLocationFromMockProvider(this, LocationUtils.getCachedLocation())){
            LocationValidatorUtil.getListOfFakeLocationApps(this);
            Toast.makeText(this, "Mock Setting On", Toast.LENGTH_SHORT).show();
            return true;
        }else{
            return false;
        }
    }

    @Override
    public void updateMenuKeBackcheckingLocationList() {
        btnGoBackchecking.setOnClickListener(v -> {
            Intent i = new Intent(getApplicationContext(), BackCheckingLocationListActivity.class);
            startActivity(i);
        });
    }

    @Override
    public void updateMenuKeBackcheckingLocationDetail(BackCheckingLocationListResponse response) {
        btnGoBackchecking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), DetailBackCheckingLocationActivity.class);
                i.putExtra("detail",response);
                startActivity(i);
            }
        });
    }

    @Override
    public void setUserBackcheckingStatus(BackcheckingMain response) {
        TextView todayInstitutionBackcheckingCount = findViewById(R.id.tv_hari_jumlah_poi_outlet_backchecking_location_institution);
        TextView todayOssOskBackcheckingCount = findViewById(R.id.tv_hari_jumlah_poi_outlet_backchecking_location_oss_osk);
        TextView monthInstitutionBackcheckingCount = findViewById(R.id.tv_bulan_jumlah_poi_outlet_backchecking_location_institution);
        TextView monthOssOskBackcheckingCount = findViewById(R.id.tv_bulan_jumlah_poi_outlet_backchecking_location_oss_osk);

        todayInstitutionBackcheckingCount.setText("(" + response.getTotalBackcheckingInstitutionPtd() +" | " + response.getTotalFeedbackInstitutionPtd()+ ")");
        todayOssOskBackcheckingCount.setText("(" + response.getTotalBackcheckingOskPtd() + " | " + response.getTotalFeedbackOskPtd() +  ")");
        monthInstitutionBackcheckingCount.setText("(" + response.getTotalBackcheckingInstitutionMtd() + " | " + response.getTotalFeedbackInstitutionMtd() + ")");
        monthOssOskBackcheckingCount.setText("(" + response.getTotalBackcheckingOskMtd() + " | " + response.getTotalFeedbackOskMtd() + ")");
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isMockingLocation()){
            showMessage("Maaf mock location Terdeteksi");
            canGoBackchecking = false;
            return;
        }else{
            canGoBackchecking = true;
            try{
                LocationUtils.requestLocationUpdateWithCheckPermission(this, new LocationUtils.IUserLocationListener() {
                    @Override
                    public void onStateChange(State state) {

                    }

                    @Override
                    public void onLocationChanged(Location location) {
                        if (LocationValidatorUtil.isLocationFromMockProvider(BackCheckingLocationActivity.this,location)){
                            showMessage("Maaf mock location terdeteksi");
                            return;
                        }
                        if (location != null){
                            mPresenter.getCheckoutStatus(
                                    String.valueOf(location.getLatitude()),
                                    String.valueOf(location.getLongitude()));
                            mLocation = location;
                        }else {
                            showMessage("Tidak bisa mendapatkan lokasi dari GPS coba beberapa saat lagi");
                        }
                    }

                    @Override
                    public void onStatusChanged(String s, int i, Bundle bundle) {

                    }

                    @Override
                    public void onProviderEnabled(String s) {

                    }

                    @Override
                    public void onProviderDisabled(String s) {

                    }

                });
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public void showLoading(String message) {
        super.showLoading(message);
        mProgressDialog.setMessage(message);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        try{
            if (mProgressDialog != null ){
                mProgressDialog.dismiss();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }
}