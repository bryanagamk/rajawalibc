package com.androiddeveloper.pens.namiapp.ui.backcheckinglocation;

import com.androiddeveloper.pens.namiapp.di.PerActivity;
import com.androiddeveloper.pens.namiapp.ui.base.MvpPresenter;

@PerActivity
public interface BackCheckingLocationMvpPresenter<V extends BackCheckingLocationMvpView> extends MvpPresenter<V> {


    void getCheckoutStatus(String latitude, String longitude);

    void getBackcheckingMainStatus();
}
