package com.androiddeveloper.pens.namiapp.ui.backcheckinglocation;

import com.androiddeveloper.pens.namiapp.data.network.model.BackCheckingLocationListResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.BackcheckingMain;
import com.androiddeveloper.pens.namiapp.ui.base.MvpView;

public interface BackCheckingLocationMvpView extends MvpView {

    boolean isMockingLocation();

    void updateMenuKeBackcheckingLocationList();

    void updateMenuKeBackcheckingLocationDetail(BackCheckingLocationListResponse response);

    void setUserBackcheckingStatus(BackcheckingMain response);
}
