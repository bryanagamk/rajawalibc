package com.androiddeveloper.pens.namiapp.ui.backcheckinglocation;

import android.util.Log;

import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.data.network.model.BackCheckingLocationListResponse;
import com.androiddeveloper.pens.namiapp.ui.base.BasePresenter;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class BackCheckingLocationPresenter<V extends BackCheckingLocationMvpView> extends BasePresenter<V>
        implements BackCheckingLocationMvpPresenter<V> {

    private static final String TAG = "BackCheckingLocationPresenter";

    @Inject
    public BackCheckingLocationPresenter(DataManager dataManager,
                                       CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);
    }

    @Override
    public void onAttach(V mvpView) {
        super.onAttach(mvpView);
        getBackcheckingMainStatus();
    }

    @Override
    public void getCheckoutStatus(String latitude, String longitude) {
        String userId = getDataManager().getCurrentUserId();
        getCompositeDisposable().add(getDataManager().getCheckoutStatus(userId, latitude, longitude)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(backcheckingStatusResponse -> {
                    if (backcheckingStatusResponse.isSuccess()){
                        if (!isViewAttached()){
                            return;
                        }
                        getMvpView().updateMenuKeBackcheckingLocationList();
                        getMvpView().hideLoading();
                    }else{
                        if (!isViewAttached()){
                            return;
                        }
                        BackCheckingLocationListResponse response = new BackCheckingLocationListResponse();
                        response.setLocationId(backcheckingStatusResponse.getBackcheckingMain().getLocationId());
                        response.setLocationType(backcheckingStatusResponse.getBackcheckingMain().getLocationType());
                        response.setInstitutionName(backcheckingStatusResponse.getBackcheckingMain().getInstitutionName());

                        getMvpView().hideLoading();

                        Log.d("BACKCHECKING MAIN  :", backcheckingStatusResponse.getBackcheckingMain().toString());
                        getMvpView().hideLoading();
                        getMvpView().updateMenuKeBackcheckingLocationDetail(backcheckingStatusResponse.getBackcheckingMain());
                        getMvpView().hideLoading();
                    }
                },throwable -> {
                    if (!isViewAttached()){
                        return;
                    }
                    Log.d("Error : ", throwable.toString());
                    getMvpView().hideLoading();
                    getMvpView().showMessage("Gagal mendapatkan checkout status ke internet");
                    getCheckoutStatus(latitude, longitude);
                    getMvpView().hideLoading();
                }));
    }

    @Override
    public void getBackcheckingMainStatus() {
        String userId = getDataManager().getCurrentUserId();
        getCompositeDisposable().add(getDataManager().getBackcheckingMain(userId)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(backcheckingMainResponse -> {
            if (backcheckingMainResponse.isSuccess()){
                if (!isViewAttached()){
                    return;
                }
                getMvpView().setUserBackcheckingStatus(backcheckingMainResponse.getBackcheckingMain());
            }else {
                if (!isViewAttached()){
                    return;
                }
                getMvpView().showMessage("Terjadi kesalahan pada client");
            }
        },throwable -> {
            if (!isViewAttached()){
                return;
            }
            getMvpView().showMessage("Tidak bisa terhubung dengan server");
            getMvpView().showErrorPage("Tidak bisa terhubung dengan server");
        }));
    }
}
