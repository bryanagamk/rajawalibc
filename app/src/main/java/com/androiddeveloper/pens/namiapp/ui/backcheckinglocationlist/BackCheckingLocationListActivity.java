package com.androiddeveloper.pens.namiapp.ui.backcheckinglocationlist;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.data.network.model.BackCheckingLocationListResponse;
import com.androiddeveloper.pens.namiapp.ui.backcheckinglocation.BackCheckingLocationActivity;
import com.androiddeveloper.pens.namiapp.ui.base.BaseActivity;
import com.androiddeveloper.pens.namiapp.ui.detailbackcheckinglocation.DetailBackCheckingLocationActivity;
import com.androiddeveloper.pens.namiapp.ui.feedback.FeedbackAdapter;
import com.androiddeveloper.pens.namiapp.utils.LocationUtils;
import com.androiddeveloper.pens.namiapp.utils.LocationValidatorUtil;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BackCheckingLocationListActivity extends BaseActivity implements BackCheckingLocationListMvpView, BackCheckingLocationListAdapter.Callback {

    @Inject
    BackCheckingLocationListMvpPresenter<BackCheckingLocationListMvpView> mPresenter;

    @Inject
    BackCheckingLocationListAdapter mBackCheckingLocationListAdapter;

    @Inject
    LinearLayoutManager mLayoutManager;

    @BindView(R.id.rv_backchecking_location_list)
    RecyclerView recyclerViewBackcheckingLocationList;

    @BindView(R.id.search_view_surrounding)
    MaterialSearchView searchViewSurrounding;

    @BindView(R.id.toolbarSearchSurrounding)
    Toolbar toolbarSearchSurrounding;


    public static Activity bcLocationListActivity;

    Handler handler;

    Runnable getLocationRunnable;

    private List<BackCheckingLocationListResponse> mBackCheckingLocationListResponseList;

    Location mLocation;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, BackCheckingLocationListActivity.class);
        return intent;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_back_checking_location_list);

        bcLocationListActivity = this;

        getActivityComponent().inject(this);

        if (isNetworkAvailable()) {
            setUnBinder(ButterKnife.bind(this));

            mPresenter.onAttach(this);

            setUp();
        }
    }

    @Override
    protected void setUp() {
        setSupportActionBar(toolbarSearchSurrounding);
        getSupportActionBar().setTitle(getResources().getString(R.string.surrounding));

        toolbarSearchSurrounding.setNavigationIcon(R.drawable.ic_action_navigation_arrow_back_inverted);
        toolbarSearchSurrounding.setTitleTextColor(getResources().getColor(R.color.white));
        searchViewSurrounding.setEllipsize(true);
        toolbarSearchSurrounding.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                if (mLocation != null){
                    mPresenter.getBackCheckingLocationList(BackCheckingLocationListActivity.this,
                            String.valueOf(mLocation.getLatitude()),
                            String.valueOf(mLocation.getLongitude()));
                    mLocation = mLocation;
                }else {
                    showMessage("Tidak bisa mendapatkan lokasi dari GPS coba beberapa saat lagi");
                }
            }
        });

        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewBackcheckingLocationList.setLayoutManager(mLayoutManager);
        recyclerViewBackcheckingLocationList.setItemAnimator(new DefaultItemAnimator());
        recyclerViewBackcheckingLocationList.setAdapter(mBackCheckingLocationListAdapter);
        mBackCheckingLocationListAdapter.setCallback(this);

        handler = new Handler();
        int delay = 10000; //milliseconds

        showLoading("Mendapatkan Lokasi");
        shutDownLoading(25000,"Gagal Mendapatkan Lokasi");
        LocationUtils.requestLocationUpdateWithCheckPermission(this, new LocationUtils.IUserLocationListener() {
            @Override
            public void onStateChange(State state) {

            }

            @Override
            public void onLocationChanged(Location location) {
                if (LocationValidatorUtil.isLocationFromMockProvider(BackCheckingLocationListActivity.this,location)){
                    showMessage("Maaf mock location terdeteksi");
                    return;
                }
                if (location != null){
                    mPresenter.getBackCheckingLocationList(BackCheckingLocationListActivity.this,
                            String.valueOf(location.getLatitude()),
                            String.valueOf(location.getLongitude()));
                    mLocation = location;
                }else {
                    showMessage("Tidak bisa mendapatkan lokasi dari GPS coba beberapa saat lagi");
                }
                hideLoading();
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }

        });




        searchViewSurrounding.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                //Do some magic
                try{
                    if (TextUtils.isEmpty(query)){
                        if (mLocation != null){
                            mPresenter.getBackCheckingLocationList(BackCheckingLocationListActivity.this,
                                    String.valueOf(mLocation.getLatitude()),
                                    String.valueOf(mLocation.getLongitude()));
                            mLocation = mLocation;
                        }else {
                            showMessage("Tidak bisa mendapatkan lokasi dari GPS coba beberapa saat lagi");
                        }
                    }else {
                        if (mBackCheckingLocationListAdapter.getItemCount() < 0){
                            Toast.makeText(getApplicationContext(), "Data tidak ditemukan!", Toast.LENGTH_SHORT).show();
                        }else {
                            mBackCheckingLocationListAdapter.filterByNameLocation(query);
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                try{
                    if (TextUtils.isEmpty(newText)){
                        if (mLocation != null){
                            mPresenter.getBackCheckingLocationList(BackCheckingLocationListActivity.this,
                                    String.valueOf(mLocation.getLatitude()),
                                    String.valueOf(mLocation.getLongitude()));
                            mLocation = mLocation;
                        }else {
                            showMessage("Tidak bisa mendapatkan lokasi dari GPS coba beberapa saat lagi");
                        }
                    }else {
                        if (mBackCheckingLocationListAdapter.getItemCount() < 0){
                            Toast.makeText(getApplicationContext(), "Data tidak ditemukan!", Toast.LENGTH_SHORT).show();
                        }else {
                            mBackCheckingLocationListAdapter.filterByNameLocation(newText);
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                return true;
            }
        });

        searchViewSurrounding.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
                //Do some magic
            }

            @Override
            public void onSearchViewClosed() {
                //Do some magic
            }
        });
    }

    @Override
    public void updateBackCheckingLocationList(List<BackCheckingLocationListResponse> backCheckingLocationListResponses) {
        Log.d("@@@@", "updateBackCheckingLocationList: " +backCheckingLocationListResponses.size());
        mBackCheckingLocationListResponseList = backCheckingLocationListResponses;
        mBackCheckingLocationListAdapter.addItems(backCheckingLocationListResponses);
        mBackCheckingLocationListAdapter.notifyDataSetChanged();
    }

    @Override
    public void openGoBackcheckingActivity() {
        Intent intent = BackCheckingLocationActivity.getStartIntent(this);
        intent.addFlags(intent.FLAG_ACTIVITY_CLEAR_TOP | intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        if (searchViewSurrounding.isSearchOpen()) {
            if (mLocation != null){
                mPresenter.getBackCheckingLocationList(BackCheckingLocationListActivity.this,
                        String.valueOf(mLocation.getLatitude()),
                        String.valueOf(mLocation.getLongitude()));
                mLocation = mLocation;
            }else {
                showMessage("Tidak bisa mendapatkan lokasi dari GPS coba beberapa saat lagi");
            }
            searchViewSurrounding.closeSearch();
        } else {
            if (mLocation != null){
                mPresenter.getBackCheckingLocationList(BackCheckingLocationListActivity.this,
                        String.valueOf(mLocation.getLatitude()),
                        String.valueOf(mLocation.getLongitude()));
                mLocation = mLocation;
            }else {
                showMessage("Tidak bisa mendapatkan lokasi dari GPS coba beberapa saat lagi");
            }
            openGoBackcheckingActivity();
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_surrounding_poi_outlet, menu);

        MenuItem item = menu.findItem(R.id.action_search_surrounding);
        searchViewSurrounding.setMenuItem(item);

        return true;
    }

    @Override
    public void showLoading(String message) {
        super.showLoading(message);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(message);
        mProgressDialog.show();
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        if (mProgressDialog != null ){
            mProgressDialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        handler.removeCallbacks(getLocationRunnable);
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onItemLocationListClick(int position) {
        Intent intent = DetailBackCheckingLocationActivity.getStartIntent(this);
        intent.putExtra("detail",mBackCheckingLocationListResponseList.get(position));
        if (mLocation != null){
            intent.putExtra("latitude",mLocation.getLatitude());
            intent.putExtra("longitude",mLocation.getLongitude());
        }
        startActivity(intent);
    }
}
