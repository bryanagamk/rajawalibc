package com.androiddeveloper.pens.namiapp.ui.backcheckinglocationlist;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.data.network.model.BackCheckingLocationListResponse;
import com.androiddeveloper.pens.namiapp.ui.base.BaseViewHolder;
import com.androiddeveloper.pens.namiapp.ui.detailbackcheckinglocation.DetailBackCheckingLocationActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BackCheckingLocationListAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private BackCheckingLocationListAdapter.Callback mCallback;
    private List<BackCheckingLocationListResponse> mBackCheckingLocationListResponseList;
    private List<BackCheckingLocationListResponse> mBackcheckingListDefault;

    public BackCheckingLocationListAdapter(List<BackCheckingLocationListResponse> backCheckingLocationListResponseList) {
        mBackCheckingLocationListResponseList = backCheckingLocationListResponseList;
    }

    public void setCallback(BackCheckingLocationListAdapter.Callback callback) {
        mCallback = callback;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new BackCheckingLocationListAdapter.ViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_back_checking_poi_outlet_view, parent, false));
            case VIEW_TYPE_EMPTY:
            default:
                return new BackCheckingLocationListAdapter.EmptyViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_view_location, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mBackCheckingLocationListResponseList != null && mBackCheckingLocationListResponseList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public int getItemCount() {
        if (mBackCheckingLocationListResponseList != null && mBackCheckingLocationListResponseList.size() > 0) {
            return mBackCheckingLocationListResponseList.size();
        } else {
            return 1;
        }
    }

    public void addItems(List<BackCheckingLocationListResponse> backCheckingLocationListResponseList) {
        mBackCheckingLocationListResponseList.clear();
        mBackCheckingLocationListResponseList.addAll(backCheckingLocationListResponseList);
        mBackCheckingLocationListResponseList = backCheckingLocationListResponseList;
        notifyDataSetChanged();
    }

    public void filterByNameLocation(String query){
        List<BackCheckingLocationListResponse> filteredSurrounding = new ArrayList<>();
        for (BackCheckingLocationListResponse surrounding : mBackCheckingLocationListResponseList){
            if (surrounding.getInstitutionName().toLowerCase().contains(query.toLowerCase()) || surrounding.getLocationType().toLowerCase().contains(query.toLowerCase())){
                filteredSurrounding.add(surrounding);
            }
        }
        mBackCheckingLocationListResponseList.clear();
        mBackCheckingLocationListResponseList.addAll(filteredSurrounding);
        notifyDataSetChanged();
    }

    public interface Callback {
        void onItemLocationListClick(int position);
    }

    public class ViewHolder extends BaseViewHolder {


        TextView tvNamaPoi;
        TextView tvLatitudePoi;
        TextView tvLongitudePoi;
        TextView tvInrange;
        LinearLayout linearLayout;
        View view;
        ImageView ivIcon;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            tvNamaPoi = itemView.findViewById(R.id.tv_nama_poi_outlet_backchecking_view);
            tvLatitudePoi = itemView.findViewById(R.id.tv_latitude_poi_outlet_backchecking_view);
            tvLongitudePoi = itemView.findViewById(R.id.tv_longitude_poi_outlet_backchecking_view);
            tvInrange = itemView.findViewById(R.id.tv_inrange_poi_outlet_backchecking_view);
            linearLayout = itemView.findViewById(R.id.line_info_backchecking_poi_outlet_view);
            view = itemView.findViewById(R.id.v_view_surrounding);
            ivIcon = itemView.findViewById(R.id.iv_kampus_ossosk_backchecking);
        }

        protected void clear() {

        }

        public void onBind(int position) {
            super.onBind(position);

            final BackCheckingLocationListResponse backCheckingLocationListResponse = mBackCheckingLocationListResponseList.get(position);
            String typePoi = backCheckingLocationListResponse.getLocationType();
            String namaPoi = backCheckingLocationListResponse.getInstitutionName();
            String latitudePoi = backCheckingLocationListResponse.getLatitude();
            String longitudePoi = backCheckingLocationListResponse.getLongitude();
            String distancePoi = backCheckingLocationListResponse.getDistance();

            if (typePoi.equals("OSS_OSK")){
                tvNamaPoi.setText(namaPoi.replace("OSS/OSK - ", ""));
                ivIcon.setImageResource(R.drawable.simcard);
                view.setBackgroundResource(R.color.red);
                linearLayout.setBackgroundResource(R.drawable.background_ossosk);
            }else{
                tvNamaPoi.setText(namaPoi.replace("SEKOLAH/KAMPUS - ", ""));
                ivIcon.setImageResource(R.drawable.bus);
                view.setBackgroundResource(R.color.orange);
                linearLayout.setBackgroundResource(R.drawable.background_institution);
            }

            tvLatitudePoi.setText(latitudePoi);
            tvLongitudePoi.setText(longitudePoi);
            tvInrange.setText(distancePoi + " Meter");

            itemView.setOnClickListener(v->{
                if (mCallback != null){
                    mCallback.onItemLocationListClick(position);
                }
            });

        }
    }

    public class EmptyViewHolder extends BaseViewHolder {

        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void clear() {

        }
    }
}
