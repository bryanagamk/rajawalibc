package com.androiddeveloper.pens.namiapp.ui.backcheckinglocationlist;

import android.app.Activity;

import com.androiddeveloper.pens.namiapp.di.PerActivity;
import com.androiddeveloper.pens.namiapp.ui.base.MvpPresenter;

@PerActivity
public interface BackCheckingLocationListMvpPresenter<V extends BackCheckingLocationListMvpView> extends MvpPresenter<V> {
    void getBackCheckingLocationList(Activity activity, String latitude, String longitude);
}
