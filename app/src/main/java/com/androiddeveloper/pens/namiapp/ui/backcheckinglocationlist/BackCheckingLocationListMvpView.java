package com.androiddeveloper.pens.namiapp.ui.backcheckinglocationlist;

import com.androiddeveloper.pens.namiapp.data.network.model.BackCheckingLocationListResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.FeedbackResponse;
import com.androiddeveloper.pens.namiapp.ui.base.MvpView;

import java.util.List;

public interface BackCheckingLocationListMvpView extends MvpView {

    void updateBackCheckingLocationList(List<BackCheckingLocationListResponse> backCheckingLocationListResponses);

    void openGoBackcheckingActivity();
}
