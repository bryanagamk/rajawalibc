package com.androiddeveloper.pens.namiapp.ui.backcheckinglocationlist;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.ui.base.BasePresenter;
import com.androiddeveloper.pens.namiapp.utils.StartupConfig;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class BackCheckingLocationListPresenter<V extends BackCheckingLocationListMvpView> extends BasePresenter<V>
        implements BackCheckingLocationListMvpPresenter<V> {
    private static final String TAG = "BackCheckingLocationListPresenter";

    @Inject
    public BackCheckingLocationListPresenter(DataManager dataManager,
                             CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);
    }

    @Override
    public void getBackCheckingLocationList(Activity activity, String latitude, String longitude) {

        Log.v("Tag", "Terpanggil");
        Log.d("Debug : ","Latitude" + latitude + "Longitude : " + longitude);

        if (StartupConfig.getInstance(getDataManager(),false) == null){
            Toast.makeText(activity, "Terjadi Kesalahan Saat melakukan request Silahkan coba beberapa saat lagi", Toast.LENGTH_SHORT).show();
            return;
        }
        String userType = getDataManager().getCurrentUserType().toLowerCase();
        String distinstitution = StartupConfig.getInstance(getDataManager(),false).getBackcheckingInstitutionRadius();
        String distOssOsk = StartupConfig.getInstance(getDataManager(),false).getBackcheckingOssOskRadius();

        Log.d("Current Location", String.format("latitude : %s, longitude : %s, distinstitution :  %s , distossosk : %s",
                latitude,
                longitude,
                distinstitution,
                distOssOsk));

        Log.d("Debug : ","Latitude" + latitude + "Longitude : " + longitude);

        if (userType.equals("mgt1")){
            distOssOsk = "100000000";
            distinstitution = "100000000";
        }

        getCompositeDisposable().add(getDataManager()
                .getBackCheckingLocationList(latitude, longitude,distinstitution,distOssOsk)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(backCheckingLocationListResponses -> {
                    if (!isViewAttached()){
                        return;
                    }
                    if (backCheckingLocationListResponses != null && backCheckingLocationListResponses.size() != 0){
                        getMvpView().updateBackCheckingLocationList(backCheckingLocationListResponses);
                        getMvpView().hideLoading();
                    }else{
                        getMvpView().showMessage("Tidak ada POI & Outlet Surrounding");
                        getMvpView().hideLoading();
                    }
                },throwable -> {
                    if (!isViewAttached()){
                        return;
                    }
                    getMvpView().showMessage("Tidak bisa terhubung ke server");
                    getMvpView().hideLoading();
                    Log.d("Backchecking : " ,throwable.toString());
                }));
    }
}
