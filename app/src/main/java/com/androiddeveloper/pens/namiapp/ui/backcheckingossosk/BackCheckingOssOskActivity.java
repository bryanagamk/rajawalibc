package com.androiddeveloper.pens.namiapp.ui.backcheckingossosk;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.data.network.model.BackCheckingLocationListResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.Backchecking;
import com.androiddeveloper.pens.namiapp.data.network.rest.ApiClient;
import com.androiddeveloper.pens.namiapp.ui.backchecking.BaseBackcheckingActivity;
import com.androiddeveloper.pens.namiapp.ui.backcheckinglocation.BackCheckingLocationActivity;
import com.androiddeveloper.pens.namiapp.ui.backcheckinglocationlist.BackCheckingLocationListActivity;
import com.androiddeveloper.pens.namiapp.ui.backcheckingsekolahkampus.BackCheckingSekolahKampusActivity;
import com.androiddeveloper.pens.namiapp.ui.detailbackcheckinglocation.DetailBackCheckingLocationActivity;
import com.androiddeveloper.pens.namiapp.utils.LocationUtils;
import com.androiddeveloper.pens.namiapp.utils.SingleShotLocationProvider;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ceylonlabs.imageviewpopup.ImagePopup;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BackCheckingOssOskActivity extends BaseBackcheckingActivity implements BackCheckingOssOskMvpView {

    @Inject
    BackCheckingOssOskMvpPresenter<BackCheckingOssOskMvpView> mPresenter;
    // id spinner
    @BindView(R.id.brand_spinner)
    Spinner brandSpinner;
    @BindView(R.id.brand_frontliner_spinner)
    Spinner brandFrontlinerSpinner;
    // id radiobutton sesuai
    @BindView(R.id.rb_sesuai_kunjungan_oss_osk)
    RadioButton rbSesuaiKunjunganOssOsk;
    @BindView(R.id.rb_sesuai_sales_oss_osk)
    RadioButton rbSesuaiSalesOssOsk;
    @BindView(R.id.rb_sesuai_harga_tp)
    RadioButton rbSesuaiHargaTP;
    @BindView(R.id.rb_sesuai_outlet_brand)
    RadioButton rbSesuaiOutletBrand;
    @BindView(R.id.tv_pilih_brand)
    TextView tvPilihBrand;
    @BindView(R.id.tv_pilih_brand_frontliner)
    TextView tvPilihBrandFrontliner;
    @BindView(R.id.rb_ya_frontliner)
    RadioButton rbYaFrontliner;
    @BindView(R.id.rb_paham_program_tsel)
    RadioButton rbPahamProgramTsel;
    @BindView(R.id.rb_kurang_program_tsel)
    RadioButton rbKurangPahamProgramTsel;
    @BindView(R.id.rb_tidak_program_tsel)
    RadioButton rbRTidakPahamProgramTsel;
    @BindView(R.id.rb_youth_usia_pelanggan)
    RadioButton rbYouthUsiaPelanggan;
    @BindView(R.id.rb_non_youth_usia_pelanggan)
    RadioButton rbNonYouthUsiaPelanggan;
    @BindView(R.id.rb_umum_usia_pelanggan)
    RadioButton rbUmumUsiaPelanggan;
    // id radiobutton tidak sesuai
    @BindView(R.id.rb_tidak_sesuai_kunjungan_oss_osk)
    RadioButton rbTidakSesuaiKunjunganOssOsk;
    @BindView(R.id.rb_tidak_sesuai_sales_oss_osk)
    RadioButton rbTidakSesuaiSalesOssOsk;
    @BindView(R.id.rb_tidak_sesuai_harga_tp)
    RadioButton rbTidakSesuaiHargaTP;
    @BindView(R.id.rb_tidak_sesuai_outlet_brand)
    RadioButton rbTidakSesuaiOutletBrand;
    @BindView(R.id.rb_tidak_frontliner)
    RadioButton rbTidakFrontliner;
    // id note
    @BindView(R.id.et_note_kunjungan_oss_osk)
    EditText etNoteKunjunganOssOsk;
    @BindView(R.id.et_note_sales_oss_osk)
    EditText etNoteSalesOssOsk;
    @BindView(R.id.et_note_harga_tp)
    EditText etNoteHargaTP;
    @BindView(R.id.et_note_broadband_share)
    EditText etNoteShare;
    @BindView(R.id.et_note_matpro_ossosk)
    EditText etNoteMatpro;
    @BindView(R.id.et_note_produk_terlaku)
    EditText etNoteProdukTerlaku;
    @BindView(R.id.et_note_program_tsel)
    EditText etNoteProgramTsel;
    // id broadband share
    @BindView(R.id.et_bs_tsel)
    EditText etBsTsel;
    @BindView(R.id.et_bs_ooredoo)
    EditText etBsOoredoo;
    @BindView(R.id.et_bs_xl)
    EditText etBsXl;
    @BindView(R.id.et_bs_axis)
    EditText etBsAxis;
    @BindView(R.id.et_bs_3)
    EditText etBs3;
    @BindView(R.id.et_bs_others)
    EditText etBsOthers;
    // id display share
    @BindView(R.id.et_display_tsel)
    EditText etDisplayTsel;
    @BindView(R.id.et_display_ooredoo)
    EditText etDisplayOoredoo;
    @BindView(R.id.et_display_xl)
    EditText etDisplayXl;
    @BindView(R.id.et_display_axis)
    EditText etDisplayAxis;
    @BindView(R.id.et_display_3)
    EditText etDisplay3;
    @BindView(R.id.et_display_others)
    EditText etDisplayOthers;
    // id recharge share
    @BindView(R.id.et_recharge_tsel)
    EditText etRechargeTsel;
    @BindView(R.id.et_recharge_ooredoo)
    EditText etRechargeOoredoo;
    @BindView(R.id.et_recharge_xl)
    EditText etRechargeXl;
    @BindView(R.id.et_recharge_axis)
    EditText etRechargeAxis;
    @BindView(R.id.et_recharge_3)
    EditText etRecharge3;
    @BindView(R.id.et_recharge_others)
    EditText etRechargeOthers;
    // id button kirim oss osk
    @BindView(R.id.btn_send_backchecking_oss_osk)
    Button btnSendBackcheckingOssOsk;
    // id nama dan phone frontliner , question
    @BindView(R.id.et_nama_frontliner)
    EditText etNamaFrontliner;
    @BindView(R.id.et_contact_frontliner)
    EditText etContactFrontliner;
    @BindView(R.id.line_question_frontliner_produk)
    LinearLayout lineQuestionFrontlinerProduk;
    LinearLayout cvQuestionFrontlinerProduk;

    private boolean acceptable = true;
    private Backchecking mBackchecking;
    ProgressDialog progressDialog;
    private String namaAocPjp;
    private String nomorAocPjp;
    private BackCheckingLocationListResponse mBackcheckingDetail;
    private String dominasiBrandingProduct;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, BackCheckingOssOskActivity.class);
        return intent;
    }
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_back_checking_oss_osk);

        getActivityComponent().inject(this);

        if (isNetworkAvailable()){
            setUnBinder(ButterKnife.bind(this));

            mPresenter.onAttach(this);

            setUp();
        }
    }

    @Override
    protected void setUp() {
        // default rb
        etNoteKunjunganOssOsk.setText("Sesuai");
        etNoteSalesOssOsk.setText("Sesuai");
        etNoteHargaTP.setText("Sesuai");

        // default edittext kontak frontliner
        etContactFrontliner.setTransformationMethod(null);

        progressDialog = new ProgressDialog(this);


        getBackcheckingDataFromServer(getIntent().getStringExtra("location_id"));
        // backpress
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Back Checking OSK");

        // radiobutton validasi kunjungan terakhir
        rbSesuaiKunjunganOssOsk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    etNoteKunjunganOssOsk.setEnabled(false);
                    etNoteKunjunganOssOsk.setFocusable(false);
                    etNoteKunjunganOssOsk.setFocusableInTouchMode(false);
                    etNoteKunjunganOssOsk.setHint("Note disable");
                    etNoteKunjunganOssOsk.setText("Sesuai");
                    etNoteKunjunganOssOsk.setVisibility(View.GONE);
                    rbSesuaiKunjunganOssOsk.getText().toString();
                    rbSesuaiKunjunganOssOsk.setChecked(true);
                    rbTidakSesuaiKunjunganOssOsk.setChecked(false);
                }
            }
        });
        rbTidakSesuaiKunjunganOssOsk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    etNoteKunjunganOssOsk.setEnabled(true);
                    etNoteKunjunganOssOsk.setFocusable(true);
                    etNoteKunjunganOssOsk.setFocusableInTouchMode(true);
                    etNoteKunjunganOssOsk.setHint("Catatan tentang kunjungan");
                    etNoteKunjunganOssOsk.setText("");
                    etNoteKunjunganOssOsk.setVisibility(View.VISIBLE);
                    rbTidakSesuaiKunjunganOssOsk.getText().toString();
                    rbTidakSesuaiKunjunganOssOsk.setChecked(true);
                    rbSesuaiKunjunganOssOsk.setChecked(false);
                }
            }
        });

        // radiobutton validasi sales oss osk
        rbSesuaiSalesOssOsk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    etNoteSalesOssOsk.setEnabled(false);
                    etNoteSalesOssOsk.setFocusable(false);
                    etNoteSalesOssOsk.setFocusableInTouchMode(false);
                    etNoteSalesOssOsk.setHint("Note disable");
                    etNoteSalesOssOsk.setText("Sesuai");
                    etNoteSalesOssOsk.setVisibility(View.GONE);
                    rbSesuaiSalesOssOsk.getText().toString();
                    rbSesuaiSalesOssOsk.setChecked(true);
                    rbTidakSesuaiSalesOssOsk.setChecked(false);
                }
            }
        });
        rbTidakSesuaiSalesOssOsk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    etNoteSalesOssOsk.setEnabled(true);
                    etNoteSalesOssOsk.setFocusable(true);
                    etNoteSalesOssOsk.setFocusableInTouchMode(true);
                    etNoteSalesOssOsk.setHint("Catatan tentang sales through perdana");
                    etNoteSalesOssOsk.setText("");
                    etNoteSalesOssOsk.setVisibility(View.VISIBLE);
                    rbTidakSesuaiSalesOssOsk.getText().toString();
                    rbTidakSesuaiSalesOssOsk.setChecked(true);
                    rbSesuaiSalesOssOsk.setChecked(false);
                }
            }
        });

        // radiobutton validasi harga tp
        rbSesuaiHargaTP.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    etNoteHargaTP.setEnabled(false);
                    etNoteHargaTP.setFocusable(false);
                    etNoteHargaTP.setFocusableInTouchMode(false);
                    etNoteHargaTP.setHint("Note disable");
                    etNoteHargaTP.setText("Sesuai");
                    etNoteHargaTP.setVisibility(View.GONE);
                    rbSesuaiHargaTP.getText().toString();
                    rbSesuaiHargaTP.setChecked(true);
                    rbTidakSesuaiHargaTP.setChecked(false);
                }
            }
        });
        rbTidakSesuaiHargaTP.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    etNoteHargaTP.setEnabled(true);
                    etNoteHargaTP.setFocusable(true);
                    etNoteHargaTP.setFocusableInTouchMode(true);
                    etNoteHargaTP.setHint("Catatan tentang harga TP");
                    etNoteHargaTP.setText("");
                    etNoteHargaTP.setVisibility(View.VISIBLE);
                    rbTidakSesuaiHargaTP.getText().toString();
                    rbTidakSesuaiHargaTP.setChecked(true);
                    rbSesuaiHargaTP.setChecked(false);
                }
            }
        });
        // radio button branding
        rbSesuaiOutletBrand.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    rbSesuaiOutletBrand.getText().toString();
                    brandSpinner.setVisibility(View.GONE);
                    tvPilihBrand.setVisibility(View.GONE);
                    rbSesuaiOutletBrand.setChecked(true);
                    rbTidakSesuaiOutletBrand.setChecked(false);
                }
            }
        });
        rbTidakSesuaiOutletBrand.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    rbTidakSesuaiOutletBrand.getText().toString();
                    brandSpinner.setVisibility(View.VISIBLE);
                    tvPilihBrand.setVisibility(View.VISIBLE);
                    rbTidakSesuaiOutletBrand.setChecked(true);
                    rbSesuaiOutletBrand.setChecked(false);
                }
            }
        });
        // radio button frontliner
        rbYaFrontliner.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    rbYaFrontliner.getText().toString();
                    brandFrontlinerSpinner.setVisibility(View.VISIBLE);
                    tvPilihBrandFrontliner.setVisibility(View.VISIBLE);
                    lineQuestionFrontlinerProduk.setVisibility(View.VISIBLE);
                    rbYaFrontliner.setChecked(true);
                    rbTidakFrontliner.setChecked(false);
                }
            }
        });
        rbTidakFrontliner.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    rbTidakFrontliner.getText().toString();
                    brandFrontlinerSpinner.setVisibility(View.GONE);
                    tvPilihBrandFrontliner.setVisibility(View.GONE);
                    lineQuestionFrontlinerProduk.setVisibility(View.GONE);
                    rbTidakFrontliner.setChecked(true);
                    rbYaFrontliner.setChecked(false);
                }
            }
        });
        // radio button program telkomsel
        rbPahamProgramTsel.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    rbPahamProgramTsel.getText().toString();
                    rbPahamProgramTsel.setChecked(true);
                    rbKurangPahamProgramTsel.setChecked(false);
                    rbRTidakPahamProgramTsel.setChecked(false);
                }
            }
        });
        rbKurangPahamProgramTsel.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    rbKurangPahamProgramTsel.getText().toString();
                    rbPahamProgramTsel.setChecked(false);
                    rbKurangPahamProgramTsel.setChecked(true);
                    rbRTidakPahamProgramTsel.setChecked(false);
                }
            }
        });
        rbRTidakPahamProgramTsel.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    rbRTidakPahamProgramTsel.getText().toString();
                    rbPahamProgramTsel.setChecked(false);
                    rbKurangPahamProgramTsel.setChecked(false);
                    rbRTidakPahamProgramTsel.setChecked(true);
                }
            }
        });
        // radio button usia pelanggan
        rbYouthUsiaPelanggan.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    rbYouthUsiaPelanggan.getText().toString();
                    rbYouthUsiaPelanggan.setChecked(true);
                    rbNonYouthUsiaPelanggan.setChecked(false);
                    rbUmumUsiaPelanggan.setChecked(false);
                }
            }
        });
        rbNonYouthUsiaPelanggan.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    rbNonYouthUsiaPelanggan.getText().toString();
                    rbYouthUsiaPelanggan.setChecked(false);
                    rbNonYouthUsiaPelanggan.setChecked(true);
                    rbUmumUsiaPelanggan.setChecked(false);
                }
            }
        });
        rbUmumUsiaPelanggan.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    rbUmumUsiaPelanggan.getText().toString();
                    rbYouthUsiaPelanggan.setChecked(false);
                    rbNonYouthUsiaPelanggan.setChecked(false);
                    rbUmumUsiaPelanggan.setChecked(true);
                }
            }
        });

        // validate all broadband share
        List<EditText> listBsShare = new ArrayList<>();
        EditText [] editTextsBs = {etBsTsel, etBsOoredoo, etBsXl, etBsAxis, etBs3, etBsOthers};
        TextView[] tvBroadbandShare = {
                findViewById(R.id.tv_generate_bs_ossosk_tsel),
                findViewById(R.id.tv_generate_bs_ossosk_ooredoo),
                findViewById(R.id.tv_generate_bs_ossosk_xl),
                findViewById(R.id.tv_generate_bs_ossosk_axis),
                findViewById(R.id.tv_generate_bs_ossosk_3),
                findViewById(R.id.tv_generate_bs_ossosk_others),
        };
        for (EditText editTextmarket : editTextsBs) {
            editTextmarket.setTransformationMethod(null);
            listBsShare.add(editTextmarket);
        }
        addBsShareValidation(editTextsBs, tvBroadbandShare);

        List<EditText> listDsShare = new ArrayList<>();
        EditText [] editTextsDs = {etDisplayTsel, etDisplayOoredoo, etDisplayXl, etDisplayAxis, etDisplay3, etDisplayOthers};
        TextView[] tvDisplayShare = {
                findViewById(R.id.tv_generate_ds_ossosk_tsel),
                findViewById(R.id.tv_generate_ds_ossosk_ooredoo),
                findViewById(R.id.tv_generate_ds_ossosk_xl),
                findViewById(R.id.tv_generate_ds_ossosk_axis),
                findViewById(R.id.tv_generate_ds_ossosk_3),
                findViewById(R.id.tv_generate_ds_ossosk_others),
        };
        for (EditText editTextmarket : editTextsDs) {
            editTextmarket.setTransformationMethod(null);
            listDsShare.add(editTextmarket);
        }
        addDsShareValidation(editTextsDs, tvDisplayShare);

        // validasi all display share
        List<EditText> listRsShare = new ArrayList<>();
        EditText [] editTextsRs = {etRechargeTsel, etRechargeOoredoo, etRechargeXl, etRechargeAxis, etRecharge3, etRechargeOthers};
        TextView[] tvRechargeShare = {
                findViewById(R.id.tv_generate_rs_ossosk_tsel),
                findViewById(R.id.tv_generate_rs_ossosk_ooredoo),
                findViewById(R.id.tv_generate_rs_ossosk_xl),
                findViewById(R.id.tv_generate_rs_ossosk_axis),
                findViewById(R.id.tv_generate_rs_ossosk_3),
                findViewById(R.id.tv_generate_rs_ossosk_others),
        };
        for (EditText editTextmarket : editTextsRs) {
            editTextmarket.setTransformationMethod(null);
            listRsShare.add(editTextmarket);
        }
        addRsShareValidation(editTextsRs, tvRechargeShare);

        // save data when backpress
        EditText textSave[] = { etNoteKunjunganOssOsk, etNoteSalesOssOsk, etNoteHargaTP, etNoteShare
                , etNoteProdukTerlaku, etNoteMatpro, etNoteProgramTsel, etNamaFrontliner, etContactFrontliner };
        for (EditText editTextSaveData : textSave){
            SharedPreferences prefs = getPreferences(MODE_PRIVATE);
            String noteProdukTerlaku = prefs.getString("produk_terlaku", "");
            String bsTsel = prefs.getString("bstsel", "");
            String bsOoredoo = prefs.getString("bsooredoo", "");
            String bsXl = prefs.getString("bsxl", "");
            String bsAxis = prefs.getString("bsaxis", "");
            String bs3 = prefs.getString("bs3", "");
            String bsOthers = prefs.getString("bsothers", "");
            String dsTsel = prefs.getString("dstsel", "");
            String dsOoredoo = prefs.getString("dsooredoo", "");
            String dsXl = prefs.getString("dsxl", "");
            String dsAxis = prefs.getString("dsaxis", "");
            String ds3 = prefs.getString("ds3", "");
            String dsOthers = prefs.getString("dsothers", "");
            String rsTsel = prefs.getString("rstsel", "");
            String rsOoredoo = prefs.getString("rsooredoo", "");
            String rsXl = prefs.getString("rsxl", "");
            String rsAxis = prefs.getString("rsaxis", "");
            String rs3 = prefs.getString("rs3", "");
            String rsOthers = prefs.getString("rsothers", "");
            String noteShare = prefs.getString("share", "");
            String noteMatpro = prefs.getString("matpro", "");
            String namaFrontliner = prefs.getString("nama_frontliner", "");
            String kontakFrontliner = prefs.getString("kontak_frontliner", "");
            String noteProgramTsel = prefs.getString("program_tsel", "");
            if(editTextSaveData.getText().toString() != null || !TextUtils.isEmpty(editTextSaveData.getText().toString())){
                etNoteProdukTerlaku.setText(noteProdukTerlaku);
                etBsTsel.setText(bsTsel);
                etBsOoredoo.setText(bsOoredoo);
                etBsXl.setText(bsXl);
                etBsAxis.setText(bsAxis);
                etBs3.setText(bs3);
                etBsOthers.setText(bsOthers);
                etDisplayTsel.setText(dsTsel);
                etDisplayOoredoo.setText(dsOoredoo);
                etDisplayXl.setText(dsXl);
                etDisplayAxis.setText(dsAxis);
                etDisplay3.setText(ds3);
                etDisplayOthers.setText(dsOthers);
                etRechargeTsel.setText(rsTsel);
                etRechargeOoredoo.setText(rsOoredoo);
                etRechargeXl.setText(rsXl);
                etRechargeAxis.setText(rsAxis);
                etRecharge3.setText(rs3);
                etRechargeOthers.setText(rsOthers);
                etNoteShare.setText(noteShare);
                etNoteMatpro.setText(noteMatpro);
                etNamaFrontliner.setText(namaFrontliner);
                etContactFrontliner.setText(kontakFrontliner);
                etNoteProgramTsel.setText(noteProgramTsel);
            }
        }

        // button send backchecking
        btnSendBackcheckingOssOsk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (acceptable) {
                    String phone = etContactFrontliner.getText().toString();
                    if(!validateEditText(ids))
                    {
                        //if not empty do something
                        sendOssOskBackcheckingData();
                    }else if (!isValidPhone(phone)) {
                        etContactFrontliner.setError("Nomor ponsel tidak valid");
                    } else {
                        Toast.makeText(getApplicationContext(), "Ada isian yang kosong", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    showMessage("Maaf salah satu data yang anda isikan ada yang tidak sesuai");
                }
            }


        });

        // membuat arrayadapter dan default layout spinner
        mPresenter.getSpinnerMenu(brandSpinner);
        mPresenter.getSpinnerMenu(brandFrontlinerSpinner);
        setUserSenderMenu();
    }

    private void addDsShareValidation(EditText[] listDsShare,TextView[] textViews) {
        for (EditText market : listDsShare){
            market.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    String text = s.toString();
                    if (text.equals("")) {
                        market.setError("Data tidak boleh kosong");
                        acceptable = false;
                    } else {
                        acceptable = true;
                        Log.d("Persentase Share",String.format("Lengt et dislay share : %d, lenght tv display share %d",listDsShare.length,textViews.length));
                        generatePersentaseShare(listDsShare,textViews);
                    }
                }
            });
        }
    }

    private void addRsShareValidation(EditText[] listRsShare,TextView[] textViews) {
        for (EditText market : listRsShare){
            market.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    String text = s.toString();
                    if (text.equals("")) {
                        market.setError("Data tidak boleh kosong");
                        acceptable = false;
                    } else {
                        acceptable = true;
                        Log.d("Persentase Share",String.format("Lengt et recharge share : %d, lenght tv recharge share %d",listRsShare.length,textViews.length));
                        generatePersentaseShare(listRsShare,textViews);
                    }
                }
            });
        }
    }

    private void addBsShareValidation(EditText[] listBsShare,TextView[] textViews) {
        for (EditText market : listBsShare){
            market.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    String text = s.toString();
                    if (text.equals("")) {
                        market.setError("Data tidak boleh kosong");
                        acceptable = false;
                    } else {
                        acceptable = true;
                        Log.d("Persentase Share",String.format("Lengt et broadband share : %d, lenght tv broadband share %d",listBsShare.length,textViews.length));
                        generatePersentaseShare(listBsShare,textViews);
                    }
                }
            });
        }
    }

    int[] ids = new int[]
    {
            R.id.et_contact_frontliner,

            R.id.et_note_kunjungan_oss_osk,
            R.id.et_note_sales_oss_osk,
            R.id.et_note_harga_tp,
            // broadband share
            R.id.et_bs_tsel,
            R.id.et_bs_ooredoo,
            R.id.et_bs_xl,
            R.id.et_bs_axis,
            R.id.et_bs_3,
            R.id.et_bs_others,
            // display share
            R.id.et_display_tsel,
            R.id.et_display_ooredoo,
            R.id.et_display_xl,
            R.id.et_display_axis,
            R.id.et_display_3,
            R.id.et_display_others,
            // recharge share
            R.id.et_recharge_tsel,
            R.id.et_recharge_ooredoo,
            R.id.et_recharge_xl,
            R.id.et_recharge_axis,
            R.id.et_recharge_3,
            R.id.et_recharge_others,

            R.id.et_note_broadband_share,
            R.id.et_note_produk_terlaku,
            R.id.et_note_matpro_ossosk,
            R.id.et_nama_frontliner,
            R.id.et_note_program_tsel,
    };

    public boolean validateEditText(int[] ids)
    {
        boolean isEmpty = false;

        for(int id: ids)
        {
            EditText et = (EditText)findViewById(id);

            if(TextUtils.isEmpty(et.getText().toString()))
            {
                et.setError("Wajib diisi");
                isEmpty = true;
            }
        }

        return isEmpty;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public static boolean checkNullValues(String valueToCheck) {
        if(!(valueToCheck == null)) {
            String valueCheck = valueToCheck.trim();
            if(valueCheck.equals("") || valueCheck.equals("0")  ) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean isValidPhone(String phone) {
        String expression = "(\\()?(\\+62|628|08|8)(\\d{2,4})?\\)?[ .-]?\\d{2,7}";
        CharSequence inputString = phone;
        Pattern pattern = Pattern.compile(expression);
        Matcher matcher = pattern.matcher(inputString);
        if (inputString.length() < 11){
            matcher.matches();
            return false;
        }else if (matcher.matches())
        {
            return true;
        }
        else{
            return false;
        }
    }

    @Override
    public void setupBrandingMenu(Backchecking backchecking) {
        ImageView ivOutletBrand = findViewById(R.id.image_outlet_brand);
        Glide.with(this)
                .load(ApiClient.BASE_URL_NAMI_IMAGE+ backchecking.getOutletPath())
                .apply(new RequestOptions()
                .placeholder(R.drawable.ic_none_picture))
                .into(ivOutletBrand);

        TextView tvInfoDominasi = findViewById(R.id.tv_info_dominasi_branding);
        tvInfoDominasi.setText("Dominasi branding " + backchecking.getDominantBranding());
        dominasiBrandingProduct = backchecking.getDominantBranding();


        Spinner brandSpinner = findViewById(R.id.brand_spinner);
        /** Set popup height, width & background color as you need or just leave default settings **/

        final ImagePopup imagePopup = new ImagePopup(this);
        imagePopup.setWindowHeight(800); // Optional
        imagePopup.setWindowWidth(800); // Optional
        imagePopup.setBackgroundColor(Color.WHITE);  // Optional
        imagePopup.setFullScreen(true); // Optional
        imagePopup.setHideCloseIcon(true);  // Optional
        imagePopup.setImageOnClickClose(true);  // Optional

        imagePopup.initiatePopupWithGlide(ApiClient.BASE_URL_NAMI_IMAGE+ backchecking.getOutletPath());

        ivOutletBrand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /** Initiate Popup view **/
                imagePopup.viewPopup();

            }
        });

        if (!backchecking.getOutletPath().equals("UNKNOWN")){

        }
        if (!backchecking.getDominantBranding().equals("UNKNOWN")){
            switch (backchecking.getDominantBranding().toLowerCase()){
                case "tsel":
                    brandSpinner.setSelection(0);
                case "ooredoo":
                    brandSpinner.setSelection(1);
                case "xk":
                    brandSpinner.setSelection(2);
                case "axis":
                    brandSpinner.setSelection(3);
                case "3":
                    brandSpinner.setSelection(4);
                case "others":
                    brandSpinner.setSelection(4);
            }
        }
        this.mBackchecking = backchecking;
    }

    @Override
    public void setupSalesThroughMenu(Backchecking backchecking) {
        TextView tvBulanBerjalanKunjunganTerakhir = findViewById(R.id.tv_sales_through_inet_total_name_ossosk);
        TextView tvBulanBerjalanKunjunganTerakhirName = findViewById(R.id.tv_sales_through_name_ossosk);
        TextView tvBulanBerjalanKunjunganTerakhirJumlah = findViewById(R.id.tv_sales_through_jumlah_ossosk);
        TextView tvBulanBerjalanKunjunganTerakhirTitikDua = findViewById(R.id.tv_titik_dua_ossosk);
        ImageView ivSalesThrough = findViewById(R.id.iv_sales_through_ossosk);

        Glide.with(this)
                .load(ApiClient.BASE_URL_NAMI_IMAGE+ backchecking.getCanteenPath())
                .apply(new RequestOptions()
                        .placeholder(R.drawable.ic_none_picture)
                        .error(R.drawable.ic_none_picture))
                .into(ivSalesThrough);

        /** Set popup height, width & background color as you need or just leave default settings **/

        final ImagePopup imagePopup = new ImagePopup(this);
        imagePopup.setWindowHeight(800); // Optional
        imagePopup.setWindowWidth(800); // Optional
        imagePopup.setBackgroundColor(Color.WHITE);  // Optional
        imagePopup.setFullScreen(true); // Optional
        imagePopup.setHideCloseIcon(true);  // Optional
        imagePopup.setImageOnClickClose(true);  // Optional

        imagePopup.initiatePopupWithGlide(ApiClient.BASE_URL_NAMI_IMAGE+ backchecking.getCanteenPath());

        ivSalesThrough.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /** Initiate Popup view **/
                imagePopup.viewPopup();

            }
        });

        if (backchecking.getBroadbandSales() == null ){
            tvBulanBerjalanKunjunganTerakhir.setText("?");
            return;
        }

        StringBuilder sbName = new StringBuilder();
        StringBuilder sbGb = new StringBuilder();
        StringBuilder titikDua = new StringBuilder();
        StringBuilder sbJumlah = new StringBuilder();

        if (backchecking.getBroadbandSales() != null){
            Map<String, Boolean> checker = new HashMap<>();
            for (String text : backchecking.getBroadbandSales().split("\\|")){
                String[] texts = text.split(" ");
                if (checker.get(texts[0]) == null){
                    sbName.append(texts[0]).append("\n");
                    checker.put(texts[0], Boolean.TRUE);
                }else {
                    sbName.append("\n");
                }
                sbGb.append(texts[1].replace(":", "")).append("\n");
                titikDua.append(":\t\t\t\n");
                sbJumlah.append(texts[2].replace(";", "\t\t\t\t\t\t\t")).append("\n");
            }
            tvBulanBerjalanKunjunganTerakhirName.setText(sbName.toString());
            tvBulanBerjalanKunjunganTerakhir.setText(sbGb.toString());
            tvBulanBerjalanKunjunganTerakhirTitikDua.setText(titikDua.toString());
            tvBulanBerjalanKunjunganTerakhirJumlah.setText(sbJumlah.toString());
        }else{
            tvBulanBerjalanKunjunganTerakhir.setText("Null");
        }

    }

    @Override
    public void setupTopSellingMenu(Backchecking backchecking) {
        TextView tvProdukLakuTsel = findViewById(R.id.tv_produk_terlaku_tsel);
        TextView tvProdukLakuOoredoo = findViewById(R.id.tv_produk_terlaku_ooredoo);
        TextView tvProdukLakuXl = findViewById(R.id.tv_produk_terlaku_xl);
        TextView tvProdukLakuAxis = findViewById(R.id.tv_produk_terlaku_axis);
        TextView tvProdukLaku3 = findViewById(R.id.tv_produk_terlaku_3);
        TextView tvProdukLakuOthers = findViewById(R.id.tv_produk_terlaku_others);

        TextView tvProdukLakuTpTsel = findViewById(R.id.tv_produk_terlaku_tsel_tp);
        TextView tvProdukLakuTpOoredoo = findViewById(R.id.tv_produk_terlaku_ooredoo_tp);
        TextView tvProdukLakuTpXl = findViewById(R.id.tv_produk_terlaku_xl_tp);
        TextView tvProdukLakuTpAxis = findViewById(R.id.tv_produk_terlaku_axis_tp);
        TextView tvProdukLakuTp3 = findViewById(R.id.tv_produk_terlaku_3_tp);
        TextView tvProdukLakuTpOthers = findViewById(R.id.tv_produk_terlaku_others_tp);

        TextView tvProdukLakuEupTsel = findViewById(R.id.tv_produk_terlaku_tsel_eup);
        TextView tvProdukLakuEupOoredoo = findViewById(R.id.tv_produk_terlaku_ooredoo_eup);
        TextView tvProdukLakuEupXl = findViewById(R.id.tv_produk_terlaku_xl_eup);
        TextView tvProdukLakuEupAxis = findViewById(R.id.tv_produk_terlaku_axis_eup);
        TextView tvProdukLakuEup3 = findViewById(R.id.tv_produk_terlaku_3_eup);
        TextView tvProdukLakuEupOthers = findViewById(R.id.tv_produk_terlaku_others_eup);

        TextView tvProdukLakuDpiTsel = findViewById(R.id.tv_produk_terlaku_dpi_tsel);
        TextView tvProdukLakuDpiOoredoo = findViewById(R.id.tv_produk_terlaku_dpi_ooredoo);
        TextView tvProdukLakuDpiXl = findViewById(R.id.tv_produk_terlaku_dpi_xl);
        TextView tvProdukLakuDpiAxis = findViewById(R.id.tv_produk_terlaku_dpi_axis);
        TextView tvProdukLakuDpi3 = findViewById(R.id.tv_produk_terlaku_dpi_3);
        TextView tvProdukLakuDpiOthers = findViewById(R.id.tv_produk_terlaku_dpi_others);

        TextView tvProdukLakuOthersTsel = findViewById(R.id.tv_produk_terlaku_others_tsel);
        TextView tvProdukLakuOthersOoredoo = findViewById(R.id.tv_produk_terlaku_others_ooredoo);
        TextView tvProdukLakuOthersXl = findViewById(R.id.tv_produk_terlaku_others_xl);
        TextView tvProdukLakuOthersAxis = findViewById(R.id.tv_produk_terlaku_others_axis);
        TextView tvProdukLakuOthers3 = findViewById(R.id.tv_produk_terlaku_others_3);
        TextView tvProdukLakuOthersOthers = findViewById(R.id.tv_produk_terlaku_others_others);


        tvProdukLakuTsel.setText(backchecking.getQuotaInetTelkomsel() + "GB");
        tvProdukLakuOoredoo.setText(backchecking.getQuotaInetOoredoo() + "GB");
        tvProdukLakuXl.setText(backchecking.getQuotaInetXl() + "GB");
        tvProdukLakuAxis.setText(backchecking.getQuotaInetAxis() + "GB");
        tvProdukLaku3.setText(backchecking.getQuotaInet3() + "GB");
        tvProdukLakuOthers.setText(backchecking.getQuotaInetOthers() + "GB");

        tvProdukLakuTpTsel.setText(backchecking.getTpTelkomsel());
        tvProdukLakuTpOoredoo.setText(backchecking.getTpOoredoo());
        tvProdukLakuTpXl.setText(backchecking.getTpXl());
        tvProdukLakuTpAxis.setText(backchecking.getTpAxis());
        tvProdukLakuTp3.setText(backchecking.getTp3());
        tvProdukLakuTpOthers.setText(backchecking.getTpOthers());

        tvProdukLakuEupTsel.setText(backchecking.getEupTelkomsel());
        tvProdukLakuEupOoredoo.setText(backchecking.getEupOoredoo());
        tvProdukLakuEupXl.setText(backchecking.getEupXl());
        tvProdukLakuEupAxis.setText(backchecking.getEupAxis());
        tvProdukLakuEup3.setText(backchecking.getEup3());
        tvProdukLakuEupOthers.setText(backchecking.getEupOthers());

        tvProdukLakuDpiTsel.setText(backchecking.getQuotaDpiTelkomsel() + "GB");
        tvProdukLakuDpiOoredoo.setText(backchecking.getQuotaDpiOoredoo() + "GB");
        tvProdukLakuDpiXl.setText(backchecking.getQuotaDpiXl() + "GB");
        tvProdukLakuDpiAxis.setText(backchecking.getQuotaDpiAxis() + "GB");
        tvProdukLakuDpi3.setText(backchecking.getQuotaDpi3() + "GB");
        tvProdukLakuDpiOthers.setText(backchecking.getQuotaDpiOthers() + "GB");

        tvProdukLakuOthersTsel.setText(backchecking.getQuotaOthersTelkomsel() + "GB");
        tvProdukLakuOthersOoredoo.setText(backchecking.getQuotaOthersOoredoo() + "GB");
        tvProdukLakuOthersXl.setText(backchecking.getQuotaOthersXl() + "GB");
        tvProdukLakuOthersAxis.setText(backchecking.getQuotaOthersAxis() + "GB");
        tvProdukLakuOthers3.setText(backchecking.getQuotaOthers3() + "GB");
        tvProdukLakuOthersOthers.setText(backchecking.getQuotaOthersOthers() + "GB");
    }

    @Override
    public void setupBsDsRsMenu(Backchecking backchecking) {
        TextView tvBsTelkomsel = findViewById(R.id.tv_display_bs_tsel);
        TextView tvBsOoredoo = findViewById(R.id.tv_display_bs_ooredoo);
        TextView tvBsXl = findViewById(R.id.tv_display_bs_xl);
        TextView tvBsAxis = findViewById(R.id.tv_display_bs_axis);
        TextView tvBs3 = findViewById(R.id.tv_display_bs_3);
        TextView tvBsOthers = findViewById(R.id.tv_display_bs_others);

        TextView tvDsTelkomsel = findViewById(R.id.tv_display_tsel);
        TextView tvDsOoredoo = findViewById(R.id.tv_display_ooredoo);
        TextView tvDsXl = findViewById(R.id.tv_display_xl);
        TextView tvDsAxis = findViewById(R.id.tv_display_axis);
        TextView tvDs3 = findViewById(R.id.tv_display_3);
        TextView tvDsOthers = findViewById(R.id.tv_display_others);

        TextView tvRsTelkomsel = findViewById(R.id.tv_recharge_tsel);
        TextView tvRsOoredoo = findViewById(R.id.tv_recharge_ooredoo);
        TextView tvRsXl = findViewById(R.id.tv_recharge_xl);
        TextView tvRsAxis = findViewById(R.id.tv_recharge_axis);
        TextView tvRs3 = findViewById(R.id.tv_recharge_3);
        TextView tvRsOthers = findViewById(R.id.tv_recharge_others);

        tvBsTelkomsel.setText(backchecking.getBsTelkomsel() + "%");
        tvBsOoredoo.setText(backchecking.getBsOoredoo() + "%");
        tvBsXl.setText(backchecking.getBsXl() + "%");
        tvBsAxis.setText(backchecking.getBsAxis() + "%");
        tvBs3.setText(backchecking.getBs3() + "%");
        tvBsOthers.setText(backchecking.getBsOthers() + "%");

        tvDsTelkomsel.setText(backchecking.getDsTelkomsel() + "%");
        tvDsOoredoo.setText(backchecking.getDsOoredoo() + "%");
        tvDsXl.setText(backchecking.getDsXl() + "%");
        tvDsAxis.setText(backchecking.getDsAxis() + "%");
        tvDs3.setText(backchecking.getDs3() + "%");
        tvDsOthers.setText(backchecking.getDsOthers() + "%");

        tvRsTelkomsel.setText(backchecking.getRsTelkomsel() + "%");
        tvRsOoredoo.setText(backchecking.getRsOoredoo() + "%");
        tvRsXl.setText(backchecking.getRsXl() + "%");
        tvRsAxis.setText(backchecking.getRsAxis() + "%");
        tvRs3.setText(backchecking.getRs3() + "%");
        tvRsOthers.setText(backchecking.getRsOthers() + "%");

    }

    @Override
    public void setupVisitMenu(Backchecking backchecking) {
        TextView tvNamaAoc = findViewById(R.id.tv_nama_aoc_backchecking_oss_osk);
        TextView tvNomorAoc = findViewById(R.id.tv_nomor_aoc_backchecking_oss_osk);
        TextView tvWaktuBackchecking = findViewById(R.id.tv_date_backchecking_oss_osk);


        tvNamaAoc.setText(backchecking.getUserName());
        tvNomorAoc.setText(backchecking.getUserId());

        if (backchecking.getUserName() != null )
            tvNamaAoc.setText(backchecking.getUserName());
        else{
            tvNamaAoc.setText("Null");
        }

        if (backchecking.getUserId() != null )
            tvNomorAoc.setText(backchecking.getUserId());
        else{
            tvNomorAoc.setText("Null");
        }

        if (backchecking.getMaxVisit() != null )
            tvWaktuBackchecking.setText(backchecking.getMaxVisit());
        else{
            tvWaktuBackchecking.setText("Null");
        }

    }

    @Override
    public void setupReadSignal(Backchecking backchecking) {
        TextView tvTselVoice = findViewById(R.id.tv_osk_kualitas_signal_tsel_voice);
        TextView tvTsel4g = findViewById(R.id.tv_osk_kualitas_signal_tsel_4g);
        TextView tvTsel3g = findViewById(R.id.tv_osk_kualitas_signal_tsel_3g);
        TextView tvOoredooVoice = findViewById(R.id.tv_osk_kualitas_signal_ooredoo_voice);
        TextView tvOoredoo4g = findViewById(R.id.tv_osk_kualitas_signal_ooredoo_4g);
        TextView tvOoredoo3g = findViewById(R.id.tv_osk_kualitas_signal_ooredoo_3g);
        TextView tvXlVoice = findViewById(R.id.tv_osk_kualitas_signal_xl_voice);
        TextView tvXl4g = findViewById(R.id.tv_osk_kualitas_signal_xl_4g);
        TextView tvXl3g = findViewById(R.id.tv_osk_kualitas_signal_xl_3g);
        TextView tvAxisVoice = findViewById(R.id.tv_osk_kualitas_signal_axis_voice);
        TextView tvAxis4g = findViewById(R.id.tv_osk_kualitas_signal_axis_4g);
        TextView tvAxis3g = findViewById(R.id.tv_osk_kualitas_signal_axis_3g);
        TextView tvthreeVoice = findViewById(R.id.tv_osk_kualitas_signal_3_voice);
        TextView tvthree4g = findViewById(R.id.tv_osk_kualitas_signal_3_4g);
        TextView tvthree3g = findViewById(R.id.tv_osk_kualitas_signal_3_3g);
        TextView tvOthersVoice = findViewById(R.id.tv_osk_kualitas_signal_others_voice);
        TextView tvOthers4g = findViewById(R.id.tv_osk_kualitas_signal_others_4g);
        TextView tvOthers3g = findViewById(R.id.tv_osk_kualitas_signal_others_3g);


        //tsel
        tvTselVoice.setText(backchecking.getVoiceTelkomsel());
        tvTsel4g.setText(backchecking.getJsonMember4gTelkomsel());
        tvTsel3g.setText(backchecking.getJsonMember3gTelkomsel());
        //ooredoo
        tvOoredooVoice.setText(backchecking.getVoiceOoredoo());
        tvOoredoo4g.setText(backchecking.getJsonMember4gOoredoo());
        tvOoredoo3g.setText(backchecking.getJsonMember3gOoredoo());
        //xl
        tvXlVoice.setText(backchecking.getVoiceXl());
        tvXl4g.setText(backchecking.getJsonMember4gXl());
        tvXl3g.setText(backchecking.getJsonMember3gXl());
        //axis
        tvAxisVoice.setText(backchecking.getVoiceAxis());
        tvAxis4g.setText(backchecking.getJsonMember4gAxis());
        tvAxis3g.setText(backchecking.getJsonMember3gAxis());
        //3
        tvthreeVoice.setText(backchecking.getVoice3());
        tvthree4g.setText(backchecking.getJsonMember4g3());
        tvthree3g.setText(backchecking.getJsonMember3g3());
        //others
        tvOthersVoice.setText(backchecking.getVoiceOthers());
        tvOthers4g.setText(backchecking.getJsonMember4gOthers());
        tvOthers3g.setText(backchecking.getJsonMember3gOthers());
    }

    @Override
    public void sendOssOskBackcheckingData() {
        String kunjunganOssOskSesuai = "";
        String kunjunganOssOskNote = "";
        String salesThroughSesuai = "";
        String salesThroughNote = "";
        String hargaTpSesuai = "";
        String hargaTpNote = "";

        String dominantBrandingMatched = "";
        String dominantBrandingByAoc = "";
        String dominantBranding = "";

        String bsTelkomsel = "";
        String bsOoredoo = "";
        String bsXl = "";
        String bsAxis = "";
        String bs3 = "";
        String bsOthers = "";
        String bsNote = "";

        String dsTelkomsel = "";
        String dsOoredoo = "";
        String dsXl = "";
        String dsAxis = "";
        String ds3 = "";
        String dsOthers = "";

        String rsTelkomsel = "";
        String rsOoredoo = "";
        String rsXl = "";
        String rsAxis = "";
        String rs3 = "";
        String rsOthers = "";

        String topSell = "";

        String namaFrontliner = "";
        String contactFrontliner = "";

        String telkomselProductKnowledge = "";
        String telkomselProductKnowledgeNote = "";

        String frontlinerOffering = "";
        String frontlinerProductOffering = "";
        String outletSegment = "";

        String matproNote = "";

        if (rbSesuaiKunjunganOssOsk.isChecked()){
            kunjunganOssOskSesuai = rbSesuaiKunjunganOssOsk.getText().toString();
        }else{
            kunjunganOssOskSesuai = rbTidakSesuaiKunjunganOssOsk.getText().toString();
            EditText etKunjunganOssOskNote = findViewById(R.id.et_note_kunjungan_oss_osk);
            kunjunganOssOskNote = etKunjunganOssOskNote.getText().toString();
        }

        if (rbSesuaiSalesOssOsk.isChecked()){
            salesThroughSesuai = rbSesuaiKunjunganOssOsk.getText().toString();
        }else{
            salesThroughSesuai = rbTidakSesuaiKunjunganOssOsk.getText().toString();
            EditText etSalesOssOsk = findViewById(R.id.et_note_sales_oss_osk);
            salesThroughNote = etSalesOssOsk.getText().toString();
        }

        if (rbSesuaiHargaTP.isChecked()){
            hargaTpSesuai = rbSesuaiHargaTP.getText().toString();
        }else{
            hargaTpSesuai = rbTidakSesuaiHargaTP.getText().toString();
            EditText etHargaTP = findViewById(R.id.et_note_harga_tp);
            hargaTpNote = etHargaTP.getText().toString();
        }

        bsTelkomsel = ((TextView) findViewById(R.id.tv_generate_bs_ossosk_tsel)).getText().toString().replace("%","");
        bsOoredoo = ((TextView) findViewById(R.id.tv_generate_bs_ossosk_ooredoo)).getText().toString().replace("%","");
        bsXl = ((TextView) findViewById(R.id.tv_generate_bs_ossosk_xl)).getText().toString().replace("%","");
        bsAxis = ((TextView) findViewById(R.id.tv_generate_bs_ossosk_axis)).getText().toString().replace("%","");
        bs3 = ((TextView) findViewById(R.id.tv_generate_bs_ossosk_3)).getText().toString().replace("%","");
        bsOthers = ((TextView) findViewById(R.id.tv_generate_bs_ossosk_others)).getText().toString().replace("%","");
        bsNote = ((EditText) findViewById(R.id.et_note_broadband_share)).getText().toString().replace("%","");

        dsTelkomsel = ((TextView) findViewById(R.id.tv_generate_ds_ossosk_tsel)).getText().toString().replace("%","");
        dsOoredoo = ((TextView) findViewById(R.id.tv_generate_ds_ossosk_ooredoo)).getText().toString().replace("%","");
        dsXl = ((TextView) findViewById(R.id.tv_generate_ds_ossosk_xl)).getText().toString().replace("%","");
        dsAxis = ((TextView) findViewById(R.id.tv_generate_ds_ossosk_axis)).getText().toString().replace("%","");
        ds3 = ((TextView) findViewById(R.id.tv_generate_ds_ossosk_3)).getText().toString().replace("%","");
        dsOthers = ((TextView) findViewById(R.id.tv_generate_ds_ossosk_others)).getText().toString().replace("%","");

        rsTelkomsel = ((TextView) findViewById(R.id.tv_generate_rs_ossosk_tsel)).getText().toString().replace("%","");
        rsOoredoo = ((TextView) findViewById(R.id.tv_generate_rs_ossosk_ooredoo)).getText().toString().replace("%","");
        rsXl = ((TextView) findViewById(R.id.tv_generate_rs_ossosk_xl)).getText().toString().replace("%","");
        rsAxis = ((TextView) findViewById(R.id.tv_generate_rs_ossosk_axis)).getText().toString().replace("%","");
        rs3 = ((TextView) findViewById(R.id.tv_generate_rs_ossosk_3)).getText().toString().replace("%","");
        rsOthers = ((TextView) findViewById(R.id.tv_generate_rs_ossosk_others)).getText().toString().replace("%","");

        matproNote = ((EditText) findViewById(R.id.et_note_matpro_ossosk)).getText().toString();

        if (rbSesuaiOutletBrand.isChecked()){
            dominantBrandingMatched = rbSesuaiOutletBrand.getText().toString();

        }else{
            dominantBrandingMatched = rbTidakSesuaiOutletBrand.getText().toString();
            dominantBranding = ((Spinner) findViewById(R.id.brand_frontliner_spinner)).getSelectedItem().toString();
        }

        topSell = ((EditText)findViewById(R.id.et_note_produk_terlaku)).getText().toString();

        namaFrontliner = ((EditText)findViewById(R.id.et_nama_frontliner)).getText().toString();
        contactFrontliner = ((EditText)findViewById(R.id.et_contact_frontliner)).getText().toString().replace("+","");

        if (contactFrontliner.charAt(0) == '8'){
            contactFrontliner = "628" + contactFrontliner.substring(1,contactFrontliner.length());
        }else if (contactFrontliner.charAt(0) == '0'){
            contactFrontliner = "62" + contactFrontliner.substring(1,contactFrontliner.length());
        }

        telkomselProductKnowledge = ((RadioButton) findViewById(((RadioGroup)findViewById(R.id.rg_info_program_tsel)).getCheckedRadioButtonId())).getText().toString();
        telkomselProductKnowledgeNote = ((EditText) findViewById(R.id.et_note_program_tsel)).getText().toString();

        frontlinerOffering = ((RadioButton) findViewById(((RadioGroup)findViewById(R.id.rg_info_frontliner)).getCheckedRadioButtonId())).getText().toString();
        if (frontlinerOffering.equals("Ya")){
            frontlinerProductOffering = ((Spinner) findViewById(R.id.brand_frontliner_spinner)).getSelectedItem().toString();
        }

        outletSegment = ((RadioButton) findViewById(((RadioGroup) findViewById(R.id.rg_info_usia_pelanggan)).getCheckedRadioButtonId())).getText().toString();
        dominantBrandingByAoc = dominasiBrandingProduct;

        Map<String, String> map = new HashMap<>();
        map.put("visit_matched",kunjunganOssOskSesuai);
        map.put("visit_note",kunjunganOssOskNote);
        map.put("sales_through_matched", salesThroughSesuai);
        map.put("sales_through_note", salesThroughNote);
        map.put("purchasing_matched", hargaTpSesuai);
        map.put("purchasing_note", hargaTpNote);

        map.put("bs_telkomsel", bsTelkomsel);
        map.put("bs_ooredoo", bsOoredoo);
        map.put("bs_xl", bsXl);
        map.put("bs_axis", bsAxis);
        map.put("bs_3", bs3);
        map.put("bs_others", bsOthers);
        map.put("share_note", bsNote);

        map.put("ds_telkomsel", dsTelkomsel);
        map.put("ds_ooredoo", dsOoredoo);
        map.put("ds_xl", dsXl);
        map.put("ds_axis", dsAxis);
        map.put("ds_3", ds3);
        map.put("ds_others", dsOthers);

        map.put("rs_telkomsel", rsTelkomsel);
        map.put("rs_ooredoo", rsOoredoo);
        map.put("rs_xl", rsXl);
        map.put("rs_axis", rsAxis);
        map.put("rs_3", rs3);
        map.put("rs_others", rsOthers);

        map.put("dominant_branding_matched", dominantBrandingMatched);
        map.put("dominant_branding_by_aoc", dominantBrandingByAoc);
        map.put("dominant_branding", dominantBranding);
        map.put("matpro_note",matproNote);

        map.put("most_selling_product_note",topSell);

        map.put("frontliner_name",namaFrontliner);
        map.put("frontliner_contact",contactFrontliner);

        map.put("telkomsel_product_knowledge",telkomselProductKnowledge);
        map.put("telkomsel_product_knowledge_note",telkomselProductKnowledgeNote);
        map.put("frontliner_offering",frontlinerOffering);
        map.put("frontliner_product_offering",frontlinerProductOffering);
        map.put("outlet_segment",outletSegment);

        map.put("location_id",mBackcheckingDetail.getLocationId());
        map.put("location_name",mBackcheckingDetail.getInstitutionName());
        map.put("userid_sendto",nomorAocPjp);
        map.put("username_sendto",namaAocPjp);

        final boolean[] isSuccess = {false};

        LocationUtils.requestLocationUpdateWithCheckPermission(this, new LocationUtils.IUserLocationListener() {
            @Override
            public void onStateChange(State state) {

            }

            @Override
            public void onLocationChanged(Location location) {
                Log.d("LOCATION BC", "onLocationChanged: " + location);
                if (location != null && !isSuccess[0]){
                    isSuccess[0] = true;
                    dataClearPref(BackCheckingOssOskActivity.this);
                    mPresenter.submitBackchecking(map, BackCheckingOssOskActivity.this,mBackcheckingDetail,String.valueOf(location.getLatitude()),
                            String.valueOf(location.getLongitude()));

                }else if (location == null){
                    isSuccess[0] = false;
                    dataClearPref(BackCheckingOssOskActivity.this);
                    mPresenter.submitBackcheckingIfLocationNull(map, BackCheckingOssOskActivity.this, mBackcheckingDetail);
                    showMessage("Silahkan checkout manual");
                }
                LocationUtils.removeLocationUpdate(this);
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // show menu when menu button is pressed
        String usertypebc = mPresenter.usertypeBackchecker();
        Log.d("USERTYPE BC", "onCreateOptionsMenu: " + usertypebc);
        if (usertypebc != null && usertypebc.equals("mgt1")){
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_checkout, menu);
            return true;
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // display a message when a button was pressed
        String message = "";
        if (item.getItemId() == R.id.action_checkout) {
            // action checkout
            final boolean[] isSuccess = {false};
            LocationUtils.requestLocationUpdateWithCheckPermission(BackCheckingOssOskActivity.this, new LocationUtils.IUserLocationListener() {
                @Override
                public void onStateChange(State state) {

                }

                @Override
                public void onLocationChanged(Location location) {
                    if (location != null && !isSuccess[0]){
                        isSuccess[0] = true;
                        Intent intent = BackCheckingLocationListActivity.getStartIntent(BackCheckingOssOskActivity.this);
                        intent.addFlags(intent.FLAG_ACTIVITY_CLEAR_TOP
                                | intent.FLAG_ACTIVITY_NEW_TASK);
                        dataClearPref(BackCheckingOssOskActivity.this);
                        mPresenter.insertUserCheckout(
                                String.valueOf(location.getLatitude()),
                                String.valueOf(location.getLongitude()));
                        startActivity(intent);
                    }else if (location == null){
                        isSuccess[0] = false;
                        Intent intent = BackCheckingLocationListActivity.getStartIntent(BackCheckingOssOskActivity.this);
                        intent.addFlags(intent.FLAG_ACTIVITY_CLEAR_TOP
                                | intent.FLAG_ACTIVITY_NEW_TASK);
                        dataClearPref(BackCheckingOssOskActivity.this);
                        mPresenter.insertUserCheckout(
                                String.valueOf(location.getLatitude()),
                                String.valueOf(location.getLongitude()));
                        startActivity(intent);
                    }
                    LocationUtils.removeLocationUpdate(this);
                }

                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {

                }

                @Override
                public void onProviderEnabled(String s) {

                }

                @Override
                public void onProviderDisabled(String s) {

                }
            });
            return true;
        }
        return false;
    }

    @Override
    public void onBackPressed(){
        SharedPreferences.Editor editor = getPreferences(MODE_PRIVATE).edit();
        editor.putString("produk_terlaku", etNoteProdukTerlaku.getText().toString());
        editor.putString("bstsel", etBsTsel.getText().toString());
        editor.putString("bsooredoo", etBsOoredoo.getText().toString());
        editor.putString("bsxl", etBsXl.getText().toString());
        editor.putString("bsaxis", etBsAxis.getText().toString());
        editor.putString("bs3", etBs3.getText().toString());
        editor.putString("bsothers", etBsOthers.getText().toString());
        editor.putString("dstsel", etDisplayTsel.getText().toString());
        editor.putString("dsooredoo", etDisplayOoredoo.getText().toString());
        editor.putString("dsxl", etDisplayXl.getText().toString());
        editor.putString("dsaxis", etDisplayAxis.getText().toString());
        editor.putString("ds3", etDisplay3.getText().toString());
        editor.putString("dsothers", etDisplayOthers.getText().toString());
        editor.putString("rstsel", etRechargeTsel.getText().toString());
        editor.putString("rsooredoo", etRechargeOoredoo.getText().toString());
        editor.putString("rsxl", etRechargeXl.getText().toString());
        editor.putString("rsaxis", etRechargeAxis.getText().toString());
        editor.putString("rs3", etRecharge3.getText().toString());
        editor.putString("rsothers", etRechargeOthers.getText().toString());
        editor.putString("share", etNoteShare.getText().toString());
        editor.putString("matpro", etNoteMatpro.getText().toString());
        editor.putString("nama_frontliner", etNamaFrontliner.getText().toString());
        editor.putString("kontak_frontliner", etContactFrontliner.getText().toString());
        editor.putString("program_tsel", etNoteProgramTsel.getText().toString());
        editor.commit();
        super.onBackPressed();
    }

    public void dataClearPref(Context context){
        SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }

    @Override
    public void openBackcheckingDetailActivity(BackCheckingLocationListResponse response) {

        Intent i = new Intent(this, DetailBackCheckingLocationActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        i.putExtra("detail",response);
        i.putExtra("backchecking_state","checkout");
        startActivity(i);
    }

    @Override
    public void openGoBackcheckingActivity() {
        Intent intent = BackCheckingLocationActivity.getStartIntent(this);
        intent.addFlags(intent.FLAG_ACTIVITY_CLEAR_TOP | intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void finishActivity() {
        finish();
    }

    private String dateConverter(String dateInput){
        try {
            SimpleDateFormat spf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date newDate= null;
            newDate = spf.parse(dateInput);
            spf= new SimpleDateFormat("EEE, d MMMM HH:mm");
            String returnDate = spf.format(newDate);
            return returnDate;

        }catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void getBackcheckingDataFromServer(String locationId){
        mPresenter.getBackcheckingOssOskVisit(locationId);
        mPresenter.getBackcheckingOssOskBranding(locationId);
        mPresenter.getBackcheckingOssOskBsDsRs(locationId);
        mPresenter.getBackcheckingOssOskSalesThrough(locationId);
        mPresenter.getBackcheckingOssOskTopSelling(locationId);
        mPresenter.getBackcheckingOssOskReadSignal(locationId);
    }

    @Override
    public void showLoading(String message) {
        super.showLoading(message);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        if (progressDialog != null ){
            progressDialog.dismiss();
        }
    }
    public void setUserSenderMenu() {
        BackCheckingLocationListResponse response = (BackCheckingLocationListResponse) getIntent().getSerializableExtra("backchecking");
        mBackcheckingDetail = response;
        if (mBackcheckingDetail != null){
            namaAocPjp = response.getUserName();
            nomorAocPjp = response.getUserId();
        }else{
            showMessage("Terjadi kesalahan mengirim data");
        }
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }
}
