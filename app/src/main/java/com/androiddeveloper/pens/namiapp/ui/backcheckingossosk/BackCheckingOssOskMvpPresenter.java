package com.androiddeveloper.pens.namiapp.ui.backcheckingossosk;

import android.content.Context;

import com.androiddeveloper.pens.namiapp.data.network.model.BackCheckingLocationListResponse;
import com.androiddeveloper.pens.namiapp.di.PerActivity;
import com.androiddeveloper.pens.namiapp.ui.backchecking.BaseBackcheckingMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.base.BaseActivity;
import com.androiddeveloper.pens.namiapp.ui.base.MvpPresenter;

import java.util.Map;

@PerActivity
public interface BackCheckingOssOskMvpPresenter<V extends BackCheckingOssOskMvpView> extends BaseBackcheckingMvpPresenter<V> {

    void getBackcheckingOssOskVisit(String locationId);

    void getBackcheckingOssOskBranding(String locationId);

    void getBackcheckingOssOskBsDsRs(String locationId);

    void getBackcheckingOssOskTopSelling(String locationId);

    void getBackcheckingOssOskSalesThrough(String locationId);

    void getBackcheckingOssOskReadSignal(String locationId);

    void submitBackchecking(Map<String, String> map, Context context, BackCheckingLocationListResponse response, String latitude,
                            String longitude);

    void submitBackcheckingIfLocationNull(Map<String, String> map, Context context, BackCheckingLocationListResponse response);

    void insertUserCheckout(String latitude, String longitude);

    String usertypeBackchecker();
}
