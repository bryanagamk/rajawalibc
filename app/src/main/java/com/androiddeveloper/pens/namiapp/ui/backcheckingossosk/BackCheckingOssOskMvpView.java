package com.androiddeveloper.pens.namiapp.ui.backcheckingossosk;

import com.androiddeveloper.pens.namiapp.data.network.model.BackCheckingLocationListResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.Backchecking;
import com.androiddeveloper.pens.namiapp.ui.backchecking.BaseBackcheckingMvpView;
import com.androiddeveloper.pens.namiapp.ui.base.MvpView;

public interface BackCheckingOssOskMvpView extends BaseBackcheckingMvpView {

    void setupBrandingMenu(Backchecking backchecking);

    void setupSalesThroughMenu(Backchecking backchecking);

    void setupTopSellingMenu(Backchecking backchecking);

    void setupBsDsRsMenu(Backchecking backchecking);

    void setupVisitMenu(Backchecking backchecking);

    void setupReadSignal(Backchecking backchecking);

    void sendOssOskBackcheckingData();

    void finishActivity();


    void openBackcheckingDetailActivity(BackCheckingLocationListResponse response);

    void openGoBackcheckingActivity();
}
