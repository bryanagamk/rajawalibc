package com.androiddeveloper.pens.namiapp.ui.backcheckingossosk;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.data.network.model.BackCheckingLocationListResponse;
import com.androiddeveloper.pens.namiapp.service.NotificationService;
import com.androiddeveloper.pens.namiapp.ui.backchecking.BaseBackcheckingMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.backchecking.BaseBackcheckingPresenter;
import com.androiddeveloper.pens.namiapp.ui.base.BaseActivity;
import com.androiddeveloper.pens.namiapp.ui.base.BasePresenter;
import com.androiddeveloper.pens.namiapp.ui.dasboard.DasboardActivity;

import java.util.Map;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class BackCheckingOssOskPresenter<V extends BackCheckingOssOskMvpView> extends BaseBackcheckingPresenter<V>
        implements BackCheckingOssOskMvpPresenter<V> {

    private static final String TAG = "BackCheckingOssOskPresenter";

    @Override
    public void onAttach(V mvpView) {
        super.onAttach(mvpView);

    }

    @Inject
    public BackCheckingOssOskPresenter(DataManager dataManager,
                                       CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);
    }

    @Override
    public void getBackcheckingOssOskVisit(String locationId) {
        getCompositeDisposable().add(getDataManager().getBackcheckingOssOskVisit(locationId)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(backcheckingResponse -> {
            if (!isViewAttached()){
                return;
            }
            if (backcheckingResponse.getSuccess()){
                getMvpView().setupVisitMenu(backcheckingResponse.getBackchecking());

            }else{
                getMvpView().showMessage(backcheckingResponse.getMessage());
            }
        },throwable -> {
            if (!isViewAttached()){
                return;
            }
            getBackcheckingOssOskVisit(locationId);
            String message = "Gagal mendapatkan data visit dari server";
            getMvpView().showMessage(message);
            Log.d(this.getClass().getSimpleName(), throwable.toString());
        }));
    }

    @Override
    public void getBackcheckingOssOskBranding(String locationId) {
        getCompositeDisposable().add(getDataManager().getBackcheckingOssOskBranding(locationId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(backcheckingResponse -> {
                    if (!isViewAttached()){
                        return;
                    }
                    if (backcheckingResponse.getSuccess()){
                        getMvpView().setupBrandingMenu(backcheckingResponse.getBackchecking());

                    }else{
                        getMvpView().showMessage(backcheckingResponse.getMessage());
                    }
                },throwable -> {
                    if (!isViewAttached()){
                        return;
                    }
                    String message = "Gagal mendapatkan data branding dari server";
                    getBackcheckingOssOskBranding(locationId);
                    getMvpView().showMessage(message);
                    Log.d(this.getClass().getSimpleName(), throwable.toString());
                }));
    }

    @Override
    public void getBackcheckingOssOskBsDsRs(String locationId) {
        getCompositeDisposable().add(getDataManager().getBackcheckingOssOskBsDsRs(locationId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(backcheckingResponse -> {
                    if (!isViewAttached()){
                        return;
                    }
                    if (backcheckingResponse.getSuccess()){
                        getMvpView().setupBsDsRsMenu(backcheckingResponse.getBackchecking());

                    }else{
                        getMvpView().showMessage(backcheckingResponse.getMessage());
                    }
                },throwable -> {
                    if (!isViewAttached()){
                        return;
                    }
                    String message = "Gagal mendapatkan data bsdsrs dari server";
                    getBackcheckingOssOskBsDsRs(locationId);
                    getMvpView().showMessage(message);
                    Log.d(this.getClass().getSimpleName(), throwable.toString());
                }));
    }

    @Override
    public void getBackcheckingOssOskTopSelling(String locationId) {
        getCompositeDisposable().add(getDataManager().getBackcheckingOssOskTopSelling(locationId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(backcheckingResponse -> {
                    if (!isViewAttached()){
                        return;
                    }
                    if (backcheckingResponse.getSuccess()){
                        getMvpView().setupTopSellingMenu(backcheckingResponse.getBackchecking());

                    }else{
                        getMvpView().showMessage(backcheckingResponse.getMessage());
                    }
                },throwable -> {
                    if (!isViewAttached()){
                        return;
                    }
                    String message = "Gagal mendapatkan top selling dari server";
                    getBackcheckingOssOskTopSelling(locationId);
                    getMvpView().showMessage(message);
                    Log.d(this.getClass().getSimpleName(), throwable.toString());
                }));
    }

    @Override
    public void getBackcheckingOssOskSalesThrough(String locationId) {
        getCompositeDisposable().add(getDataManager().getBackcheckingOssOskSalesThrough(locationId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(backcheckingResponse -> {
                    if (!isViewAttached()){
                        return;
                    }
                    if (backcheckingResponse.getSuccess()){
                        getMvpView().setupSalesThroughMenu(backcheckingResponse.getBackchecking());

                    }else{
                        getMvpView().showMessage(backcheckingResponse.getMessage());
                    }
                },throwable -> {
                    if (!isViewAttached()){
                        return;
                    }
                    String message = "Gagal mendapatkan data salesthrough dari server";
                    getBackcheckingOssOskSalesThrough(locationId);
                    getMvpView().showMessage(message);
                    Log.d(this.getClass().getSimpleName(), throwable.toString());
                }));
    }

    @Override
    public void getBackcheckingOssOskReadSignal(String locationId) {
        getCompositeDisposable().add(getDataManager().getBackcheckingOssOskReadSignal(locationId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(backcheckingResponse -> {
                    if (!isViewAttached()){
                        return;
                    }
                    if (backcheckingResponse.getSuccess()){
                        getMvpView().setupReadSignal(backcheckingResponse.getBackchecking());
                    }else {
                        getMvpView().showMessage(backcheckingResponse.getMessage());
                    }
                }, throwable -> {
                    if (!isViewAttached()){
                        return;
                    }
                    String message = "Gagal mendapatkan data read signal dari server";
                    getBackcheckingOssOskReadSignal(locationId);
                    getMvpView().showMessage(message);
                    Log.d(this.getClass().getSimpleName(), throwable.toString());
                }));
    }


    @Override
    public void submitBackchecking(Map<String, String> map, Context context, BackCheckingLocationListResponse response,
                                   String latitude, String longitude) {
        map.put("userid",getDataManager().getCurrentUserId());
        map.put("user_type",getDataManager().getCurrentUserType());
        map.put("deviceid",getDataManager().getCurrentDeviceId());
        map.put("username",getDataManager().getCurrentUserName());

        getMvpView().showLoading("Submit Backchecking");

        for(String value : map.keySet()){
            String temp = map.get(value);
            map.put(value,temp.replace(",","."));
        }

        getMvpView().showMessage("Backchecking anda sedang dikirim");
        Log.d(BackCheckingOssOskPresenter.class.getSimpleName(),response.toString());


        getCompositeDisposable().add(getDataManager().submitBackcheckingFormOssOsk(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(basicResponse -> {
                    if (!isViewAttached()){
                        return;
                    }
                    if (basicResponse.getSuccess()){
                        NotificationService.getInstance(context).backcheckingSelesaiNotification("Backhecking Executed!","Backchecking Anda pada " + map.get("location_name") + "Sudah selesai tersubmit", "Laporan backchecking Anda");
                        NotificationService.getInstance(context).sendNotifToAOC(map.get("userid_sendto"),"PJP " + map.get("location_name") + "Sudah di backcheck oleh " + map.get("username") + " Harap untuk membalas feedback","Kunjugan Terbackcheck!");
                        Log.d("Fail  :", basicResponse.getMessage());

                        insertUserCheckout(latitude,longitude);
                        getMvpView().openGoBackcheckingActivity();
                        getMvpView().finishActivity();
                        getMvpView().hideLoading();

                    }else{
                        NotificationService.getInstance(context).backcheckingSelesaiNotification("Backhecking Gagal!","Backchecking Anda pada " + map.get("location_name") + "GAGAL Tersubmit", "Laporan backchecking Anda");
                        Log.d("Fail : ",map.toString());
                        Log.d("Fail  :",basicResponse.getMessage());

                        getMvpView().hideLoading();
                    }

                },throwable -> {
                    if (!isViewAttached()){
                        return;
                    }
                    NotificationService.getInstance(context).backcheckingSelesaiNotification("Backchecking Gagal!","Backchecking terakhir anda gagal, Silahkan coba lagi", "Laporan backchecking Anda");
                    getMvpView().hideLoading();
                    getMvpView().showMessage("Gagal backchecking, Silahkan coba lagi");
                    Log.d("Backchecking : ", throwable.toString());
                }));
    }

    @Override
    public void submitBackcheckingIfLocationNull(Map<String, String> map, Context context, BackCheckingLocationListResponse response) {
        map.put("userid",getDataManager().getCurrentUserId());
        map.put("user_type",getDataManager().getCurrentUserType());
        map.put("deviceid",getDataManager().getCurrentDeviceId());
        map.put("username",getDataManager().getCurrentUserName());

        getMvpView().showLoading("Submit Backchecking");

        for(String value : map.keySet()){
            String temp = map.get(value);
            map.put(value,temp.replace(",","."));
        }

        getMvpView().showMessage("Backchecking anda sedang dikirim");
        Log.d(BackCheckingOssOskPresenter.class.getSimpleName(),response.toString());


        getCompositeDisposable().add(getDataManager().submitBackcheckingFormOssOsk(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(basicResponse -> {
                    if (!isViewAttached()){
                        return;
                    }
                    if (basicResponse.getSuccess()){
                        NotificationService.getInstance(context).backcheckingSelesaiNotification("Backhecking Executed!","Backchecking Anda pada " + map.get("location_name") + "Sudah selesai tersubmit", "Laporan backchecking Anda");
                        NotificationService.getInstance(context).sendNotifToAOC(map.get("userid_sendto"),"PJP " + map.get("location_name") + "Sudah di backcheck oleh " + map.get("username") + " Harap untuk membalas feedback","Kunjugan Terbackcheck!");
                        Log.d("Fail  :", basicResponse.getMessage());

                        getMvpView().finishActivity();
                        getMvpView().hideLoading();

                    }else{
                        NotificationService.getInstance(context).backcheckingSelesaiNotification("Backhecking Gagal!","Backchecking Anda pada " + map.get("location_name") + "GAGAL Tersubmit", "Laporan backchecking Anda");
                        Log.d("Fail : ",map.toString());
                        Log.d("Fail  :",basicResponse.getMessage());

                        getMvpView().hideLoading();
                    }

                },throwable -> {
                    if (!isViewAttached()){
                        return;
                    }
                    NotificationService.getInstance(context).backcheckingSelesaiNotification("Backchecking Gagal!","Backchecking terakhir anda gagal, Silahkan coba lagi", "Laporan backchecking Anda");
                    getMvpView().hideLoading();
                    getMvpView().showMessage("Gagal backchecking, Silahkan coba lagi");
                    Log.d("Backchecking : ", throwable.toString());
                }));
    }

    @Override
    public void insertUserCheckout(String latitude, String longitude) {
        String userId = getDataManager().getCurrentUserId();

        getCompositeDisposable().add(getDataManager().insertCheckoutStatus(userId,latitude,longitude)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(basicResponse -> {
                    if (!isViewAttached()){
                        return;
                    }
                    getMvpView().showMessage(basicResponse.getMessage());
                    getMvpView().finish();
                },throwable -> {
                    if (!isViewAttached()){
                        return;
                    }
                    getMvpView().showMessage("Gagal Checkout, Tidak Tersambung");
                    insertUserCheckout(latitude,longitude);
                    getMvpView().finish();
                }));
    }

    @Override
    public String usertypeBackchecker(){
        return getDataManager().getCurrentUserType().toLowerCase();
    }
}
