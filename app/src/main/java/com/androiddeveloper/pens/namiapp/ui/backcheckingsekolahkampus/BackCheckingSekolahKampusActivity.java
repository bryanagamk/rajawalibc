package com.androiddeveloper.pens.namiapp.ui.backcheckingsekolahkampus;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.data.AppDataManager;
import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.data.network.model.BackCheckingLocationListResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.Backchecking;
import com.androiddeveloper.pens.namiapp.data.network.model.LoginResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.User;
import com.androiddeveloper.pens.namiapp.data.network.rest.ApiClient;
import com.androiddeveloper.pens.namiapp.ui.backchecking.BaseBackcheckingActivity;
import com.androiddeveloper.pens.namiapp.ui.backcheckinglocation.BackCheckingLocationActivity;
import com.androiddeveloper.pens.namiapp.ui.backcheckinglocationlist.BackCheckingLocationListActivity;
import com.androiddeveloper.pens.namiapp.ui.backcheckingossosk.BackCheckingOssOskActivity;
import com.androiddeveloper.pens.namiapp.ui.detailbackcheckinglocation.DetailBackCheckingLocationActivity;
import com.androiddeveloper.pens.namiapp.utils.LocationUtils;
import com.androiddeveloper.pens.namiapp.utils.LocationValidatorUtil;
import com.androiddeveloper.pens.namiapp.utils.SingleShotLocationProvider;
import com.androiddeveloper.pens.namiapp.utils.StartupConfig;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ceylonlabs.imageviewpopup.ImagePopup;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BackCheckingSekolahKampusActivity extends BaseBackcheckingActivity implements BackCheckingSekolahKampusMvpView {

    @Inject
    BackCheckingSekolahKampusMvpPresenter<BackCheckingSekolahKampusMvpView> mPresenter;

    // id radiobutton sesuai
    @BindView(R.id.rb_sesuai_kunjungan_sekolah_kampus)
    RadioButton rbSesuaiKunjunganSekolahKampus;
    @BindView(R.id.rb_sesuai_sales_sekolah_kampus)
    RadioButton rbSesuaiSalesSekolahKampus;
    @BindView(R.id.rb_sesuai_jml_siswa_sekolah_kampus)
    RadioButton rbSesuaiJumlahSiswaSekolahKampus;
    // id radiobutton tidak sesuai
    @BindView(R.id.rb_tidak_sesuai_kunjungan_sekolah_kampus)
    RadioButton rbTidakSesuaiKunjunganSekolahKampus;
    @BindView(R.id.rb_tidak_sesuai_sales_sekolah_kampus)
    RadioButton rbTidakSesuaiSalesSekolahKampus;
    @BindView(R.id.rb_tidak_sesuai_jml_siswa_sekolah_kampus)
    RadioButton rbTidakSesuaiJumlahSiswaSekolahKampus;
    // id note
    @BindView(R.id.et_note_kunjungan_sekolah_kampus)
    EditText etNoteKunjunganSekolahKampus;
    @BindView(R.id.et_note_sales_sekolah_kampus)
    EditText etNoteSalesSekolahKampus;
    @BindView(R.id.et_note_jml_siswa_sekolah_kampus)
    EditText etNoteJumlahSiswaSekolahKampus;
    @BindView(R.id.et_branding_kantin_sekolah_kampus)
    EditText etBrandingSekolahKampus;
    @BindView(R.id.et_merchandising_sekolah_kampus)
    EditText etMerchandisingSekolahKampus;
    // id market share
    @BindView(R.id.et_market_share_sekolah_kampus_tsel)
    EditText etMarketTselSekolahKampus;
    @BindView(R.id.et_market_share_sekolah_kampus_ooredoo)
    EditText etMarketOoredooSekolahKampus;
    @BindView(R.id.et_market_share_sekolah_kampus_xl)
    EditText etMarketXlSekolahKampus;
    @BindView(R.id.et_market_share_sekolah_kampus_axis)
    EditText etMarketAxisSekolahKampus;
    @BindView(R.id.et_market_share_sekolah_kampus_3)
    EditText etMarket3SekolahKampus;
    @BindView(R.id.et_market_share_sekolah_kampus_others)
    EditText etMarketOthersSekolahKampus;
    @BindView(R.id.et_note_market_share_sekolah_kampus)
    EditText etNoteMarketShareSekolahKampus;
    @BindView(R.id.btn_send_backchecking_sekolah_kampus)
    Button btnSubmit;

    private List<EditText> listMarketShare;
    EditText textSave[] = { etNoteKunjunganSekolahKampus, etNoteSalesSekolahKampus, etNoteJumlahSiswaSekolahKampus,
            etBrandingSekolahKampus, etMerchandisingSekolahKampus, etMarketTselSekolahKampus, etMarketOoredooSekolahKampus,
            etMarketXlSekolahKampus, etMarketAxisSekolahKampus, etMarket3SekolahKampus, etMarketOthersSekolahKampus, etNoteMarketShareSekolahKampus};


    private String mLocationId = "";
    private Backchecking mBackchecking;
    boolean acceptable = true;
    private String namaAocPjp;
    private String nomorAocPjp;
    private BackCheckingLocationListResponse mBackcheckingDetail;
    private ProgressDialog progressDialog;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, BackCheckingSekolahKampusActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_back_checking_sekolah_kampus);

        getActivityComponent().inject(this);

        if (isNetworkAvailable()){

            setUnBinder(ButterKnife.bind(this));

            mPresenter.onAttach(this);

            setUp();
        }


    }

    @Override
    protected void setUp() {
        progressDialog = new ProgressDialog(this);
        listMarketShare = new ArrayList<>();
        //default rb
        etNoteKunjunganSekolahKampus.setText("Sesuai");
        etNoteSalesSekolahKampus.setText("Sesuai");
        etNoteJumlahSiswaSekolahKampus.setText("Sesuai");
        // backpress
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Back Checking POI");


        // radiobutton validasi kunjungan terakhir
        rbSesuaiKunjunganSekolahKampus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    etNoteKunjunganSekolahKampus.setEnabled(false);
                    etNoteKunjunganSekolahKampus.setFocusable(false);
                    etNoteKunjunganSekolahKampus.setFocusableInTouchMode(false);
                    etNoteKunjunganSekolahKampus.setHint("Note disable");
                    etNoteKunjunganSekolahKampus.setText("Sesuai");
                    rbSesuaiKunjunganSekolahKampus.getText().toString();
                    etNoteKunjunganSekolahKampus.setVisibility(View.GONE);
                    rbSesuaiKunjunganSekolahKampus.setChecked(true);
                    rbTidakSesuaiKunjunganSekolahKampus.setChecked(false);
                }
            }
        });
        rbTidakSesuaiKunjunganSekolahKampus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    etNoteKunjunganSekolahKampus.setEnabled(true);
                    etNoteKunjunganSekolahKampus.setFocusable(true);
                    etNoteKunjunganSekolahKampus.setFocusableInTouchMode(true);
                    etNoteKunjunganSekolahKampus.setHint("Catatan tentang kunjungan");
                    etNoteKunjunganSekolahKampus.setText("");
                    rbTidakSesuaiKunjunganSekolahKampus.getText().toString();
                    etNoteKunjunganSekolahKampus.setVisibility(View.VISIBLE);
                    rbTidakSesuaiKunjunganSekolahKampus.setChecked(true);
                    rbSesuaiKunjunganSekolahKampus.setChecked(false);
                }
            }
        });

        // radiobutton validasi sales sekolah kampus
        rbSesuaiSalesSekolahKampus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    etNoteSalesSekolahKampus.setEnabled(false);
                    etNoteSalesSekolahKampus.setFocusable(false);
                    etNoteSalesSekolahKampus.setFocusableInTouchMode(false);
                    etNoteSalesSekolahKampus.setHint("Note disable");
                    etNoteSalesSekolahKampus.setText("Sesuai");
                    rbSesuaiSalesSekolahKampus.getText().toString();
                    etNoteSalesSekolahKampus.setVisibility(View.GONE);
                    rbSesuaiSalesSekolahKampus.setChecked(true);
                    rbTidakSesuaiSalesSekolahKampus.setChecked(false);
                }
            }
        });
        rbTidakSesuaiSalesSekolahKampus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    etNoteSalesSekolahKampus.setEnabled(true);
                    etNoteSalesSekolahKampus.setFocusable(true);
                    etNoteSalesSekolahKampus.setFocusableInTouchMode(true);
                    etNoteSalesSekolahKampus.setHint("Catatan tentang sales through perdana");
                    etNoteSalesSekolahKampus.setText("");
                    rbTidakSesuaiSalesSekolahKampus.getText().toString();
                    etNoteSalesSekolahKampus.setVisibility(View.VISIBLE);
                    rbTidakSesuaiSalesSekolahKampus.setChecked(true);
                    rbSesuaiSalesSekolahKampus.setChecked(false);
                }
            }
        });

        // radiobutton validasi jumlah siswa sekolah kampus
        rbSesuaiJumlahSiswaSekolahKampus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    etNoteJumlahSiswaSekolahKampus.setEnabled(false);
                    etNoteJumlahSiswaSekolahKampus.setFocusable(false);
                    etNoteJumlahSiswaSekolahKampus.setFocusableInTouchMode(false);
                    etNoteJumlahSiswaSekolahKampus.setHint("Note disable");
                    etNoteJumlahSiswaSekolahKampus.setText("Sesuai");
                    rbSesuaiJumlahSiswaSekolahKampus.getText().toString();
                    etNoteJumlahSiswaSekolahKampus.setVisibility(View.GONE);
                    rbSesuaiJumlahSiswaSekolahKampus.setChecked(true);
                    rbTidakSesuaiJumlahSiswaSekolahKampus.setChecked(false);
                }
            }
        });
        rbTidakSesuaiJumlahSiswaSekolahKampus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    etNoteJumlahSiswaSekolahKampus.setEnabled(true);
                    etNoteJumlahSiswaSekolahKampus.setFocusable(true);
                    etNoteJumlahSiswaSekolahKampus.setFocusableInTouchMode(true);
                    etNoteJumlahSiswaSekolahKampus.setHint("Potensi jumlah siswa");
                    etNoteJumlahSiswaSekolahKampus.setText("");
                    rbTidakSesuaiJumlahSiswaSekolahKampus.getText().toString();
                    etNoteJumlahSiswaSekolahKampus.setVisibility(View.VISIBLE);
                    rbTidakSesuaiJumlahSiswaSekolahKampus.setChecked(true);
                    rbSesuaiJumlahSiswaSekolahKampus.setChecked(false);
                }
            }
        });

        // save data when backpress
        EditText textSave[] = { etNoteKunjunganSekolahKampus, etNoteSalesSekolahKampus, etNoteJumlahSiswaSekolahKampus,
                etBrandingSekolahKampus, etMerchandisingSekolahKampus, etMarketTselSekolahKampus, etMarketOoredooSekolahKampus,
                etMarketXlSekolahKampus, etMarketAxisSekolahKampus, etMarket3SekolahKampus, etMarketOthersSekolahKampus, etNoteMarketShareSekolahKampus};
        for (EditText editTextSaveData : textSave){
            SharedPreferences prefs = getPreferences(MODE_PRIVATE);
            String noteBranding = prefs.getString("branding", "");
            String noteMerchandising = prefs.getString("merchandising", "");
            String msTsel = prefs.getString("mstsel", "");
            String msOoredoo = prefs.getString("msooredoo", "");
            String msXl = prefs.getString("msxl", "");
            String msAxis = prefs.getString("msaxis", "");
            String ms3 = prefs.getString("ms3", "");
            String msOthers = prefs.getString("msothers", "");
            String marketShare = prefs.getString("market_share", "");
            if(editTextSaveData.getText().toString() != null || !TextUtils.isEmpty(editTextSaveData.getText().toString())){
                etBrandingSekolahKampus.setText(noteBranding);
                etMerchandisingSekolahKampus.setText(noteMerchandising);
                etMarketTselSekolahKampus.setText(msTsel);
                etMarketOoredooSekolahKampus.setText(msOoredoo);
                etMarketXlSekolahKampus.setText(msXl);
                etMarketAxisSekolahKampus.setText(msAxis);
                etMarket3SekolahKampus.setText(ms3);
                etMarketOthersSekolahKampus.setText(msOthers);
                etNoteMarketShareSekolahKampus.setText(marketShare);
            }
        }

        // validasi all market share
        EditText[] editTextsMarket = {etMarketTselSekolahKampus, etMarketOoredooSekolahKampus, etMarketXlSekolahKampus
                , etMarketAxisSekolahKampus, etMarket3SekolahKampus, etMarketOthersSekolahKampus};
        TextView[] tvMarketShare = {
                findViewById(R.id.tv_generate_market_share_sekolah_kampus_tsel),
                findViewById(R.id.tv_generate_market_share_sekolah_kampus_ooredoo),
                findViewById(R.id.tv_generate_market_share_sekolah_kampus_xl),
                findViewById(R.id.tv_generate_market_share_sekolah_kampus_axis),
                findViewById(R.id.tv_generate_market_share_sekolah_kampus_3),
                findViewById(R.id.tv_generate_market_share_sekolah_kampus_others),
        };
        for (EditText editTextmarket : editTextsMarket) {
            editTextmarket.setTransformationMethod(null);
            listMarketShare.add(editTextmarket);
        }
        addMarketShareValidation(editTextsMarket,tvMarketShare);



        mLocationId = getIntent().getStringExtra("location_id");

        getBackcheckingDataFromServer(mLocationId);

        btnSubmit.setOnClickListener(v -> {
            if (acceptable){
                if(!validateEditText(ids))
                {
                    //if not empty do something
                    sendInstitutionBackcheckingData();
                }else{
                    //if empty do somethingelse
                    Toast.makeText(getApplicationContext(), "Ada isian yang kosong", Toast.LENGTH_SHORT).show();
                }
            }else{
                showMessage("Maaf salah satu data yang anda isikan ada yang tidak sesuai");
            }
        });

        setUserSenderMenu();
    }

    int[] ids = new int[]
    {
            // market share
            R.id.et_market_share_sekolah_kampus_tsel,
            R.id.et_market_share_sekolah_kampus_ooredoo,
            R.id.et_market_share_sekolah_kampus_xl,
            R.id.et_market_share_sekolah_kampus_axis,
            R.id.et_market_share_sekolah_kampus_3,
            R.id.et_market_share_sekolah_kampus_others,

            R.id.et_note_kunjungan_sekolah_kampus,
            R.id.et_note_sales_sekolah_kampus,
            R.id.et_note_jml_siswa_sekolah_kampus,
            R.id.et_branding_kantin_sekolah_kampus,
            R.id.et_merchandising_sekolah_kampus,
            R.id.et_note_market_share_sekolah_kampus,
    };

    public boolean validateEditText(int[] ids)
    {
        boolean isEmpty = false;

        for(int id: ids)
        {
            EditText et = (EditText)findViewById(id);

            if(TextUtils.isEmpty(et.getText().toString()))
            {
                et.setError("Wajib diisi");
                isEmpty = true;
            }
        }

        return isEmpty;
    }

    private void addMarketShareValidation(EditText[] listMarketShare,TextView[] textViews) {
        for (EditText market : listMarketShare){
            market.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    String text = s.toString();
                    if (text.equals("")) {
                        market.setError("Data tidak boleh kosong");
                        acceptable = false;
                    } else {
                        acceptable = true;
                        Log.d("Persentase Share",String.format("Lengt et market share : %d, lenght tv market share %d",listMarketShare.length,textViews.length));
                        generatePersentaseShare(listMarketShare,textViews);
                    }
                }
            });
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public static boolean checkNullValues (String valueToCheck){
        if (!(valueToCheck == null)) {
            String valueCheck = valueToCheck.trim();
            if (valueCheck.equals("") || valueCheck.equals("0")) {
                return false;
            }
            return true;
        }
        return false;
    }

    @Override
    public void setupBrandingMenu(Backchecking backchecking) {
        TextView tvShopsign = findViewById(R.id.tv_info_shopsign_sekolah_kampus);
        TextView tvTableCloth = findViewById(R.id.tv_info_taplak_meja_sekolah_kampus);
        TextView tvSachet = findViewById(R.id.tv_info_sachet_sekolah_kampus);
        ImageView ivCanteen = findViewById(R.id.image_brand_kantin);

        Glide.with(this)
                .load(ApiClient.BASE_URL_NAMI_IMAGE+ backchecking.getCanteenPath())
                .apply(new RequestOptions()
                .placeholder(R.drawable.ic_none_picture)
                .error(R.drawable.ic_none_picture))
                .into(ivCanteen);

        /** Set popup height, width & background color as you need or just leave default settings **/

        final ImagePopup imagePopup = new ImagePopup(this);
        imagePopup.setWindowHeight(800); // Optional
        imagePopup.setWindowWidth(800); // Optional
        imagePopup.setBackgroundColor(Color.WHITE);  // Optional
        imagePopup.setFullScreen(true); // Optional
        imagePopup.setHideCloseIcon(true);  // Optional
        imagePopup.setImageOnClickClose(true);  // Optional

        imagePopup.initiatePopupWithGlide(ApiClient.BASE_URL_NAMI_IMAGE+ backchecking.getCanteenPath());

        ivCanteen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /** Initiate Popup view **/
                imagePopup.viewPopup();

            }
        });

        if (backchecking.getShopsignBranding().equals("UNKNOWN")){
            tvShopsign.setText("0");
        }else{
            tvShopsign.setText(backchecking.getShopsignBranding());
        }

        if (backchecking.getTableclothBranding().equals("UNKNOWN")){
            tvTableCloth.setText("0");
        }else{
            tvTableCloth.setText(backchecking.getTableclothBranding());
        }

        if (backchecking.getSachet().equals("UNKNOWN")){
            tvSachet.setText("0");
        }else{
            tvSachet.setText(backchecking.getSachet());
        }

        this.mBackchecking= backchecking;

    }

    @Override
    public void setupMerchandisingMenu(Backchecking backchecking) {
        TextView tvInfoPoster = findViewById(R.id.tv_info_poster_sekolah_kampus);
        TextView tvInfoBanner = findViewById(R.id.tv_info_banner_sekolah_kampus);
        TextView tvInfoShopsign = findViewById(R.id.tv_info_shopsign_merchandising_sekolah_kampus);
        TextView tvInfoTaplakMeja = findViewById(R.id.tv_info_taplak_meja_merchandising_sekolah_kampus);

        tvInfoPoster.setText(backchecking.getTotalPoster());
        tvInfoBanner.setText(backchecking.getTotalBanner());
        tvInfoShopsign.setText(backchecking.getTotalShopsign());
        tvInfoTaplakMeja.setText(backchecking.getTotalTablecloth());
    }

    @Override
    public void setupMarketShareMenu(Backchecking backchecking) {
        TextView tvMsTelkomsel = findViewById(R.id.tv_market_share_sekolah_kampus_tsel);
        TextView tvMsOoredoo = findViewById(R.id.tv_market_share_sekolah_kampus_ooredoo);
        TextView tvMsXl = findViewById(R.id.tv_market_share_sekolah_kampus_xl);
        TextView tvMsAxis = findViewById(R.id.tv_market_share_sekolah_kampus_axis);
        TextView tvMs3 = findViewById(R.id.tv_market_share_sekolah_kampus_3);
        TextView tvMsOthers = findViewById(R.id.tv_market_share_sekolah_kampus_others);

        //Ms Telkomsel
        if (backchecking.getMsTelkomsel() == null ){
            tvMsTelkomsel.setText(0 + "%");
        }else{
            tvMsTelkomsel.setText(backchecking.getMsTelkomsel() + "%");
        }

        //Ms Ooredoo
        if (backchecking.getMsOoredoo() == null ){
            tvMsOoredoo.setText(0 + "%");
        }else{
            tvMsOoredoo.setText(backchecking.getMsOoredoo() + "%");
        }

        //Ms Xl
        if (backchecking.getMsXl() == null ){
            tvMsXl.setText(0 + "%");
        }else{
            tvMsXl.setText(backchecking.getMsXl() + "%");
        }

        //Ms Axis
        if (backchecking.getMsAxis() == null ){
            tvMsAxis.setText(0 + "%");
        }else{
            tvMsAxis.setText(backchecking.getMsAxis() + "%");
        }

        //Ms 3
        if (backchecking.getMs3() == null ){
            tvMs3.setText(0 + "%");
        }else{
            tvMs3.setText(backchecking.getMs3() + "%");
        }

        //Ms Others
        if (backchecking.getMsOthers() == null ){
            tvMsOthers.setText(0 + "%");
        }else{
            tvMsOthers.setText(backchecking.getMsOthers() + "%");
        }
    }

    @Override
    public void setupSalesThroughMenu(Backchecking backchecking) {
        TextView tvBulanBerjalanKunjunganTerakhir = findViewById(R.id.tv_sales_through_inet_total_name);
        TextView tvBulanBerjalanKunjunaganTerakhirName = findViewById(R.id.tv_sales_through_name);
        TextView tvBulanBerjalanKunjunaganTerakhirJumlah = findViewById(R.id.tv_sales_through_jumlah);
        TextView tvBulanBerjalanTitikDua = findViewById(R.id.tv_titik_dua_poi);
        ImageView ivSalesThrough = findViewById(R.id.iv_sales_through);

        Glide.with(this)
                .load(ApiClient.BASE_URL_NAMI_IMAGE+ backchecking.getCanteenPath())
                .apply(new RequestOptions()
                        .placeholder(R.drawable.ic_none_picture)
                        .error(R.drawable.ic_none_picture))
                .into(ivSalesThrough);

        /** Set popup height, width & background color as you need or just leave default settings **/

        final ImagePopup imagePopup = new ImagePopup(this);
        imagePopup.setWindowHeight(800); // Optional
        imagePopup.setWindowWidth(800); // Optional
        imagePopup.setBackgroundColor(Color.WHITE);  // Optional
        imagePopup.setFullScreen(true); // Optional
        imagePopup.setHideCloseIcon(true);  // Optional
        imagePopup.setImageOnClickClose(true);  // Optional

        imagePopup.initiatePopupWithGlide(ApiClient.BASE_URL_NAMI_IMAGE+ backchecking.getCanteenPath());

        ivSalesThrough.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /** Initiate Popup view **/
                imagePopup.viewPopup();

            }
        });


        if (backchecking.getBroadbandSales() == null ){
            tvBulanBerjalanKunjunganTerakhir.setText("?");
            return;
        }

        StringBuilder sbName = new StringBuilder();
        StringBuilder sbGb = new StringBuilder();
        StringBuilder titikDua = new StringBuilder();
        StringBuilder sbJumlah = new StringBuilder();

        if (backchecking.getBroadbandSales() != null){
            Map<String, Boolean> checker = new HashMap<>();
            for (String text : backchecking.getBroadbandSales().split("\\|")){
                String[] texts = text.split(" ");
                if (checker.get(texts[0]) == null){
                    sbName.append(texts[0]).append("\n");
                    checker.put(texts[0], Boolean.TRUE);
                }else {
                    sbName.append("\n");
                }
                sbGb.append(texts[1].replace(":", "")).append("\n");
                titikDua.append(":\t\t\t\n");
                sbJumlah.append(texts[2].replace(";", "\t\t\t\t\t\t\t")).append("\n");
            }
            tvBulanBerjalanKunjunaganTerakhirName.setText(sbName.toString());
            tvBulanBerjalanKunjunganTerakhir.setText(sbGb.toString());
            tvBulanBerjalanTitikDua.setText(titikDua.toString());
            tvBulanBerjalanKunjunaganTerakhirJumlah.setText(sbJumlah.toString());
        }else {
            tvBulanBerjalanKunjunganTerakhir.setText("Null");
        }
    }

    @Override
    public void setupTotalStudent(Backchecking backchecking){
        TextView tvTotalStudent = findViewById(R.id.tv_jml_siswa_sekolah_kampus);

        if (backchecking.getJumlahSiswa() == null){
            tvTotalStudent.setText(0 + "");
        }else {
            tvTotalStudent.setText(backchecking.getJumlahSiswa() + "");
        }
    }

    @Override
    public void setupVisitMenu(Backchecking backchecking) {
        TextView tvWaktuBackchecking = findViewById(R.id.tv_date_backchecking_sekolah_kampus);
        TextView tvNamaAoc = findViewById(R.id.tv_nama_aoc_backchecking_sekolah_kampus);
        TextView tvNomorAoc = findViewById(R.id.tv_nomor_aoc_backchecking_sekolah_kampus);

        if (backchecking.getMaxVisit() != null )
            tvWaktuBackchecking.setText(backchecking.getMaxVisit());
        tvNamaAoc.setText(backchecking.getUserName());
        tvNomorAoc.setText(backchecking.getUserId());
    }

    @Override
    public void setupReadSignal(Backchecking backchecking) {
        TextView tvTselVoice = findViewById(R.id.tv_poi_kualitas_signal_tsel_voice);
        TextView tvTsel4g = findViewById(R.id.tv_poi_kualitas_signal_tsel_4g);
        TextView tvTsel3g = findViewById(R.id.tv_poi_kualitas_signal_tsel_3g);
        TextView tvOoredooVoice = findViewById(R.id.tv_poi_kualitas_signal_ooredoo_voice);
        TextView tvOoredoo4g = findViewById(R.id.tv_poi_kualitas_signal_ooredoo_4g);
        TextView tvOoredoo3g = findViewById(R.id.tv_poi_kualitas_signal_ooredoo_3g);
        TextView tvXlVoice = findViewById(R.id.tv_poi_kualitas_signal_xl_voice);
        TextView tvXl4g = findViewById(R.id.tv_poi_kualitas_signal_xl_4g);
        TextView tvXl3g = findViewById(R.id.tv_poi_kualitas_signal_xl_3g);
        TextView tvAxisVoice = findViewById(R.id.tv_poi_kualitas_signal_axis_voice);
        TextView tvAxis4g = findViewById(R.id.tv_poi_kualitas_signal_axis_4g);
        TextView tvAxis3g = findViewById(R.id.tv_poi_kualitas_signal_axis_3g);
        TextView tvthreeVoice = findViewById(R.id.tv_poi_kualitas_signal_3_voice);
        TextView tvthree4g = findViewById(R.id.tv_poi_kualitas_signal_3_4g);
        TextView tvthree3g = findViewById(R.id.tv_poi_kualitas_signal_3_3g);
        TextView tvOthersVoice = findViewById(R.id.tv_poi_kualitas_signal_others_voice);
        TextView tvOthers4g = findViewById(R.id.tv_poi_kualitas_signal_others_4g);
        TextView tvOthers3g = findViewById(R.id.tv_poi_kualitas_signal_others_3g);

        //tsel
        tvTselVoice.setText(backchecking.getVoiceTelkomsel());
        tvTsel4g.setText(backchecking.getJsonMember4gTelkomsel());
        tvTsel3g.setText(backchecking.getJsonMember3gTelkomsel());
        //ooredoo
        tvOoredooVoice.setText(backchecking.getVoiceOoredoo());
        tvOoredoo4g.setText(backchecking.getJsonMember4gOoredoo());
        tvOoredoo3g.setText(backchecking.getJsonMember3gOoredoo());
        //xl
        tvXlVoice.setText(backchecking.getVoiceXl());
        tvXl4g.setText(backchecking.getJsonMember4gXl());
        tvXl3g.setText(backchecking.getJsonMember3gXl());
        //axis
        tvAxisVoice.setText(backchecking.getVoiceAxis());
        tvAxis4g.setText(backchecking.getJsonMember4gAxis());
        tvAxis3g.setText(backchecking.getJsonMember3gAxis());
        //3
        tvthreeVoice.setText(backchecking.getVoice3());
        tvthree4g.setText(backchecking.getJsonMember4g3());
        tvthree3g.setText(backchecking.getJsonMember3g3());
        //others
        tvOthersVoice.setText(backchecking.getVoiceOthers());
        tvOthers4g.setText(backchecking.getJsonMember4gOthers());
        tvOthers3g.setText(backchecking.getJsonMember3gOthers());

    }

    @Override
    public void sendInstitutionBackcheckingData() {
        String kunjunganKampusSesuai = "";
        String kunjunganKampusNote = "";
        String salesThroughSesuai = "";
        String salesThroughNote = "";
        String totalSiswa = "";
        String jumlahSiswaSesuai = "";
        String jumlahSiswaNote = "";


        String brandingNote = "";

        String merchandisingNote = "";

        String msTelkomsel = "";
        String msOoredoo = "";
        String msXl = "";
        String msAxis = "";
        String ms3 = "";
        String msOthers = "";
        String msNote = "";


        if (rbSesuaiKunjunganSekolahKampus.isChecked()){
            kunjunganKampusSesuai = rbSesuaiKunjunganSekolahKampus.getText().toString();
        }else{
            kunjunganKampusSesuai = rbTidakSesuaiKunjunganSekolahKampus.getText().toString();
            EditText etKunjunganKampusNote = findViewById(R.id.et_note_kunjungan_sekolah_kampus);
            kunjunganKampusNote = etKunjunganKampusNote.getText().toString();
        }

        if (rbSesuaiSalesSekolahKampus.isChecked()){
            salesThroughSesuai = rbSesuaiKunjunganSekolahKampus.getText().toString();
        }else{
            salesThroughSesuai = rbTidakSesuaiKunjunganSekolahKampus.getText().toString();
            EditText etSalesThroughSesuai = findViewById(R.id.et_note_sales_sekolah_kampus);
            salesThroughNote= etSalesThroughSesuai.getText().toString();
        }

        totalSiswa = ((TextView)findViewById(R.id.tv_jml_siswa_sekolah_kampus)).getText().toString().replace("Siswa","");
        if (rbSesuaiJumlahSiswaSekolahKampus.isChecked()){
            jumlahSiswaSesuai = rbSesuaiJumlahSiswaSekolahKampus.getText().toString();
            jumlahSiswaNote = totalSiswa;
        }else {
            jumlahSiswaSesuai = rbTidakSesuaiJumlahSiswaSekolahKampus.getText().toString();
            EditText etjumlahSiswaSesuai = findViewById(R.id.et_note_jml_siswa_sekolah_kampus);
            jumlahSiswaNote = etjumlahSiswaSesuai.getText().toString();
        }

        totalSiswa = ((TextView)findViewById(R.id.tv_jml_siswa_sekolah_kampus)).getText().toString();

        brandingNote = ((EditText)findViewById(R.id.et_branding_kantin_sekolah_kampus)).getText().toString();

        merchandisingNote = ((EditText) findViewById(R.id.et_merchandising_sekolah_kampus)).getText().toString();

        msTelkomsel = ((TextView) findViewById(R.id.tv_generate_market_share_sekolah_kampus_tsel)).getText().toString().replace("%","");
        msOoredoo = ((TextView) findViewById(R.id.tv_generate_market_share_sekolah_kampus_ooredoo)).getText().toString().replace("%","");
        msXl = ((TextView) findViewById(R.id.tv_generate_market_share_sekolah_kampus_xl)).getText().toString().replace("%","");
        msAxis = ((TextView) findViewById(R.id.tv_generate_market_share_sekolah_kampus_axis)).getText().toString().replace("%","");
        ms3 = ((TextView) findViewById(R.id.tv_generate_market_share_sekolah_kampus_3)).getText().toString().replace("%","");
        msOthers = ((TextView) findViewById(R.id.tv_generate_market_share_sekolah_kampus_others)).getText().toString().replace("%","");
        msNote = ((EditText) findViewById(R.id.et_note_market_share_sekolah_kampus)).getText().toString().replace("%","");

        Map<String, String> map = new HashMap<>();
        map.put("visit_matched",kunjunganKampusSesuai);
        map.put("visit_note",kunjunganKampusNote);
        map.put("sales_through_matched", salesThroughSesuai);
        map.put("sales_through_note", salesThroughNote);
        map.put("total_student", totalSiswa);
        map.put("total_student_matched", jumlahSiswaSesuai);
        map.put("total_student_by_user", jumlahSiswaNote);

        map.put("branding_note", brandingNote);

        map.put("merchandising_note", merchandisingNote);

        map.put("ms_telkomsel", msTelkomsel);
        map.put("ms_ooredoo", msOoredoo);
        map.put("ms_xl", msXl);
        map.put("ms_axis", msAxis);
        map.put("ms_3", ms3);
        map.put("ms_others", msOthers);
        map.put("ms_note", msNote);

        map.put("location_id",mBackcheckingDetail.getLocationId());
        map.put("location_name",mBackcheckingDetail.getInstitutionName());
        map.put("userid_sendto",nomorAocPjp);
        map.put("username_sendto",namaAocPjp);

        final boolean[] isSuccess = {false};

        LocationUtils.requestLocationUpdateWithCheckPermission(this, new LocationUtils.IUserLocationListener() {
            @Override
            public void onStateChange(State state) {

            }

            @Override
            public void onLocationChanged(Location location) {
                Log.d("LOCATION BC", "onLocationChanged: " + location);
                if (location != null && !isSuccess[0]){
                    isSuccess[0] = true;
                    dataClearPref(BackCheckingSekolahKampusActivity.this);
                    mPresenter.submitBackchecking(map, BackCheckingSekolahKampusActivity.this,mBackcheckingDetail,String.valueOf(location.getLatitude()),
                            String.valueOf(location.getLongitude()));
                }else if (location == null){
                    isSuccess[0] =false;
                    dataClearPref(BackCheckingSekolahKampusActivity.this);
                    mPresenter.submitBackcheckingIfLocationNull(map, BackCheckingSekolahKampusActivity.this, mBackcheckingDetail);
                    showMessage("Silahkan checkout manual");
                }
                LocationUtils.removeLocationUpdate(this);
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // show menu when menu button is pressed
        String usertypebc = mPresenter.usertypeBackchecker();
        Log.d("USERTYPE BC", "onCreateOptionsMenu: " + usertypebc);
        if (usertypebc != null && usertypebc.equals("mgt1")){
                MenuInflater inflater = getMenuInflater();
                inflater.inflate(R.menu.menu_checkout, menu);
                return true;
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // display a message when a button was pressed
        String message = "";
        if (item.getItemId() == R.id.action_checkout) {
            // action checkout
            final boolean[] isSuccess = {false};
            LocationUtils.requestLocationUpdateWithCheckPermission(BackCheckingSekolahKampusActivity.this, new LocationUtils.IUserLocationListener() {
                @Override
                public void onStateChange(State state) {

                }

                @Override
                public void onLocationChanged(Location location) {
                    if (location != null && !isSuccess[0]){
                        isSuccess[0] = true;
                        Intent intent = BackCheckingLocationListActivity.getStartIntent(BackCheckingSekolahKampusActivity.this);
                        intent.addFlags(intent.FLAG_ACTIVITY_CLEAR_TOP
                                | intent.FLAG_ACTIVITY_NEW_TASK);
                        dataClearPref(BackCheckingSekolahKampusActivity.this);
                        mPresenter.insertUserCheckout(
                                String.valueOf(location.getLatitude()),
                                String.valueOf(location.getLongitude()));
                        startActivity(intent);
                    }else if (location == null){
                        isSuccess[0] = false;
                        Intent intent = BackCheckingLocationListActivity.getStartIntent(BackCheckingSekolahKampusActivity.this);
                        intent.addFlags(intent.FLAG_ACTIVITY_CLEAR_TOP
                                | intent.FLAG_ACTIVITY_NEW_TASK);
                        dataClearPref(BackCheckingSekolahKampusActivity.this);
                        mPresenter.insertUserCheckout(
                                String.valueOf(location.getLatitude()),
                                String.valueOf(location.getLongitude()));
                        startActivity(intent);
                    }
                    LocationUtils.removeLocationUpdate(this);
                }

                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {

                }

                @Override
                public void onProviderEnabled(String s) {

                }

                @Override
                public void onProviderDisabled(String s) {

                }
            });
            return true;
        }
        return false;
    }

    @Override
    public void onBackPressed(){
        SharedPreferences.Editor editor = getPreferences(MODE_PRIVATE).edit();
        editor.putString("branding", etBrandingSekolahKampus.getText().toString());
        editor.putString("merchandising", etMerchandisingSekolahKampus.getText().toString());
        editor.putString("mstsel", etMarketTselSekolahKampus.getText().toString());
        editor.putString("msooredoo", etMarketOoredooSekolahKampus.getText().toString());
        editor.putString("msxl", etMarketXlSekolahKampus.getText().toString());
        editor.putString("msaxis", etMarketAxisSekolahKampus.getText().toString());
        editor.putString("ms3", etMarket3SekolahKampus.getText().toString());
        editor.putString("msothers", etMarketOthersSekolahKampus.getText().toString());
        editor.putString("market_share", etNoteMarketShareSekolahKampus.getText().toString());
        editor.commit();
        super.onBackPressed();
    }

    public void dataClearPref(Context context){
        SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }

    @Override
    public void finishActivity() {
        finish();
    }

    @Override
    public void openBackcheckingDetailActivity(BackCheckingLocationListResponse response) {
        Intent i = new Intent(this, DetailBackCheckingLocationActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        i.putExtra("detail",response);
        i.putExtra("backchecking_state","checkout");
        startActivity(i);
    }

    @Override
    public void openGoBackcheckingActivity() {
        Intent intent = BackCheckingLocationActivity.getStartIntent(this);
        intent.addFlags(intent.FLAG_ACTIVITY_CLEAR_TOP | intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void setUserSenderMenu() {
        BackCheckingLocationListResponse response = (BackCheckingLocationListResponse) getIntent().getSerializableExtra("backchecking");
        mBackcheckingDetail = response;
        if (mBackcheckingDetail != null){
            namaAocPjp = response.getUserName();
            nomorAocPjp = response.getUserId();
        }else{
            showMessage("Terjadi kesalahan mengirim data");
        }

    }

    private String dateConverter(String dateInput){
        try {
            SimpleDateFormat spf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date newDate = null;
            newDate = spf.parse(dateInput);
            spf= new SimpleDateFormat("EEEEE, dd MMMM yyyy HH:mm:ss");
            String returnDate = spf.format(newDate);
            return returnDate;

        }catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void getBackcheckingDataFromServer(String locationId){
        mPresenter.getBackcheckingInstitutionBranding(locationId);
        mPresenter.getBackcheckingInstitutionMarketShare(locationId);
        mPresenter.getBackcheckingInstitutionSalesThrough(locationId);
        mPresenter.getBackcheckingInstitutionVisit(locationId);
        mPresenter.getBackcheckingInstitutionMerchandising(locationId);
        mPresenter.getBackcheckingInstitutionTotalStudent(locationId);
        mPresenter.getBackcheckingInstitutionReadSignal(locationId);
    }

    @Override
    public void showLoading(String message) {
        super.showLoading(message);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        if (progressDialog != null ){
            progressDialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }
}
