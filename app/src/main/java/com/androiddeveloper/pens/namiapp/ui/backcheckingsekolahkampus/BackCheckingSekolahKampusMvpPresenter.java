package com.androiddeveloper.pens.namiapp.ui.backcheckingsekolahkampus;

import android.content.Context;

import com.androiddeveloper.pens.namiapp.data.network.model.BackCheckingLocationListResponse;
import com.androiddeveloper.pens.namiapp.di.PerActivity;
import com.androiddeveloper.pens.namiapp.ui.backchecking.BaseBackcheckingMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.backchecking.BaseBackcheckingPresenter;
import com.androiddeveloper.pens.namiapp.ui.base.BaseActivity;
import com.androiddeveloper.pens.namiapp.ui.base.MvpPresenter;

import java.util.Map;

@PerActivity
public interface BackCheckingSekolahKampusMvpPresenter<V extends BackCheckingSekolahKampusMvpView> extends BaseBackcheckingMvpPresenter<V> {

    void getBackcheckingInstitutionTotalStudent(String locationId);

    void getBackcheckingInstitutionVisit(String locationId);

    void getBackcheckingInstitutionBranding(String locationId);

    void getBackcheckingInstitutionMerchandising(String locationId);

    void getBackcheckingInstitutionMarketShare(String locationId);

    void getBackcheckingInstitutionSalesThrough(String locationId);

    void getBackcheckingInstitutionReadSignal(String locationId);

    void submitBackchecking(Map<String, String> map, Context context, BackCheckingLocationListResponse response, String latitude,
                            String longitude);

    void submitBackcheckingIfLocationNull(Map<String, String> map, Context context, BackCheckingLocationListResponse response);

    void insertUserCheckout(String latitude, String longitude);

    String usertypeBackchecker();
}
