package com.androiddeveloper.pens.namiapp.ui.backcheckingsekolahkampus;

import com.androiddeveloper.pens.namiapp.data.network.model.BackCheckingLocationListResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.Backchecking;
import com.androiddeveloper.pens.namiapp.ui.backchecking.BaseBackcheckingMvpView;
import com.androiddeveloper.pens.namiapp.ui.base.MvpView;

public interface BackCheckingSekolahKampusMvpView extends BaseBackcheckingMvpView {

    void setupBrandingMenu(Backchecking backchecking);

    void setupMerchandisingMenu(Backchecking backchecking);

    void setupMarketShareMenu(Backchecking backchecking);

    void setupSalesThroughMenu(Backchecking backchecking);

    void setupTotalStudent(Backchecking backchecking);

    void setupVisitMenu(Backchecking backchecking);

    void setupReadSignal(Backchecking backchecking);

    void sendInstitutionBackcheckingData();

    void finishActivity();

    void openBackcheckingDetailActivity(BackCheckingLocationListResponse response);

    void openGoBackcheckingActivity();
}
