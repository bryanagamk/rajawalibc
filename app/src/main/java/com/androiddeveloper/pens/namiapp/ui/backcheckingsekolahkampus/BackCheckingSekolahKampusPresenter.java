package com.androiddeveloper.pens.namiapp.ui.backcheckingsekolahkampus;

import android.content.Context;
import android.util.Log;

import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.data.network.model.BackCheckingLocationListResponse;
import com.androiddeveloper.pens.namiapp.service.NotificationService;
import com.androiddeveloper.pens.namiapp.ui.backchecking.BaseBackcheckingPresenter;

import java.util.Map;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class BackCheckingSekolahKampusPresenter<V extends BackCheckingSekolahKampusMvpView> extends BaseBackcheckingPresenter<V>
        implements BackCheckingSekolahKampusMvpPresenter<V> {
    private static final String TAG = "BackCheckingSekolahKampusPresenter";

    @Override
    public void onAttach(V mvpView) {
        super.onAttach(mvpView);
    }

    @Inject
    public BackCheckingSekolahKampusPresenter(DataManager dataManager,
                                 CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);
    }

    @Override
    public void getBackcheckingInstitutionTotalStudent(String locationId){
        getCompositeDisposable().add(getDataManager().getBackcheckingInstitutionTotalStudent(locationId)
        .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
        .subscribe(backcheckingResponse -> {
            if (!isViewAttached()){
                return;
            }
            backcheckingResponse.toString();
            Log.d("BCCC", "getBackcheckingInstitutionTotalStudent: " + backcheckingResponse.toString());
            if (backcheckingResponse.getSuccess()) {
                getMvpView().setupTotalStudent(backcheckingResponse.getBackchecking());
            } else {
                getMvpView().showMessage(backcheckingResponse.getMessage());
            }
        }, throwable -> {
            if (!isViewAttached()){
                return;
            }
            getBackcheckingInstitutionTotalStudent(locationId);
            getMvpView().showMessage("Gagal mendapatkan data total student dari server, Silahkan coba lagi");
        }));
    }

    @Override
    public void getBackcheckingInstitutionVisit(String locationId) {
        getCompositeDisposable().add(getDataManager().getBackcheckingInstitutionVisit(locationId)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(backcheckingResponse -> {
            if (!isViewAttached()){
                return;
            }
            if (backcheckingResponse.getSuccess()){
                getMvpView().setupVisitMenu(backcheckingResponse.getBackchecking());
            }else{
                getMvpView().showMessage(backcheckingResponse.getMessage());
            }
        },throwable -> {
            if (!isViewAttached()){
                return;
            }
            getBackcheckingInstitutionVisit(locationId);
            getMvpView().showMessage("Gagal mendapatkan data Visit dari server, Silahkan coba lagi");
        }));
    }

    @Override
    public void getBackcheckingInstitutionBranding(String locationId) {
        getCompositeDisposable().add(getDataManager().getBackcheckingInstitutionBranding(locationId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(backcheckingResponse -> {
                    if (!isViewAttached()){
                        return;
                    }
                    if (backcheckingResponse.getSuccess()){
                        getMvpView().setupBrandingMenu(backcheckingResponse.getBackchecking());
                    }else{
                        getMvpView().showMessage(backcheckingResponse.getMessage());
                    }
                },throwable -> {
                    if (!isViewAttached()){
                        return;
                    }
                    getBackcheckingInstitutionBranding(locationId);
                    getMvpView().showMessage("Gagal mendapatkan data Branding dari server, Silahkan coba lagi");
                }));
    }

    @Override
    public void getBackcheckingInstitutionMerchandising(String locationId) {
        getCompositeDisposable().add(getDataManager().getBackcheckingInstitutionMerchandising(locationId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(backcheckingResponse -> {
                    if (!isViewAttached()){
                        return;
                    }
                    if (backcheckingResponse.getSuccess()){
                        getMvpView().setupMerchandisingMenu(backcheckingResponse.getBackchecking());
                    }else{
                        getMvpView().showMessage(backcheckingResponse.getMessage());
                    }
                },throwable -> {
                    if (!isViewAttached()){
                        return;
                    }
                    getBackcheckingInstitutionMerchandising(locationId);
                    getMvpView().showMessage("Gagal mendapatkan data Merchandising dari server, Silahkan coba lagi");
                }));
    }

    @Override
    public void getBackcheckingInstitutionMarketShare(String locationId) {
        getCompositeDisposable().add(getDataManager().getBackcheckingInstitutionMarketShare(locationId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(backcheckingResponse -> {
                    if (!isViewAttached()){
                        return;
                    }
                    if (backcheckingResponse.getSuccess()){
                        Log.v("Debug ", backcheckingResponse.getMessage() );
                        getMvpView().setupMarketShareMenu(backcheckingResponse.getBackchecking());
                    }else{
                        getMvpView().showMessage(backcheckingResponse.getMessage());
                    }
                },throwable -> {
                    if (!isViewAttached()){
                        return;
                    }
                    getBackcheckingInstitutionMarketShare(locationId);
                    getMvpView().showMessage("Gagal mendapatkan data MarketShare dari server, Silahkan coba lagi");
                }));
    }

    @Override
    public void getBackcheckingInstitutionSalesThrough(String locationId) {
        getCompositeDisposable().add(getDataManager().getBackcheckingInstitutionSalesThrough(locationId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(backcheckingResponse -> {
                    if (!isViewAttached()){
                        return;
                    }
                    if (backcheckingResponse.getSuccess()){
                        getMvpView().setupSalesThroughMenu(backcheckingResponse.getBackchecking());
                    }else{
                        getMvpView().showMessage(backcheckingResponse.getMessage());
                    }
                },throwable -> {
                    if (!isViewAttached()){
                        return;
                    }
                    getBackcheckingInstitutionSalesThrough(locationId);
                    getMvpView().showMessage("Gagal mendapatkan data SalesThrough dari server, Silahkan coba lagi");
                }));
    }

    @Override
    public void getBackcheckingInstitutionReadSignal(String locationId) {
        getCompositeDisposable().add(getDataManager().getBackcheckingInstitutionReadSignal(locationId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(backcheckingResponse -> {
                    if (!isViewAttached()){
                        return;
                    }
                   if (backcheckingResponse.getSuccess()){
                       getMvpView().setupReadSignal(backcheckingResponse.getBackchecking());
                   }else {
                       getMvpView().showMessage(backcheckingResponse.getMessage());
                   }
                },throwable -> {
                    if (!isViewAttached()){
                        return;
                    }
                    getBackcheckingInstitutionReadSignal(locationId);
                    getMvpView().showMessage("Gagal mendapatkan data read signal dari server, Silahkan coba lagi");
                }));
    }


    @Override
    public void submitBackchecking(Map<String, String> map, Context context,BackCheckingLocationListResponse response,
                                   String latitude,
                                   String longitude) {
        map.put("userid",getDataManager().getCurrentUserId());
        map.put("user_type",getDataManager().getCurrentUserType());
        map.put("deviceid",getDataManager().getCurrentDeviceId());
        map.put("username",getDataManager().getCurrentUserName());

        getMvpView().showLoading("Submit Backchecking");

        for(String value : map.keySet()){
            String temp = map.get(value);
            map.put(value,temp.replace(",","."));
        }


        getMvpView().showMessage("Backchecking anda sedang dikirim");
        Log.d("Fail  :","lala");


        getCompositeDisposable().add(getDataManager().submitBackcheckingForm(map)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(basicResponse -> {
            if (!isViewAttached()){
                return;
            }
            if (basicResponse.getSuccess()){
                NotificationService.getInstance(context).backcheckingSelesaiNotification("Backchecking Executed!","Backchecking Anda pada " + map.get("location_name") + "Sudah selesai tersubmit", "Laporan backchecking Anda");
                NotificationService.getInstance(context).sendNotifToAOC(map.get("userid_sendto"),"PJP " + map.get("location_name") + "Sudah di backcheck oleh " + map.get("username") + " Harap untuk membalas feedback","Kunjugan Terbackcheck!");
                Log.d("Fail  :",basicResponse.getMessage());

                insertUserCheckout(latitude,longitude);
                getMvpView().openGoBackcheckingActivity();
                getMvpView().finishActivity();
                getMvpView().hideLoading();

            }else {
                NotificationService.getInstance(context).backcheckingSelesaiNotification("Backchecking Gagal!","Backchecking Anda pada " + map.get("location_name") + "Gagal Tersubmit", "Laporan backchecking Anda");
                Log.d("Fail : ",map.toString());
                Log.d("Fail  :",basicResponse.getMessage());

                getMvpView().hideLoading();
            }

        },throwable -> {
            if (!isViewAttached()){
                return;
            }
            NotificationService.getInstance(context).backcheckingSelesaiNotification("Backchecking Gagal!","Backchecking terakhir anda gagal, Silahkan coba lagi", "Laporan backchecking Anda");
            getMvpView().hideLoading();
            getMvpView().showMessage("Gagal backchecking, Silahkan coba lagi");
            Log.d("Backchecking : ", throwable.toString());
        }));
    }

    @Override
    public void submitBackcheckingIfLocationNull(Map<String, String> map, Context context, BackCheckingLocationListResponse response) {
        map.put("userid",getDataManager().getCurrentUserId());
        map.put("user_type",getDataManager().getCurrentUserType());
        map.put("deviceid",getDataManager().getCurrentDeviceId());
        map.put("username",getDataManager().getCurrentUserName());

        getMvpView().showLoading("Submit Backchecking");

        for(String value : map.keySet()){
            String temp = map.get(value);
            map.put(value,temp.replace(",","."));
        }


        getMvpView().showMessage("Backchecking anda sedang dikirim");
        Log.d("Fail  :","lala");


        getCompositeDisposable().add(getDataManager().submitBackcheckingForm(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(basicResponse -> {
                    if (!isViewAttached()){
                        return;
                    }
                    if (basicResponse.getSuccess()){
                        NotificationService.getInstance(context).backcheckingSelesaiNotification("Backchecking Executed!","Backchecking Anda pada " + map.get("location_name") + "Sudah selesai tersubmit", "Laporan backchecking Anda");
                        NotificationService.getInstance(context).sendNotifToAOC(map.get("userid_sendto"),"PJP " + map.get("location_name") + "Sudah di backcheck oleh " + map.get("username") + " Harap untuk membalas feedback","Kunjugan Terbackcheck!");
                        Log.d("Fail  :",basicResponse.getMessage());

                        getMvpView().finishActivity();
                        getMvpView().hideLoading();

                    }else {
                        NotificationService.getInstance(context).backcheckingSelesaiNotification("Backchecking Gagal!","Backchecking Anda pada " + map.get("location_name") + "Gagal Tersubmit", "Laporan backchecking Anda");
                        Log.d("Fail : ",map.toString());
                        Log.d("Fail  :",basicResponse.getMessage());

                        getMvpView().hideLoading();
                    }

                },throwable -> {
                    if (!isViewAttached()){
                        return;
                    }
                    NotificationService.getInstance(context).backcheckingSelesaiNotification("Backchecking Gagal!","Backchecking terakhir anda gagal, Silahkan coba lagi", "Laporan backchecking Anda");
                    getMvpView().hideLoading();
                    getMvpView().showMessage("Gagal backchecking, Silahkan coba lagi");
                    Log.d("Backchecking : ", throwable.toString());
                }));
    }

    @Override
    public void insertUserCheckout(String latitude, String longitude) {
        String userId = getDataManager().getCurrentUserId();

        getCompositeDisposable().add(getDataManager().insertCheckoutStatus(userId,latitude,longitude)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(basicResponse -> {
                    if (!isViewAttached()){
                        return;
                    }
                    getMvpView().showMessage(basicResponse.getMessage());
                    getMvpView().finish();
                },throwable -> {
                    if (!isViewAttached()){
                        return;
                    }
                    getMvpView().showMessage("Gagal Checkout, Tidak Tersambung");
                    insertUserCheckout(latitude,longitude);
                    getMvpView().finish();
                }));
    }

    @Override
    public String usertypeBackchecker(){
        return getDataManager().getCurrentUserType().toLowerCase();
    }

}
