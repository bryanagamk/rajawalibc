package com.androiddeveloper.pens.namiapp.ui.base;

import com.androidnetworking.error.ANError;

/**
 * Created by miftahun on 6/11/18.
 * <p>
 * Author Miftahun Najat
 * Email miftahunajat@gmail.com
 * Github https://github.com/miftahunajat
 */

public interface MvpPresenter<V extends MvpView> {

    void onAttach(V mvpView);

    void onDetach();

    void onAttachFragment(V mvpView);

    void onDetachFragment();

    void setUserAsLoggedOut();

    void handleApiError(ANError error);

    String getUserName();
}
