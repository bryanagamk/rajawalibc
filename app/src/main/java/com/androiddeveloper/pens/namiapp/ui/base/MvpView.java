package com.androiddeveloper.pens.namiapp.ui.base;

import android.support.annotation.StringRes;

/**
 * Created by miftahun on 6/11/18.
 * <p>
 * Author Miftahun Najat
 * Email miftahunajat@gmail.com
 * Github https://github.com/miftahunajat
 */

public interface MvpView {

    void showLoading(String message);

    void hideLoading();

    void openActivityOnTokenExpire();

    void onError(@StringRes int resId);

    void onError(String message);

    void showMessage(String message);

    void showMessage(@StringRes int resId);

    boolean isNetworkConnected();

    void hideKeyboard();

    void showErrorPage(String message);

    boolean isShowingError();

    void finish();
}
