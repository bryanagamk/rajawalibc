package com.androiddeveloper.pens.namiapp.ui.crashreport;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.ui.base.BaseActivity;
import com.androiddeveloper.pens.namiapp.ui.dasboard.DasboardActivity;
import com.androiddeveloper.pens.namiapp.ui.dasboard.DasboardMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.dasboard.DasboardMvpView;
import com.androiddeveloper.pens.namiapp.utils.HandleAppCrash;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class CrashReportActivity extends BaseActivity implements CrashReportMvpView  {

    @Inject
    CrashReportMvpPresenter<CrashReportMvpView> mPresenter;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, CrashReportActivity.class);
        return intent;
    }

    private Button btnclose;
    private TextView errorReport;
    Runnable runnable;
    Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crash_report);

        getActivityComponent().inject(this);

        if (isNetworkAvailable()){
            setUnBinder(ButterKnife.bind(this));

            mPresenter.onAttach(this);

            setUp();
        }
    }

    @Override
    protected void setUp() {
        // backpress
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Report");

        HandleAppCrash.deploy(this,CrashReportActivity.class);

        handleCrashingApp(CrashReportActivity.this);

    }

    private void handleCrashingApp(Context context){
        ActionBar ab = getSupportActionBar();
        ab.hide();
        btnclose = (Button) findViewById(R.id.btnclose);
        btnclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String body = null;
                String error = getIntent().getStringExtra("errorTrace");
                try {
                    body = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
                    body = "\n\n-----------------------------\nPlease don't remove this information\n Device OS: Android \n Device OS version: " +
                            Build.VERSION.RELEASE + "\n myNAMI Version: " + body + "\n Device Brand: " + Build.BRAND +
                            "\n Device Model: " + Build.MODEL + "\n Device Manufacturer: " + Build.MANUFACTURER + "\n";
                }catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }

                Intent intents = new Intent(Intent.ACTION_SEND);
                intents.setType("message/rfc822");
                intents.setType("text/plain");
                intents.putExtra(Intent.EXTRA_EMAIL, new String[]{"mfatkhun28@gmail.com", "andyvebby@gmail.com"});
                intents.putExtra(Intent.EXTRA_SUBJECT, "Error from myNAMI");
                intents.putExtra(Intent.EXTRA_TEXT, error + body);
                context.startActivity(Intent.createChooser(intents, "Share via"));
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    protected void onResume() {
        super.onResume();
        handleCrashingApp(CrashReportActivity.this);
    }

    @Override
    public void onPause() {
        super.onPause();
        handleCrashingApp(CrashReportActivity.this);
    }

    @Override
    protected void onDestroy() {
        handler.removeCallbacks(runnable);
        super.onDestroy();
    }
}
