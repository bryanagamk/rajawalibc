package com.androiddeveloper.pens.namiapp.ui.crashreport;

import com.androiddeveloper.pens.namiapp.di.PerActivity;
import com.androiddeveloper.pens.namiapp.ui.base.MvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.dasboard.DasboardMvpView;

@PerActivity
public interface CrashReportMvpPresenter<V extends CrashReportMvpView> extends MvpPresenter<V> {
}
