package com.androiddeveloper.pens.namiapp.ui.crashreport;

import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.ui.base.BasePresenter;
import com.androiddeveloper.pens.namiapp.ui.dasboard.DasboardMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.dasboard.DasboardMvpView;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class CrashReportPresenter<V extends CrashReportMvpView> extends BasePresenter<V>
        implements CrashReportMvpPresenter<V> {

    @Inject
    public CrashReportPresenter(DataManager dataManager, CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);
    }
}
