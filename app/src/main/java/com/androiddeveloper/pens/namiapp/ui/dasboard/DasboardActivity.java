package com.androiddeveloper.pens.namiapp.ui.dasboard;

import android.Manifest;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androiddeveloper.pens.namiapp.App;
import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceBackchecking;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceEvent;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformancePoiOsk;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceShare;
import com.androiddeveloper.pens.namiapp.data.network.model.SummaryEvent;
import com.androiddeveloper.pens.namiapp.data.network.model.SummaryInstitution;
import com.androiddeveloper.pens.namiapp.data.network.model.SummaryOsk;
import com.androiddeveloper.pens.namiapp.data.network.model.SummarySales;
import com.androiddeveloper.pens.namiapp.data.network.rest.ApiClient;
import com.androiddeveloper.pens.namiapp.ui.aoc.home.HomeActivity;
import com.androiddeveloper.pens.namiapp.ui.backcheckinglocation.BackCheckingLocationActivity;
import com.androiddeveloper.pens.namiapp.ui.backcheckinglocationlist.BackCheckingLocationListActivity;
import com.androiddeveloper.pens.namiapp.ui.base.BaseActivity;
import com.androiddeveloper.pens.namiapp.ui.crashreport.CrashReportActivity;
import com.androiddeveloper.pens.namiapp.ui.feddbacklist.FeedbackListActivity;
import com.androiddeveloper.pens.namiapp.ui.feedbacklocation.FeedbackLocationActivity;
import com.androiddeveloper.pens.namiapp.ui.aoc.ossosk.ossoskmenu.OssOskMenuActivity;
import com.androiddeveloper.pens.namiapp.ui.aoc.sekolahkampus.sekolahkampusmenu.SekolahKampusMenuActivity;
import com.androiddeveloper.pens.namiapp.ui.sharepercentage.ShareAreaActivity;
import com.androiddeveloper.pens.namiapp.ui.sharepercentage.fragmentshare.ShareType;
import com.androiddeveloper.pens.namiapp.ui.soobackcheckinglocationlist.SooBackCheckingLocationListActivity;
import com.androiddeveloper.pens.namiapp.ui.summary.SummaryAreaActivity;
import com.androiddeveloper.pens.namiapp.ui.summary.fragment.AreaFragment;
import com.androiddeveloper.pens.namiapp.ui.summary.fragment.SummaryType;
import com.androiddeveloper.pens.namiapp.ui.summarybackchecking.SummaryBackcheckingActivity;
import com.androiddeveloper.pens.namiapp.ui.summarybackchecking.fragment.BackcheckingType;
import com.androiddeveloper.pens.namiapp.ui.verifiynumber.VerifyNumberActivity;
import com.androiddeveloper.pens.namiapp.utils.HandleAppCrash;
import com.androiddeveloper.pens.namiapp.utils.LocationValidatorUtil;
import com.androiddeveloper.pens.namiapp.utils.MenuEnum;
import com.androiddeveloper.pens.namiapp.utils.MyGlideApp;
import com.androiddeveloper.pens.namiapp.utils.NetworkUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.MemoryCategory;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.signature.ObjectKey;
import com.ceylonlabs.imageviewpopup.ImagePopup;
import com.crashlytics.android.Crashlytics;
import com.dinuscxj.progressbar.CircleProgressBar;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DasboardActivity extends BaseActivity implements DasboardMvpView {

    private static final int READ_PHONE_ICCID_PERMISSION = 3;
    @Inject
    DasboardMvpPresenter<DasboardMvpView> mPresenter;

    @BindView(R.id.relative_info_back_checking)
    RelativeLayout relativeInfoBackchecking;

    @BindView(R.id.relative_info_feedback)
    RelativeLayout relativeInfoFeedback;

    @BindView(R.id.relative_info_faq)
    RelativeLayout relativeInfoFaq;

    @BindView(R.id.relative_info_event)
    RelativeLayout relativeInfoEvent;

    @BindView(R.id.relative_info_pjp)
    RelativeLayout relativeInfoPjp;

    @BindView(R.id.root)
    View rootLayout;

    @BindView(R.id.tv_message)
    TextView tvMessage;

    @BindView(R.id.progressbar)
    ProgressBar progressBar;

    @BindView(R.id.tv_ponsel_nami)
    TextView tvPonselNami;

    @BindView(R.id.tv_nama_user_nami)
    TextView tvNamaUserName;

    @BindView(R.id.tv_level_nami)
    TextView tvUserType;

    @BindView(R.id.btn_setting_logout)
    ImageButton btnLogout;

    @BindView(R.id.btn_notification)
    ImageButton btnNotification;

    @BindView(R.id.line_info_summary_share)
    LinearLayout lineInfoSummaryShare;

    @BindView(R.id.cv_performance_actual_event)
    CardView cvPerformanceActualEvent;

    @BindView(R.id.cv_bc_performance)
    CardView cvBcPerformance;

    @BindView(R.id.cv_pjp_productivity_mtd_osk_poi)
    CardView cvPjpProdMtd;

    @BindView(R.id.cv_pjp_productivity_ptd_osk_poi)
    CardView cvPjpProdPtd;

   // id broadband share
    @BindView(R.id.line_info_bs_share)
    LinearLayout lineInfoBsShare;
    @BindView(R.id.persentage_tselbs)
    CircleProgressBar persentageTselBs;
    @BindView(R.id.persentage_ooredoobs)
    CircleProgressBar persentageOoredooBs;
    @BindView(R.id.persentage_xlbs)
    CircleProgressBar persentageXlBs;
    @BindView(R.id.persentage_axisbs)
    CircleProgressBar persentageAxisBs;
    @BindView(R.id.persentage_3bs)
    CircleProgressBar persentage3Bs;
    @BindView(R.id.persentage_othersbs)
    CircleProgressBar persentageOthersBs;
    // id display share
    @BindView(R.id.line_info_ds_share)
    LinearLayout lineInfoDsShare;
    @BindView(R.id.persentage_tselds)
    CircleProgressBar persentageTselDs;
    @BindView(R.id.persentage_ooredoods)
    CircleProgressBar persentageOoredooDs;
    @BindView(R.id.persentage_xlds)
    CircleProgressBar persentageXlDs;
    @BindView(R.id.persentage_axisds)
    CircleProgressBar persentageAxisDs;
    @BindView(R.id.persentage_3ds)
    CircleProgressBar persentage3Ds;
    @BindView(R.id.persentage_othersds)
    CircleProgressBar persentageOthersDs;
    // id recharge share
    @BindView(R.id.line_info_rs_share)
    LinearLayout lineInfoRsShare;
    @BindView(R.id.persentage_tselrs)
    CircleProgressBar persentageTselRs;
    @BindView(R.id.persentage_ooredoors)
    CircleProgressBar persentageOoredooRs;
    @BindView(R.id.persentage_xlrs)
    CircleProgressBar persentageXlRs;
    @BindView(R.id.persentage_axisrs)
    CircleProgressBar persentageAxisRs;
    @BindView(R.id.persentage_3rs)
    CircleProgressBar persentage3Rs;
    @BindView(R.id.persentage_othersrs)
    CircleProgressBar persentageOthersRs;

    // id market share
    @BindView(R.id.line_info_ms_share)
    LinearLayout lineInfoMsShare;
    @BindView(R.id.persentage_tselms)
    CircleProgressBar persentageTselMs;
    @BindView(R.id.persentage_ooredooms)
    CircleProgressBar persentageOoredooMs;
    @BindView(R.id.persentage_xlms)
    CircleProgressBar persentageXlMs;
    @BindView(R.id.persentage_axisms)
    CircleProgressBar persentageAxisMs;
    @BindView(R.id.persentage_3ms)
    CircleProgressBar persentage3Ms;
    @BindView(R.id.persentage_othersms)
    CircleProgressBar persentageOthersMs;

    //id picture upload
    @BindView(R.id.iv_picture_user_profile)
    ImageView ivPictureUserProfile;

    //id version code
    @BindView(R.id.tv_version_code_app)
    TextView tvVersionCodeApp;

    Uri mCropImageUri;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, DasboardActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dasboard);

        getActivityComponent().inject(this);

        if (isNetworkAvailable()){
            setUnBinder(ButterKnife.bind(this));

            mPresenter.onAttach(this);

            setUp();
        }

    }

    @Override
    protected void setUp() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Glide.get(DasboardActivity.this).clearDiskCache();
            }
        }).start();
        loadUserProfile();
        versionNameApp();

        if (Build.VERSION.SDK_INT >= 23) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},   2000);
        }else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2000);
        }
        changeStatusBarColor("#fafafa");

        ivPictureUserProfile.setOnClickListener(View ->{
            uploadPhoto();
        });

        btnNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent i = new Intent(getApplicationContext(), NotificationActivity.class);
//                startActivity(i);
                Toast.makeText(getApplicationContext(), "Tahap pengembangan", Toast.LENGTH_SHORT).show();
            }
        });

        relativeInfoPjp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent i = new Intent(getApplicationContext(), HomeActivity.class);
//                startActivity(i);
                Toast.makeText(getApplicationContext(), "Tahap pengembangan", Toast.LENGTH_SHORT).show();
            }
        });

        relativeInfoBackchecking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), BackCheckingLocationActivity.class);
                startActivity(i);
            }
        });

        relativeInfoFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = FeedbackLocationActivity.getStartIntent(DasboardActivity.this);
                startActivity(i);
            }
        });

        relativeInfoFaq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent i = new Intent(getApplicationContext(), FaqActivity.class);
//                startActivity(i);
                Toast.makeText(getApplicationContext(), "Tahap pengembangan", Toast.LENGTH_SHORT).show();
            }
        });

        relativeInfoEvent.setOnClickListener(v -> {
//                Intent i = new Intent(getApplicationContext(), EventAocActivity.class);
//                startActivity(i);
            Toast.makeText(getApplicationContext(), "Tahap pengembangan", Toast.LENGTH_SHORT).show();
        });

        btnLogout.setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(DasboardActivity.this);
            builder.setCancelable(true);
            builder.setMessage("Apakah anda yakin ingin logout ?");

            builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    Toast.makeText(getApplicationContext(), "Logging out", Toast.LENGTH_SHORT).show();
                    mPresenter.logoutProcess();
                }
            });
            builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    builder.setCancelable(true);
                }
            });
            builder.show();
        });

        mPresenter.validateICCIDNumber(this);

        if (LocationValidatorUtil.isLocationHighAccuracyEnabled(this)){

        }else{
            showAlert("Mode High Accuracy pada setting GPS Anda belum aktif. Aktifkan terlebih dahulu untuk memulai NAMI. Masuk ke menu Setting - Location - Mode - High Accuracy. Letak menu bisa jadi berbeda pada masing-masing device");
        }
    }


    // version name app
    public void versionNameApp(){
        Context context = getApplicationContext(); // or activity.getApplicationContext()
        PackageManager packageManager = context.getPackageManager();
        String packageName = context.getPackageName();

        String myVersionName = "not available"; // initialize String

        try {
            myVersionName = packageManager.getPackageInfo(packageName, 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        tvVersionCodeApp.setText(myVersionName);
    }

    @Override
    public void loadUserProfile() {
        String pathImage = mPresenter.getUploadProfile();
        Log.d("Debug : ", pathImage);
        String userid = mPresenter.getUseridProfile();
        Log.d("Debug : ", userid);
        if (pathImage != null ){
            if (!userid.isEmpty() && pathImage.equals("-1L")){
                Glide.with(this)
                        .load(ApiClient.BASE_URL_NAMI + "Images/profile/" + userid + ".jpg")
                        .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true).placeholder(R.drawable.ic_profile_user).error(R.drawable.ic_profile_user))
                        .apply(RequestOptions.circleCropTransform())
                        .into(ivPictureUserProfile);

                ivPictureUserProfile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        uploadPhoto();
                    }
                });
            }else if (!userid.isEmpty() && !pathImage.equals("-1L")){
                Glide.with(this)
                        .load(ApiClient.BASE_URL_NAMI + pathImage)
                        .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true).placeholder(R.drawable.ic_profile_user).error(R.drawable.ic_profile_user))
                        .apply(RequestOptions.circleCropTransform())
                        .into(ivPictureUserProfile);

                ivPictureUserProfile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        uploadPhoto();
                    }
                });
            }
        }
    }

    public void uploadPhoto(){
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.item_upload_foto, null);
        Dialog alertDialogBuilder = new Dialog(this);
        alertDialogBuilder.setCanceledOnTouchOutside(false);
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialogBuilder.setContentView(promptView);
        Button btnLihatPhoto = (Button) promptView.findViewById(R.id.btn_lihat_foto);
        Button btnUploadPhoto= (Button) promptView.findViewById(R.id.btn_upload_foto);
        btnLihatPhoto.setOnClickListener(View -> {
            /** Set popup height, width & background color as you need or just leave default settings **/
            final ImagePopup imagePopup = new ImagePopup(this);
            imagePopup.setWindowHeight(800); // Optional
            imagePopup.setWindowWidth(800); // Optional
            imagePopup.setBackgroundColor(Color.WHITE);  // Optional
            imagePopup.setFullScreen(true); // Optional
            imagePopup.setHideCloseIcon(true);  // Optional
            imagePopup.setImageOnClickClose(true);  // Optional

            String pathImage = mPresenter.getUploadProfile();
            String userid = mPresenter.getUseridProfile();
            deleteCache(this);
            if (!userid.isEmpty() && pathImage.equals("-1L")){
                imagePopup.initiatePopupWithGlide(ApiClient.BASE_URL_NAMI + "Images/profile/" + userid + ".jpg");
                imagePopup.viewPopup();
            }else{
                imagePopup.initiatePopupWithGlide(ApiClient.BASE_URL_NAMI + pathImage);
                imagePopup.viewPopup();
            }
            alertDialogBuilder.dismiss();
        });
        btnUploadPhoto.setOnClickListener(View -> {
            onSelectImageClick(ivPictureUserProfile);
            alertDialogBuilder.dismiss();
        });
        alertDialogBuilder.show();
    }

    // clear cache
    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
            } else if(dir.isFile()) {
                dir.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if(dir!= null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }


    @SuppressLint("NewApi")
    public void onSelectImageClick(View view) {
        if (CropImage.isExplicitCameraPermissionRequired(this)) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE);
        } else {
            CropImage.startPickImageActivity(this);
        }
    }

    @Override
    @SuppressLint("NewApi")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);
            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()
                mCropImageUri = imageUri;
                Toast.makeText(this, "Permission dibutuhkan untuk mengambil gambar", Toast.LENGTH_SHORT).show();
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},   CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
            }else {
                // no permissions required or already granted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE){
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            Log.d("DEbug","Kepanggil");
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                Bitmap bitmap = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                    mPresenter.uploadPhotoProfile(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Log.d("Error",error.toString());
            }
        }
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setCropShape(CropImageView.CropShape.OVAL)
                .setMinCropWindowSize(0,0)
                .setMinCropResultSize(500,500)
                .setMaxCropResultSize(2000,2000)
                .setShowCropOverlay(true)
                .start(this);
    }

    @Override
    public void openVerifyNumberActivity() {
        Intent intent = VerifyNumberActivity.getStartIntent(this);
        startActivity(intent);
        finish();
    }

    private void changeStatusBarColor(String color){
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            View decor = getWindow().getDecorView();
            decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor(color));
        }
    }


    @Override
    public void showLoading(String message) {
        super.showLoading(message);
        rootLayout.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        tvMessage.setVisibility(View.VISIBLE);
        tvMessage.setText(message);
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        progressBar.setVisibility(View.GONE);
        tvMessage.setVisibility(View.GONE);
        rootLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void setUserData(String phone, String userName, String currentTitle,String userType) {
        tvPonselNami.setText(phone);
        tvUserType.setText(currentTitle);
        tvNamaUserName.setText(userName);
        setOnClickListeners(userType);
    }

    @Override
    public void requestPermissionReadPhonePermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
            != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_PHONE_STATE},
                    READ_PHONE_ICCID_PERMISSION);
        }
    }

    @Override
    public void showAvailableMenut(List<MenuEnum> enumList) {
        for (MenuEnum menu : enumList  ){
            View v = findViewById(menu.getType());
            if (v!=null)
                v.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setupSummaryPerformancePoiOsk(PerformancePoiOsk performancePoiOsk, String userType) {

        TextView tvPoiAchCallPtd = findViewById(R.id.tv_pjp_ach_call_ptd_poi);
        TextView tvPoiPlanCallPtd = findViewById(R.id.tv_pjp_ptd_plan_actual_call_poi);
        TextView tvPoiActualCallPtd = findViewById(R.id.tv_pjp_ptd_actual_actual_call_poi);
        TextView tvPoiEffectiveCallPtd = findViewById(R.id.tv_pjp_ptd_effective_call_poi);
        TextView tvPoiPlanMarketSurveyPtd = findViewById(R.id.tv_pjp_ptd_plan_market_survey_poi);
        TextView tvPoiActualMarketSurveyPtd = findViewById(R.id.tv_pjp_ptd_actual_market_survey_poi);
        TextView tvPoiActualSalesPtd = findViewById(R.id.tv_pjp_ptd_sales_through_poi);
        TextView tvOskAchCallPtd = findViewById(R.id.tv_pjp_ach_call_ptd_osk);
        TextView tvOskPlanCallPtd = findViewById(R.id.tv_pjp_ptd_plan_actual_call_osk);
        TextView tvOskActualCallPtd = findViewById(R.id.tv_pjp_ptd_actual_actual_call_osk);
        TextView tvOskEffectiveCallPtd = findViewById(R.id.tv_pjp_ptd_effective_call_osk);
        TextView tvOskPlanOutletProdPtd = findViewById(R.id.tv_pjp_ptd_plan_outlet_prod_osk);
        TextView tvOskOutletProdPtd = findViewById(R.id.tv_pjp_ptd_actual_outlet_prod_osk);
        TextView tvOskActualSalesPtd = findViewById(R.id.tv_pjp_ptd_sales_through_osk);

        // mtd
        TextView tvPoiAchCallMtd = findViewById(R.id.tv_pjp_ach_call_mtd_poi);
        TextView tvPoiPlanCallMtd = findViewById(R.id.tv_pjp_mtd_plan_actual_call_poi);
        TextView tvPoiActualCallMtd = findViewById(R.id.tv_pjp_mtd_actual_actual_call_poi);
        TextView tvPoiEffectiveCallMtd = findViewById(R.id.tv_pjp_mtd_effective_call_poi);
        TextView tvPoiPlanMarketSurveyMtd = findViewById(R.id.tv_pjp_mtd_plan_market_survey_poi);
        TextView tvPoiActualMarketSurveyMtd = findViewById(R.id.tv_pjp_mtd_actual_market_survey_poi);
        TextView tvPoiActualSalesMtd = findViewById(R.id.tv_pjp_mtd_sales_through_poi);
        TextView tvOskAchCallMtd = findViewById(R.id.tv_pjp_ach_call_mtd_osk);
        TextView tvOskPlanCallMtd = findViewById(R.id.tv_pjp_mtd_plan_actual_call_osk);
        TextView tvOskActualCallMtd = findViewById(R.id.tv_pjp_mtd_actual_actual_call_osk);
        TextView tvOskEffectiveCallMtd = findViewById(R.id.tv_pjp_mtd_effective_call_osk);
        TextView tvOskPlanOutletProdMtd = findViewById(R.id.tv_pjp_mtd_plan_outlet_prod_osk);
        TextView tvOskOutletProdMtd = findViewById(R.id.tv_pjp_mtd_actual_outlet_prod_osk);
        TextView tvOskActualSalesMtd = findViewById(R.id.tv_pjp_mtd_sales_through_osk);

        Log.d("Summary Performance : ", performancePoiOsk.toString());

        setTextOrnull(tvPoiAchCallPtd, performancePoiOsk.getPoiAchCallPtd(), "");
        setTextOrnull(tvPoiPlanCallPtd, performancePoiOsk.getPoiPlanCallPtd(), ",");
        setTextOrnull(tvPoiActualCallPtd, performancePoiOsk.getPoiActualCallPtd(), ",");
        setTextOrnull(tvPoiEffectiveCallPtd, performancePoiOsk.getPoiEffectiveCallPtd(), ",");
        setTextOrnull(tvPoiPlanMarketSurveyPtd, performancePoiOsk.getPoiPlanChannelPtd(), ",");
        setTextOrnull(tvPoiActualMarketSurveyPtd, performancePoiOsk.getPoiMarketSurveyPtd(), ",");
        setTextOrnull(tvPoiActualSalesPtd, performancePoiOsk.getPoiActualSalesPtd(), ",");
        setTextOrnull(tvOskAchCallPtd, performancePoiOsk.getOskAchCallPtd(), "");
        setTextOrnull(tvOskPlanCallPtd, performancePoiOsk.getOskPlanCallPtd(), ",");
        setTextOrnull(tvOskActualCallPtd, performancePoiOsk.getOskActualCallPtd(), ",");
        setTextOrnull(tvOskEffectiveCallPtd, performancePoiOsk.getOskEffectiveCallPtd(), ",");
        setTextOrnull(tvOskPlanOutletProdPtd, performancePoiOsk.getOskPlanChannelPtd(), ",");
        setTextOrnull(tvOskOutletProdPtd, performancePoiOsk.getOskOutletProductivePtd(), ",");
        setTextOrnull(tvOskActualSalesPtd, performancePoiOsk.getOskActualSalesPtd(), ",");

        /// mtd
        setTextOrnull(tvPoiAchCallMtd, performancePoiOsk.getPoiAchCallMtd(), "");
        setTextOrnull(tvPoiPlanCallMtd, performancePoiOsk.getPoiPlanCallMtd(), ",");
        setTextOrnull(tvPoiActualCallMtd, performancePoiOsk.getPoiActualCallMtd(), ",");
        setTextOrnull(tvPoiEffectiveCallMtd, performancePoiOsk.getPoiEffectiveCallMtd(), ",");
        setTextOrnull(tvPoiPlanMarketSurveyMtd, performancePoiOsk.getPoiPlanMtd(), ",");
        setTextOrnull(tvPoiActualMarketSurveyMtd, performancePoiOsk.getPoiMarketSurveyMtd(), ",");
        setTextOrnull(tvPoiActualSalesMtd, performancePoiOsk.getPoiActualSalesMtd(), ",");
        setTextOrnull(tvOskAchCallMtd, performancePoiOsk.getOskAchCallMtd(), "");
        setTextOrnull(tvOskPlanCallMtd, performancePoiOsk.getOskPlanCallMtd(), ",");
        setTextOrnull(tvOskActualCallMtd, performancePoiOsk.getOskActualCallMtd(), ",");
        setTextOrnull(tvOskEffectiveCallMtd, performancePoiOsk.getOskEffectiveCallMtd(), ",");
        setTextOrnull(tvOskPlanOutletProdMtd, performancePoiOsk.getOskPlanMtd(), ",");
        setTextOrnull(tvOskOutletProdMtd, performancePoiOsk.getOskOutletProductiveMtd(), ",");
        setTextOrnull(tvOskActualSalesMtd, performancePoiOsk.getOskActualSalesMtd(), ",");

    }

    @Override
    public void setupSummaryPerformanceEvent(PerformanceEvent performanceEvent, String userType) {
        TextView tvAchEventCallMtd = findViewById(R.id.tv_mtd_ach_actual_event);
        TextView tvAchEventCallPtd = findViewById(R.id.tv_ptd_ach_actual_event);
        TextView tvActualEventCallMtd = findViewById(R.id.tv_mtd_performance_actual_actual_event);
        TextView tvActualEventCallPtd = findViewById(R.id.tv_ptd_performance_actual_actual_event);
        TextView tvPlanEventCallPtd = findViewById(R.id.tv_ptd_target_performance_actual_event);
        TextView tvPlanEventCallMtd = findViewById(R.id.tv_mtd_target_performance_actual_event);
        TextView tvSalesEventCallPtd = findViewById(R.id.tv_ptd_sales_performance_actual_event);
        TextView tvSalesEventCallMtd = findViewById(R.id.tv_mtd_sales_performance_actual_event);

        Log.d("Performance Event :",  performanceEvent.toString());

        setTextOrnull(tvAchEventCallMtd, performanceEvent.getEventAchCallMtd(), "");
        setTextOrnull(tvAchEventCallPtd, performanceEvent.getEventAchCallPtd(), "");
        setTextOrnull(tvActualEventCallMtd, performanceEvent.getEventActualCallMtd(), ",");
        setTextOrnull(tvActualEventCallPtd, performanceEvent.getEventActualCallPtd(), ",");
        setTextOrnull(tvPlanEventCallMtd, performanceEvent.getEventPlanCallMtd(), ",");
        setTextOrnull(tvPlanEventCallPtd, performanceEvent.getEventPlanCallPtd(), ",");
        setTextOrnull(tvSalesEventCallMtd, performanceEvent.getActualSalesMtd(), ",");
        setTextOrnull(tvSalesEventCallPtd, performanceEvent.getActualSalesPtd(), ",");
    }
    SummaryType mSummaryType;
    @Override
    public void setupSummaryPerformanceBackchecking(PerformanceBackchecking performanceBackchecking, String userType) {
        TextView tvPoiAchBc = findViewById(R.id.tv_ach_bc_poi);
        TextView tvPoiTargetBc = findViewById(R.id.tv_target_bc_poi);
        TextView tvPoiActualBc = findViewById(R.id.tv_actual_bc_poi);
        TextView tvOskAchBc = findViewById(R.id.tv_ach_bc_osk);
        TextView tvOskTargetBc = findViewById(R.id.tv_target_bc_osk);
        TextView tvOskActualBc = findViewById(R.id.tv_actual_bc_osk);

        Log.d("Performance BC:", performanceBackchecking.toString());

        setTextOrnull(tvPoiAchBc, performanceBackchecking.getPoiAchBc(), "");
        setTextOrnull(tvPoiTargetBc, performanceBackchecking.getPoiTargetBc(), ",");
        setTextOrnull(tvPoiActualBc, performanceBackchecking.getPoiActualBc(), ",");
        setTextOrnull(tvOskAchBc, performanceBackchecking.getOskAchBc(), "");
        setTextOrnull(tvOskTargetBc, performanceBackchecking.getOskTargetBc(), ",");
        setTextOrnull(tvOskActualBc, performanceBackchecking.getOskActualBc(), ",");
    }

    @Override
    public void setupSummaryPerformanceShare(PerformanceShare performanceShare, String userType) {
        List<Integer> bs = new ArrayList<>();
        List<Integer> ds = new ArrayList<>();
        List<Integer> rs = new ArrayList<>();
        List<Integer> ms = new ArrayList<>();

        bs.add(Math.round(Float.valueOf(performanceShare.getBroadbandshareTelkomsel())));
        bs.add(Math.round(Float.valueOf(performanceShare.getBroadbandshareOoredoo())));
        bs.add(Math.round(Float.valueOf(performanceShare.getBroadbandshareXl())));
        bs.add(Math.round(Float.valueOf(performanceShare.getBroadbandshareAxis())));
        bs.add(Math.round(Float.valueOf(performanceShare.getBroadbandshare3())));
        bs.add(Math.round(Float.valueOf(performanceShare.getBroadbandshareOthers())));


        ds.add(Math.round(Float.valueOf(performanceShare.getDisplayshareTelkomsel())));
        ds.add(Math.round(Float.valueOf(performanceShare.getDisplayshareOoredoo())));
        ds.add(Math.round(Float.valueOf(performanceShare.getDisplayshareXl())));
        ds.add(Math.round(Float.valueOf(performanceShare.getDisplayshareAxis())));
        ds.add(Math.round(Float.valueOf(performanceShare.getDisplayshare3())));
        ds.add(Math.round(Float.valueOf(performanceShare.getDisplayshareOthers())));

        rs.add(Math.round(Float.valueOf(performanceShare.getRechargeshareTelkomsel())));
        rs.add(Math.round(Float.valueOf(performanceShare.getRechargeshareOoredoo())));
        rs.add(Math.round(Float.valueOf(performanceShare.getRechargeshareXl())));
        rs.add(Math.round(Float.valueOf(performanceShare.getRechargeshareAxis())));
        rs.add(Math.round(Float.valueOf(performanceShare.getRechargeshare3())));
        rs.add(Math.round(Float.valueOf(performanceShare.getRechargeshareOthers())));

        ms.add(Math.round(Float.valueOf(performanceShare.getMarketshareTelkomsel())));
        ms.add(Math.round(Float.valueOf(performanceShare.getMarketshareOoredoo())));
        ms.add(Math.round(Float.valueOf(performanceShare.getMarketshareXl())));
        ms.add(Math.round(Float.valueOf(performanceShare.getMarketshareAxis())));
        ms.add(Math.round(Float.valueOf(performanceShare.getMarketshare3())));
        ms.add(Math.round(Float.valueOf(performanceShare.getMarketshareOthers())));

        simulateProgress(bs,ds,rs,ms);
    }

    @Override
    public void hideMenuAoc() {
        cvBcPerformance.setVisibility(View.GONE);
        lineInfoSummaryShare.setVisibility(View.GONE);
    }

    @Override
    public void hideMenuTapYba() {
        cvPjpProdMtd.setVisibility(View.GONE);
        cvPjpProdPtd.setVisibility(View.GONE);
        cvBcPerformance.setVisibility(View.GONE);
        lineInfoSummaryShare.setVisibility(View.GONE);
    }

    @Override
    public void hideMenuMgt6() {
        cvPjpProdMtd.setVisibility(View.GONE);
        cvPjpProdPtd.setVisibility(View.GONE);
        cvBcPerformance.setVisibility(View.GONE);
        cvPerformanceActualEvent.setVisibility(View.GONE);
        lineInfoSummaryShare.setVisibility(View.GONE);
    }


    private void simulateProgress(List<Integer> bs,List<Integer> ds,List<Integer> rs, List<Integer> ms) {
        ValueAnimator animatortselbs = ValueAnimator.ofInt(0, bs.get(0));
        ValueAnimator animatorooredoobs = ValueAnimator.ofInt(0, bs.get(1));
        ValueAnimator animatorxlbs = ValueAnimator.ofInt(0, bs.get(2));
        ValueAnimator animatoraxisbs = ValueAnimator.ofInt(0, bs.get(3));
        ValueAnimator animator3bs = ValueAnimator.ofInt(0, bs.get(4));
        ValueAnimator animatorothersbs = ValueAnimator.ofInt(0, bs.get(5));
        // ds
        ValueAnimator animatortselds = ValueAnimator.ofInt(0, ds.get(0));
        ValueAnimator animatorooredoods = ValueAnimator.ofInt(0, ds.get(1));
        ValueAnimator animatorxlds = ValueAnimator.ofInt(0, ds.get(2));
        ValueAnimator animatoraxisds = ValueAnimator.ofInt(0, ds.get(3));
        ValueAnimator animator3ds = ValueAnimator.ofInt(0, ds.get(4));
        ValueAnimator animatorothersds = ValueAnimator.ofInt(0, ds.get(5));
        // rs
        ValueAnimator animatortselrs = ValueAnimator.ofInt(0, rs.get(0));
        ValueAnimator animatorooredoors = ValueAnimator.ofInt(0, rs.get(1));
        ValueAnimator animatorxlrs = ValueAnimator.ofInt(0, rs.get(2));
        ValueAnimator animatoraxisrs = ValueAnimator.ofInt(0, rs.get(3));
        ValueAnimator animator3rs = ValueAnimator.ofInt(0, rs.get(4));
        ValueAnimator animatorothersrs = ValueAnimator.ofInt(0, rs.get(5));
        //ms
        ValueAnimator animatortselms = ValueAnimator.ofInt(0, ms.get(0));
        ValueAnimator animatorooredooms = ValueAnimator.ofInt(0, ms.get(1));
        ValueAnimator animatorxlms = ValueAnimator.ofInt(0, ms.get(2));
        ValueAnimator animatoraxisms = ValueAnimator.ofInt(0, ms.get(3));
        ValueAnimator animator3ms = ValueAnimator.ofInt(0, ms.get(4));
        ValueAnimator animatorothersms = ValueAnimator.ofInt(0, ms.get(5));
        // bs
        animatortselbs.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer progress = (Integer) animation.getAnimatedValue();
                try{
                    if ( progress != null) {
                        persentageTselBs.setProgress(progress);
                    }else {
                        persentageTselBs.setProgress(0);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        animatortselbs.setDuration(100);
        animatortselbs.start();
        animatorooredoobs.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer progress = (Integer) animation.getAnimatedValue();
                try{
                    if ( progress != null) {
                        persentageOoredooBs.setProgress(progress);
                    }else {
                        persentageOoredooBs.setProgress(0);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        animatorooredoobs.setDuration(100);
        animatorooredoobs.start();
        animatorxlbs.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer progress = (Integer) animation.getAnimatedValue();
                try{
                    if ( progress != null) {
                        persentageXlBs.setProgress(progress);
                    }else {
                        persentageXlBs.setProgress(0);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        animatorxlbs.setDuration(100);
        animatorxlbs.start();
        animatoraxisbs.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer progress = (Integer) animation.getAnimatedValue();
                try{
                    if ( progress != null) {
                        persentageAxisBs.setProgress(progress);
                    }else {
                        persentageAxisBs.setProgress(0);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        animatoraxisbs.setDuration(100);
        animatoraxisbs.start();
        animator3bs.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer progress = (Integer) animation.getAnimatedValue();
                try{
                    if ( progress != null) {
                        persentage3Bs.setProgress(progress);
                    }else {
                        persentage3Bs.setProgress(0);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        animator3bs.setDuration(100);
        animator3bs.start();
        animatorothersbs.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer progress = (Integer) animation.getAnimatedValue();
                try{
                    if ( progress != null) {
                        persentageOthersBs.setProgress(progress);
                    }else {
                        persentageOthersBs.setProgress(0);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        animatorothersbs.setDuration(100);
        animatorothersbs.start();
        // ds
        animatortselds.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer progress = (Integer) animation.getAnimatedValue();
                try{
                    if ( progress != null) {
                        persentageTselDs.setProgress(progress);
                    }else {
                        persentageTselDs.setProgress(0);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        animatortselds.setDuration(100);
        animatortselds.start();
        animatorooredoods.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer progress = (Integer) animation.getAnimatedValue();
                try{
                    if ( progress != null) {
                        persentageOoredooDs.setProgress(progress);
                    }else {
                        persentageOoredooDs.setProgress(0);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        animatorooredoods.setDuration(100);
        animatorooredoods.start();
        animatorxlds.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer progress = (Integer) animation.getAnimatedValue();
                try{
                    if ( progress != null) {
                        persentageXlDs.setProgress(progress);
                    }else {
                        persentageXlDs.setProgress(0);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        animatorxlds.setDuration(100);
        animatorxlds.start();
        animatoraxisds.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer progress = (Integer) animation.getAnimatedValue();
                try{
                    if ( progress != null) {
                        persentageAxisDs.setProgress(progress);
                    }else {
                        persentageAxisDs.setProgress(0);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        animatoraxisds.setDuration(100);
        animatoraxisds.start();
        animator3ds.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer progress = (Integer) animation.getAnimatedValue();
                try{
                    if ( progress != null) {
                        persentage3Ds.setProgress(progress);
                    }else {
                        persentage3Ds.setProgress(0);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        animator3ds.setDuration(100);
        animator3ds.start();
        animatorothersds.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer progress = (Integer) animation.getAnimatedValue();
                try{
                    if ( progress != null) {
                        persentageOthersDs.setProgress(progress);
                    }else {
                        persentageOthersDs.setProgress(0);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        animatorothersds.setDuration(100);
        animatorothersds.start();
        // rs
        animatortselrs.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer progress = (Integer) animation.getAnimatedValue();
                try{
                    if ( progress != null) {
                        persentageTselRs.setProgress(progress);
                    }else {
                        persentageTselRs.setProgress(0);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        animatortselrs.setDuration(100);
        animatortselrs.start();
        animatorooredoors.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer progress = (Integer) animation.getAnimatedValue();
                try{
                    if ( progress != null) {
                        persentageOoredooRs.setProgress(progress);
                    }else {
                        persentageOoredooRs.setProgress(0);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        animatorooredoors.setDuration(100);
        animatorooredoors.start();
        animatorxlrs.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer progress = (Integer) animation.getAnimatedValue();
                try{
                    if ( progress != null) {
                        persentageXlRs.setProgress(progress);
                    }else {
                        persentageXlRs.setProgress(0);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        animatorxlrs.setDuration(100);
        animatorxlrs.start();
        animatoraxisrs.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer progress = (Integer) animation.getAnimatedValue();
                try{
                    if ( progress != null) {
                        persentageAxisRs.setProgress(progress);
                    }else {
                        persentageAxisRs.setProgress(0);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        animatoraxisrs.setDuration(100);
        animatoraxisrs.start();
        animator3rs.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer progress = (Integer) animation.getAnimatedValue();
                try{
                    if ( progress != null) {
                        persentage3Rs.setProgress(progress);
                    }else {
                        persentage3Rs.setProgress(0);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        animator3rs.setDuration(100);
        animator3rs.start();
        animatorothersrs.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer progress = (Integer) animation.getAnimatedValue();
                try{
                    if ( progress != null) {
                        persentageOthersRs.setProgress(progress);
                    }else {
                        persentageOthersRs.setProgress(0);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        animatorothersrs.setDuration(100);
        animatorothersrs.start();
        //ms
        animatortselms.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer progress = (Integer) animation.getAnimatedValue();
                try{
                    if ( progress != null) {
                        persentageTselMs.setProgress(progress);
                    }else {
                        persentageTselMs.setProgress(0);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });
        animatortselms.setDuration(100);
        animatortselms.start();
        animatorooredooms.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer progress = (Integer) animation.getAnimatedValue();
                try{
                    if ( progress != null) {
                        persentageOoredooMs.setProgress(progress);
                    }else {
                        persentageOoredooMs.setProgress(0);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        animatorooredooms.setDuration(100);
        animatorooredooms.start();
        animatorxlms.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer progress = (Integer) animation.getAnimatedValue();
                try{
                    if ( progress != null) {
                        persentageXlMs.setProgress(progress);
                    }else {
                        persentageXlMs.setProgress(0);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        animatorxlms.setDuration(100);
        animatorxlms.start();
        animatoraxisms.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer progress = (Integer) animation.getAnimatedValue();
                try{
                    if ( progress != null) {
                        persentageAxisMs.setProgress(progress);
                    }else {
                        persentageAxisMs.setProgress(0);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        animatoraxisms.setDuration(100);
        animatoraxisms.start();
        animator3ms.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer progress = (Integer) animation.getAnimatedValue();
                try{
                    if ( progress != null) {
                        persentage3Ms.setProgress(progress);
                    }else {
                        persentage3Ms.setProgress(0);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        animator3ms.setDuration(100);
        animator3ms.start();
        animatorothersms.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer progress = (Integer) animation.getAnimatedValue();
                try{
                    if ( progress != null) {
                        persentageOthersMs.setProgress(progress);
                    }else {
                        persentageOthersMs.setProgress(0);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        animatorothersms.setDuration(100);
        animatorothersms.start();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case READ_PHONE_ICCID_PERMISSION:{

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    mPresenter.validateICCIDNumber(this);
                }else{
                    showLoading("Error You Must Enable this permission to login");

                }
            }
        }
    }

    private static final class MyProgressFormatter implements CircleProgressBar.ProgressFormatter {
        private static final String DEFAULT_PATTERN = "%d%%";

        @Override
        public CharSequence format(int progress, int max) {
            return String.format(DEFAULT_PATTERN, (int) ((float) progress / (float) max * 100));
        }
    }

    private void setTextOrnull(TextView textView, String text,String option) {
        if (option.equals(",")){
            DecimalFormat decim = new DecimalFormat("#,###,###");
            textView.setText(decim.format(Long.valueOf(text)));
        }else {
            if (text == null)
                textView.setText("0" + option);
            else {
                textView.setText(text + option);
            }
        }
    }

    private void setOnClickListeners(String userType){

        if (userType.toLowerCase().equals("aoc")){
            cvPjpProdMtd.setOnClickListener(null);
            cvPjpProdPtd.setOnClickListener(null);
            cvPerformanceActualEvent.setOnClickListener(null);
        }else if (userType.toLowerCase().equals("tap") || userType.toLowerCase().equals("yba")) {
            cvPerformanceActualEvent.setOnClickListener(null);
        }else {
            cvPjpProdPtd.setOnClickListener(v -> {
                Intent i = SummaryAreaActivity.getStartIntent(this,
                        userType,
                        SummaryType.PJP_PTD_POI_OSK);
                startActivity(i);
            });

            cvPjpProdMtd.setOnClickListener(v -> {
                Intent i = SummaryAreaActivity.getStartIntent(this,
                        userType,
                        SummaryType.PJP_MTD_POI_OSK);
                startActivity(i);
            });

            cvPerformanceActualEvent.setOnClickListener(v -> {
                Intent i = SummaryAreaActivity.getStartIntent(this,
                        userType,
                        SummaryType.PERFORMANCE_ACTUAL_EVENT);
                startActivity(i);
            });

            cvBcPerformance.setOnClickListener(v -> {
                Intent i = SummaryBackcheckingActivity.getStartIntent(this,
                        userType,
                        BackcheckingType.PERFORMANCE_BACKCHECKING);
                startActivity(i);
            });

            lineInfoSummaryShare.setOnClickListener(v -> {
                Intent i = ShareAreaActivity.getStartIntent(this,
                        userType,
                        ShareType.PERCENTAGE_SHARE);
                startActivity(i);
            });
        }
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }
}
