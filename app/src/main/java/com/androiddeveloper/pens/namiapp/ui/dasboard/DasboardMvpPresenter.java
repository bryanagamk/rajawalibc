package com.androiddeveloper.pens.namiapp.ui.dasboard;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;

import com.androiddeveloper.pens.namiapp.di.PerActivity;
import com.androiddeveloper.pens.namiapp.ui.base.MvpPresenter;

import java.net.URI;

@PerActivity
public interface DasboardMvpPresenter<V extends DasboardMvpView> extends MvpPresenter<V> {

    void logoutProcess();

    void validateICCIDNumber(Context activityContext);

    void getMenuPermissionList(String userType);

    void getSummaryPerformancePoiOsk(String userId, String userId_aoc, String username_aoc, String period_type);

    void getSummaryPerformanceEvent(String userId, String username_aoc);

    void getSummaryPerformanceBackchecking(String userId);

    void getSummaryPerformanceShare(String userId);

    void uploadPhotoProfile(Bitmap bitmap);

    String getUploadProfile();

    String getUseridProfile();
}
