package com.androiddeveloper.pens.namiapp.ui.dasboard;

import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceBackchecking;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceEvent;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformancePoiOsk;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceShare;
import com.androiddeveloper.pens.namiapp.data.network.model.SummaryEvent;
import com.androiddeveloper.pens.namiapp.data.network.model.SummaryInstitution;
import com.androiddeveloper.pens.namiapp.data.network.model.SummaryOsk;
import com.androiddeveloper.pens.namiapp.data.network.model.SummarySales;
import com.androiddeveloper.pens.namiapp.ui.base.MvpView;
import com.androiddeveloper.pens.namiapp.utils.MenuEnum;

import java.util.List;

public interface DasboardMvpView extends MvpView {

    void openVerifyNumberActivity();

    void setUserData(String phone, String userName, String userTitle,String userType);

    void requestPermissionReadPhonePermission();

    void showAvailableMenut(List<MenuEnum> enumList);

    void setupSummaryPerformancePoiOsk(PerformancePoiOsk performancePoiOsk, String userType);

    void setupSummaryPerformanceEvent(PerformanceEvent performanceEvent, String userType);

    void setupSummaryPerformanceBackchecking(PerformanceBackchecking performanceBackchecking, String userType);

    void setupSummaryPerformanceShare(PerformanceShare performanceShare, String userType);

    void hideMenuAoc();

    void hideMenuTapYba();

    void hideMenuMgt6();

    void loadUserProfile();
}
