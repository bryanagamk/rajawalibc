package com.androiddeveloper.pens.namiapp.ui.dasboard;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Base64;
import android.util.Log;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.service.NotificationService;
import com.androiddeveloper.pens.namiapp.ui.base.BaseActivity;
import com.androiddeveloper.pens.namiapp.ui.base.BasePresenter;
import com.androiddeveloper.pens.namiapp.utils.MenuEnum;
import com.androiddeveloper.pens.namiapp.utils.SimUtil;
import com.androiddeveloper.pens.namiapp.utils.StartupConfig;
import com.androidnetworking.error.ANError;
import com.crashlytics.android.Crashlytics;
import com.google.firebase.messaging.FirebaseMessaging;

import org.apache.http.conn.ConnectTimeoutException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.nio.channels.ConnectionPendingException;
import java.security.PrivateKey;
import java.util.List;
import java.util.Observable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.logging.Handler;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Connection;

public class DasboardPresenter<V extends DasboardMvpView> extends BasePresenter<V>
        implements DasboardMvpPresenter<V> {

    private static final String TAG = "DasboardPresenter";
    String status;
    private BaseActivity baseActivity;
    @Override
    public void onAttach(V mvpView) {
        super.onAttach(mvpView);
        String userId_aoc = "0";
        String username_aoc = "";
        String period_type = "";
        String username = getDataManager().getCurrentUserName();
        String userType = getDataManager().getCurrentUserType();
        setCrashLytic(username, userType);
        getMenuPermissionList(userType);
        StartupConfig.getInstance(getDataManager(),true);
        if (getDataManager().getCurrentUserId() != null ){
            String userId = getDataManager().getCurrentUserId();
            FirebaseMessaging.getInstance().subscribeToTopic(userId);
            getSummaryPerformancePoiOsk(userId, userId_aoc, username_aoc, period_type);
            getSummaryPerformanceEvent(userId, username_aoc);
            getSummaryPerformanceBackchecking(userId);
            getSummaryPerformanceShare(userId);
            if(userType.toLowerCase().equals("aoc")){
                getMvpView().hideMenuAoc();
            }else if (userType.toLowerCase().equals("tap") || userType.toLowerCase().equals("yba")){
                getMvpView().hideMenuTapYba();
            }else if (userType.toLowerCase().equals("mgt6")){
                getMvpView().hideMenuMgt6();
            }
        }

    }

    private void setCrashLytic(String username, String userType) {
        Crashlytics.setUserIdentifier(username + " " + userType);
    }

    @Inject
    public DasboardPresenter(DataManager dataManager,
                             CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);
    }

    @Override
    public void logoutProcess()
    {
        //tambahkan fungsi logout
        String phoneNumber = getDataManager().getCurrentUserId().replace("+","");
        getCompositeDisposable().add(getDataManager().sentLogoutUser(
                phoneNumber
        ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(basicResponse -> {
                    if (!isViewAttached()){
                        return;
                    }
                    if (basicResponse.getSuccess()){
                        getMvpView().showMessage("Logout Berhasil");
                        getDataManager().setUserAsLoggedOut();
                        getMvpView().openVerifyNumberActivity();
                    }else{
                        getMvpView().showMessage("Logout Gagal");
                    }
                },throwable -> {
                    if (!isViewAttached()){
                        return;
                    }
                    Log.d("Error : ",throwable.toString());
                    getMvpView().showMessage("Tidak dapat Terhubung Ke Server");
                    if (throwable instanceof ANError){
                        ANError anError = (ANError) throwable;
                        Log.d("Error : " , anError.getErrorBody());
                        handleApiError(anError);
                    }
                }));
    }

    @Override
    public void validateICCIDNumber(Context activityContext) {
        getMvpView().hideLoading();
        String phone = getDataManager().getCurrentUserId();
        String userName = getDataManager().getCurrentUserName();
        String userTitle = getDataManager().getCurrentUserTitle();
        String userType = getDataManager().getCurrentUserType().toLowerCase();
        getMvpView().setUserData(phone, userName, userTitle,userType);
    }

    @Override
    public void getMenuPermissionList(String userType) {
        if (userType == null)
            userType = "";
        String finalUserType = userType;
        getCompositeDisposable().add(getDataManager().
                getPermissionMenu()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(configPermitResponse -> {
            if (!isViewAttached()){
                return;
            }
            List<MenuEnum> availableMenu = configPermitResponse.getAvailableMenu(finalUserType);
            getMvpView().showAvailableMenut(availableMenu);
        },throwable -> {
            if (!isViewAttached()){
                return;
            }
            getMenuPermissionList(finalUserType);
            Log.d("Error",throwable.toString());

            if (throwable instanceof ANError){
                ANError anError = (ANError) throwable;
                handleApiError(anError);
            }
        }));
    }

    @Override
    public void getSummaryPerformancePoiOsk(String userId, String userId_aoc, String username_aoc, String period_type) {
        String userType = getDataManager().getCurrentUserType().toLowerCase();
        getCompositeDisposable().add(getDataManager().getSummaryPerformancePoiOsk(userId,selectTeritory(userType), userId_aoc, username_aoc, period_type)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(performancePoiOskResponse -> {
            if (!isViewAttached()){
                return;
            }
            if (performancePoiOskResponse.isSuccess()){
                getMvpView().setupSummaryPerformancePoiOsk(performancePoiOskResponse.getSummaryPerformancePoiOsk().get(0),userType);
            }
        },throwable -> {
            if (!isViewAttached()){
                return;
            }
            getSummaryPerformancePoiOsk(userId, userId_aoc, username_aoc, period_type);

            Log.d("Error",throwable.toString());


            if (throwable instanceof ANError){
                ANError anError = (ANError) throwable;
                Log.d("Error : " , anError.getErrorBody());
                handleApiError(anError);
            }
        }));
    }

    @Override
    public void getSummaryPerformanceEvent(String userId, String username_aoc) {
        String userType = getDataManager().getCurrentUserType().toLowerCase();
        getCompositeDisposable().add(getDataManager().getSummaryPerformanceEvent(userId,selectTeritory(userType), username_aoc)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(performanceEventResponse -> {
                    if (!isViewAttached()){
                        return;
                    }
                    if (performanceEventResponse.isSuccess()){
                        getMvpView().setupSummaryPerformanceEvent(performanceEventResponse.getSummaryPerformanceEvent().get(0),userType);
                    }
                },throwable -> {
                    if (!isViewAttached()){
                        return;
                    }
                    getSummaryPerformanceEvent(userId, username_aoc);

                    Log.d("Error",throwable.toString());

                    if (throwable instanceof ANError){
                        ANError anError = (ANError) throwable;
                        Log.d("Error : " , anError.getErrorBody());
                        handleApiError(anError);
                    }
                }));
    }

    @Override
    public void getSummaryPerformanceBackchecking(String userId) {
        String userType = getDataManager().getCurrentUserType().toLowerCase();
        getCompositeDisposable().add(getDataManager().getSummaryPerformanceBackchecking(userId,selectTeritory(userType))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(performanceBackcheckingResponse -> {
                    if (!isViewAttached()){
                        return;
                    }
                    if (performanceBackcheckingResponse.isSuccess()){
                        getMvpView().setupSummaryPerformanceBackchecking(performanceBackcheckingResponse.getSummaryBackchecking().get(0),userType);
                    }
                },throwable -> {
                    if (!isViewAttached()){
                        return;
                    }
                    getSummaryPerformanceBackchecking(userId);

                    Log.d("Error",throwable.toString());

                    if (throwable instanceof ANError){
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

    @Override
    public void getSummaryPerformanceShare(String userId) {
        String userType = getDataManager().getCurrentUserType().toLowerCase();
        getCompositeDisposable().add(getDataManager().getSummaryPerformanceShare(userId,selectTeritory(userType))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(performanceShareResponse -> {
                    if (!isViewAttached()){
                        return;
                    }
                    if (performanceShareResponse.isSuccess()){
                        getMvpView().setupSummaryPerformanceShare(performanceShareResponse.getSummaryPerformanceShare().get(0),userType);
                    }
                },throwable -> {
                    if (!isViewAttached()){
                        return;
                    }
                    getSummaryPerformanceShare(userId);

                    Log.d("Error",throwable.toString());

                    if (throwable instanceof ANError){
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

    @Override
    public void uploadPhotoProfile(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos); //bm is the bitmap object
        byte[] b = baos.toByteArray();
        String userId = getDataManager().getCurrentUserId();
        String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

        getMvpView().showMessage("Sedang upload foto");
        Log.d("Debug : ",encodedImage);

        getCompositeDisposable().add(getDataManager().uploadProfilePhoto(userId,encodedImage)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(basicReponse -> {
                    getDataManager().setCurrentUserPathImage(basicReponse.getMessage());
                    getMvpView().showMessage("Success Upload");
                    getMvpView().hideLoading();
                    getMvpView().loadUserProfile();
                },throwable -> {
                    Log.d("Error",throwable.toString());
                    if (throwable instanceof ANError){
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

    @Override
    public String getUploadProfile() {
        return getDataManager().getCurrentUserPathImage();
    }

    @Override
    public String getUseridProfile() {
        return getDataManager().getCurrentUserId();
    }

    public String selectTeritory(String userType){
        switch (userType.toLowerCase()){
            case "mgt1":
                return "area";
            case "mgt2":
                return "regional";
            case "adm2":
                return "regional";
            case "bm":
                return "cluster";
            case "tl":
                return "cluster";
            case "aoc":
                return "aoc";
            default:
                return "branch";
        }
    }


}
