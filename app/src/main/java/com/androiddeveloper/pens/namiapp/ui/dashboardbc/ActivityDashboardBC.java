package com.androiddeveloper.pens.namiapp.ui.dashboardbc;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.data.network.rest.ApiClient;
import com.androiddeveloper.pens.namiapp.ui.base.BaseActivity;
import com.androiddeveloper.pens.namiapp.ui.dasboard.DasboardActivity;
import com.androiddeveloper.pens.namiapp.ui.soobackcheckinglocationlist.SooBackCheckingLocationListActivity;
import com.androiddeveloper.pens.namiapp.ui.verifiynumber.VerifyNumberActivity;
import com.androiddeveloper.pens.namiapp.utils.LocationValidatorUtil;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.ceylonlabs.imageviewpopup.ImagePopup;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.annotations.Nullable;

public class ActivityDashboardBC extends BaseActivity implements DashboardBCMvpView {

    private final String TAG = "ActivityDashboardBC";

    @BindView(R.id.btn_notification_bc)
    ImageButton btnNotification;

    @BindView(R.id.btn_setting_logout_bc)
    ImageButton btnLogout;

    @BindView(R.id.tv_version_code_app_bc)
    TextView tvVersionCodeApp;

    @BindView(R.id.iv_picture_user_profile_bc)
    ImageView ivPictureUserProfile;

    @BindView(R.id.tv_nama_user_rajawali)
    TextView tvNamaUser;

    @BindView(R.id.tv_ponsel_rajawali)
    TextView tvNotelp;

    @BindView(R.id.tv_level_rajawali)
    TextView tvLevel;

    @BindView(R.id.relative_info_bc)
    LinearLayout relativeInfoBC;

    @Inject
    DashboardBCMvpPresenter<DashboardBCMvpView> mPresenter;

    Uri mCropImageUri;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, ActivityDashboardBC.class);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_bc);

        getActivityComponent().inject(this);

        if ( isNetworkAvailable() ){
            setUnBinder(ButterKnife.bind(this));
            mPresenter.onAttach(this);
            setUp();
        }
    }

    @Override
    protected void setUp() {

        new Thread(() -> Glide.get(ActivityDashboardBC.this).clearDiskCache()).start();

        loadUserProfile();
        versionNameApp();

        if (Build.VERSION.SDK_INT >= 23) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},   2000);
        }else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2000);
        }

        changeStatusBarColor("#fafafa");

        mPresenter.validateICCIDNumber(getApplicationContext());

        btnNotification.setOnClickListener(v -> {
            Toast.makeText(getApplicationContext(), "Tahap pengembangan", Toast.LENGTH_SHORT).show();
        });

        btnLogout.setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(ActivityDashboardBC.this);
            builder.setCancelable(true);
            builder.setMessage("Apakah anda yakin ingin logout ?");

            builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    Toast.makeText(getApplicationContext(), "Logging out", Toast.LENGTH_SHORT).show();
                    mPresenter.logoutProcess();
                }
            });
            builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    builder.setCancelable(true);
                }
            });
            builder.show();
        });

        ivPictureUserProfile.setOnClickListener(View ->{
            uploadPhoto();
        });

        relativeInfoBC.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), SooBackCheckingLocationListActivity.class);
            startActivity(intent);
        });

        if (LocationValidatorUtil.isLocationHighAccuracyEnabled(this)){

        }else{
            showAlert("Mode High Accuracy pada setting GPS Anda belum aktif. Aktifkan terlebih dahulu untuk memulai NAMI. Masuk ke menu Setting - Location - Mode - High Accuracy. Letak menu bisa jadi berbeda pada masing-masing device");
        }

    }

    private void uploadPhoto() {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.item_upload_foto, null);
        Dialog alertDialogBuilder = new Dialog(this);
        alertDialogBuilder.setCanceledOnTouchOutside(false);
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialogBuilder.setContentView(promptView);
        Button btnLihatPhoto = (Button) promptView.findViewById(R.id.btn_lihat_foto);
        Button btnUploadPhoto= (Button) promptView.findViewById(R.id.btn_upload_foto);
        btnLihatPhoto.setOnClickListener(View -> {
            /** Set popup height, width & background color as you need or just leave default settings **/
            final ImagePopup imagePopup = new ImagePopup(this);
            imagePopup.setWindowHeight(800); // Optional
            imagePopup.setWindowWidth(800); // Optional
            imagePopup.setBackgroundColor(Color.WHITE);  // Optional
            imagePopup.setFullScreen(true); // Optional
            imagePopup.setHideCloseIcon(true);  // Optional
            imagePopup.setImageOnClickClose(true);  // Optional

            String pathImage = mPresenter.getUploadProfile();
            String userid = mPresenter.getUseridProfile();
            deleteCache(this);
            if (!userid.isEmpty() && pathImage.equals("-1L")){
                imagePopup.initiatePopupWithGlide(ApiClient.BASE_URL_NAMI + "Images/profile/" + userid + ".jpg");
                imagePopup.viewPopup();
            }else{
                imagePopup.initiatePopupWithGlide(ApiClient.BASE_URL_NAMI + pathImage);
                imagePopup.viewPopup();
            }
            alertDialogBuilder.dismiss();
        });
        btnUploadPhoto.setOnClickListener(View -> {
            onSelectImageClick(ivPictureUserProfile);
            alertDialogBuilder.dismiss();
        });
        alertDialogBuilder.show();
    }

    @SuppressLint("NewApi")
    private void onSelectImageClick(View view) {
        if (CropImage.isExplicitCameraPermissionRequired(this)) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE);
        } else {
            CropImage.startPickImageActivity(this);
        }
    }

    @Override
    public void loadUserProfile() {
        String pathImage = mPresenter.getUploadProfile();
        Log.d("Debug : ", pathImage);
        String userid = mPresenter.getUseridProfile();
        Log.d("Debug : ", userid);
        if (pathImage != null ){
            if (!userid.isEmpty() && pathImage.equals("-1L")){
                Glide.with(this)
                        .load(ApiClient.BASE_URL_NAMI + "Images/profile/" + userid + ".jpg")
                        .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true).placeholder(R.drawable.ic_profile_user).error(R.drawable.ic_profile_user))
                        .apply(RequestOptions.circleCropTransform())
                        .into(ivPictureUserProfile);

                ivPictureUserProfile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        uploadPhoto();
                    }
                });
            }else if (!userid.isEmpty() && !pathImage.equals("-1L")){
                Glide.with(this)
                        .load(ApiClient.BASE_URL_NAMI + pathImage)
                        .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true).placeholder(R.drawable.ic_profile_user).error(R.drawable.ic_profile_user))
                        .apply(RequestOptions.circleCropTransform())
                        .into(ivPictureUserProfile);

                ivPictureUserProfile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        uploadPhoto();
                    }
                });
            }
        }
    }

    @Override
    public void openVerifyNumberActivity() {
        Intent intent = VerifyNumberActivity.getStartIntent(this);
        startActivity(intent);
        finish();
    }

    @Override
    public void setUserData(String phone, String userName, String userTitle, String userType) {
        tvNotelp.setText(phone);
        tvLevel.setText(userType);
        tvNamaUser.setText(userName);
    }

    @Override
    @SuppressLint("NewApi")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);
            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()
                mCropImageUri = imageUri;
                Toast.makeText(this, "Permission dibutuhkan untuk mengambil gambar", Toast.LENGTH_SHORT).show();
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},   CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
            }else {
                // no permissions required or already granted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE){
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            Log.d("DEbug","Kepanggil");
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                Bitmap bitmap = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                    mPresenter.uploadPhotoProfile(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Log.d("Error",error.toString());
            }
        }
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setCropShape(CropImageView.CropShape.OVAL)
                .setMinCropWindowSize(0,0)
                .setMinCropResultSize(500,500)
                .setMaxCropResultSize(2000,2000)
                .setShowCropOverlay(true)
                .start(this);
    }

    public void versionNameApp(){
        Context context = getApplicationContext(); // or activity.getApplicationContext()
        PackageManager packageManager = context.getPackageManager();
        String packageName = context.getPackageName();

        String myVersionName = "not available"; // initialize String

        try {
            myVersionName = packageManager.getPackageInfo(packageName, 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        tvVersionCodeApp.setText(myVersionName);
    }

    private void changeStatusBarColor(String color){
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            View decor = getWindow().getDecorView();
            decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor(color));
        }
    }

    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
            } else if(dir.isFile()) {
                dir.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if(dir!= null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }
}
