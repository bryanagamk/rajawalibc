package com.androiddeveloper.pens.namiapp.ui.dashboardbc;

import android.content.Context;
import android.graphics.Bitmap;

import com.androiddeveloper.pens.namiapp.di.PerActivity;
import com.androiddeveloper.pens.namiapp.ui.base.MvpPresenter;

/**
 * Created by kmdr7 on 12/01/19
 * <p>
 * Author Andika Ahmad Ramadhan
 * Email Andikahmadr@gmail.com
 * Github https://github.com/kmdrn7
 **/

@PerActivity
public interface DashboardBCMvpPresenter<V extends DashboardBCMvpView> extends MvpPresenter<V> {

    void validateICCIDNumber(Context activityContext);

    String getUserid();

    String getUploadProfile();

    String getUseridProfile();

    void uploadPhotoProfile(Bitmap bitmap);

    void logoutProcess();

}