package com.androiddeveloper.pens.namiapp.ui.dashboardbc;

import com.androiddeveloper.pens.namiapp.ui.base.MvpView;

/**
 * Created by kmdr7 on 12/01/19
 * <p>
 * Author Andika Ahmad Ramadhan
 * Email Andikahmadr@gmail.com
 * Github https://github.com/kmdrn7
 **/
public interface DashboardBCMvpView extends MvpView {

    void loadUserProfile();

    void openVerifyNumberActivity();

    void setUserData(String phone, String userName, String userTitle,String userType);
}
