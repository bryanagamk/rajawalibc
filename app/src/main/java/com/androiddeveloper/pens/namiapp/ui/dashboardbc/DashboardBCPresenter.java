package com.androiddeveloper.pens.namiapp.ui.dashboardbc;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Base64;
import android.util.Log;

import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.ui.base.BasePresenter;
import com.androiddeveloper.pens.namiapp.utils.StartupConfig;
import com.androidnetworking.error.ANError;

import java.io.ByteArrayOutputStream;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by kmdr7 on 12/01/19
 * <p>
 * Author Andika Ahmad Ramadhan
 * Email Andikahmadr@gmail.com
 * Github https://github.com/kmdrn7
 **/
public class DashboardBCPresenter<V extends DashboardBCMvpView> extends BasePresenter<V> implements DashboardBCMvpPresenter<V> {

    @Override
    public void onAttach(V mvpView) {
        super.onAttach(mvpView);
    }

    @Inject
    public DashboardBCPresenter(DataManager dataManager, CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);
        StartupConfig.getInstance(getDataManager(),false, "RAJAWALI");
    }

    @Override
    public void validateICCIDNumber(Context activityContext) {
        getMvpView().hideLoading();
        String phone = getDataManager().getCurrentUserId();
        String userName = getDataManager().getCurrentUserName();
        String userTitle = getDataManager().getCurrentUserTitle();
        String userType = getDataManager().getCurrentUserType();
        getMvpView().setUserData(phone, userName, userTitle, userType);
    }

    @Override
    public String getUserid(){
        return getDataManager().getCurrentUserId();
    }

    @Override
    public String getUploadProfile() {
        return getDataManager().getCurrentUserPathImage();
    }

    @Override
    public String getUseridProfile() {
        return getDataManager().getCurrentUserId();
    }

    @Override
    public void uploadPhotoProfile(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos); //bm is the bitmap object
        byte[] b = baos.toByteArray();
        String userId = getDataManager().getCurrentUserId();
        String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

        getMvpView().showMessage("Sedang upload foto");
        Log.d("Debug : ",encodedImage);

        getCompositeDisposable().add(getDataManager().uploadProfilePhoto(userId,encodedImage)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(basicReponse -> {
                    getDataManager().setCurrentUserPathImage(basicReponse.getMessage());
                    getMvpView().showMessage("Success Upload");
                    getMvpView().hideLoading();
                    getMvpView().loadUserProfile();
                },throwable -> {
                    Log.d("Error",throwable.toString());
                    if (throwable instanceof ANError){
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

    @Override
    public void logoutProcess() {
        String phoneNumber = getDataManager().getCurrentUserId().replace("+","");
        getCompositeDisposable().add(getDataManager().sentLogoutUser(
                phoneNumber
        ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(basicResponse -> {
                    if (!isViewAttached()){
                        return;
                    }
                    if (basicResponse.getSuccess()){
                        getMvpView().showMessage("Logout Berhasil");
                        getDataManager().setUserAsLoggedOut();
                        getMvpView().openVerifyNumberActivity();
                    }else{
                        getMvpView().showMessage("Logout Gagal");
                    }
                },throwable -> {
                    if (!isViewAttached()){
                        return;
                    }
                    Log.d("Error : ",throwable.toString());
                    getMvpView().showMessage("Tidak dapat Terhubung Ke Server");
                    if (throwable instanceof ANError){
                        ANError anError = (ANError) throwable;
                        Log.d("Error : " , anError.getErrorBody());
                        handleApiError(anError);
                    }
                }));
    }
}
