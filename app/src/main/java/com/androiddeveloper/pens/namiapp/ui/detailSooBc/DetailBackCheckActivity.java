package com.androiddeveloper.pens.namiapp.ui.detailSooBc;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.data.network.model.BackcheckingResponse;
import com.androiddeveloper.pens.namiapp.ui.base.BaseActivity;
import com.androiddeveloper.pens.namiapp.ui.bcoutlet.OutletBCActivity;
import com.androiddeveloper.pens.namiapp.ui.bcsite.SiteBCActivity;
import com.androiddeveloper.pens.namiapp.utils.SingleShotLocationProvider;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailBackCheckActivity extends BaseActivity implements DetailBackCheckMvpView, OnMapReadyCallback {

    @Inject
    DetailBackCheckMvpPresenter<DetailBackCheckMvpView> mPresenter;

    @BindView(R.id.btn_go_detail_backchecking_location)
    Button btnGoBackchecking;

    @BindView(R.id.nested_backcheck)
    View bottomSheet;

    @BindView(R.id.tv_nama_outlet_detail_backchecking)
    TextView tv_nama;

    @BindView(R.id.tv_nama_aoc_detail_backchecking)
    TextView tv_nama_aoc;

    @BindView(R.id.tv_radius_detail_backchecking)
    TextView tv_radius;

    @BindView(R.id.tv_latitude_outlet_detail_backchecking)
    TextView tv_lat;

    @BindView(R.id.tv_longitude_outlet_detail_backchecking)
    TextView tv_long;

    @BindView(R.id.tv_alamat_outlet_detail_backchecking)
    TextView tv_alamat;

    @BindView(R.id.tv_regional_outlet_detail_backchecking)
    TextView tv_regional;

    @BindView(R.id.tv_branch_outlet_detail_backchecking)
    TextView tv_branch;

    @BindView(R.id.tv_cluster_outlet_detail_backchecking)
    TextView tv_cluster;

    @BindView(R.id.tv_city_outlet_detail_backchecking)
    TextView tv_city;

    @BindView(R.id.iv_arrow_detail_bc)
    ImageView ivArrowDetailBc;

    ProgressDialog progressDialog;

    private String mLocationId = "";
    private String mLocationType;

    private Menu menu;
    private GoogleMap mMap;
    private BottomSheetBehavior mBottomSheetBehavior1;
    private BackcheckingState mState;
    private BackcheckingResponse response;

    String nama, radius, long_, lat_, alamat, regional, branch, cluster, city, last_visit, display_telkomsel;
    String display_ooredoo, display_xl , display_axis, display_three, display_others, recharge_telkomsel;
    String recharge_ooredoo, recharge_xl, recharge_axis, recharge_three, recharge_others;
    String typeAction, mTypeAction;

    private static final String TAG = "DetailBackcheck";

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, DetailBackCheckActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_back_check);

        getActivityComponent().inject(this);

        if ( isNetworkAvailable() ){

            setUnBinder(ButterKnife.bind(this));
            mPresenter.onAttach(this);
            setUp();

        }

    }

    @Override
    protected void setUp() {

        getValueFromBundle();
        setFieldValue();
        mapSetup();
        progressDialog = new ProgressDialog(this);
        // backpress
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Detail Backchecking");

        mBottomSheetBehavior1 = BottomSheetBehavior.from(bottomSheet);
        mBottomSheetBehavior1.setState(BottomSheetBehavior.STATE_COLLAPSED);
        mBottomSheetBehavior1.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {

                }

                if (newState == BottomSheetBehavior.STATE_EXPANDED) {

                }

                if (newState == BottomSheetBehavior.STATE_DRAGGING) {

                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                ivArrowDetailBc.setRotation(slideOffset * 180);
            }
        });
    }

    private void mapSetup() {
        if (!CheckGooglePlayServices()) {
            showMessage("Google play service unavailable");
            return;
        }
        else {
            Log.d("onCreate","Google Play Services available.");
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_bc);
        mapFragment.getMapAsync(this);

    }

    @Override
    public void getValueFromBundle() {
        Intent getIntent = getIntent();
        Bundle extras = getIntent.getExtras();

        nama = extras.getString("nama");
        radius = extras.getString("radius");
        long_ = extras.getString("long");
        lat_ = extras.getString("lat");
        alamat = extras.getString("alamat");
        regional = extras.getString("regional");
        branch = extras.getString("branch");
        cluster = extras.getString("cluster");
        city = extras.getString("city");
        last_visit = extras.getString("last_visit");

        display_telkomsel = extras.getString("display_telkomsel");
        display_ooredoo = extras.getString("display_ooredoo");
        display_xl = extras.getString("display_xl");
        display_axis = extras.getString("display_axis");
        display_three = extras.getString("display_three");
        display_others = extras.getString("display_others");

        recharge_telkomsel = extras.getString("recharge_telkomsel");
        recharge_ooredoo = extras.getString("recharge_ooredoo");
        recharge_xl = extras.getString("recharge_xl");
        recharge_axis = extras.getString("recharge_axis");
        recharge_three = extras.getString("recharge_three");
        recharge_others = extras.getString("recharge_others");

        typeAction = extras.getString("type");
    }

    @Override
    public void setFieldValue() {
        tv_nama.setText(nama);
        tv_nama_aoc.setText(mPresenter.getLoggedUser());
        tv_radius.setText(radius);
        tv_lat.setText(lat_);
        tv_long.setText(long_);
        tv_alamat.setText(alamat);
        tv_regional.setText(regional);
        tv_branch.setText(branch);
        tv_city.setText(city);
        tv_cluster.setText(cluster);
        mTypeAction = typeAction;
    }

    @Override
    public void setBackcheckingStatus(BackcheckingState state) {
        btnGoBackchecking.setBackgroundResource(state.getType());
        btnGoBackchecking.setText(state.getText());
        mState = state;
        setBtnBackchecking(state, response);
    }

    private void setBtnBackchecking(BackcheckingState state, BackcheckingResponse response) {

        switch (state){
            case CHECKIN:
                btnGoBackchecking.setOnClickListener(v -> {
                    mPresenter.insertUserCheckin(DetailBackCheckActivity.this, lat_, long_);
                });
            case NEXT:
                btnGoBackchecking.setOnClickListener(v -> {
                    
                });
        }

    }

    @Override
    public void showLoading(String message) {
        super.showLoading(message);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        if (progressDialog != null ){
            progressDialog.dismiss();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                mMap.setMyLocationEnabled(true);
            }
        } else {
            mMap.setMyLocationEnabled(true);
        }

        showLoading("Sedang mendapatkan lokasi tempat");
        LatLng latLng_ = new LatLng(Double.parseDouble(lat_),Double.parseDouble(long_));
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng_, 16);
        mMap.animateCamera(cameraUpdate);
        hideLoading();

        MarkerOptions markerOptions = new MarkerOptions();
        LatLng latLng = new LatLng(Double.valueOf(lat_), Double.valueOf(long_));

        markerOptions.title(nama).infoWindowAnchor(0.5f, 0f);
        markerOptions.position(latLng);
        mMap.addMarker(markerOptions);
    }

    enum BackcheckingState {
        CHECKIN(R.drawable.bg_rectangle_button_checkin_backchecking,"CHECKIN"),
        NEXT(R.drawable.bg_rectangle_button_next_bc,"NEXT"),
        SUDAH_DI_BACKCHECKING(R.drawable.bg_rectangle_button_sudah_dibackchecking,"SUDAH DI BACKCHECKING"),
        CHECKOUT(R.drawable.bg_rectangle_button_checkout_bc,"CHECKOUT");

        private final int mType;
        private final String mText;

        BackcheckingState(int type,String text) {
            mType = type;
            mText = text;
        }

        public int getType() {
            return mType;
        }

        public String getText() {
            return mText;
        }
    }

    private boolean CheckGooglePlayServices() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(this);
        if(result != ConnectionResult.SUCCESS) {
            if(googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(this, result,
                        0).show();
            }
            return false;
        }
        return true;
    }

}
