package com.androiddeveloper.pens.namiapp.ui.detailSooBc;

import com.androiddeveloper.pens.namiapp.ui.base.BaseActivity;
import com.androiddeveloper.pens.namiapp.ui.base.MvpPresenter;

/**
 * Created by kmdr7 on 13/01/19
 * <p>
 * Author Andika Ahmad Ramadhan
 * Email Andikahmadr@gmail.com
 * Github https://github.com/kmdrn7
 **/
public interface DetailBackCheckMvpPresenter<V extends DetailBackCheckMvpView> extends MvpPresenter<V> {

    void insertUserCheckin(BaseActivity detailBackCheckingLocationActivity,
                           String latitude,
                           String longitude);

    void insertUserCheckout(BaseActivity detailBackCheckingLocationActivity,
                           String latitude,
                           String longitude);

    void updateCheckoutStatus(String latitude, String longitude);

    void getBackcheckingStatus(String locationId, String locationType, String latitude, String longitude);

    String getLoggedUser();

}
