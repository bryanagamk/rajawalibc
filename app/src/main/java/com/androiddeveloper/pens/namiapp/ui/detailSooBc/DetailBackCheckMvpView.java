package com.androiddeveloper.pens.namiapp.ui.detailSooBc;

import com.androiddeveloper.pens.namiapp.ui.base.MvpView;

/**
 * Created by kmdr7 on 13/01/19
 * <p>
 * Author Andika Ahmad Ramadhan
 * Email Andikahmadr@gmail.com
 * Github https://github.com/kmdrn7
 **/
public interface DetailBackCheckMvpView  extends MvpView {

    void getValueFromBundle();

    void setFieldValue();

    void setBackcheckingStatus(DetailBackCheckActivity.BackcheckingState state);

}
