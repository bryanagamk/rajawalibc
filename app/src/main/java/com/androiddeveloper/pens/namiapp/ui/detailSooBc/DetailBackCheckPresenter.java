package com.androiddeveloper.pens.namiapp.ui.detailSooBc;

import android.util.Log;

import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.ui.base.BaseActivity;
import com.androiddeveloper.pens.namiapp.ui.base.BasePresenter;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by kmdr7 on 13/01/19
 * <p>
 * Author Andika Ahmad Ramadhan
 * Email Andikahmadr@gmail.com
 * Github https://github.com/kmdrn7
 **/
public class DetailBackCheckPresenter<V extends DetailBackCheckMvpView> extends BasePresenter<V> implements DetailBackCheckMvpPresenter<V> {

    @Inject
    public DetailBackCheckPresenter(DataManager dataManager, CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);
    }

    @Override
    public void insertUserCheckin(BaseActivity detailBackCheckingLocationActivity, String latitude, String longitude) {

    }

    @Override
    public void insertUserCheckout(BaseActivity detailBackCheckingLocationActivity, String latitude, String longitude) {

    }

    @Override
    public void updateCheckoutStatus(String latitude, String longitude) {
        getMvpView().showLoading("Getting Checkout Status");
        String userId = getDataManager().getCurrentUserId();
        getCompositeDisposable().add(getDataManager().getCheckoutStatus(userId, latitude, longitude)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(backcheckingStatusResponse -> {
                    if (!isViewAttached()){
                        return;
                    }
                    if (backcheckingStatusResponse.isSuccess()){
                    }else{
                        Log.d("Error : ",backcheckingStatusResponse.toString());
//                        getMvpView().validateCheckinPlaceId(backcheckingStatusResponse.getBackcheckingMain().getLocationId());
                        if (backcheckingStatusResponse.getBackcheckingMain() != null ){
//                            getMvpView().setFieldName(backcheckingStatusResponse.getBackcheckingMain());
                        }
                    }
                    getMvpView().hideLoading();

                },throwable -> {
                    if (!isViewAttached()){
                        return;
                    }
                    getMvpView().showMessage("Gagal mendapatkan checkout status, silahkan coba beberapa saat lagi");
                    Log.d("Throwable : ",throwable.toString());
                    getMvpView().hideLoading();
                }));
    }

    @Override
    public void getBackcheckingStatus(String locationId, String locationType, String latitude, String longitude) {
        String userId = getDataManager().getCurrentUserId();
        getCompositeDisposable().add(getDataManager().getSooBackcheckingStatus(userId, locationId, locationType)
        .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(basicResponse -> {
                    if (!isViewAttached()){
                        return;
                    }
                    if (basicResponse.getSuccess()){
                        if (basicResponse.getMessage().toUpperCase().equals("CHECKIN")) {
                            getMvpView().setBackcheckingStatus(DetailBackCheckActivity.BackcheckingState.CHECKIN);
                            updateCheckoutStatus(latitude, longitude);
                        }else {
                            getMvpView().setBackcheckingStatus(DetailBackCheckActivity.BackcheckingState.SUDAH_DI_BACKCHECKING);
                            updateCheckoutStatus(latitude, longitude);
                        }
                    }
                },throwable -> {
                    if (!isViewAttached()){
                        return;
                    }
                    Log.d("Tag", "Userid : " + userId + "LocationId : " + locationId + "Location Type: " + locationType);
                    Log.d("Throwable",throwable.toString());
                    getBackcheckingStatus(locationId,locationType, latitude, longitude);
                    getMvpView().showMessage("Gagal mendapatkan backchecking");
                }));
    }

    @Override
    public String getLoggedUser() {
        return getDataManager().getCurrentUserName();
    }
}
