package com.androiddeveloper.pens.namiapp.ui.detailbackcheckinglocation;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.data.network.model.BackCheckingLocationListResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.Backchecking;
import com.androiddeveloper.pens.namiapp.ui.backcheckinglocation.BackCheckingLocationActivity;
import com.androiddeveloper.pens.namiapp.ui.backcheckingossosk.BackCheckingOssOskActivity;
import com.androiddeveloper.pens.namiapp.ui.backcheckingsekolahkampus.BackCheckingSekolahKampusActivity;
import com.androiddeveloper.pens.namiapp.ui.base.BaseActivity;
import com.androiddeveloper.pens.namiapp.utils.LocationUtils;
import com.androiddeveloper.pens.namiapp.utils.LocationValidatorUtil;
import com.androiddeveloper.pens.namiapp.utils.SingleShotLocationProvider;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Single;

public class DetailBackCheckingLocationActivity extends BaseActivity implements DetailBackCheckingLocationMvpView,
        OnMapReadyCallback
{

    @Inject
    DetailBackCheckingLocationMvpPresenter<DetailBackCheckingLocationMvpView> mPresenter;

    @BindView(R.id.btn_go_detail_backchecking_location)
    Button btnGoBackchecking;

    @BindView(R.id.nested_backcheck)
    View bottomSheet;

    @BindView(R.id.tv_nama_poi_outlet_detail_backchecking_location)
    TextView tvNamaPoi;

    @BindView(R.id.tv_nama_aoc_detail_backchecking_location)
    TextView tvNamaAoc;

    @BindView(R.id.tv_latitude_poi_outlet_detail_backchecking_location)
    TextView tvLatitudePoi;

    @BindView(R.id.tv_longitude_poi_outlet_detail_backchecking_location)
    TextView tvLongitudePoi;

    @BindView(R.id.tv_alamat_poi_outlet_detail_backchecking_location)
    TextView tvAlamatPoi;

    @BindView(R.id.tv_branch_poi_outlet_detail_backchecking_location)
    TextView tvBranchPoi;

    @BindView(R.id.tv_cluster_poi_outlet_detail_backchecking_location)
    TextView tvClusterPoi;

    @BindView(R.id.tv_city_poi_outlet_detail_backchecking_location)
    TextView tvCityPoi;

    @BindView(R.id.tv_regional_poi_outlet_detail_backchecking_location)
    TextView tvRegionalPoi;

    @BindView(R.id.tv_jarak_poi_outlet_detail_backchecking_location)
    TextView tvJarakPoi;

    @BindView(R.id.iv_arrow_detail_bc)
    ImageView ivArrowDetailBc;

    // cv id share
    @BindView(R.id.cv_detail_backchecking_bs_share)
    CardView detailBsShare;
    @BindView(R.id.cv_detail_backchecking_ds_share)
    CardView detailDsShare;
    @BindView(R.id.cv_detail_backchecking_rs_share)
    CardView detailRsShare;
    @BindView(R.id.cv_detail_backchecking_ms_share)
    CardView detailMsShare;
    @BindView(R.id.line_total_responden_detail_bc)
    LinearLayout lineTotalRespondenPoi;

    ProgressDialog progressDialog;

    private String mLocationId = "";
    private String mLocationType;

    private Menu menu;

    private BackCheckingLocationListResponse backCheckingDetail;

    private BackcheckingState mState;
    private String mLocationName;

    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    LocationRequest mLocationRequest;
    int PROXIMITY_RADIUS = 10000;
    double latitude, longitude;
    double end_latitude, end_longitude;
    private GoogleMap mMap;


    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, DetailBackCheckingLocationActivity.class);
        return intent;
    }
    private BottomSheetBehavior mBottomSheetBehavior1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_back_checking_location);

        getActivityComponent().inject(this);

        if (isNetworkAvailable()) {
            setUnBinder(ButterKnife.bind(this));

            mPresenter.onAttach(this);

            setUp();
        }
    }

    @Override
    protected void setUp() {
        mapSetup();

        progressDialog = new ProgressDialog(this);
        // backpress
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Detail Backchecking");



        mBottomSheetBehavior1 = BottomSheetBehavior.from(bottomSheet);
        mBottomSheetBehavior1.setState(BottomSheetBehavior.STATE_COLLAPSED);
        mBottomSheetBehavior1.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {

                }

                if (newState == BottomSheetBehavior.STATE_EXPANDED) {

                }

                if (newState == BottomSheetBehavior.STATE_DRAGGING) {

                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                ivArrowDetailBc.setRotation(slideOffset * 180);
            }
        });




    }

    private void mapSetup() {
        if (!CheckGooglePlayServices()) {
            showMessage("Google play service unavailable");
            return;
        }
        else {
            Log.d("onCreate","Google Play Services available.");
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


    }

    private void getBackcheckingIntent() {

        if (getIntent().getSerializableExtra("detail") != null ){
            backCheckingDetail = (BackCheckingLocationListResponse) getIntent().getSerializableExtra("detail");
            setFieldName(backCheckingDetail);
            getBackcheckingDataFromServer(backCheckingDetail);
            mPresenter.getBackcheckingStatus(backCheckingDetail.getLocationId(),backCheckingDetail.getLocationType()
                    , backCheckingDetail.getLatitude(), backCheckingDetail.getLongitude());

            mLocationId = backCheckingDetail.getLocationId();
            mLocationType = backCheckingDetail.getLocationType();
            mLocationName = backCheckingDetail.getInstitutionName();
        }
    }

    private void setBtnGoBackcheckingClickListener(BackcheckingState state, BackCheckingLocationListResponse backcheckingResponse) {
        switch (state){
            case CHECKIN:
                btnGoBackchecking.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try{
                            if (getIntent() != null){
                                double latitude = getIntent().getDoubleExtra("latitude",-1L);
                                double longitude = getIntent().getDoubleExtra("longitude",-1L);
                                mPresenter.insertUserCheckin(backcheckingResponse,DetailBackCheckingLocationActivity.this,
                                        String.valueOf(latitude),
                                        String.valueOf(longitude));
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
                break;
            case NEXT:
                btnGoBackchecking.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try{
                            Intent i = new Intent(getApplicationContext(), backcheckingResponse.getLocationType().equals("INSTITUTION") ?
                                    BackCheckingSekolahKampusActivity.class : BackCheckingOssOskActivity.class);
                            i.putExtra("location_id",backcheckingResponse.getLocationId());
                            if (backcheckingResponse.getUserId() == null ){
                                Log.d("DEBUG : ", backCheckingDetail.toString());
                                Log.d("DEBUG : ", backCheckingDetail.getUserId());
                                i.putExtra("backchecking",backCheckingDetail);
                            }else{
                                i.putExtra("backchecking",backcheckingResponse);
                            }
                            startActivity(i);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
                break;
            case CHECKOUT:
                btnGoBackchecking.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try{
                            LocationUtils.requestLocationUpdateWithCheckPermission(DetailBackCheckingLocationActivity.this, new LocationUtils.IUserLocationListener() {
                                @Override
                                public void onStateChange(State state) {

                                }

                                @Override
                                public void onLocationChanged(Location location) {
                                    if (location != null){
                                        mPresenter.insertUserCheckout(DetailBackCheckingLocationActivity.this,
                                                String.valueOf(location.getLatitude()),
                                                String.valueOf(location.getLongitude()));
                                    }else{
                                        showMessage("Gagal mendapatkan lokasi dari GPS");
                                    }
                                    LocationUtils.removeLocationUpdate(this);
                                }

                                @Override
                                public void onStatusChanged(String s, int i, Bundle bundle) {

                                }

                                @Override
                                public void onProviderEnabled(String s) {

                                }

                                @Override
                                public void onProviderDisabled(String s) {

                                }
                            });
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
                break;
        }
    }

    public void onClick(View v) {
        try{
            if (v != null) {
                v.performClick();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        openGoBackcheckingActivity();
        super.onBackPressed();
    }

    @Override
    public void setFieldName(BackCheckingLocationListResponse response) {
        tvNamaAoc.setText(response.getUserName());
        tvNamaPoi.setText(response.getInstitutionName());
        tvLatitudePoi.setText(response.getLatitude());
        tvLongitudePoi.setText(response.getLongitude());
        tvAlamatPoi.setText(response.getAddress());
        tvBranchPoi.setText(response.getBranch());
        tvClusterPoi.setText(response.getCluster());
        tvCityPoi.setText(response.getCity());
        tvRegionalPoi.setText(response.getRegional());
        if (response.getDistance() != null ){
            tvJarakPoi.setText(response.getDistance() + " Meter");
        }else{
            tvJarakPoi.setText("");
        }
        mLocationId = response.getLocationId();
        backCheckingDetail = response;
    }

    @Override
    public void setBackcheckingStatus(BackcheckingState state) {
        btnGoBackchecking.setBackgroundResource(state.getType());
        btnGoBackchecking.setText(state.getText());
        mState = state;
        BackCheckingLocationListResponse response = backCheckingDetail;
        setBtnGoBackcheckingClickListener(state,response);
    }

    @Override
    public void openBackcheckingActivity() {
        Intent i = new Intent(getApplicationContext(), mLocationType.equals("INSTITUTION") ?
                BackCheckingSekolahKampusActivity.class : BackCheckingOssOskActivity.class);
        i.putExtra("location_id",mLocationId);
        i.putExtra("backchecking",backCheckingDetail);

        startActivity(i);
    }

    @Override
    public void validateCheckinPlaceId(String placeId) {
        String id = backCheckingDetail.getLocationId();

        if (placeId.equals(id) && mState.equals(BackcheckingState.CHECKIN)){
            setBackcheckingStatus(BackcheckingState.NEXT);
        }else if ((mState.equals(BackcheckingState.CHECKIN))){
            setBackcheckingStatus(BackcheckingState.CHECKIN);
        }else if ((placeId.equals(id) && mState.equals(BackcheckingState.SUDAH_DI_BACKCHECKING))){
            setBackcheckingStatus(BackcheckingState.CHECKOUT);
        }else {
            setBackcheckingStatus(BackcheckingState.SUDAH_DI_BACKCHECKING);
        }
    }

    @Override
    public void openGoBackcheckingActivity() {
        Intent intent = BackCheckingLocationActivity.getStartIntent(this);
        intent.addFlags(intent.FLAG_ACTIVITY_CLEAR_TOP | intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void setupMarketShare(Backchecking backchecking) {
        TextView tvMsTelkomsel = findViewById(R.id.tv_ms_tsel_detail_backchecking_location);
        TextView tvMsOoredoo = findViewById(R.id.tv_ms_ooredoo_detail_backchecking_location);
        TextView tvMsXl = findViewById(R.id.tv_ms_xl_detail_backchecking_location);
        TextView tvMsAxis = findViewById(R.id.tv_ms_axis_detail_backchecking_location);
        TextView tvMs3 = findViewById(R.id.tv_ms_3_detail_backchecking_location);
        TextView tvMsOthers = findViewById(R.id.tv_ms_others_detail_backchecking_location);

        TextView tvPoiTotalResponden = findViewById(R.id.tv_total_responden_poi_osk_ptd_detail_backchecking_location);
        //Ms Telkomsel
        if (backchecking.getMsTelkomsel() != null){
            tvMsTelkomsel.setText(backchecking.getMsTelkomsel() + "%");
        }else {
            tvMsTelkomsel.setText(0.0 + "%");
        }

        //Ms Ooredoo
        if (backchecking.getMsOoredoo() != null){
            tvMsOoredoo.setText(backchecking.getMsOoredoo() + "%");
        }else {
            tvMsOoredoo.setText(0.0 + "%");
        }

        //Ms Xl
        if (backchecking.getMsXl() != null){
            tvMsXl.setText(backchecking.getMsXl() + "%");
        }else {
            tvMsXl.setText(0.0 + "%");
        }

        //Ms Axis
        if (backchecking.getMsAxis() != null){
            tvMsAxis.setText(backchecking.getMsAxis() + "%");
        }else {
            tvMsAxis.setText(0.0 + "%");
        }

        //Ms 3
        if (backchecking.getMs3() != null){
            tvMs3.setText(backchecking.getMs3() + "%");
        }else {
            tvMs3.setText(0.0 + "%");
        }

        //Ms Others
        if (backchecking.getMsOthers() != null){
            tvMsOthers.setText(backchecking.getMsOthers() + "%");
        }else {
            tvMsOthers.setText(0.0 + "%");
        }

        // total responden ms
        if (backchecking.getTotalRespondent() != null){
            tvPoiTotalResponden.setText(backchecking.getTotalRespondent());
        }else {
            tvPoiTotalResponden.setText("-");
        }
    }

    @Override
    public void setupBsDsRs(Backchecking backchecking) {
        TextView tvBsTelkomsel = findViewById(R.id.tv_bs_tsel_detail_backchecking_location);
        TextView tvBsOoredoo = findViewById(R.id.tv_bs_ooredoo_detail_backchecking_location);
        TextView tvBsXl = findViewById(R.id.tv_bs_xl_detail_backchecking_location);
        TextView tvBsAxis = findViewById(R.id.tv_bs_axis_detail_backchecking_location);
        TextView tvBs3 = findViewById(R.id.tv_bs_3_detail_backchecking_location);
        TextView tvBsOthers = findViewById(R.id.tv_bs_others_detail_backchecking_location);

        TextView tvDsTelkomsel = findViewById(R.id.tv_ds_tsel_detail_backchecking_location);
        TextView tvDsOoredoo = findViewById(R.id.tv_ds_ooredoo_detail_backchecking_location);
        TextView tvDsXl = findViewById(R.id.tv_ds_xl_detail_backchecking_location);
        TextView tvDsAxis = findViewById(R.id.tv_ds_axis_detail_backchecking_location);
        TextView tvDs3 = findViewById(R.id.tv_ds_3_detail_backchecking_location);
        TextView tvDsOthers = findViewById(R.id.tv_ds_others_detail_backchecking_location);

        TextView tvRsTelkomsel = findViewById(R.id.tv_rs_tsel_detail_backchecking_location);
        TextView tvRsOoredoo = findViewById(R.id.tv_rs_ooredoo_detail_backchecking_location);
        TextView tvRsXl = findViewById(R.id.tv_rs_xl_detail_backchecking_location);
        TextView tvRsAxis = findViewById(R.id.tv_rs_axis_detail_backchecking_location);
        TextView tvRs3 = findViewById(R.id.tv_rs_3_detail_backchecking_location);
        TextView tvRsOthers = findViewById(R.id.tv_rs_others_detail_backchecking_location);


        tvBsTelkomsel.setText(backchecking.getBsTelkomsel() + "%");
        tvBsOoredoo.setText(backchecking.getBsOoredoo() + "%");
        tvBsXl.setText(backchecking.getBsXl() + "%");
        tvBsAxis.setText(backchecking.getBsAxis() + "%");
        tvBs3.setText(backchecking.getBs3() + "%");
        tvBsOthers.setText(backchecking.getBsOthers() + "%");

        tvDsTelkomsel.setText(backchecking.getDsTelkomsel() + "%");
        tvDsOoredoo.setText(backchecking.getDsOoredoo() + "%");
        tvDsXl.setText(backchecking.getDsXl() + "%");
        tvDsAxis.setText(backchecking.getDsAxis() + "%");
        tvDs3.setText(backchecking.getDs3() + "%");
        tvDsOthers.setText(backchecking.getDsOthers() + "%");

        tvRsTelkomsel.setText(backchecking.getRsTelkomsel() + "%");
        tvRsOoredoo.setText(backchecking.getRsOoredoo() + "%");
        tvRsXl.setText(backchecking.getRsXl() + "%");
        tvRsAxis.setText(backchecking.getRsAxis() + "%");
        tvRs3.setText(backchecking.getRs3() + "%");
        tvRsOthers.setText(backchecking.getRsOthers() + "%");
    }

    @Override
    public void setupSalesThrough(Backchecking backchecking) {
        TextView tvPoiOskSalesMtd = findViewById(R.id.tv_sales_poi_osk_mtd_detail_backchecking_location);
        TextView tvPoiOskSalesPtd = findViewById(R.id.tv_sales_poi_osk_ptd_detail_backchecking_location);


        //poi osk
        if (backchecking.getSalesMtd() != null){
            tvPoiOskSalesMtd.setText(backchecking.getSalesMtd());
        }else {
            tvPoiOskSalesMtd.setText("-");
        }

        if (backchecking.getSalesPtd() != null){
            tvPoiOskSalesPtd.setText(backchecking.getSalesPtd());
        }else {
            tvPoiOskSalesPtd.setText("-");
        }


    }

    private void getBackcheckingDataFromServer(BackCheckingLocationListResponse listResponse){
        mLocationId = listResponse.getLocationId();
        mLocationType = listResponse.getLocationType();

        if (mLocationType.equals("INSTITUTION")){
            detailBsShare.setVisibility(View.GONE);
            detailDsShare.setVisibility(View.GONE);
            detailRsShare.setVisibility(View.GONE);
            mPresenter.getInstitutionSalesThrough(mLocationId);
            mPresenter.getInstitutionMarketShare(mLocationId);
        }else {
            detailMsShare.setVisibility(View.GONE);
            lineTotalRespondenPoi.setVisibility(View.GONE);
            mPresenter.getOssOskSalesThrough(mLocationId);
            mPresenter.getBsDsRs(mLocationId);
        }
    }

    @Override
    public void showLoading(String message) {
        super.showLoading(message);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        if (progressDialog != null ){
            progressDialog.dismiss();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        try{
            if (mState != null){
                setBackcheckingStatus(mState);
            }
            getBackcheckingIntent();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                mMap.setMyLocationEnabled(true);
            }
        } else {
            mMap.setMyLocationEnabled(true);
        }


        showLoading("Mendapatkan lokasi saat ini");
        SingleShotLocationProvider.requestSingleUpdateWithPermission(this,location -> {
            if (location != null){
                LatLng latLng = new LatLng(location.latitude,location.longitude);
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 14);
                mMap.animateCamera(cameraUpdate);
                hideLoading();
            }else{
                showMessage("Gagal mendapatkan lokasi");
            }
            hideLoading();
        });

        MarkerOptions markerOptions = new MarkerOptions();
        LatLng latLng = new LatLng(Double.valueOf(backCheckingDetail.getLatitude()), Double.valueOf(backCheckingDetail.getLongitude()));


        markerOptions.title(backCheckingDetail.getInstitutionName()).infoWindowAnchor(0.5f,0f);
        markerOptions.position(latLng);
        mMap.addMarker(markerOptions);

    }

    enum BackcheckingState {
        CHECKIN(R.drawable.bg_rectangle_button_checkin_backchecking,"CHECKIN"),
        NEXT(R.drawable.bg_rectangle_button_next_bc,"NEXT"),
        SUDAH_DI_BACKCHECKING(R.drawable.bg_rectangle_button_sudah_dibackchecking,"SUDAH DI BACKCHECKING"),
        CHECKOUT(R.drawable.bg_rectangle_button_checkout_bc,"CHECKOUT");

        private final int mType;
        private final String mText;

        BackcheckingState(int type,String text) {
            mType = type;
            mText = text;
        }

        public int getType() {
            return mType;
        }

        public String getText() {
            return mText;
        }
    }

    private boolean CheckGooglePlayServices() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(this);
        if(result != ConnectionResult.SUCCESS) {
            if(googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(this, result,
                        0).show();
            }
            return false;
        }
        return true;
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }
}
