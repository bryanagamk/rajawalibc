package com.androiddeveloper.pens.namiapp.ui.detailbackcheckinglocation;

import com.androiddeveloper.pens.namiapp.data.network.model.BackCheckingLocationListResponse;
import com.androiddeveloper.pens.namiapp.di.PerActivity;
import com.androiddeveloper.pens.namiapp.ui.base.BaseActivity;
import com.androiddeveloper.pens.namiapp.ui.base.MvpPresenter;
import com.google.android.gms.location.FusedLocationProviderClient;

@PerActivity
public interface DetailBackCheckingLocationMvpPresenter<V extends DetailBackCheckingLocationMvpView> extends MvpPresenter<V> {

    void updateCheckoutStatus(String latitude, String longitude);

    void insertUserCheckin(BackCheckingLocationListResponse response, BaseActivity detailBackCheckingLocationActivity,
                           String latitude,
                           String longitude);

    void insertUserCheckout(BaseActivity detailBackCheckingLocationActivity,
                            String latitude,
                            String longitude);

    void getBackcheckingStatus(String locationId, String locationType, String latitude, String longitude);

    void getInstitutionMarketShare(String locationId);

    void getBsDsRs(String locationId);

    void getInstitutionSalesThrough(String locationId);

    void getOssOskSalesThrough(String locationId);
}
