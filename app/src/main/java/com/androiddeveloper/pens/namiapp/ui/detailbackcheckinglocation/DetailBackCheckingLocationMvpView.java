package com.androiddeveloper.pens.namiapp.ui.detailbackcheckinglocation;

import com.androiddeveloper.pens.namiapp.data.network.model.BackCheckingLocationListResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.Backchecking;
import com.androiddeveloper.pens.namiapp.ui.base.MvpView;

public interface DetailBackCheckingLocationMvpView extends MvpView {
    void setFieldName(BackCheckingLocationListResponse response);

    void setBackcheckingStatus(DetailBackCheckingLocationActivity.BackcheckingState state);

    void openBackcheckingActivity();

    void validateCheckinPlaceId(String placeId);

    void openGoBackcheckingActivity();

    void setupMarketShare(Backchecking backchecking);

    void setupBsDsRs(Backchecking backchecking);

    void setupSalesThrough(Backchecking backchecking);
}
