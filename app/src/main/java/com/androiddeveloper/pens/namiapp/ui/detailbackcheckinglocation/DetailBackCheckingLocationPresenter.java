package com.androiddeveloper.pens.namiapp.ui.detailbackcheckinglocation;

import android.location.Location;
import android.text.method.TextKeyListener;
import android.util.Log;
import android.widget.Toast;

import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.data.network.model.BackCheckingLocationListResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.BackcheckingMainResponse;
import com.androiddeveloper.pens.namiapp.ui.backcheckinglocationlist.BackCheckingLocationListActivity;
import com.androiddeveloper.pens.namiapp.ui.base.BaseActivity;
import com.androiddeveloper.pens.namiapp.ui.base.BasePresenter;
import com.androiddeveloper.pens.namiapp.utils.LocationUtils;
import com.androiddeveloper.pens.namiapp.utils.LocationValidatorUtil;
import com.androiddeveloper.pens.namiapp.utils.StartupConfig;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class DetailBackCheckingLocationPresenter<V extends DetailBackCheckingLocationMvpView> extends BasePresenter<V>
        implements DetailBackCheckingLocationMvpPresenter<V> {

    private static final String TAG = "DetailBackCheckingLocationPresenter";

    @Inject
    public DetailBackCheckingLocationPresenter(DataManager dataManager,
                                   CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);
    }

    @Override
    public void updateCheckoutStatus(String latitude, String longitude) {
        getMvpView().showLoading("Getting Checkout Status");
        String userId = getDataManager().getCurrentUserId();
        getCompositeDisposable().add(getDataManager().getCheckoutStatus(userId, latitude, longitude)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(backcheckingStatusResponse -> {
            if (!isViewAttached()){
                return;
            }
            if (backcheckingStatusResponse.isSuccess()){
            }else{
                Log.d("Error : ",backcheckingStatusResponse.toString());
                getMvpView().validateCheckinPlaceId(backcheckingStatusResponse.getBackcheckingMain().getLocationId());
                if (backcheckingStatusResponse.getBackcheckingMain() != null ){
                    getMvpView().setFieldName(backcheckingStatusResponse.getBackcheckingMain());
                }
            }
            getMvpView().hideLoading();

        },throwable -> {
            if (!isViewAttached()){
                return;
            }
            getMvpView().showMessage("Gagal mendapatkan checkout status, silahkan coba beberapa saat lagi");
            Log.d("Throwable : ",throwable.toString());
            getMvpView().hideLoading();
        }));
    }

    @Override
    public void insertUserCheckin(BackCheckingLocationListResponse response, BaseActivity detailBackCheckingLocationActivity,
                                  String latitude, String longitude) {
        if (StartupConfig.getInstance(getDataManager(),false) == null){
            getMvpView().showMessage("Gagal terhubung silahkan coba beberapa saat lagi");
            getMvpView().hideLoading();
            return;
        }

        int radius;

        if (LocationValidatorUtil.isMockSettingsOn(detailBackCheckingLocationActivity)){
            getMvpView().showMessage("Maaf mock location terdeteksi");
            getMvpView().hideLoading();
            return;
        }
        if (response.getLocationType().equals("INSTITUTION")){
            radius = Integer.valueOf(StartupConfig.getInstance(getDataManager(),false).getCheckinInstitutionRadius());
        }else {
            radius = Integer.valueOf(StartupConfig.getInstance(getDataManager(),false).getCheckinOssOskRadius());
        }

        String userType = getDataManager().getCurrentUserType().toLowerCase();
        if (userType.equals("mgt1")){
            radius = 100000000;
        }

        if (Double.valueOf(response.getDistance()) > radius){
            getMvpView().showMessage("Maaf tapi anda berada di luar radius " + radius + " Meter");
            getMvpView().hideLoading();
            return;
        }

        if (getMvpView() == null){
            return;
        }
        getMvpView().showLoading("Checking In");
        String userId = getDataManager().getCurrentUserId();
        String userName = getDataManager().getCurrentUserName();
        String Distance = response.getDistance();

        Map<String,String> params = new HashMap<>();
        params.put("location_id",response.getLocationId());
        params.put("location_name",response.getInstitutionName());
        params.put("userid",userId);
        params.put("username",userName);
        params.put("distance", Distance);
        params.put("latitude",String.valueOf(latitude));
        params.put("longitude",String.valueOf(longitude));
        params.put("location_type",response.getLocationType());

        getCompositeDisposable().add(getDataManager().insertCheckinStatus(params)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(basicResponse -> {
            if (!isViewAttached()){
                return;
            }
            if (basicResponse.getSuccess()){
                getMvpView().openBackcheckingActivity();
                getMvpView().setBackcheckingStatus(DetailBackCheckingLocationActivity.BackcheckingState.NEXT);
                getMvpView().showMessage("Checkin Berhasil");
                getMvpView().hideLoading();

            }else{
                getMvpView().showMessage(basicResponse.getMessage());
                getMvpView().hideLoading();
            }
        },throwable -> {
            if (!isViewAttached()){
                return;
            }
            Toast.makeText(detailBackCheckingLocationActivity, "Terjadi kesalahan saat checkin", Toast.LENGTH_SHORT).show();
            Log.d("Throwable : ",throwable.toString());
            getMvpView().hideLoading();
        }));
    }

    @Override
    public void insertUserCheckout(BaseActivity detailBackCheckingLocationActivity, String latitude, String longitude) {
        String userId = getDataManager().getCurrentUserId();
        getCompositeDisposable().add(getDataManager().insertCheckoutStatus(userId,latitude,longitude)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(basicResponse -> {
                    if (!isViewAttached()){
                        return;
                    }
                    getMvpView().showMessage(basicResponse.getMessage());
                    getMvpView().finish();
                },throwable -> {
                    if (!isViewAttached()){
                        return;
                    }
                    getMvpView().showMessage("Gagal Checkout, Tidak Tersambung");
                    insertUserCheckout(detailBackCheckingLocationActivity,latitude,longitude);
                }));
    }

    @Override
    public void getBackcheckingStatus(String locationId, String locationType, String latitude, String longitude) {
        String userId = getDataManager().getCurrentUserId();
        getCompositeDisposable().add(getDataManager().getBackcheckingStatus(userId, locationId,locationType)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(basicResponse -> {
            if (!isViewAttached()){
                return;
            }
            if (basicResponse.getSuccess()){
                if (basicResponse.getMessage().toUpperCase().equals("CHECKIN")) {
                    getMvpView().setBackcheckingStatus(DetailBackCheckingLocationActivity.BackcheckingState.CHECKIN);
                    updateCheckoutStatus(latitude, longitude);
                }else {
                    getMvpView().setBackcheckingStatus(DetailBackCheckingLocationActivity.BackcheckingState.SUDAH_DI_BACKCHECKING);
                    updateCheckoutStatus(latitude, longitude);
                }
            }
        },throwable -> {
            if (!isViewAttached()){
                return;
            }
            Log.d("Tag", "Userid : " + userId + "LocationId : " + locationId + "Location Type: " + locationType);
            Log.d("Throwable",throwable.toString());
            getBackcheckingStatus(locationId,locationType, latitude, longitude);
            getMvpView().showMessage("Gagal mendapatkan backchecking");
        }));
    }

    @Override
    public void getInstitutionMarketShare(String locationId) {
        getCompositeDisposable().add(getDataManager().getBackcheckingInstitutionMarketShare(locationId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(backcheckingResponse -> {
                    if (!isViewAttached()){
                        return;
                    }
                    if (backcheckingResponse.getSuccess()){
                        Log.v("Debug ", backcheckingResponse.getMessage() );
                        getMvpView().setupMarketShare(backcheckingResponse.getBackchecking());
                    }else{
                        getMvpView().showMessage(backcheckingResponse.getMessage());
                    }
                },throwable -> {
                    if (!isViewAttached()){
                        return;
                    }
                    getInstitutionMarketShare(locationId);
                    getMvpView().showMessage("Gagal mendapatkan data MarketShare dari server, Silahkan coba lagi");
                }));
    }

    @Override
    public void getBsDsRs(String locationId) {
        getCompositeDisposable().add(getDataManager().getBackcheckingOssOskBsDsRs(locationId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(backcheckingResponse -> {
                    if (!isViewAttached()){
                        return;
                    }
                    if (backcheckingResponse.getSuccess()){
                        getMvpView().setupBsDsRs(backcheckingResponse.getBackchecking());

                    }else{
                        getMvpView().showMessage(backcheckingResponse.getMessage());
                    }
                },throwable -> {
                    if (!isViewAttached()){
                        return;
                    }
                    String message = "Gagal mendapatkan data bsdsrs dari server";
                    getBsDsRs(locationId);
                    getMvpView().showMessage(message);
                    Log.d(this.getClass().getSimpleName(), throwable.toString());
                }));
    }

    @Override
    public void getInstitutionSalesThrough(String locationId) {
        getCompositeDisposable().add(getDataManager().getBackcheckingInstitutionSalesThrough(locationId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(backcheckingResponse -> {
                    if (!isViewAttached()){
                        return;
                    }
                    if (backcheckingResponse.getSuccess()){
                        getMvpView().setupSalesThrough(backcheckingResponse.getBackchecking());
                    }else{
                        getMvpView().showMessage(backcheckingResponse.getMessage());
                    }
                },throwable -> {
                    if (!isViewAttached()){
                        return;
                    }
                    getInstitutionSalesThrough(locationId);
                    getMvpView().showMessage("Gagal mendapatkan data SalesThrough dari server, Silahkan coba lagi");
                }));
    }

    @Override
    public void getOssOskSalesThrough(String locationId) {
        getCompositeDisposable().add(getDataManager().getBackcheckingOssOskSalesThrough(locationId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(backcheckingResponse -> {
                    if (!isViewAttached()){
                        return;
                    }
                    if (backcheckingResponse.getSuccess()){
                        getMvpView().setupSalesThrough(backcheckingResponse.getBackchecking());

                    }else{
                        getMvpView().showMessage(backcheckingResponse.getMessage());
                    }
                },throwable -> {
                    if (!isViewAttached()){
                        return;
                    }
                    String message = "Gagal mendapatkan data salesthrough dari server";
                    getOssOskSalesThrough(locationId);
                    getMvpView().showMessage(message);
                    Log.d(this.getClass().getSimpleName(), throwable.toString());
                }));
    }

}
