package com.androiddeveloper.pens.namiapp.ui.event;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.ui.backcheckingossosk.BackCheckingOssOskActivity;
import com.androiddeveloper.pens.namiapp.ui.base.BaseActivity;

import java.util.Calendar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EventActivity extends BaseActivity implements EventMvpView, View.OnClickListener {

    @Inject
    EventMvpPresenter<EventMvpView> mPresenter;

    @BindView(R.id.et_in_start_date)
    EditText etStartDate;

    @BindView(R.id.et_in_start_time)
    EditText etStartTime;

    @BindView(R.id.btn_start_date_event)
    Button btnStartDatePicker;

    @BindView(R.id.btn_start_time)
    Button btnStartTimePicker;

    @BindView(R.id.et_in_end_date)
    EditText etEndDate;

    @BindView(R.id.et_in_end_time)
    EditText etEndTime;

    @BindView(R.id.btn_end_date_event)
    Button btnEndDatePicker;

    @BindView(R.id.btn_end_time)
    Button btnEndTimePicker;

    @BindView(R.id.btn_send_event)
    Button btnSendEvent;

    @BindView(R.id.et_nama_event)
    EditText etNamaEvent;

    @BindView(R.id.spinner_tipe_aktivitas_event)
    Spinner spinnerTipeAktivitas;

    @BindView(R.id.spinner_tipe_lokasi_event)
    Spinner spinnerTipeLokasi;

    @BindView(R.id.et_target_sales_event)
    EditText etTargetSales;

    @BindView(R.id.et_target_tcash_event)
    EditText etTargetTcash;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, EventActivity.class);
        return intent;
    }
    private int mYear, mMonth, mDay, mHour, mMinute;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);
        getActivityComponent().inject(this);

        if (isNetworkConnected()) {
            setUnBinder(ButterKnife.bind(this));

            mPresenter.onAttach(this);

            setUp();
        }
    }

    @Override
    protected void setUp() {
        // backpress
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Event");
        // start date
        btnStartDatePicker.setOnClickListener(EventActivity.this);
        btnStartTimePicker.setOnClickListener(EventActivity.this);
        //  end date
        btnEndDatePicker.setOnClickListener(EventActivity.this);
        btnEndTimePicker.setOnClickListener(EventActivity.this);
        btnSendEvent.setOnClickListener(EventActivity.this);
        etTargetSales.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = s.toString();
                if(checkNullValues(text)) {
                    if(Integer.parseInt(text.trim()) < 1){
                        AlertDialog.Builder builder = new AlertDialog.Builder(EventActivity.this);
                        builder.setCancelable(false);
                        builder.setMessage("Isian target sales tidak boleh kurang dari 1");

                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                etTargetSales.setText("1");
                            }
                        });
                        builder.show();
                    }
                }
            }
        });
        etTargetTcash.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = s.toString();
                if(checkNullValues(text)) {
                    if(Integer.parseInt(text.trim()) <= 0){
                        AlertDialog.Builder builder = new AlertDialog.Builder(EventActivity.this);
                        builder.setCancelable(false);
                        builder.setMessage("Isian target t-cah tidak boleh kurang dari 0");

                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                etTargetTcash.setText("0");
                            }
                        });
                        builder.show();
                    }
                }
            }
        });
        // membuat arrayadapter dan default layout spinner
        ArrayAdapter<CharSequence> adaptertipeaktivitas = ArrayAdapter.createFromResource(EventActivity.this,
                R.array.event_tipe_aktivitas, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> adaptertipelokasi = ArrayAdapter.createFromResource(EventActivity.this,
                R.array.event_tipe_lokasi, android.R.layout.simple_spinner_item);
        // muncul pilihan brand
        adaptertipeaktivitas.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adaptertipelokasi.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // set adapter
        spinnerTipeAktivitas.setAdapter(adaptertipeaktivitas);
        spinnerTipeLokasi.setAdapter(adaptertipelokasi);
    }

    @Override
    public void onClick(View v) {
        if (v == btnStartDatePicker) {

            // Get Current Date
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            etStartDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }else if (v == btnEndDatePicker){
            // Get Current Date
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            etEndDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }
        if (v == btnStartTimePicker) {

            // Get Current Time
            final Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);

            // Launch Time Picker Dialog
            TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {

                            etStartTime.setText(hourOfDay + ":" + minute);
                        }
                    }, mHour, mMinute, false);
            timePickerDialog.show();
        }else if (v == btnEndTimePicker){
            // Get Current Time
            final Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);

            // Launch Time Picker Dialog
            TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {

                            etEndTime.setText(hourOfDay + ":" + minute);
                        }
                    }, mHour, mMinute, false);
            timePickerDialog.show();
        }
        if (v == btnSendEvent){
            EditText[] editText = { etNamaEvent, etTargetSales, etTargetTcash};
            for (EditText texts : editText){
                if (texts.getText().toString().trim().isEmpty()){
                    texts.setError("Belum diisi");
                }
            }
            if (etNamaEvent.getText().toString().isEmpty() ||
                    etTargetSales.getText().toString().isEmpty() ||
                    etTargetTcash.getText().toString().isEmpty()){
                Toast.makeText(getApplicationContext(),"Gagal mengirim data", Toast.LENGTH_SHORT).show();
            }else {
                Toast.makeText(getApplicationContext(),"Berhasil mengirim data", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public static boolean checkNullValues(String valueToCheck) {
        if(!(valueToCheck == null)) {
            String valueCheck = valueToCheck.trim();
            if(valueCheck.equals("") || valueCheck.equals("0")  ) {
                return false;
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
