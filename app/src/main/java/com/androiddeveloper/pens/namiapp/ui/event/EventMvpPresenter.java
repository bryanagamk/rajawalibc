package com.androiddeveloper.pens.namiapp.ui.event;

import com.androiddeveloper.pens.namiapp.di.PerActivity;
import com.androiddeveloper.pens.namiapp.ui.base.MvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.main.MainMvpView;

@PerActivity
public interface EventMvpPresenter<V extends EventMvpView> extends MvpPresenter<V> {
}
