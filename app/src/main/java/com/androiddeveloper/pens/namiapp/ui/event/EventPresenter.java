package com.androiddeveloper.pens.namiapp.ui.event;

import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.ui.base.BasePresenter;
import com.androiddeveloper.pens.namiapp.ui.main.MainMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.main.MainMvpView;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class EventPresenter<V extends EventMvpView> extends BasePresenter<V>
        implements EventMvpPresenter<V> {
    private static final String TAG = "EventAocPresenter";

    @Inject
    public EventPresenter(DataManager dataManager, CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);
    }


}
