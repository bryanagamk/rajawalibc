package com.androiddeveloper.pens.namiapp.ui.faq;

import com.androiddeveloper.pens.namiapp.di.PerActivity;
import com.androiddeveloper.pens.namiapp.ui.base.MvpPresenter;

@PerActivity
public interface FaqMvpPresenter<V extends FaqMvpView> extends MvpPresenter<V> {
}
