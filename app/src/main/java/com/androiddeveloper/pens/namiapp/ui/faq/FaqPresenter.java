package com.androiddeveloper.pens.namiapp.ui.faq;

import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.ui.base.BasePresenter;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class FaqPresenter<V extends FaqMvpView> extends BasePresenter<V>
        implements FaqMvpPresenter<V> {

    private static final String TAG = "FaqPresenter";

    @Inject
    public FaqPresenter(DataManager dataManager,
                        CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);
    }
}
