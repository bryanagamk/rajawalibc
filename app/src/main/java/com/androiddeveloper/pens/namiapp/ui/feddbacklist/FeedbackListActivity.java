package com.androiddeveloper.pens.namiapp.ui.feddbacklist;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.data.network.model.FeedbackLocationResponse;
import com.androiddeveloper.pens.namiapp.ui.base.BaseActivity;
import com.androiddeveloper.pens.namiapp.ui.feedbackossosk.FeedbackOssOskActivity;
import com.androiddeveloper.pens.namiapp.ui.feedbackossoskinsert.FeedbackOssOskInsertActivity;
import com.androiddeveloper.pens.namiapp.ui.feedbacksekolahkampus.FeedbackSekolahKampusActivity;
import com.androiddeveloper.pens.namiapp.ui.feedbacksekolahkampusinsert.FeedbackSekolahKampusInsertActivity;
import com.androiddeveloper.pens.namiapp.ui.summary.fragment.SummaryType;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FeedbackListActivity extends BaseActivity implements FeedbackListMvpView,
        FeedbackListAdapter.Callback{

    @Inject
    FeedbackListMvpPresenter<FeedbackListMvpView> mPresenter;

    @Inject
    FeedbackListAdapter mAdapter;

    @Inject
    LinearLayoutManager mLayoutManager;

    @BindView(R.id.rv_feedback_detail)
    RecyclerView rvFeedbackList;

    private String mTypeBackcheck;

    List<FeedbackLocationResponse> feedbackLocationResponseList;

    public static Intent getStartIntent(Context context,String type) {
        Intent intent = new Intent(context, FeedbackListActivity.class);
        intent.putExtra("type",type);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback_list);

        getActivityComponent().inject(this);

        if (isNetworkAvailable()) {
            setUnBinder(ButterKnife.bind(this));

            mPresenter.onAttach(this);

            setUp();
        }
    }

    @Override
    protected void setUp() {
        mTypeBackcheck = getIntent().getStringExtra("type");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Status Respon");

        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvFeedbackList.setLayoutManager(mLayoutManager);
        rvFeedbackList.setItemAnimator(new DefaultItemAnimator());
        rvFeedbackList.setAdapter(mAdapter);
        mAdapter.setCallback(this);

        mPresenter.getFeedbackDetailList(mTypeBackcheck);

    }

    @Override
    public void updateFeedbackDetailList(List<FeedbackLocationResponse> feedbackListResponseList) {
        Log.d("Debug size : ",feedbackListResponseList.size() + "");
        feedbackLocationResponseList = feedbackListResponseList;
        mAdapter.addItems(feedbackListResponseList);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onFeedbackListItemClick(int position) {

        FeedbackLocationResponse feedbackLocationResponse = feedbackLocationResponseList.get(position);

        String userType = mPresenter.getUserType();

            if ((feedbackLocationResponse.getConfirmationNote().toLowerCase().equals("sudah direspon aoc") ||
                    feedbackLocationResponse.getConfirmationNote().toLowerCase().equals("menunggu respon aoc"))
                    && !userType.toLowerCase().equals("aoc") ) {
                if (feedbackLocationResponse.getLocationType().equals("INSTITUTION")){
                    startActivity(FeedbackSekolahKampusActivity.getStartIntent(this,
                            feedbackLocationResponse.getUserId(),
                            feedbackLocationResponse.getUserName(),
                            feedbackLocationResponse.getUserIdSendTo(),
                            feedbackLocationResponse.getIdOutletDigipos(),
                            feedbackLocationResponse.getLastUpdate(),
                            feedbackLocationResponse.getLocationType()));
                }else{
                    startActivity(FeedbackOssOskActivity.getStartIntent(this,
                            feedbackLocationResponse.getUserId(),
                            feedbackLocationResponse.getUserName(),
                            feedbackLocationResponse.getUserIdSendTo(),
                            feedbackLocationResponse.getIdOutletDigipos(),
                            feedbackLocationResponse.getLastUpdate(),
                            feedbackLocationResponse.getLocationType()));
                }
            }else{
                if (feedbackLocationResponse.getConfirmationNote().toLowerCase().equals("sudah direspon aoc") && userType.toLowerCase().equals("aoc")){
                    Toast.makeText(getApplicationContext(), "Sudah difeedback", Toast.LENGTH_SHORT).show();
                }else {
                    if (feedbackLocationResponse.getLocationType().equals("INSTITUTION")){
                        startActivity(FeedbackSekolahKampusInsertActivity.getStartIntent(this,
                                feedbackLocationResponse.getUserId(),
                                feedbackLocationResponse.getUserIdSendTo(),
                                feedbackLocationResponse.getUserName(),
                                feedbackLocationResponse.getIdOutletDigipos(),
                                feedbackLocationResponse.getLastUpdate(),
                                feedbackLocationResponse.getLocationType()));
                    }else{
                        startActivity(FeedbackOssOskInsertActivity.getStartIntent(this,
                                feedbackLocationResponse.getUserId(),
                                feedbackLocationResponse.getUserIdSendTo(),
                                feedbackLocationResponse.getUserName(),
                                feedbackLocationResponse.getIdOutletDigipos(),
                                feedbackLocationResponse.getLastUpdate(),
                                feedbackLocationResponse.getLocationType()));
                    }
                }
            }
    }

    @Override
    protected void onResume() {
        mPresenter.getFeedbackDetailList(mTypeBackcheck);
        super.onResume();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }
}
