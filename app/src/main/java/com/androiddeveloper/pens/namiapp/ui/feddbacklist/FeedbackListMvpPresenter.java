package com.androiddeveloper.pens.namiapp.ui.feddbacklist;


import com.androiddeveloper.pens.namiapp.di.PerActivity;
import com.androiddeveloper.pens.namiapp.ui.base.MvpPresenter;

/**
 * Created by miftahun on 3/8/18.
 */

@PerActivity
public interface FeedbackListMvpPresenter<V extends FeedbackListMvpView> extends MvpPresenter<V> {

    void getFeedbackDetailList(String statusBackcheck);

    String getUserType();
}