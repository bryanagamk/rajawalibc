package com.androiddeveloper.pens.namiapp.ui.feddbacklist;


import com.androiddeveloper.pens.namiapp.data.network.model.FeedbackLocationResponse;
import com.androiddeveloper.pens.namiapp.ui.base.MvpView;

import java.util.List;

/**
 * Created by miftahun on 3/8/18.
 */

public interface FeedbackListMvpView extends MvpView {

    void updateFeedbackDetailList(List<FeedbackLocationResponse> feedbackListResponseList);
}