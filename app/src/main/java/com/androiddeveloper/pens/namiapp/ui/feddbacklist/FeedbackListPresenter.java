package com.androiddeveloper.pens.namiapp.ui.feddbacklist;


import android.util.Log;

import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.ui.base.BasePresenter;
import com.androidnetworking.error.ANError;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by miftahun on 3/8/18.
 */

public class FeedbackListPresenter<V extends FeedbackListMvpView> extends BasePresenter<V>
        implements FeedbackListMvpPresenter<V> {

    private static final String TAG = "FeedbackListPresenter";

    @Inject
    public FeedbackListPresenter(DataManager dataManager,
                                 CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);
    }

    @Override
    public void getFeedbackDetailList(String statusFeedback) {
        String userId = getDataManager().getCurrentUserId();
        getCompositeDisposable().add(getDataManager()
        .getFeedbackLocation(userId)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(feedbackListResponseList -> {
            if (!isViewAttached()){
                return;
            }
            if (feedbackListResponseList != null && feedbackListResponseList.size() != 0 ){
                getMvpView().updateFeedbackDetailList(feedbackListResponseList);
            }
            Log.d("Debug",feedbackListResponseList.toString());
        },throwable -> {
            if (!isViewAttached()){
                return;
            }
            Log.d("Error",throwable.toString());

            // handle the error here
            if (throwable instanceof ANError) {
                ANError anError = (ANError) throwable;
                handleApiError(anError);
            }
        }));
    }

    @Override
    public String getUserType() {
        return getDataManager().getCurrentUserType();
    }
}