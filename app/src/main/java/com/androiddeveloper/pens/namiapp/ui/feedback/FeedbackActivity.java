package com.androiddeveloper.pens.namiapp.ui.feedback;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.data.network.model.Feedback;
import com.androiddeveloper.pens.namiapp.data.network.model.FeedbackLocationResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.FeedbackResponse;
import com.androiddeveloper.pens.namiapp.ui.base.BaseActivity;
import com.androiddeveloper.pens.namiapp.ui.feddbacklist.FeedbackListAdapter;
import com.androiddeveloper.pens.namiapp.ui.feedbackossosk.FeedbackOssOskActivity;
import com.androiddeveloper.pens.namiapp.ui.feedbackossoskinsert.FeedbackOssOskInsertActivity;
import com.androiddeveloper.pens.namiapp.ui.feedbacksekolahkampus.FeedbackSekolahKampusActivity;
import com.androiddeveloper.pens.namiapp.ui.feedbacksekolahkampusinsert.FeedbackSekolahKampusInsertActivity;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Optional;

public class FeedbackActivity extends BaseActivity implements FeedbackMvpView, FeedbackAdapter.Callback {

    @Inject
    FeedbackMvpPresenter<FeedbackMvpView> mPresenter;

    @Inject
    FeedbackAdapter mFeedbackAdapter;

    @Inject
    LinearLayoutManager mLayoutManager;

    @BindView(R.id.rv_feedback)
    RecyclerView recyclerViewFeedback;

    private String mTypeBackcheck;
    private String userid_user;

    List<FeedbackLocationResponse> feedbackLocationResponseList;

    public static Intent getStartIntent(Context context, String type) {
        Intent intent = new Intent(context, FeedbackActivity.class);
        intent.putExtra("type",type);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);

        getActivityComponent().inject(this);

        if (isNetworkAvailable()) {
            setUnBinder(ButterKnife.bind(this));

            mPresenter.onAttach(this);

            setUp();
        }
    }

    @Override
    protected void setUp() {
        mTypeBackcheck = getIntent().getStringExtra("type");
        userid_user = getIntent().getStringExtra("userid_user");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Status Respon");
        // search item

        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewFeedback.setLayoutManager(mLayoutManager);
        recyclerViewFeedback.setItemAnimator(new DefaultItemAnimator());
        recyclerViewFeedback.setAdapter(mFeedbackAdapter);
        mFeedbackAdapter.setCallback(this);

        mPresenter.getFeedbackList(userid_user);
    }

    private void getIntentFeedback(){
        if (getIntent().getStringExtra("userid_user") != null){
            mTypeBackcheck = getIntent().getStringExtra("type");
            userid_user = getIntent().getStringExtra("userid_user");
            Log.d("@@@@", "getPerformanceIntent: " + userid_user);
            mPresenter.getFeedbackList(userid_user);
        }
    }

    @Override
    public void updateFeedback(List<FeedbackLocationResponse> feedbackResponses) {
        feedbackLocationResponseList = feedbackResponses;
        mFeedbackAdapter.addItems(feedbackResponses);
        mFeedbackAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onResume() {
        getIntentFeedback();
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onFeedbackListItemClick(int position) {
        FeedbackLocationResponse feedbackLocationResponse = feedbackLocationResponseList.get(position);

        String userType = mPresenter.getUserType();

        if ((feedbackLocationResponse.getConfirmationNote().toLowerCase().equals("sudah direspon aoc") ||
                feedbackLocationResponse.getConfirmationNote().toLowerCase().equals("menunggu respon aoc"))
                && !userType.toLowerCase().equals("aoc") ) {
            if (feedbackLocationResponse.getLocationType().equals("INSTITUTION")){
                startActivity(FeedbackSekolahKampusActivity.getStartIntent(this,
                        feedbackLocationResponse.getUserId(),
                        feedbackLocationResponse.getUserName(),
                        feedbackLocationResponse.getUserIdSendTo(),
                        feedbackLocationResponse.getIdOutletDigipos(),
                        feedbackLocationResponse.getLastUpdate(),
                        feedbackLocationResponse.getLocationType()));
            }else{
                startActivity(FeedbackOssOskActivity.getStartIntent(this,
                        feedbackLocationResponse.getUserId(),
                        feedbackLocationResponse.getUserName(),
                        feedbackLocationResponse.getUserIdSendTo(),
                        feedbackLocationResponse.getIdOutletDigipos(),
                        feedbackLocationResponse.getLastUpdate(),
                        feedbackLocationResponse.getLocationType()));
            }
        }
    }
}
