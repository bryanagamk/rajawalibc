package com.androiddeveloper.pens.namiapp.ui.feedback;

import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.data.network.model.Feedback;
import com.androiddeveloper.pens.namiapp.data.network.model.FeedbackLocationResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.FeedbackResponse;
import com.androiddeveloper.pens.namiapp.ui.base.BaseViewHolder;
import com.androiddeveloper.pens.namiapp.ui.feddbacklist.FeedbackListActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by miftahun on 6/16/18.
 * <p>
 * Author Miftahun Najat
 * Email miftahunajat@gmail.com
 * Github https://github.com/miftahunajat
 */

public class FeedbackAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private Callback mCallback;
    private List<FeedbackLocationResponse> mFeedbackList;
    private List<FeedbackLocationResponse> mFeedbackListDefault;

    public FeedbackAdapter(List<FeedbackLocationResponse> feedbackResponseList) {
        mFeedbackList = feedbackResponseList;
    }

    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new ViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_feedback_detail_view, parent, false));
            case VIEW_TYPE_EMPTY:
            default:
                return new EmptyViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_view_backchecking_feedback, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mFeedbackList != null && mFeedbackList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public int getItemCount() {
        if (mFeedbackList != null && mFeedbackList.size() > 0) {
            return mFeedbackList.size();
        } else {
            return 1;
        }
    }

    public void addItems(List<FeedbackLocationResponse> blogList) {
        mFeedbackList.clear();
        mFeedbackList.addAll(blogList);
        mFeedbackList = blogList;
        notifyDataSetChanged();
    }

    public interface Callback {
        void onFeedbackListItemClick(int position);
    }

    public class ViewHolder extends BaseViewHolder {

        TextView tvTempat;
        TextView tvUserId;
        TextView tvUserIdSendto;
        TextView tvTanggal;
        ImageView ivLogoBc;
        View view;

        TextView tvStatusRespon;
        LinearLayout lineInfoFeedbackResponAoc;

        public ViewHolder(View itemView) {
            super(itemView);
            tvTempat = itemView.findViewById(R.id.tv_tempat);
            tvUserId = itemView.findViewById(R.id.tv_nama_user_backchecker);
            tvUserIdSendto = itemView.findViewById(R.id.tv_nama_aoc);
            tvTanggal = itemView.findViewById(R.id.tv_tanggal_bc);
            ivLogoBc = itemView.findViewById(R.id.iv_kampus_ossosk_backchecking);
            view = itemView.findViewById(R.id.v_view_surrounding);
            tvStatusRespon = itemView.findViewById(R.id.tv_status_respon);
            lineInfoFeedbackResponAoc = itemView.findViewById(R.id.line_info_feedback_respon_aoc);
        }

        protected void clear() {

        }

        public void onBind(int position) {
            super.onBind(position);

            final FeedbackLocationResponse feedbackListResponse = mFeedbackList.get(position);


            tvTempat.setText(feedbackListResponse.getOutletName());
            tvUserId.setText(feedbackListResponse.getUserName());
            tvUserIdSendto.setText(feedbackListResponse.getUserNameSendTo());
            tvTanggal.setText(feedbackListResponse.getLastUpdate());

            if (feedbackListResponse.getLocationType().equals("INSTITUTION")){
                ivLogoBc.setImageDrawable(ContextCompat.getDrawable(itemView.getContext(),R.drawable.bus));
                view.setBackgroundResource(R.color.orange);
            }else{
                ivLogoBc.setImageDrawable(ContextCompat.getDrawable(itemView.getContext(),R.drawable.simcard));
                view.setBackgroundResource(R.color.red);
            }

            Log.d("Debug ",feedbackListResponse.getConfirmationNote().toLowerCase());
            if (feedbackListResponse.getConfirmationNote().toLowerCase().equals("menunggu respon aoc")){
                tvStatusRespon.setText("Menunggu respon AOC");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    lineInfoFeedbackResponAoc.setBackground(ContextCompat.getDrawable(itemView.getContext(),R.drawable.background_menunggu_aoc));
                }
            }else {
                tvStatusRespon.setText("Sudah direspon AOC");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    lineInfoFeedbackResponAoc.setBackground(ContextCompat.getDrawable(itemView.getContext(),R.drawable.background_sudah_aoc));
                }
            }

            itemView.setOnClickListener(v->{
                mCallback.onFeedbackListItemClick(position);
            });

        }
    }

    public class EmptyViewHolder extends BaseViewHolder {

        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void clear() {

        }

    }
}