package com.androiddeveloper.pens.namiapp.ui.feedback;


import com.androiddeveloper.pens.namiapp.di.PerActivity;
import com.androiddeveloper.pens.namiapp.ui.base.MvpPresenter;

/**
 * Created by miftahun on 3/8/18.
 */

@PerActivity
public interface FeedbackMvpPresenter<V extends FeedbackMvpView> extends MvpPresenter<V> {

    void getFeedbackList(String userid_user);

    String getUserType();
}
