package com.androiddeveloper.pens.namiapp.ui.feedback;


import com.androiddeveloper.pens.namiapp.data.network.model.Feedback;
import com.androiddeveloper.pens.namiapp.data.network.model.FeedbackLocationResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.FeedbackResponse;
import com.androiddeveloper.pens.namiapp.ui.base.MvpView;

import java.util.List;

/**
 * Created by miftahun on 3/8/18.
 */

public interface FeedbackMvpView extends MvpView {

    void updateFeedback(List<FeedbackLocationResponse> feedbackResponses);
}