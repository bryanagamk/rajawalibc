package com.androiddeveloper.pens.namiapp.ui.feedback;


import android.util.Log;

import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.data.network.model.FeedbackResponse;
import com.androiddeveloper.pens.namiapp.ui.base.BasePresenter;
import com.androidnetworking.error.ANError;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by miftahun on 3/8/18.
 */

public class FeedbackPresenter<V extends FeedbackMvpView> extends BasePresenter<V>
        implements FeedbackMvpPresenter<V> {

    private static final String TAG = "FeedbackPresenter";

    @Inject
    public FeedbackPresenter(DataManager dataManager,
                             CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);
    }

    @Override
    public void getFeedbackList(String userid_user) {
        getCompositeDisposable().add(getDataManager()
                .getFeedbackLocation(userid_user)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(feedbackListResponseList -> {
                    if (!isViewAttached()){
                        return;
                    }
                    if (feedbackListResponseList != null && feedbackListResponseList.size() != 0 ){
                        getMvpView().updateFeedback(feedbackListResponseList);
                    }
                    Log.d("Debug",feedbackListResponseList.toString());
                },throwable -> {
                    if (!isViewAttached()){
                        return;
                    }
                    Log.d("Error",throwable.toString());

                    // handle the error here
                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));

    }

    @Override
    public String getUserType() {
        return getDataManager().getCurrentUserType();
    }
}