package com.androiddeveloper.pens.namiapp.ui.feedbacklocation;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.data.network.model.BackcheckingMain;
import com.androiddeveloper.pens.namiapp.ui.backcheckinglocation.BackCheckingLocationMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.backcheckinglocation.BackCheckingLocationMvpView;
import com.androiddeveloper.pens.namiapp.ui.base.BaseActivity;
import com.androiddeveloper.pens.namiapp.ui.feddbacklist.FeedbackListActivity;
import com.androiddeveloper.pens.namiapp.ui.feedbacksekolahkampusinsert.FeedbackSekolahKampusInsertActivity;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.Target;
import com.github.amlcurran.showcaseview.targets.ViewTarget;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FeedbackLocationActivity extends BaseActivity implements FeedbackLocationMvpView{

    @Inject
    FeedbackLocationMvpPresenter<FeedbackLocationMvpView> mPresenter;

    @BindView(R.id.btn_hasil_backchecking_feedback)
    Button btnHasilBackcheckingFeedback;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, FeedbackLocationActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback_location);

        getActivityComponent().inject(this);
        if (isNetworkAvailable()) {
            setUnBinder(ButterKnife.bind(this));

            mPresenter.onAttach(this);

            setUp();
        }
    }

    @Override
    protected void setUp() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Feedback");

        Target viewTarget = new ViewTarget(R.id.tv_bulan_jumlah_backchecking_feedback_location_osk, this);
        new ShowcaseView.Builder(this)
                .setTarget(viewTarget)
                .withMaterialShowcase()
                .setContentTitle("Back Checking & Feedback")
                .setContentText("Back Checking untuk total ter-backchecking & Feedback untuk total ter-Feedback dari AOC")
                .setStyle(R.style.CustomShowcaseTheme2)
                .singleShot(60)
                .build();

        btnHasilBackcheckingFeedback.setOnClickListener(view -> {
//            lihatHasilBackcheck();
            Intent i = FeedbackListActivity.getStartIntent(FeedbackLocationActivity.this,"");
            startActivity(i);
        });


    }

    public void lihatHasilBackcheck(){
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.item_feedback_respon, null);
        Dialog alertDialogBuilder = new Dialog(this);
        alertDialogBuilder.setCanceledOnTouchOutside(false);
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialogBuilder.setContentView(promptView);
        Button btnMenungguResponAoc= (Button) promptView.findViewById(R.id.btn_menunggu_respon_aoc);
        Button btnSudahDiresponAoc= (Button) promptView.findViewById(R.id.btn_direspon_aoc);
        btnMenungguResponAoc.setOnClickListener(View -> {
            Intent i = FeedbackListActivity.getStartIntent(FeedbackLocationActivity.this,"menunggu respon aoc");
            startActivity(i);
        });
        btnSudahDiresponAoc.setOnClickListener(View -> {
            Intent i = FeedbackListActivity.getStartIntent(FeedbackLocationActivity.this,"sudah direspon aoc");
            startActivity(i);
        });
        alertDialogBuilder.show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void setupBackcheckingMainMenu(BackcheckingMain backcheckingMain) {
        TextView tvBcPoiPtd = findViewById(R.id.tv_hari_jumlah_backchecking_feedback_location_poi);
        TextView tvBcOskPtd = findViewById(R.id.tv_hari_jumlah_backchecking_feedback_location_osk);
        TextView tvBcPoiMtd = findViewById(R.id.tv_bulan_jumlah_backchecking_feedback_location_poi);
        TextView tvBcOskMtd = findViewById(R.id.tv_bulan_jumlah_backchecking_feedback_location_osk);

        tvBcPoiPtd.setText("(" + backcheckingMain.getTotalBackcheckingInstitutionPtd() + " | " + backcheckingMain.getTotalFeedbackInstitutionPtd() + ")");
        tvBcOskPtd.setText("(" + backcheckingMain.getTotalBackcheckingOskPtd() + " | " + backcheckingMain.getTotalFeedbackOskPtd() + ")");
        tvBcPoiMtd.setText("(" + backcheckingMain.getTotalBackcheckingInstitutionMtd() + " | " + backcheckingMain.getTotalFeedbackInstitutionMtd() + ")");
        tvBcOskMtd.setText("(" + backcheckingMain.getTotalBackcheckingOskMtd() + " | " + backcheckingMain.getTotalFeedbackOskMtd() + ")");

    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }


}
