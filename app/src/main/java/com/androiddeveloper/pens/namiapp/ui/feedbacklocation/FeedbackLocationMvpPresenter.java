package com.androiddeveloper.pens.namiapp.ui.feedbacklocation;

import com.androiddeveloper.pens.namiapp.di.PerActivity;
import com.androiddeveloper.pens.namiapp.ui.backcheckinglocation.BackCheckingLocationMvpView;
import com.androiddeveloper.pens.namiapp.ui.base.MvpPresenter;

@PerActivity
public interface FeedbackLocationMvpPresenter<V extends FeedbackLocationMvpView> extends MvpPresenter<V> {

    void getFeedbackMain(String userId);
}
