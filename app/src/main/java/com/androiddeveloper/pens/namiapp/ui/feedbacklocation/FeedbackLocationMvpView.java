package com.androiddeveloper.pens.namiapp.ui.feedbacklocation;

import com.androiddeveloper.pens.namiapp.data.network.model.BackcheckingMain;
import com.androiddeveloper.pens.namiapp.ui.base.MvpView;

public interface FeedbackLocationMvpView extends MvpView {

    void setupBackcheckingMainMenu(BackcheckingMain backcheckingMain);
}
