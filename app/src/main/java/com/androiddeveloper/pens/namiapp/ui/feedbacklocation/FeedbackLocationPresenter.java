package com.androiddeveloper.pens.namiapp.ui.feedbacklocation;

import android.util.Log;

import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.ui.backcheckinglocation.BackCheckingLocationMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.backcheckinglocation.BackCheckingLocationMvpView;
import com.androiddeveloper.pens.namiapp.ui.base.BasePresenter;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class FeedbackLocationPresenter<V extends FeedbackLocationMvpView> extends BasePresenter<V>
        implements FeedbackLocationMvpPresenter<V> {

    @Inject
    public FeedbackLocationPresenter(DataManager dataManager, CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);

        String userId = getDataManager().getCurrentUserId();
        getFeedbackMain(userId);
    }

    @Override
    public void getFeedbackMain(String userId) {
        getCompositeDisposable().add(getDataManager().getFeedbackMain(userId)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
        .subscribe(backcheckingMains -> {
            if (!isViewAttached()){
                return;
            }
            if (backcheckingMains != null && backcheckingMains.size() != 0){
                getMvpView().setupBackcheckingMainMenu(backcheckingMains.get(0));
            }
            Log.d("Debug",backcheckingMains.toString());
        },throwable -> {
            if (!isViewAttached()){
                return;
            }
            Log.d("Error",throwable.toString());
        }));
    }
}
