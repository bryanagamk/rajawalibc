package com.androiddeveloper.pens.namiapp.ui.feedbackossosk;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.TextView;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.data.network.model.FeedbackLeaderNote;
import com.androiddeveloper.pens.namiapp.ui.base.BaseActivity;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class FeedbackOssOskActivity extends BaseActivity implements FeedbackOssOskMvpView {

	@Inject
    FeedbackOssOskMvpPresenter<FeedbackOssOskMvpView> mPresenter;



    public static Intent getStartIntent(Context context,String userid,
                                        String userName,
                                        String userIdSendTo,
                                        String locationId,
                                        String lastupdate,
                                        String typeBackcheck) {
        Intent intent = new Intent(context, FeedbackOssOskActivity.class);
        intent.putExtra("userid",userid);
        intent.putExtra("username", userName);
        intent.putExtra("useridsendto",userIdSendTo);
        intent.putExtra("locationid",locationId);
        intent.putExtra("lastupdate",lastupdate);
        intent.putExtra("typebackcheck",typeBackcheck);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback_oss_osk);

        getActivityComponent().inject(this);

        if (isNetworkAvailable()) {
            setUnBinder(ButterKnife.bind(this));

            mPresenter.onAttach(this);

            setUp();
        }
    }

    @Override
    protected void setUp() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Feedback OSK");

        String userid = getIntent().getStringExtra("userid");
        String userIdSendTo = getIntent().getStringExtra("useridsendto");
        String locationId = getIntent().getStringExtra("locationid");
        String lastupdate = getIntent().getStringExtra("lastupdate");
        String typeBackcheck = getIntent().getStringExtra("typebackcheck");

        mPresenter.getLeaderNote(userid,
                userIdSendTo,
                locationId,
                lastupdate,
                typeBackcheck);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    public void setupFeedbackMenu(FeedbackLeaderNote note) {
        String usernameLeader = getIntent().getStringExtra("username");
        Log.e("Debug " ,note.toString());

        TextView tvNamaAoc = findViewById(R.id.tv_nama_aoc_feedback_osk);
        TextView tvNamaLokasi = findViewById(R.id.tv_lokasi_feedback_osk);
        TextView tvVisitAoc = findViewById(R.id.tv_question_kunjungan_aoc_feedback_osk);
        TextView tvSalesThrough = findViewById(R.id.tv_question_sales_through_feedback_osk);
        TextView tvJualPerdana = findViewById(R.id.tv_question_harga_jual_perdana_feedback_osk);
        TextView tvShare = findViewById(R.id.tv_question_share_feedback_osk);
        TextView tvDominantBranding = findViewById(R.id.tv_question_dominasi_branding_feedback_osk);
        TextView tvMostSell = findViewById(R.id.tv_question_produk_terlaku_feedback_osk);
        TextView tvMatpro = findViewById(R.id.tv_question_matpro_feedback_osk);
        TextView tvProductKnowledge = findViewById(R.id.tv_question_pengetahuan_produk_telkomsel_feedback_osk);

        TextView tvVisitFeedback = findViewById(R.id.tv_answer_kunjungan_aoc_feedback_osk);
        TextView tvSalesThroughFeedback = findViewById(R.id.tv_answer_sales_through_feedback_osk);
        TextView tvJualPerdanaFeedback = findViewById(R.id.tv_answer_harga_jual_perdana_feedback_osk);
        TextView tvShareFeedback = findViewById(R.id.tv_answer_share_feedback_osk);
        TextView tvDominantBrandingFeedback = findViewById(R.id.tv_answer_dominasi_branding_feedback_osk);
        TextView tvMostSellFeedback = findViewById(R.id.tv_answer_produk_terlaku_feedback_osk);
        TextView tvMatproFeedback = findViewById(R.id.tv_answer_matpro_feedback_osk);
        TextView tvProductKnowledgeFeedback = findViewById(R.id.tv_answer_pengetahuan_produk_telkomsel_feedback_osk);

        tvNamaAoc.setText(usernameLeader);
        tvNamaLokasi.setText(note.getInstitutionName());
        tvVisitAoc.setText(note.getVisitNote());
        tvSalesThrough.setText(note.getSalesThroughNote());
        tvJualPerdana.setText(note.getPurchasingNote());
        tvShare.setText(note.getShareNote());
        tvDominantBranding.setText(note.getBrandingNote());
        tvMatpro.setText(note.getMatproNote());
        tvProductKnowledge.setText(note.getProductKnowledge());
        tvMostSell.setText(note.getMostSellingNote());

        tvVisitFeedback.setText(note.getVisitFeedback());
        tvSalesThroughFeedback.setText(note.getSalesThroughFeedback());
        tvJualPerdanaFeedback.setText(note.getPurchasingFeedback());
        tvShareFeedback.setText(note.getShareFeedback());
        tvDominantBrandingFeedback.setText(note.getBrandingFeedback());
        tvMatproFeedback.setText(note.getMatproFeedback());
        tvProductKnowledgeFeedback.setText(note.getProductKnowledgeFeedback());
        tvMostSellFeedback.setText(note.getMostSellingFeedback());


    }
}
