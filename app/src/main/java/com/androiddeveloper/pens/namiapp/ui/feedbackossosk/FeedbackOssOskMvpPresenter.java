package com.androiddeveloper.pens.namiapp.ui.feedbackossosk;


import com.androiddeveloper.pens.namiapp.di.PerActivity;
import com.androiddeveloper.pens.namiapp.ui.base.MvpPresenter;

/**
 * Created by miftahun on 3/8/18.
 */

@PerActivity
public interface FeedbackOssOskMvpPresenter<V extends FeedbackOssOskMvpView> extends MvpPresenter<V> {

    void getLeaderNote(String userid,
                       String userIdSendTo,
                       String locationId,
                       String lastupdate,
                       String typeBackcheck);

}

