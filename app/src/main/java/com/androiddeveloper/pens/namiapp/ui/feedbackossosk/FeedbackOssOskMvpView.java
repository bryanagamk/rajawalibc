package com.androiddeveloper.pens.namiapp.ui.feedbackossosk;


import com.androiddeveloper.pens.namiapp.data.network.model.FeedbackLeaderNote;
import com.androiddeveloper.pens.namiapp.ui.base.MvpView;

/**
 * Created by miftahun on 3/8/18.
 */

public interface FeedbackOssOskMvpView extends MvpView {

    void setupFeedbackMenu(FeedbackLeaderNote note);

}
