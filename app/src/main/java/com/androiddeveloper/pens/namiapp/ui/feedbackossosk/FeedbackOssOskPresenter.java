package com.androiddeveloper.pens.namiapp.ui.feedbackossosk;


import android.util.Log;

import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.ui.base.BasePresenter;
import com.androidnetworking.error.ANError;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by miftahun on 3/8/18.
 */

public class FeedbackOssOskPresenter<V extends FeedbackOssOskMvpView> extends BasePresenter<V>
        implements FeedbackOssOskMvpPresenter<V> {

    private static final String TAG = "FeedbackOssOskPresenter";

    @Inject
    public FeedbackOssOskPresenter(DataManager dataManager,
                                   CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);
    }

    @Override
    public void getLeaderNote(String userid, String userIdSendTo, String locationId, String lastupdate, String typeBackcheck) {
        getCompositeDisposable().add(getDataManager().getFeedbackLeaderNote(
                userid,
                userIdSendTo,
                locationId,
                lastupdate,
                typeBackcheck
        ).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(feedbackLeaderNotes -> {
                    if (!isViewAttached()){
                        return;
                    }
                    if (feedbackLeaderNotes != null && feedbackLeaderNotes.getMessage() == null){
                        getMvpView().setupFeedbackMenu(feedbackLeaderNotes);
                    }
                    Log.d("Debug",feedbackLeaderNotes.toString());
                },throwable -> {
                    if (!isViewAttached()){
                        return;
                    }
                    Log.d("Error",throwable.toString());

                    if (throwable instanceof ANError){
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }
}
