package com.androiddeveloper.pens.namiapp.ui.feedbackossoskinsert;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.data.network.model.FeedbackLeaderNote;
import com.androiddeveloper.pens.namiapp.ui.base.BaseActivity;
import com.androiddeveloper.pens.namiapp.ui.feedbackossosk.FeedbackOssOskActivity;
import com.androiddeveloper.pens.namiapp.ui.feedbackossosk.FeedbackOssOskMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.feedbackossosk.FeedbackOssOskMvpView;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class FeedbackOssOskInsertActivity extends BaseActivity implements FeedbackOssOskInsertMvpView {

    @Inject
    FeedbackOssOskInsertMvpPresenter<FeedbackOssOskInsertMvpView> mPresenter;

    String userid;
    String userIdSendTo;
    String userBackchecker;
    String locationId;
    String lastupdate;
    String typeBackcheck;
    String locationName;

    public static Intent getStartIntent(Context context,String userid,
                                        String userIdSendTo,
                                        String userBackchecker,
                                        String locationId,
                                        String lastupdate,
                                        String typeBackcheck) {
        Intent intent = new Intent(context, FeedbackOssOskInsertActivity.class);
        intent.putExtra("userid",userid);
        intent.putExtra("useridsendto",userIdSendTo);
        intent.putExtra("userBackchecker", userBackchecker);
        intent.putExtra("locationid",locationId);
        intent.putExtra("lastupdate",lastupdate);
        intent.putExtra("typebackcheck",typeBackcheck);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback_oss_osk_insert);

        getActivityComponent().inject(this);

        if (isNetworkAvailable()) {
            setUnBinder(ButterKnife.bind(this));

            mPresenter.onAttach(this);

            setUp();
        }
    }

    @Override
    protected void setUp() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Feedback Insert OSK");

        userid = getIntent().getStringExtra("userid");
        userIdSendTo = getIntent().getStringExtra("useridsendto");
        userBackchecker = getIntent().getStringExtra("userBackchecker");
        locationId = getIntent().getStringExtra("locationid");
        lastupdate = getIntent().getStringExtra("lastupdate");
        typeBackcheck = getIntent().getStringExtra("typebackcheck");

        mPresenter.getLeaderNote(userid,
                userIdSendTo,
                locationId,
                lastupdate,
                typeBackcheck);

        setOnClickListeners();
    }

    private void setOnClickListeners() {
        Button btnSubmit = findViewById(R.id.btn_send_feedback_osk_insert);
        btnSubmit.setOnClickListener(view -> {
            onFeedbackSubmit();
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void setupFeedbackMenu(FeedbackLeaderNote note) {
        TextView tvNamaAoc = findViewById(R.id.tv_nama_aoc_feedback_osk);
        TextView tvNamaLokasi = findViewById(R.id.tv_lokasi_feedback_osk);
        TextView tvNamaBackchecker = findViewById(R.id.tv_nama_backchecker_feedback_osk);
        TextView tvTanggalBackcheck = findViewById(R.id.tv_tanggal_hasil_bc_feedback_osk);
        TextView tvVisitAoc = findViewById(R.id.tv_question_kunjungan_aoc_feedback_osk);
        TextView tvSalesThrough = findViewById(R.id.tv_question_sales_through_feedback_osk);
        TextView tvJualPerdana = findViewById(R.id.tv_question_harga_jual_perdana_feedback_osk);
        TextView tvShare = findViewById(R.id.tv_question_share_feedback_osk);
        TextView tvDominantBranding = findViewById(R.id.tv_question_dominasi_branding_feedback_osk);
        TextView tvProdukterlaku = findViewById(R.id.tv_question_produk_terlaku_feedback_osk);
        TextView tvMatpro = findViewById(R.id.tv_question_matpro_feedback_osk);
        TextView tvProductKnowledge = findViewById(R.id.tv_question_pengetahuan_produk_telkomsel_feedback_osk);

        String userName = mPresenter.getUserName();

        tvNamaAoc.setText(userName);
        tvNamaLokasi.setText(note.getInstitutionName());
        tvNamaBackchecker.setText(userBackchecker);
        tvTanggalBackcheck.setText(lastupdate);
        tvVisitAoc.setText(note.getVisitNote());
        tvSalesThrough.setText(note.getSalesThroughNote());
        tvJualPerdana.setText(note.getPurchasingNote());
        tvShare.setText(note.getShareNote());
        tvDominantBranding.setText(note.getBrandingNote());
        tvMatpro.setText(note.getMatproNote());
        tvProductKnowledge.setText(note.getProductKnowledge());
        tvProdukterlaku.setText(note.getMostSellingNote());

        locationName = note.getInstitutionName();
    }

    private void onFeedbackSubmit(){
        String feedbackKunjungan = ((EditText) findViewById(R.id.et_note_answer_kunjungan_aoc_feedback_osk)).getText().toString();
        String feedbackSalesthrough = ((EditText) findViewById(R.id.et_note_answer_sales_through_feedback_osk)).getText().toString();
        String jualPerdana = ((EditText) findViewById(R.id.et_note_answer_harga_jual_perdana_feedback_osk)).getText().toString();
        String share = ((EditText) findViewById(R.id.et_note_answer_share_feedback_osk)).getText().toString();
        String dominantBranding = ((EditText) findViewById(R.id.et_note_answer_dominasi_branding_feedback_osk)).getText().toString();
        String produkTerlaku = ((EditText) findViewById(R.id.et_note_answer_produk_terlaku_feedback_osk)).getText().toString();
        String matpro = ((EditText) findViewById(R.id.et_note_answer_matpro_feedback_osk)).getText().toString();
        String productKnowledge = ((EditText) findViewById(R.id.et_note_answer_pengetahuan_produk_telkomsel_feedback_osk)).getText().toString();

        Map<String, String> map = new HashMap<>();
        map.put("location_id",locationId);
        map.put("visitfeedback",feedbackKunjungan);
        map.put("salesthroughfeedback",feedbackSalesthrough);
        map.put("purchasingfeedback",jualPerdana);
        map.put("most_selling_feedback",produkTerlaku);

        map.put("sharefeedback",share);
        map.put("brandingfeedback",dominantBranding);
        map.put("matprofeedback",matpro);
        map.put("productknowledgefeedback",productKnowledge);
        //TODO
        map.put("userid_sendto",userIdSendTo);
        map.put("userid",userid);
        map.put("lastupdate",lastupdate);
        map.put("location_name",locationName);


        mPresenter.submitFeedback(map,this);
    }
}