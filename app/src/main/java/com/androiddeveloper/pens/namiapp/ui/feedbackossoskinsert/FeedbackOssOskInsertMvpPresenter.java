package com.androiddeveloper.pens.namiapp.ui.feedbackossoskinsert;

import android.content.Context;

import com.androiddeveloper.pens.namiapp.di.PerActivity;
import com.androiddeveloper.pens.namiapp.ui.base.MvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.feedbackossosk.FeedbackOssOskMvpView;

import java.util.Map;

@PerActivity
public interface FeedbackOssOskInsertMvpPresenter<V extends FeedbackOssOskInsertMvpView> extends MvpPresenter<V> {

    void getLeaderNote(String userid,
                       String userIdSendTo,
                       String locationId,
                       String lastupdate,
                       String typeBackcheck);

    void submitFeedback(Map<String, String> map, Context context);
}
