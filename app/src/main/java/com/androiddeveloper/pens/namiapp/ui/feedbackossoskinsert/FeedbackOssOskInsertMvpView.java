package com.androiddeveloper.pens.namiapp.ui.feedbackossoskinsert;

import com.androiddeveloper.pens.namiapp.data.network.model.FeedbackLeaderNote;
import com.androiddeveloper.pens.namiapp.ui.base.MvpView;

public interface FeedbackOssOskInsertMvpView extends MvpView {

    void setupFeedbackMenu(FeedbackLeaderNote note);

}
