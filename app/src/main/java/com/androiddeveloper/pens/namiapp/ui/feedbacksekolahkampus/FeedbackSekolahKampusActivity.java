package com.androiddeveloper.pens.namiapp.ui.feedbacksekolahkampus;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.data.network.model.FeedbackLeaderNote;
import com.androiddeveloper.pens.namiapp.ui.base.BaseActivity;
import com.androiddeveloper.pens.namiapp.ui.feedbacksekolahkampusinsert.FeedbackSekolahKampusInsertActivity;
import com.androiddeveloper.pens.namiapp.ui.main.MainActivity;
import com.androiddeveloper.pens.namiapp.ui.main.MainMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.main.MainMvpView;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FeedbackSekolahKampusActivity extends BaseActivity implements FeedbackSekolahKampusMvpView {

    @Inject
    FeedbackSekolahKampusMvpPresenter<FeedbackSekolahKampusMvpView> mPresenter;

    String userid;
    String usernameLeader;
    String userIdSendTo;
    String locationId;
    String lastupdate;
    String typeBackcheck;

    public static Intent getStartIntent(Context context,String userid,
                                        String userName,
                                        String userIdSendTo,
                                        String locationId,
                                        String lastupdate,
                                        String typeBackcheck) {
        Intent intent = new Intent(context, FeedbackSekolahKampusActivity.class);
        intent.putExtra("userid",userid);
        intent.putExtra("username", userName);
        intent.putExtra("useridsendto",userIdSendTo);
        intent.putExtra("locationid",locationId);
        intent.putExtra("lastupdate",lastupdate);
        intent.putExtra("typebackcheck",typeBackcheck);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback_sekolah_kampus);

        getActivityComponent().inject(this);

        if (isNetworkAvailable()) {
            setUnBinder(ButterKnife.bind(this));

            mPresenter.onAttach(this);

            setUp();
        }
    }

    @Override
    protected void setUp() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Feedback POI");

        userid = getIntent().getStringExtra("userid");
        userIdSendTo = getIntent().getStringExtra("useridsendto");
        locationId = getIntent().getStringExtra("locationid");
        lastupdate = getIntent().getStringExtra("lastupdate");
        typeBackcheck = getIntent().getStringExtra("typebackcheck");

        mPresenter.getLeaderNote(userid,
                userIdSendTo,
                locationId,
                lastupdate,
                typeBackcheck);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void setupFeedbackMenu(FeedbackLeaderNote note) {
        usernameLeader = getIntent().getStringExtra("username");


        TextView tvNamaAoc = findViewById(R.id.tv_nama_aoc_feedback_poi);
        TextView tvNamaLokasi = findViewById(R.id.tv_lokasi_feedback_poi);
        TextView tvVisitAoc = findViewById(R.id.tv_question_kunjungan_aoc_feedback_poi);
        TextView tvSalesThrough = findViewById(R.id.tv_question_sales_through_feedback_poi);
        TextView tvBranding = findViewById(R.id.tv_question_branding_kantin_feedback_poi);
        TextView tvMerchandising = findViewById(R.id.tv_question_merchandising_feedback_poi);
        TextView tvMarketShare = findViewById(R.id.tv_question_market_survey_feedback_poi);

        TextView tvFeedbackVisit = findViewById(R.id.tv_answer_kunjungan_aoc_feedback_poi);
        TextView tvFeedbackSalesThrough = findViewById(R.id.tv_answer_sales_through_feedback_poi);
        TextView tvFeedbackBranding = findViewById(R.id.tv_answer_branding_kantin_feedback_poi);
        TextView tvFeedbackMerchandising = findViewById(R.id.tv_answer_merchandising_feedback_poi);
        TextView tvFeedbackMarketShare= findViewById(R.id.tv_answer_market_survey_feedback_poi);

        tvNamaAoc.setText(usernameLeader);
        tvNamaLokasi.setText(note.getInstitutionName());
        tvVisitAoc.setText(note.getVisitNote());
        tvSalesThrough.setText(note.getSalesThroughNote());
        tvBranding.setText(note.getBrandingNote());
        tvMerchandising.setText(note.getMerchandisingNote());
        tvMarketShare.setText(note.getMarketShareNote());

        tvFeedbackVisit.setText(note.getVisitFeedback());
        tvFeedbackSalesThrough.setText(note.getSalesThroughFeedback());
        tvFeedbackBranding.setText(note.getBrandingFeedback());
        tvFeedbackMerchandising.setText(note.getMerchandisingFeedback());
        tvFeedbackMarketShare.setText(note.getMarketShareFeedback());
    }
}
