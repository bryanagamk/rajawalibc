package com.androiddeveloper.pens.namiapp.ui.feedbacksekolahkampus;

import com.androiddeveloper.pens.namiapp.di.PerActivity;
import com.androiddeveloper.pens.namiapp.ui.base.MvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.base.MvpView;
import com.androiddeveloper.pens.namiapp.ui.main.MainMvpView;

import java.util.Map;

@PerActivity
public interface FeedbackSekolahKampusMvpPresenter<V extends FeedbackSekolahKampusMvpView> extends MvpPresenter<V> {

    void getLeaderNote(String userid,
                       String userIdSendTo,
                       String locationId,
                       String lastupdate,
                       String typeBackcheck);
}
