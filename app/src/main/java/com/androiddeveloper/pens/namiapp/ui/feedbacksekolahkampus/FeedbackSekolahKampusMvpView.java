package com.androiddeveloper.pens.namiapp.ui.feedbacksekolahkampus;

import com.androiddeveloper.pens.namiapp.data.network.model.FeedbackLeaderNote;
import com.androiddeveloper.pens.namiapp.ui.base.MvpView;

public interface FeedbackSekolahKampusMvpView extends MvpView {

    void setupFeedbackMenu(FeedbackLeaderNote note);

}
