package com.androiddeveloper.pens.namiapp.ui.feedbacksekolahkampus;

import android.util.Log;

import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.ui.base.BasePresenter;
import com.androiddeveloper.pens.namiapp.ui.main.MainMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.main.MainMvpView;
import com.androidnetworking.error.ANError;

import java.util.Map;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class FeedbackSekolahKampusPresenter<V extends FeedbackSekolahKampusMvpView> extends BasePresenter<V>
        implements FeedbackSekolahKampusMvpPresenter<V> {

    @Inject
    public FeedbackSekolahKampusPresenter(DataManager dataManager, CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);
    }

    @Override
    public void getLeaderNote(String userid, String userIdSendTo, String locationId, String lastupdate, String typeBackcheck) {
        getCompositeDisposable().add(getDataManager().getFeedbackLeaderNote(
                userid,
                userIdSendTo,
                locationId,
                lastupdate,
                typeBackcheck
        ).observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
        .subscribe(feedbackLeaderNotes -> {
            if (!isViewAttached()){
                return;
            }
            if (feedbackLeaderNotes != null && feedbackLeaderNotes.getMessage() == null){
                getMvpView().setupFeedbackMenu(feedbackLeaderNotes);
            }
            Log.d("Debug",feedbackLeaderNotes.toString());
        },throwable -> {
            if (!isViewAttached()){
                return;
            }
            Log.d("Error",throwable.toString());

            if (throwable instanceof ANError){
                ANError anError = (ANError) throwable;
                handleApiError(anError);
            }
        }));
    }
}
