package com.androiddeveloper.pens.namiapp.ui.feedbacksekolahkampusinsert;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.data.network.model.FeedbackLeaderNote;
import com.androiddeveloper.pens.namiapp.ui.base.BaseActivity;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class FeedbackSekolahKampusInsertActivity extends BaseActivity implements FeedbackSekolahKampusInsertMvpView {

    @Inject
    FeedbackSekolahKampusInsertMvpPresenter<FeedbackSekolahKampusInsertMvpView> mPresenter;

    String userid;
    String userIdSendTo;
    String userBackchecker;
    String locationId;
    String lastupdate;
    String typeBackcheck;
    String locationName;

    public static Intent getStartIntent(Context context,String userid,
                                        String userIdSendTo,
                                        String userBackchecker,
                                        String locationId,
                                        String lastupdate,
                                        String typeBackcheck) {
        Intent intent = new Intent(context, FeedbackSekolahKampusInsertActivity.class);
        intent.putExtra("userid",userid);
        intent.putExtra("useridsendto",userIdSendTo);
        intent.putExtra("userBackchecker", userBackchecker);
        intent.putExtra("locationid",locationId);
        intent.putExtra("lastupdate",lastupdate);
        intent.putExtra("typebackcheck",typeBackcheck);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback_sekolah_kampus_insert);

        getActivityComponent().inject(this);

        if (isNetworkAvailable()) {
            setUnBinder(ButterKnife.bind(this));

            mPresenter.onAttach(this);

            setUp();
        }
    }

    @Override
    protected void setUp() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Feedback Insert POI");

        userid = getIntent().getStringExtra("userid");
        userIdSendTo = getIntent().getStringExtra("useridsendto");
        userBackchecker = getIntent().getStringExtra("userBackchecker");
        locationId = getIntent().getStringExtra("locationid");
        lastupdate = getIntent().getStringExtra("lastupdate");
        typeBackcheck = getIntent().getStringExtra("typebackcheck");

        mPresenter.getLeaderNote(userid,
                userIdSendTo,
                locationId,
                lastupdate,
                typeBackcheck);

        setOnClickListeners();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void setupFeedbackMenu(FeedbackLeaderNote note) {
        TextView tvNamaAoc = findViewById(R.id.tv_nama_aoc_feedback_poi);
        TextView tvNamaLokasi = findViewById(R.id.tv_lokasi_feedback_poi);
        TextView tvNamaBackchecker = findViewById(R.id.tv_nama_backchecker_feedback_poi);
        TextView tvTanggalBackcheck = findViewById(R.id.tv_tanggal_hasil_bc_feedback_poi);
        TextView tvVisitAoc = findViewById(R.id.tv_question_kunjungan_aoc_feedback_poi);
        TextView tvSalesThrough = findViewById(R.id.tv_question_sales_through_feedback_poi);
        TextView tvBranding = findViewById(R.id.tv_question_branding_kantin_feedback_poi);
        TextView tvMerchandising = findViewById(R.id.tv_question_merchandising_feedback_poi);
        TextView tvMarketShare = findViewById(R.id.tv_question_market_survey_feedback_poi);

        String userName = mPresenter.getUserName();

        tvNamaAoc.setText(userName);
        tvNamaLokasi.setText(note.getInstitutionName());
        tvNamaBackchecker.setText(userBackchecker);
        tvTanggalBackcheck.setText(lastupdate);
        tvVisitAoc.setText(note.getVisitNote());
        tvSalesThrough.setText(note.getSalesThroughNote());
        tvBranding.setText(note.getBrandingNote());
        tvMerchandising.setText(note.getMerchandisingNote());
        tvMarketShare.setText(note.getMarketShareNote());
        locationName = note.getInstitutionName();
    }

    private void onFeedbackSubmit(){
        String feedbackKunjungan = ((EditText) findViewById(R.id.et_note_answer_kunjungan_aoc_feedback_poi)).getText().toString();
        String feedbackSalesthrough = ((EditText) findViewById(R.id.et_note_answer_sales_through_feedback_poi)).getText().toString();
        String kantin = ((EditText) findViewById(R.id.et_note_answer_branding_kantin_feedback_poi)).getText().toString();
        String merchandising = ((EditText) findViewById(R.id.et_note_answer_merchandising_feedback_poi)).getText().toString();
        String marketShare = ((EditText) findViewById(R.id.et_note_answer_market_survey_feedback_poi)).getText().toString();

        Map<String, String> map = new HashMap<>();
        map.put("location_id",locationId);
        map.put("visitfeedback",feedbackKunjungan);
        map.put("salesthroughfeedback",feedbackSalesthrough);
        map.put("brandingfeedback",kantin);
        map.put("merchandisingfeedback",merchandising);
        map.put("marketsharefeedback",marketShare);
        map.put("userid_sendto",userIdSendTo);
        map.put("userid",userid);
        map.put("lastupdate",lastupdate);
        map.put("location_name",locationName);

        mPresenter.submitFeedback(map,this);
    }

    private void setOnClickListeners() {
        Button btnSubmit = findViewById(R.id.btn_send_feedback_poi_insert);
        btnSubmit.setOnClickListener(view -> {
            onFeedbackSubmit();
        });
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }
}
