package com.androiddeveloper.pens.namiapp.ui.feedbacksekolahkampusinsert;

import android.content.Context;

import com.androiddeveloper.pens.namiapp.di.PerActivity;
import com.androiddeveloper.pens.namiapp.ui.base.MvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.feedbacksekolahkampus.FeedbackSekolahKampusMvpView;

import java.util.Map;

@PerActivity
public interface FeedbackSekolahKampusInsertMvpPresenter<V extends FeedbackSekolahKampusInsertMvpView> extends MvpPresenter<V> {

    void getLeaderNote(String userid,
                       String userIdSendTo,
                       String locationId,
                       String lastupdate,
                       String typeBackcheck);

    void submitFeedback(Map<String, String> map, Context context);
}
