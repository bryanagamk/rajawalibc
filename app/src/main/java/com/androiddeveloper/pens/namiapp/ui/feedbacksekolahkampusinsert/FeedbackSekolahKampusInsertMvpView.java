package com.androiddeveloper.pens.namiapp.ui.feedbacksekolahkampusinsert;

import com.androiddeveloper.pens.namiapp.data.network.model.FeedbackLeaderNote;
import com.androiddeveloper.pens.namiapp.ui.base.MvpView;

public interface FeedbackSekolahKampusInsertMvpView extends MvpView {

    void setupFeedbackMenu(FeedbackLeaderNote note);

}
