package com.androiddeveloper.pens.namiapp.ui.feedbacksekolahkampusinsert;

import android.content.Context;
import android.util.Log;

import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.service.NotificationService;
import com.androiddeveloper.pens.namiapp.ui.base.BasePresenter;
import com.androidnetworking.error.ANError;

import java.util.Map;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class FeedbackSekolahKampusInsertPresenter<V extends FeedbackSekolahKampusInsertMvpView> extends BasePresenter<V>
        implements FeedbackSekolahKampusInsertMvpPresenter<V> {

    @Inject
    public FeedbackSekolahKampusInsertPresenter(DataManager dataManager, CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);
    }

    @Override
    public void getLeaderNote(String userid, String userIdSendTo, String locationId, String lastupdate, String typeBackcheck) {
        getCompositeDisposable().add(getDataManager().getFeedbackLeaderNote(
                userid,
                userIdSendTo,
                locationId,
                lastupdate,
                typeBackcheck
        ).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(feedbackLeaderNotes -> {
                    if (!isViewAttached()){
                        return;
                    }
                    if (feedbackLeaderNotes != null && feedbackLeaderNotes.getMessage() == null){
                        getMvpView().setupFeedbackMenu(feedbackLeaderNotes);
                    }
                    Log.d("Debug",feedbackLeaderNotes.toString());
                },throwable -> {
                    if (!isViewAttached()){
                        return;
                    }
                    Log.d("Error",throwable.toString());

                    if (throwable instanceof ANError){
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

    @Override
    public void submitFeedback(Map<String, String> map, Context context) {
        getCompositeDisposable().add(getDataManager().feedbackInstitutionInsert(map)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(basicResponse -> {
                    if (!isViewAttached()){
                        return;
                    }
                    if (basicResponse.getSuccess()){
                        getMvpView().showMessage("Feedback Selesai Tersubmit");
                        NotificationService.getInstance(context).backcheckingSelesaiNotification("Feedback Executed","Feedback Anda " + map.get("location_name") + "Sudah selesai tersubmit", "Laporan feedback Anda");
                        NotificationService.getInstance(context).sendNotifToAOC(map.get("userid"),"Feedback " + map.get("location_name") + "Sudah di Feedback oleh " + map.get("username"),"Kunjugan Terfeedback!");
                        getMvpView().finish();
                    }else {
                        NotificationService.getInstance(context).backcheckingSelesaiNotification("Feedback Gagal!","Feedback Anda pada " + map.get("location_name") + "Gagal Tersubmit", "Laporan Feedback Anda");
                        Log.d("Fail : ",map.toString());
                        Log.d("Fail  :",basicResponse.getMessage());
                        getMvpView().showMessage("Terjadi kesalahan saat submit feedback");
                        getMvpView().hideLoading();
                    }
                },throwable -> {
                    if (!isViewAttached()){
                        return;
                    }
                    Log.d("Error",throwable.toString());

                    if (throwable instanceof ANError){
                        ANError anError = (ANError) throwable;
                        Log.d("Error",anError.getErrorBody());
                        handleApiError(anError);
                    }
                }));
    }
}
