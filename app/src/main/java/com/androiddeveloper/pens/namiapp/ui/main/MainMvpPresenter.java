package com.androiddeveloper.pens.namiapp.ui.main;

import com.androiddeveloper.pens.namiapp.di.PerActivity;
import com.androiddeveloper.pens.namiapp.ui.base.MvpPresenter;

/**
 * Created by miftahun on 6/11/18.
 * <p>
 * Author Miftahun Najat
 * Email miftahunajat@gmail.com
 * Github https://github.com/miftahunajat
 */

@PerActivity
public interface MainMvpPresenter<V extends MainMvpView> extends MvpPresenter<V> {

}
