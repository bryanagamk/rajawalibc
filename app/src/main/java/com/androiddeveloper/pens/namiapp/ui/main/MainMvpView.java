package com.androiddeveloper.pens.namiapp.ui.main;

import com.androiddeveloper.pens.namiapp.ui.base.MvpView;

/**
 * Created by miftahun on 6/11/18.
 * <p>
 * Author Miftahun Najat
 * Email miftahunajat@gmail.com
 * Github https://github.com/miftahunajat
 */

public interface MainMvpView extends MvpView {

}
