package com.androiddeveloper.pens.namiapp.ui.notification;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.data.network.model.NotificationResponse;
import com.androiddeveloper.pens.namiapp.ui.base.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NotificationAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private NotificationAdapter.Callback mCallback;
    private List<NotificationResponse> mNotificationResponseList;

    public NotificationAdapter(ArrayList<NotificationResponse> notificationResponses) {
        mNotificationResponseList = notificationResponses;
    }

    public void setCallback(NotificationAdapter.Callback callback) {
        this.mCallback = callback;
    }

    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new NotificationAdapter.ViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification, parent, false));
            case VIEW_TYPE_EMPTY:
            default:
                return new NotificationAdapter.EmptyViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_error_message, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mNotificationResponseList != null && mNotificationResponseList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {
        if (mNotificationResponseList != null && mNotificationResponseList.size() > 0) {
            return mNotificationResponseList.size();
        } else {
            return 1;
        }
    }

    public void addItems(List<NotificationResponse> notificationResponseList) {
        mNotificationResponseList.addAll(notificationResponseList);
        notifyDataSetChanged();
    }

    public interface Callback {
        void onBlogEmptyViewRetryClick();
    }

    public class ViewHolder extends BaseViewHolder {
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void clear() {

        }


    }

    public class EmptyViewHolder extends BaseViewHolder {

        @BindView(R.id.btn_retry)
        Button retryButton;

        @BindView(R.id.tv_message)
        TextView messageTextView;

        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void clear() {

        }

        @OnClick(R.id.btn_retry)
        void onRetryClick() {
            if (mCallback != null)
                mCallback.onBlogEmptyViewRetryClick();
        }
    }
}
