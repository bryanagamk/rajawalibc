package com.androiddeveloper.pens.namiapp.ui.notification;

import com.androiddeveloper.pens.namiapp.di.PerActivity;
import com.androiddeveloper.pens.namiapp.ui.base.MvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.main.MainMvpView;

@PerActivity
public interface NotificationMvpPresenter<V extends NotificationMvpView> extends MvpPresenter<V> {
    void getNotification();
}
