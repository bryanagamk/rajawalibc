package com.androiddeveloper.pens.namiapp.ui.notification;

import com.androiddeveloper.pens.namiapp.data.network.model.NotificationResponse;
import com.androiddeveloper.pens.namiapp.ui.base.MvpView;

import java.util.List;

public interface NotificationMvpView extends MvpView {
    void updateNotification(List<NotificationResponse> notificationResponses);
}
