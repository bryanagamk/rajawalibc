package com.androiddeveloper.pens.namiapp.ui.notification;

import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.ui.base.BasePresenter;
import com.androiddeveloper.pens.namiapp.ui.main.MainMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.main.MainMvpView;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class NotificationPresenter<V extends NotificationMvpView> extends BasePresenter<V>
        implements NotificationMvpPresenter<V> {

    @Inject
    public NotificationPresenter(DataManager dataManager, CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);
    }

    @Override
    public void getNotification() {
        getCompositeDisposable().add(getDataManager()
                .getNotificationList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(notificationResponses -> {
                    if (notificationResponses != null && notificationResponses.size() != 0){
                        getMvpView().updateNotification(notificationResponses);
                    }
                }));
    }
}
