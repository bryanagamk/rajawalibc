package com.androiddeveloper.pens.namiapp.ui.performancepoiosklist;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceSummary;
import com.androiddeveloper.pens.namiapp.ui.base.BaseActivity;
import com.androiddeveloper.pens.namiapp.ui.summary.fragment.SummaryType;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PerformancePoiOskListActivity extends BaseActivity implements PerformancePoiOskListMvpView, PerformancePoiOskListAdapter.Callback {


    @Inject
    PerformancePoiOskListMvpPresenter<PerformancePoiOskListMvpView> mPresenter;

    @Inject
    PerformancePoiOskListAdapter mPerformancePoiOskListAdapter;

    @Inject
    LinearLayoutManager mLayoutManager;

    @BindView(R.id.rv_performance_poi_osk_list)
    RecyclerView recyclerViewPerformancePoiOskList;

    @BindView(R.id.search_view_performance_poi_osk)
    MaterialSearchView searchViewPerformancePoiOsk;

    @BindView(R.id.toolbarSearchPerformancePoiOsk)
    Toolbar toolbarSearchPerformancePoiOsk;

    @BindView(R.id.shimmer_view_container_list)
    ShimmerFrameLayout mShimmerViewContainer;

    List<PerformanceSummary> mPerformancePoiOskListResponseLists;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, PerformancePoiOskListActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_performance_poi_osk_list);
        getActivityComponent().inject(this);

        if (isNetworkAvailable()) {
            setUnBinder(ButterKnife.bind(this));

            mPresenter.onAttach(this);

            setUp();
        }
    }

    @Override
    protected void setUp() {
        setSupportActionBar(toolbarSearchPerformancePoiOsk);
        getSupportActionBar().setTitle(getResources().getString(R.string.detail_aoc));

        SummaryType type = (SummaryType) getIntent().getSerializableExtra("type");
        String territory = getIntent().getStringExtra("territory");
        String userid_aoc = (String) getIntent().getStringExtra("userid_aoc");
        String username_aoc = (String) getIntent().getStringExtra("username_aoc");
        String period_type_mtd = getIntent().getStringExtra("period_type_mtd");
        String period_type_ptd = getIntent().getStringExtra("period_type_ptd");

        toolbarSearchPerformancePoiOsk.setNavigationIcon(R.drawable.ic_action_navigation_arrow_back_inverted);
        toolbarSearchPerformancePoiOsk.setTitleTextColor(getResources().getColor(R.color.white));
        searchViewPerformancePoiOsk.setEllipsize(true);
        toolbarSearchPerformancePoiOsk.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mPerformancePoiOskListAdapter = new PerformancePoiOskListAdapter(new ArrayList<>(), type, territory.toLowerCase());

        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewPerformancePoiOskList.setLayoutManager(mLayoutManager);
        recyclerViewPerformancePoiOskList.setItemAnimator(new DefaultItemAnimator());
        recyclerViewPerformancePoiOskList.setAdapter(mPerformancePoiOskListAdapter);

        if (type == SummaryType.PJP_PTD_POI_OSK || type == SummaryType.PJP_MTD_POI_OSK){
            recyclerViewPerformancePoiOskList.setAdapter(mPerformancePoiOskListAdapter);
            mPerformancePoiOskListAdapter.setCallback(this);
        }

        if (type.getType().equals("mtd")){
            mPresenter.onViewPrepared(type, territory, userid_aoc, username_aoc, period_type_mtd);
        }else {
            mPresenter.onViewPrepared(type, territory, userid_aoc, username_aoc, period_type_ptd);
        }

        searchViewPerformancePoiOsk.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                //Do some magic
                try{
                    mPerformancePoiOskListAdapter.filterByNameLocation(query);
                }catch (Exception e){
                    e.printStackTrace();
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                try{
                    mPerformancePoiOskListAdapter.filterByNameLocation(newText);
                }catch (Exception e){
                    e.printStackTrace();
                }
                return true;
            }
        });

        searchViewPerformancePoiOsk.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
                //Do some magic
            }

            @Override
            public void onSearchViewClosed() {
                //Do some magic
            }
        });
    }

    private void getPerformanceIntent(){
        if (getIntent().getStringExtra("userid_aoc") != null){
            String territory = getIntent().getStringExtra("territory");
            SummaryType type = (SummaryType) getIntent().getSerializableExtra("type");
            String userid_aoc = (String) getIntent().getStringExtra("userid_aoc");
            String username_aoc = (String) getIntent().getStringExtra("username_aoc");
            String period_type_mtd = getIntent().getStringExtra("period_type_mtd");
            String period_type_ptd = getIntent().getStringExtra("period_type_ptd");
            Log.d("@@@@", "getPerformanceIntent: " + userid_aoc);
            Log.d("@@@@", "getPerformanceIntent: " + username_aoc);
            if (type.getType().equals("mtd")){
                mPresenter.onViewPrepared(type, territory, userid_aoc, username_aoc, period_type_mtd);
            }else {
                mPresenter.onViewPrepared(type, territory, userid_aoc, username_aoc, period_type_ptd);
            }
        }
    }

    @Override
    public void onItemLocationListClick(int position) {

    }

    @Override
    public void onBackPressed() {
        if (searchViewPerformancePoiOsk.isSearchOpen()) {
            searchViewPerformancePoiOsk.closeSearch();
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_summary, menu);

        MenuItem item = menu.findItem(R.id.action_search_summary);
        searchViewPerformancePoiOsk.setMenuItem(item);

        return true;
    }

    @Override
    public void showLoading(String message) {
        super.showLoading(message);
        mShimmerViewContainer.startShimmer();
        mShimmerViewContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        // stop animating Shimmer and hide the layout
        mShimmerViewContainer.stopShimmer();
        mShimmerViewContainer.setVisibility(View.GONE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmer();
        getPerformanceIntent();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmer();
        super.onPause();
    }

    @Override
    public void updatePerformanceSummary(List<PerformanceSummary> performanceSummaryList) {
        Log.d("Debug",performanceSummaryList.size() + "");
        mPerformancePoiOskListAdapter.addItems(performanceSummaryList);
        this.mPerformancePoiOskListResponseLists = performanceSummaryList;
        mPerformancePoiOskListAdapter.setCallback(this);
    }
}
