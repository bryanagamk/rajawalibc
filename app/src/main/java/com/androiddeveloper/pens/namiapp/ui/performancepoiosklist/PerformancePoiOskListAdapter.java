package com.androiddeveloper.pens.namiapp.ui.performancepoiosklist;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.media.effect.Effect;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceSummary;
import com.androiddeveloper.pens.namiapp.ui.base.BaseViewHolder;
import com.androiddeveloper.pens.namiapp.ui.summary.fragment.SummaryType;
import com.androiddeveloper.pens.namiapp.ui.summary.performancelistadapter.SummaryPerformancePoiOskAdaptiveAdapter;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PerformancePoiOskListAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private PerformancePoiOskListAdapter.Callback mCallback;
    private List<PerformanceSummary> mSummaryPerformancePoiOsks;
    private List<PerformanceSummary> mSummaryPerformancePoiOsksDefault;
    private SummaryType mSummaryType;
    private String mType;

    ColorStateList color;

    public PerformancePoiOskListAdapter(List<PerformanceSummary> performancePoiOskList, SummaryType summaryType, String types) {
        mSummaryPerformancePoiOsks = performancePoiOskList;
        mSummaryType = summaryType;
        mType = types;
    }

    public void setCallback(PerformancePoiOskListAdapter.Callback callback) {
        mCallback = callback;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new PerformancePoiOskListAdapter.ViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_performance_poiosk_list, parent, false));
            case VIEW_TYPE_EMPTY:
            default:
                return new PerformancePoiOskListAdapter.EmptyViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_detail_aoc, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mSummaryPerformancePoiOsks != null && mSummaryPerformancePoiOsks.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public int getItemCount() {
        if (mSummaryPerformancePoiOsks != null && mSummaryPerformancePoiOsks.size() > 0) {
            return mSummaryPerformancePoiOsks.size();
        } else {
            return 1;
        }
    }

    public void addItems(List<PerformanceSummary> summaryPerformancePoiOsks) {
        mSummaryPerformancePoiOsks.clear();
        mSummaryPerformancePoiOsks.addAll(summaryPerformancePoiOsks);
        mSummaryPerformancePoiOsksDefault = summaryPerformancePoiOsks;
        notifyDataSetChanged();
    }

    public void filterByNameLocation(String query) {
        List<PerformanceSummary> filteredSummaryPerformancePoiOsk = new ArrayList<>();
        for (PerformanceSummary performancePoiOsk : mSummaryPerformancePoiOsksDefault){
            if (performancePoiOsk.getTitlePoiOsk().toLowerCase().contains(query.toLowerCase()) ){
                filteredSummaryPerformancePoiOsk.add(performancePoiOsk);
            }
        }
        mSummaryPerformancePoiOsks.clear();
        mSummaryPerformancePoiOsks.addAll(filteredSummaryPerformancePoiOsk);
        notifyDataSetChanged();
    }

    public interface Callback {
        void onItemLocationListClick(int position);
    }

    public class ViewHolder extends BaseViewHolder {

        TextView AchCallPtd;
        TextView PlanCallPtd;
        TextView ActualCallPtd;
        TextView EffectiveCallPtd;
        TextView ActualMarketSurveyPtd;
        TextView ActualSalesPtd;
        TextView OutletProdPtd;
        TextView AchCallMtd;
        TextView PlanCallMtd;
        TextView ActualCallMtd;
        TextView EffectiveCallMtd;
        TextView ActualMarketSurveyMtd;
        TextView ActualSalesMtd;
        TextView OutletProdMtd;
        LinearLayout lineOutlet, lineMarket;

        TextView tvTitle;
        CardView cvSummaryPerformancePoiOsk;

        public ViewHolder(View itemView) {
            super(itemView);

            AchCallPtd = itemView.findViewById(R.id.tv_ptd_ach_performance_list_summary);
            PlanCallPtd = itemView.findViewById(R.id.tv_ptd_target_performance_performance_list_summary);
            ActualCallPtd = itemView.findViewById(R.id.tv_ptd_performance_actual_performance_list_summary);
            EffectiveCallPtd = itemView.findViewById(R.id.tv_ptd_effective_performance_performance_list_summary);
            ActualMarketSurveyPtd = itemView.findViewById(R.id.tv_ptd_market_performance_performance_list_summary);
            ActualSalesPtd = itemView.findViewById(R.id.tv_ptd_sales_performance_performance_list_summary);
            OutletProdPtd = itemView.findViewById(R.id.tv_ptd_outlet_performance_performance_list_summary);
            AchCallMtd = itemView.findViewById(R.id.tv_mtd_ach_performance_list_summary);
            PlanCallMtd = itemView.findViewById(R.id.tv_mtd_target_performance_performance_list_summary);
            ActualCallMtd = itemView.findViewById(R.id.tv_mtd_performance_actual_performance_list_summary);
            EffectiveCallMtd = itemView.findViewById(R.id.tv_mtd_effective_performance_performance_list_summary);
            ActualMarketSurveyMtd = itemView.findViewById(R.id.tv_mtd_market_performance_performance_list_summary);
            ActualSalesMtd = itemView.findViewById(R.id.tv_mtd_sales_performance_performance_list_summary);
            OutletProdMtd = itemView.findViewById(R.id.tv_mtd_outlet_performance_performance_list_summary);
            lineOutlet = itemView.findViewById(R.id.line_outletprod_performance);
            lineMarket = itemView.findViewById(R.id.line_marketsurvey_performance);

            cvSummaryPerformancePoiOsk = itemView.findViewById(R.id.cv_performance_performance_list_summary);
            tvTitle = itemView.findViewById(R.id.tv_title_performance_list_summary);


        }

        protected void clear() {

        }

        public void onBind(int position) {
            super.onBind(position);

            PerformanceSummary item = mSummaryPerformancePoiOsks.get(position);
            Log.d("Debug",mSummaryPerformancePoiOsks.toString());

            String color = "#" + Integer.toHexString(ContextCompat.getColor(itemView.getContext(), mSummaryType.getColor()));
            Log.d("Debug : ",color);

            if (mType.toLowerCase().equals("channel")){
                String locationType = item.getLocationType().toLowerCase();
                if (locationType.toLowerCase().equals("poi")){
                    lineOutlet.setVisibility(View.GONE);
                    lineMarket.setVisibility(View.VISIBLE);
                }else {
                    lineOutlet.setVisibility(View.VISIBLE);
                    lineMarket.setVisibility(View.GONE);
                }
            }



            AchCallPtd.setText(item.getAchCallPtd());
            setTextOrnull(PlanCallPtd, item.getPlanCallPtd(), ",");
            setTextOrnull(ActualCallPtd, item.getActualCallPtd(), ",");
            setTextOrnull(EffectiveCallPtd, item.getEffectiveCallPtd(), ",");
            setTextOrnull(OutletProdPtd, item.getOutletProdPtd(), ",");
            setTextOrnull(ActualMarketSurveyPtd, item.getActualMarketSurveyPtd(), ",");
            setTextOrnull(ActualSalesPtd, item.getActualSalesPtd(), ",");
            AchCallMtd.setText(item.getAchCallMtd());
            setTextOrnull(PlanCallMtd, item.getPlanCallMtd(), ",");
            setTextOrnull(ActualCallMtd, item.getActualCallMtd(), ",");
            setTextOrnull(EffectiveCallMtd, item.getEffectiveCallMtd(), ",");
            setTextOrnull(OutletProdMtd, item.getOutletProdMtd(), ",");
            setTextOrnull(ActualMarketSurveyMtd, item.getActualMarketSurveyMtd(), ",");
            setTextOrnull(ActualSalesMtd, item.getActualSalesMtd(), ",");
            cvSummaryPerformancePoiOsk.setCardBackgroundColor(Color.parseColor(color));
            tvTitle.setText(item.getTitlePoiOsk());


            itemView.setOnClickListener(v->{

            });

        }
    }

    public class EmptyViewHolder extends BaseViewHolder {


        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void clear() {

        }

    }

    private void setTextOrnull(TextView textView, String text,String option) {
        if (option.equals(",")){
            DecimalFormat decim = new DecimalFormat("#,###,###");
            textView.setText(decim.format(Long.valueOf(text)));
        }else {
            if (text == null)
                textView.setText("0" + option);
            else {
                textView.setText(text + option);
            }
        }
    }
}
