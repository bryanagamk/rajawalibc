package com.androiddeveloper.pens.namiapp.ui.performancepoiosklist;

import com.androiddeveloper.pens.namiapp.di.PerActivity;
import com.androiddeveloper.pens.namiapp.ui.base.MvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.summary.fragment.SummaryType;

import java.util.Map;

@PerActivity
public interface PerformancePoiOskListMvpPresenter<V extends PerformancePoiOskListMvpView>
        extends MvpPresenter<V> {

    void onViewPrepared(SummaryType type, String territory, String userId_aoc, String username_aoc, String period_type);
}
