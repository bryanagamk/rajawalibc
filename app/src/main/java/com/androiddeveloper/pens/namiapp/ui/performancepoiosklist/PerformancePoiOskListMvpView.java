package com.androiddeveloper.pens.namiapp.ui.performancepoiosklist;

import com.androiddeveloper.pens.namiapp.data.network.model.PerformancePoiOsk;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceSummary;
import com.androiddeveloper.pens.namiapp.ui.base.MvpView;

import java.util.List;

public interface PerformancePoiOskListMvpView extends MvpView {

    void updatePerformanceSummary(List<PerformanceSummary> performanceSummaryList);

}
