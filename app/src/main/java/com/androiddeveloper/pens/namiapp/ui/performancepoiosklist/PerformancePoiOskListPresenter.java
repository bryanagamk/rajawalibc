package com.androiddeveloper.pens.namiapp.ui.performancepoiosklist;

import android.util.Log;

import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceEvent;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformancePoiOsk;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformancePoiOskResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceSummary;
import com.androiddeveloper.pens.namiapp.ui.base.BasePresenter;
import com.androiddeveloper.pens.namiapp.ui.summary.fragment.SummaryType;
import com.androidnetworking.error.ANError;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class PerformancePoiOskListPresenter<V extends PerformancePoiOskListMvpView> extends BasePresenter<V>
        implements PerformancePoiOskListMvpPresenter<V> {

    @Inject
    public PerformancePoiOskListPresenter(DataManager dataManager, CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);
    }

    @Override
    public void onViewPrepared(SummaryType type,String territory, String userId_aoc, String username_aoc, String period_type) {
        Log.d("Debug : ",type.getType() + territory);
        if (type.getType().equals("ptd")){
            loadPerformancePtdPoiOsks(type, territory, userId_aoc, username_aoc, period_type);
        }else if (type.getType().equals("mtd")){
            loadPerformanceMtdPoiOsks(type, territory, userId_aoc, username_aoc, period_type);
        }

    }


    private void loadPerformancePtdPoiOsks(SummaryType type, String territory, String userId_aoc, String username_aoc, String period_type){
        String userId = getDataManager().getCurrentUserId();
        getCompositeDisposable().add(getDataManager().getSummaryPerformancePoiOsk(userId,territory, userId_aoc, username_aoc, period_type)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(performancePoiOskResponse -> {
                    if (!isViewAttached()){
                        return;
                    }
                    Log.d("DEbug",performancePoiOskResponse.toString());
                    if (performancePoiOskResponse != null && performancePoiOskResponse.isSuccess()){
                        List<PerformanceSummary> performanceSummaries = performanceSummariesPtdConverters(performancePoiOskResponse.getSummaryPerformancePoiOsk(),type);
                        getMvpView().updatePerformanceSummary(performanceSummaries);
                    }

                    getMvpView().hideLoading();
                },throwable -> {
                    if (!isViewAttached()){
                        return;
                    }
                    Log.d("Error : ",throwable.toString());

                    getMvpView().hideLoading();

                    // handle the error here
                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

    private void loadPerformanceMtdPoiOsks(SummaryType type, String territory, String userId_aoc, String username_aoc, String period_type){
        String userId = getDataManager().getCurrentUserId();
        getCompositeDisposable().add(getDataManager().getSummaryPerformancePoiOsk(userId,territory, userId_aoc, username_aoc, period_type)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(performancePoiOskResponse -> {
                    if (!isViewAttached()){
                        return;
                    }
                    Log.d("DEbug",performancePoiOskResponse.toString());
                    if (performancePoiOskResponse != null && performancePoiOskResponse.isSuccess()){
                        List<PerformanceSummary> performanceSummaries = performanceSummariesMtdConverters(performancePoiOskResponse.getSummaryPerformancePoiOsk(),type);
                        getMvpView().updatePerformanceSummary(performanceSummaries);
                    }

                    getMvpView().hideLoading();
                },throwable -> {
                    if (!isViewAttached()){
                        return;
                    }
                    Log.d("Error : ",throwable.toString());
                    getMvpView().hideLoading();

                    // handle the error here
                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

    private List<PerformanceSummary> performanceSummariesPtdConverters(List<PerformancePoiOsk> performanceSummariesPtd,
                                                                      SummaryType type){
        if (type == SummaryType.PJP_PTD_POI_OSK){
            List<PerformanceSummary> performanceSummaries = new ArrayList<>();
            for (PerformancePoiOsk summaryPtd : performanceSummariesPtd){
                performanceSummaries.add(summaryPtd.getPjpPtdPoiOskDetail());
            }
            return performanceSummaries;
        }else{
            return null;
        }
    }

    private List<PerformanceSummary> performanceSummariesMtdConverters(List<PerformancePoiOsk> performanceSummariesMtd,
                                                                      SummaryType type){
        if (type == SummaryType.PJP_MTD_POI_OSK){
            List<PerformanceSummary> performanceSummaries = new ArrayList<>();
            for (PerformancePoiOsk summaryMtd : performanceSummariesMtd){
                performanceSummaries.add(summaryMtd.getPjpMtdPoiOskDetail());
            }
            return performanceSummaries;
        }else{
            return null;
        }
    }
}
