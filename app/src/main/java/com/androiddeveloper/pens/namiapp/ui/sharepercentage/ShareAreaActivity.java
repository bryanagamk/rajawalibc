package com.androiddeveloper.pens.namiapp.ui.sharepercentage;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.ui.base.BaseActivity;
import com.androiddeveloper.pens.namiapp.ui.sharepercentage.fragmentshare.ShareAreaFragment;
import com.androiddeveloper.pens.namiapp.ui.sharepercentage.fragmentshare.ShareType;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ShareAreaActivity extends BaseActivity implements ShareAreaMvpView {

    @Inject
    ShareAreaMvpPresenter<ShareAreaMvpView> mPresenter;

    @BindView(R.id.search_view_share)
    MaterialSearchView searchViewShare;

    @BindView(R.id.toolbarSearchShare)
    Toolbar toolbarSearchShare;

    @BindView(R.id.fragment_share)
    FrameLayout frameLayoutShare;

    private TextView mTextMessage;
    private ShareType shareType;
    ShareAreaFragment shareAreaFragment;


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_regional_share:
                    shareAreaFragment = ShareAreaFragment.newInstance(shareType,"regional");
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragment_share,shareAreaFragment)
                            .commit();
                    return true;
                case R.id.navigation_branch_share:
                    shareAreaFragment = ShareAreaFragment.newInstance(shareType,"branch");
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragment_share,
                                    shareAreaFragment)
                            .commit();
                    return true;
                case R.id.navigation_cluster_share:
                    shareAreaFragment = ShareAreaFragment.newInstance(shareType,"cluster");
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragment_share,
                                    shareAreaFragment)
                            .commit();
                    return true;
                case R.id.navigation_city_share:
                    shareAreaFragment = ShareAreaFragment.newInstance(shareType,"city");
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragment_share,
                                    shareAreaFragment)
                            .commit();
                    return true;
                case R.id.navigation_poi_osk_share:
                    shareAreaFragment = ShareAreaFragment.newInstance(shareType,"channel");
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragment_share,
                                    shareAreaFragment)
                            .commit();
                    return true;
            }
            return false;
        }
    };

    public static Intent getStartIntent(Context context, String userType, ShareType type) {
        Intent intent = new Intent(context, ShareAreaActivity.class);
        intent.putExtra("type",type);
        intent.putExtra("area",userType);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_area);

        getActivityComponent().inject(this);

        if (isNetworkAvailable()) {
            setUnBinder(ButterKnife.bind(this));

            mPresenter.onAttach(this);

            setUp();
        }
    }

    @Override
    protected void setUp() {
        shareType = (ShareType) getIntent().getSerializableExtra("type");

        setupBottomNavigationMenu(getIntent().getStringExtra("area"));


        setSupportActionBar(toolbarSearchShare);
        setActionBarTitle();

        toolbarSearchShare.setNavigationIcon(R.drawable.ic_action_navigation_arrow_back_inverted);
        toolbarSearchShare.setTitleTextColor(getResources().getColor(R.color.white));
        searchViewShare.setEllipsize(true);
        toolbarSearchShare.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void setActionBarTitle() {
        getSupportActionBar().setTitle(shareType.getTitle());
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    private void setupBottomNavigationMenu(String area){
        Log.d("Error : ",area);
        mTextMessage = findViewById(R.id.message);
        BottomNavigationView navigation = findViewById(R.id.navigation_share);

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        switch (area.toLowerCase()){
            case "mgt1":
                shareAreaFragment = ShareAreaFragment.newInstance(shareType,"regional");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_share,shareAreaFragment)
                        .commit();
                break;
            case "mgt2":
                navigation.getMenu().removeItem(R.id.navigation_regional_share);
                shareAreaFragment = ShareAreaFragment.newInstance(shareType,"branch");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_share,shareAreaFragment)
                        .commit();
                break;
            case "mgt3":
                navigation.getMenu().removeItem(R.id.navigation_regional_share);
                navigation.getMenu().removeItem(R.id.navigation_branch_share);

                shareAreaFragment = ShareAreaFragment.newInstance(shareType,"cluster");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_share,shareAreaFragment)
                        .commit();
                break;
            case "mgt4":
                navigation.getMenu().removeItem(R.id.navigation_regional_share);
                navigation.getMenu().removeItem(R.id.navigation_branch_share);

                shareAreaFragment = ShareAreaFragment.newInstance(shareType,"cluster");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_share,shareAreaFragment)
                        .commit();
                break;
            case "mgt5":
                navigation.getMenu().removeItem(R.id.navigation_regional_share);
                navigation.getMenu().removeItem(R.id.navigation_branch_share);

                shareAreaFragment = ShareAreaFragment.newInstance(shareType,"cluster");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_share,shareAreaFragment)
                        .commit();
                break;
            case "adm2":
                navigation.getMenu().removeItem(R.id.navigation_regional_share);
                shareAreaFragment = ShareAreaFragment.newInstance(shareType,"branch");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_share,shareAreaFragment)
                        .commit();
                break;
            case "adm3":
                navigation.getMenu().removeItem(R.id.navigation_regional_share);
                navigation.getMenu().removeItem(R.id.navigation_branch_share);

                shareAreaFragment = ShareAreaFragment.newInstance(shareType,"cluster");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_share,shareAreaFragment)
                        .commit();
                break;
            case "bm":
                navigation.getMenu().removeItem(R.id.navigation_regional_share);
                navigation.getMenu().removeItem(R.id.navigation_branch_share);
                navigation.getMenu().removeItem(R.id.navigation_cluster_share);

                shareAreaFragment = ShareAreaFragment.newInstance(shareType,"city");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_share,shareAreaFragment)
                        .commit();
                break;
            case "tl":
                navigation.getMenu().removeItem(R.id.navigation_regional_share);
                navigation.getMenu().removeItem(R.id.navigation_branch_share);
                navigation.getMenu().removeItem(R.id.navigation_cluster_share);

                shareAreaFragment = ShareAreaFragment.newInstance(shareType,"city");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_share,shareAreaFragment)
                        .commit();
                break;
            case "aoc":
                navigation.getMenu().removeItem(R.id.navigation_regional_share);
                navigation.getMenu().removeItem(R.id.navigation_branch_share);
                navigation.getMenu().removeItem(R.id.navigation_cluster_share);
                navigation.getMenu().removeItem(R.id.navigation_city_share);
                navigation.getMenu().removeItem(R.id.navigation_poi_osk_share);

                shareAreaFragment = ShareAreaFragment.newInstance(shareType,"regional");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_share,shareAreaFragment)
                        .commit();
                break;
            default:
                navigation.getMenu().removeItem(R.id.navigation_regional_share);
                navigation.getMenu().removeItem(R.id.navigation_branch_share);
                navigation.getMenu().removeItem(R.id.navigation_cluster_share);
                navigation.getMenu().removeItem(R.id.navigation_city_share);
                navigation.getMenu().removeItem(R.id.navigation_poi_osk_share);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (searchViewShare.isSearchOpen()) {
            searchViewShare.closeSearch();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_summary, menu);

        MenuItem item = menu.findItem(R.id.action_search_summary);
        searchViewShare.setMenuItem(item);

        return true;
    }

    public interface OnSearchInterface{
        void search(String query);
    }

    public void onSearchShareSet(ShareAreaActivity.OnSearchInterface onSearch){
        searchViewShare.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                onSearch.search(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                onSearch.search(newText);
                return true;
            }
        });

        searchViewShare.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
                //Do some magic
            }

            @Override
            public void onSearchViewClosed() {
                //Do some magic
            }
        });
    }

}
