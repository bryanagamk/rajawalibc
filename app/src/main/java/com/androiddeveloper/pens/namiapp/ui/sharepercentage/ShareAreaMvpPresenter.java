package com.androiddeveloper.pens.namiapp.ui.sharepercentage;

import com.androiddeveloper.pens.namiapp.di.PerActivity;
import com.androiddeveloper.pens.namiapp.ui.base.MvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.sharepercentage.fragmentshare.ShareType;

@PerActivity
public interface ShareAreaMvpPresenter<V extends ShareAreaMvpView> extends MvpPresenter<V> {
}
