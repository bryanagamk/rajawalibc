package com.androiddeveloper.pens.namiapp.ui.sharepercentage;

import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.ui.base.BasePresenter;
import com.androiddeveloper.pens.namiapp.ui.sharepercentage.fragmentshare.ShareType;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class ShareAreaPresenter<V extends ShareAreaMvpView> extends BasePresenter<V>
        implements ShareAreaMvpPresenter<V> {

    @Inject
    public ShareAreaPresenter(DataManager dataManager, CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);
    }

}
