package com.androiddeveloper.pens.namiapp.ui.sharepercentage.fragmentshare;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceShareSummary;
import com.androiddeveloper.pens.namiapp.di.componen.ActivityComponent;
import com.androiddeveloper.pens.namiapp.ui.base.BaseFragment;
import com.androiddeveloper.pens.namiapp.ui.sharepercentage.ShareAreaActivity;
import com.androiddeveloper.pens.namiapp.ui.sharepercentage.performancesharelistadapter.SummaryPerformanceShareAdapter;
import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ShareAreaFragment extends BaseFragment
        implements ShareAreaFragmentMvpView {

    private static final String TAG = "ShareAreaFragment";

    @Inject
    ShareAreaFragmentMvpPresenter<ShareAreaFragmentMvpView> mPresenter;

    SummaryPerformanceShareAdapter summaryPerformanceShareAdapter;

    @Inject
    LinearLayoutManager mLayoutManager;

    @BindView(R.id.rv_share_summary)
    RecyclerView mRecyclerView;

    @BindView(R.id.root_share)
    ConstraintLayout root;

    @BindView(R.id.shimmer_view_container_share)
    ShimmerFrameLayout mShimmerViewContainer;

    public ShareAreaFragment() {
        // Required empty public constructor
    }

    List<PerformanceShareSummary> performanceShareSummaryList;

    public static ShareAreaFragment newInstance(ShareType type, String territory) {
        Bundle args = new Bundle();
        args.putSerializable("type",type);
        args.putSerializable("territory",territory);
        ShareAreaFragment fragment = new ShareAreaFragment();

        Log.d("Debug ", String.format("Type  : %s, Territory : %s",type,territory));

        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_share_area, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null && isNetworkAvailable()) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {
        ShareType type = (ShareType) getArguments().getSerializable("type");
        String territory = (String) getArguments().getSerializable("territory");

        summaryPerformanceShareAdapter = new SummaryPerformanceShareAdapter(new ArrayList<>(), type, territory.toLowerCase());

        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        ShareAreaActivity shareAreaActivity = (ShareAreaActivity) getActivity();

        ShareAreaActivity.OnSearchInterface callback = null;

        if (type == ShareType.PERCENTAGE_SHARE){
            mRecyclerView.setAdapter(summaryPerformanceShareAdapter);
            callback = summaryPerformanceShareAdapter;
        }

        shareAreaActivity.onSearchShareSet(callback);

        mPresenter.onViewPrepared(type,territory);
    }

    @Override
    public void updatePerformanceShare(List<PerformanceShareSummary> performanceShareSummaryBsList) {
        Log.d("Debug",performanceShareSummaryBsList.size() + "");
        performanceShareSummaryList = performanceShareSummaryBsList;
        summaryPerformanceShareAdapter.addItems(performanceShareSummaryBsList);
        summaryPerformanceShareAdapter.notifyDataSetChanged();
    }


    @Override
    public void showErrorPage(String message) {

    }

    @Override
    public boolean isShowingError() {
        return false;
    }

    @Override
    public void finish() {

    }


    @Override
    public void showLoading(String message) {
        super.showLoading(message);
        mShimmerViewContainer.startShimmer();
        mShimmerViewContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        // stop animating Shimmer and hide the layout
        mShimmerViewContainer.stopShimmer();
        mShimmerViewContainer.setVisibility(View.GONE);
    }


    @Override
    public void onDestroyView() {
        mPresenter.onDetachFragment();
        super.onDestroyView();
    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmer();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmer();
        super.onPause();
    }
}
