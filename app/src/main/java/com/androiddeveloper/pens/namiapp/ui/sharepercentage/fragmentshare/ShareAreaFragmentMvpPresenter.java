package com.androiddeveloper.pens.namiapp.ui.sharepercentage.fragmentshare;

import com.androiddeveloper.pens.namiapp.ui.base.MvpPresenter;

public interface ShareAreaFragmentMvpPresenter<V extends ShareAreaFragmentMvpView>
        extends MvpPresenter<V> {

    void onViewPrepared(ShareType type, String territory);
}
