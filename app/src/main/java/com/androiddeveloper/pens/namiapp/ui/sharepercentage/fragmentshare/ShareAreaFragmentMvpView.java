package com.androiddeveloper.pens.namiapp.ui.sharepercentage.fragmentshare;

import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceShareSummary;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceSummary;
import com.androiddeveloper.pens.namiapp.ui.base.MvpView;

import java.util.List;

public interface ShareAreaFragmentMvpView extends MvpView {

    void updatePerformanceShare(List<PerformanceShareSummary> performanceShareSummaryBsList);
}
