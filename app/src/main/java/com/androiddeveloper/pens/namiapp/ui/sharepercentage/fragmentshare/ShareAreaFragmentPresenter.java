package com.androiddeveloper.pens.namiapp.ui.sharepercentage.fragmentshare;

import android.util.Log;

import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceShare;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceShareSummary;
import com.androiddeveloper.pens.namiapp.ui.base.BasePresenter;
import com.androidnetworking.error.ANError;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class ShareAreaFragmentPresenter<V extends ShareAreaFragmentMvpView> extends BasePresenter<V>
        implements ShareAreaFragmentMvpPresenter<V> {

    @Inject
    public ShareAreaFragmentPresenter(DataManager dataManager, CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);
    }

    @Override
    public void onViewPrepared(ShareType type, String territory) {
        Log.d("Debug : ",type.getType() + territory);
        if (type.getType().equals("Share")){
            loadPerformanceShare(type, territory);
        }
    }

    private void loadPerformanceShare(ShareType type, String territory){
        String userId = getDataManager().getCurrentUserId();
        getCompositeDisposable().add(getDataManager().getSummaryPerformanceShare(userId,territory)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(performanceShareResponse -> {
                    if (!isViewAttached())
                        return ;
                    if (performanceShareResponse != null && performanceShareResponse.isSuccess()){
                        List<PerformanceShareSummary> performanceSummaries = performanceShareConverter(performanceShareResponse.getSummaryPerformanceShare(),type);
                        getMvpView().updatePerformanceShare(performanceSummaries);
                    }

                    getMvpView().hideLoading();
                },throwable -> {
                    if (!isViewAttached())
                        return ;
                    Log.d("Error : ",throwable.toString());

                    getMvpView().hideLoading();

                    // handle the error here
                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

    private List<PerformanceShareSummary> performanceShareConverter(List<PerformanceShare> performanceShares,
                                                                    ShareType type){

        if (type == ShareType.PERCENTAGE_SHARE){
            List<PerformanceShareSummary> performanceSummaries = new ArrayList<>();
            for (PerformanceShare summaryShare : performanceShares){
                performanceSummaries.add(summaryShare.getSummaryPerfomanceShare());
            }
            return performanceSummaries;
        }else {
            return null;
        }
    }
}
