package com.androiddeveloper.pens.namiapp.ui.sharepercentage.fragmentshare;

import com.androiddeveloper.pens.namiapp.R;

public enum ShareType {
    PERCENTAGE_SHARE("Share", "Zona Marking", R.color.translucent_white);

    private final String mType;
    private final String mTitle;
    private final int mColorStateList;
    private int mDrawable;

    ShareType(String type, String title, int colorStateList){
        mType = type;
        mTitle = title;
        mColorStateList = colorStateList;
    }

    ShareType(String type, String title, int colorStateList, int mDrawable) {
        mType = type;
        mTitle = title;
        mColorStateList = colorStateList;
        this.mDrawable = mDrawable;
    }

    public String getType() {
        return mType;
    }
    public String getTitle() {
        return mTitle;
    }
    public int getColor() {
        return mColorStateList;
    }

    public int getmDrawable() {
        return mDrawable;
    }
}
