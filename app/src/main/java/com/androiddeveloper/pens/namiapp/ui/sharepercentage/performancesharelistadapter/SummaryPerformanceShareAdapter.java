package com.androiddeveloper.pens.namiapp.ui.sharepercentage.performancesharelistadapter;

import android.animation.ValueAnimator;
import android.content.res.ColorStateList;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceShare;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceShareSummary;
import com.androiddeveloper.pens.namiapp.ui.base.BaseViewHolder;
import com.androiddeveloper.pens.namiapp.ui.sharepercentage.ShareAreaActivity;
import com.androiddeveloper.pens.namiapp.ui.sharepercentage.fragmentshare.ShareType;
import com.dinuscxj.progressbar.CircleProgressBar;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

public class SummaryPerformanceShareAdapter extends RecyclerView.Adapter<BaseViewHolder> implements ShareAreaActivity.OnSearchInterface {
    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private SummaryPerformanceShareAdapter.Callback mCallback;
    private List<PerformanceShareSummary> mSummaryPerformanceShare;
    private List<PerformanceShareSummary> mSummaryPerformanceShareDefault;
    private ShareType mShareType;
    private String mType;

    ColorStateList color;

    public SummaryPerformanceShareAdapter(List<PerformanceShareSummary> performanceShareSummaryList, ShareType shareType, String Types) {
        mSummaryPerformanceShare = performanceShareSummaryList;
        mShareType = shareType;
        mType = Types;
    }

    public void setCallback(SummaryPerformanceShareAdapter.Callback callback) {
        mCallback = callback;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new SummaryPerformanceShareAdapter.ViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_performance_share_summary, parent, false));
            case VIEW_TYPE_EMPTY:
            default:
                return new SummaryPerformanceShareAdapter.EmptyViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_summary, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mSummaryPerformanceShare != null && mSummaryPerformanceShare.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public int getItemCount() {
        if (mSummaryPerformanceShare != null && mSummaryPerformanceShare.size() > 0) {
            return mSummaryPerformanceShare.size();
        } else {
            return 1;
        }
    }

    public void addItems(List<PerformanceShareSummary> performanceShareSummaryList) {
        mSummaryPerformanceShare.clear();
        mSummaryPerformanceShare.addAll(performanceShareSummaryList);
        mSummaryPerformanceShareDefault = performanceShareSummaryList;
        notifyDataSetChanged();
    }

    @Override
    public void search(String query) {
        List<PerformanceShareSummary> filteredSummaryPerformanceShare = new ArrayList<>();
        for (PerformanceShareSummary performanceShare : mSummaryPerformanceShareDefault){
            if (performanceShare.getTitle().toLowerCase().contains(query.toLowerCase()) ){
                filteredSummaryPerformanceShare.add(performanceShare);
            }
        }
        mSummaryPerformanceShare.clear();
        mSummaryPerformanceShare.addAll(filteredSummaryPerformanceShare);
        notifyDataSetChanged();
    }

    public interface Callback {
        void onBlogEmptyViewRetryClick();
    }

    public class ViewHolder extends BaseViewHolder {

        CircleProgressBar cpTselBs;
        CircleProgressBar cpOoredooBs;
        CircleProgressBar cpXlBs;
        CircleProgressBar cpAxisBs;
        CircleProgressBar cp3Bs;
        CircleProgressBar cpOthersBs;

        CircleProgressBar cpTselDs;
        CircleProgressBar cpOoredooDs;
        CircleProgressBar cpXlDs;
        CircleProgressBar cpAxisDs;
        CircleProgressBar cp3Ds;
        CircleProgressBar cpOthersDs;

        CircleProgressBar cpTselRs;
        CircleProgressBar cpOoredooRs;
        CircleProgressBar cpXlRs;
        CircleProgressBar cpAxisRs;
        CircleProgressBar cp3Rs;
        CircleProgressBar cpOthersRs;

        CircleProgressBar cpTselMs;
        CircleProgressBar cpOoredooMs;
        CircleProgressBar cpXlMs;
        CircleProgressBar cpAxisMs;
        CircleProgressBar cp3Ms;
        CircleProgressBar cpOthersMs;

        LinearLayout lineBs, lineDs, lineRs, lineMs;


        TextView tvTitle;

        public ViewHolder(View itemView) {
            super(itemView);

            cpTselBs = itemView.findViewById(R.id.persentage_tselbs_summary);
            cpOoredooBs = itemView.findViewById(R.id.persentage_ooredoobs_summary);
            cpXlBs = itemView.findViewById(R.id.persentage_xlbs_summary);
            cpAxisBs = itemView.findViewById(R.id.persentage_axisbs_summary);
            cp3Bs = itemView.findViewById(R.id.persentage_3bs_summary);
            cpOthersBs = itemView.findViewById(R.id.persentage_othersbs_summary);
            tvTitle = itemView.findViewById(R.id.tv_title_share_bs_summary);

            cpTselDs = itemView.findViewById(R.id.persentage_tselds_summary);
            cpOoredooDs = itemView.findViewById(R.id.persentage_ooredoods_summary);
            cpXlDs = itemView.findViewById(R.id.persentage_xlds_summary);
            cpAxisDs = itemView.findViewById(R.id.persentage_axisds_summary);
            cp3Ds = itemView.findViewById(R.id.persentage_3ds_summary);
            cpOthersDs = itemView.findViewById(R.id.persentage_othersds_summary);

            cpTselRs = itemView.findViewById(R.id.persentage_tselrs_summary);
            cpOoredooRs = itemView.findViewById(R.id.persentage_ooredoors_summary);
            cpXlRs = itemView.findViewById(R.id.persentage_xlrs_summary);
            cpAxisRs = itemView.findViewById(R.id.persentage_axisrs_summary);
            cp3Rs = itemView.findViewById(R.id.persentage_3rs_summary);
            cpOthersRs = itemView.findViewById(R.id.persentage_othersrs_summary);

            cpTselMs = itemView.findViewById(R.id.persentage_tselms_summary);
            cpOoredooMs = itemView.findViewById(R.id.persentage_ooredooms_summary);
            cpXlMs = itemView.findViewById(R.id.persentage_xlms_summary);
            cpAxisMs = itemView.findViewById(R.id.persentage_axisms_summary);
            cp3Ms = itemView.findViewById(R.id.persentage_3ms_summary);
            cpOthersMs = itemView.findViewById(R.id.persentage_othersms_summary);

            lineBs = itemView.findViewById(R.id.line_info_share_bs_summary);
            lineDs = itemView.findViewById(R.id.line_info_share_ds_summary);
            lineRs = itemView.findViewById(R.id.line_info_share_rs_summary);
            lineMs = itemView.findViewById(R.id.line_info_share_ms_summary);

            tvTitle = itemView.findViewById(R.id.tv_title_type_share);

        }

        protected void clear() {

        }

        public void onBind(int position) {
            super.onBind(position);

            List<Integer> bs = new ArrayList<>();
            List<Integer> ds = new ArrayList<>();
            List<Integer> rs = new ArrayList<>();
            List<Integer> ms = new ArrayList<>();

            PerformanceShareSummary item = mSummaryPerformanceShare.get(position);
            Log.d("Debug",mSummaryPerformanceShare.toString());

            String color = "#" + Integer.toHexString(ContextCompat.getColor(itemView.getContext(), mShareType.getColor()));
            Log.d("Debug : ",color);

            if (mType.toLowerCase().equals("channel")){
                String locationType = item.getLocationType().toLowerCase();
                if (locationType.toLowerCase().equals("poi")){
                    lineBs.setVisibility(View.GONE);
                    lineDs.setVisibility(View.GONE);
                    lineRs.setVisibility(View.GONE);
                    lineMs.setVisibility(View.VISIBLE);
                }else {
                    lineBs.setVisibility(View.VISIBLE);
                    lineDs.setVisibility(View.VISIBLE);
                    lineRs.setVisibility(View.VISIBLE);
                    lineMs.setVisibility(View.GONE);
                }
            }

            bs.add(Math.round(Float.valueOf(item.getCpTselBs())));
            bs.add(Math.round(Float.valueOf(item.getCpOoredooBs())));
            bs.add(Math.round(Float.valueOf(item.getCpXlBs())));
            bs.add(Math.round(Float.valueOf(item.getCpAxisBs())));
            bs.add(Math.round(Float.valueOf(item.getCp3Bs())));
            bs.add(Math.round(Float.valueOf(item.getCpOthersBs())));

            ds.add(Math.round(Float.valueOf(item.getCpTselDs())));
            ds.add(Math.round(Float.valueOf(item.getCpOoredooDs())));
            ds.add(Math.round(Float.valueOf(item.getCpXlDs())));
            ds.add(Math.round(Float.valueOf(item.getCpAxisDs())));
            ds.add(Math.round(Float.valueOf(item.getCp3Ds())));
            ds.add(Math.round(Float.valueOf(item.getCpOthersDs())));

            rs.add(Math.round(Float.valueOf(item.getCpTselRs())));
            rs.add(Math.round(Float.valueOf(item.getCpOoredooRs())));
            rs.add(Math.round(Float.valueOf(item.getCpXlRs())));
            rs.add(Math.round(Float.valueOf(item.getCpAxisRs())));
            rs.add(Math.round(Float.valueOf(item.getCp3Rs())));
            rs.add(Math.round(Float.valueOf(item.getCpOthersRs())));

            ms.add(Math.round(Float.valueOf(item.getCpTselMs())));
            ms.add(Math.round(Float.valueOf(item.getCpOoredooMs())));
            ms.add(Math.round(Float.valueOf(item.getCpXlMs())));
            ms.add(Math.round(Float.valueOf(item.getCpAxisMs())));
            ms.add(Math.round(Float.valueOf(item.getCp3Ms())));
            ms.add(Math.round(Float.valueOf(item.getCpOthersMs())));
            tvTitle.setText(item.getTitle());

            ValueAnimator animatortselbs = ValueAnimator.ofInt(0, bs.get(0));
            ValueAnimator animatorooredoobs = ValueAnimator.ofInt(0, bs.get(1));
            ValueAnimator animatorxlbs = ValueAnimator.ofInt(0, bs.get(2));
            ValueAnimator animatoraxisbs = ValueAnimator.ofInt(0, bs.get(3));
            ValueAnimator animator3bs = ValueAnimator.ofInt(0, bs.get(4));
            ValueAnimator animatorothersbs = ValueAnimator.ofInt(0, bs.get(5));
            // bs
            animatortselbs.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    Integer progress = (Integer) animation.getAnimatedValue();
                    try{
                        if ( progress != null) {
                            cpTselBs.setProgress(progress);
                        }else {
                            cpTselBs.setProgress(0);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
            animatortselbs.setDuration(100);
            animatortselbs.start();
            animatorooredoobs.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    Integer progress = (Integer) animation.getAnimatedValue();
                    try{
                        if ( progress != null) {
                            cpOoredooBs.setProgress(progress);
                        }else {
                            cpOoredooBs.setProgress(0);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
            animatorooredoobs.setDuration(100);
            animatorooredoobs.start();
            animatorxlbs.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    Integer progress = (Integer) animation.getAnimatedValue();
                    try{
                        if ( progress != null) {
                            cpXlBs.setProgress(progress);
                        }else {
                            cpXlBs.setProgress(0);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
            animatorxlbs.setDuration(100);
            animatorxlbs.start();
            animatoraxisbs.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    Integer progress = (Integer) animation.getAnimatedValue();
                    try{
                        if ( progress != null) {
                            cpAxisBs.setProgress(progress);
                        }else {
                            cpAxisBs.setProgress(0);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
            animatoraxisbs.setDuration(100);
            animatoraxisbs.start();
            animator3bs.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    Integer progress = (Integer) animation.getAnimatedValue();
                    try{
                        if ( progress != null) {
                            cp3Bs.setProgress(progress);
                        }else {
                            cp3Bs.setProgress(0);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
            animator3bs.setDuration(100);
            animator3bs.start();
            animatorothersbs.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    Integer progress = (Integer) animation.getAnimatedValue();
                    try{
                        if ( progress != null) {
                            cpOthersBs.setProgress(progress);
                        }else {
                            cpOthersBs.setProgress(0);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
            animatorothersbs.setDuration(100);
            animatorothersbs.start();
            // ds
            ValueAnimator animatortselds = ValueAnimator.ofInt(0, ds.get(0));
            ValueAnimator animatorooredoods = ValueAnimator.ofInt(0, ds.get(1));
            ValueAnimator animatorxlds = ValueAnimator.ofInt(0, ds.get(2));
            ValueAnimator animatoraxisds = ValueAnimator.ofInt(0, ds.get(3));
            ValueAnimator animator3ds = ValueAnimator.ofInt(0, ds.get(4));
            ValueAnimator animatorothersds = ValueAnimator.ofInt(0, ds.get(5));
            // ds
            animatortselds.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    Integer progress = (Integer) animation.getAnimatedValue();
                    try{
                        if ( progress != null) {
                            cpTselDs.setProgress(progress);
                        }else {
                            cpTselDs.setProgress(0);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
            animatortselds.setDuration(100);
            animatortselds.start();
            animatorooredoods.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    Integer progress = (Integer) animation.getAnimatedValue();
                    try{
                        if ( progress != null) {
                            cpOoredooDs.setProgress(progress);
                        }else {
                            cpOoredooDs.setProgress(0);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
            animatorooredoods.setDuration(100);
            animatorooredoods.start();
            animatorxlds.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    Integer progress = (Integer) animation.getAnimatedValue();
                    try{
                        if ( progress != null) {
                            cpXlDs.setProgress(progress);
                        }else {
                            cpXlDs.setProgress(0);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
            animatorxlds.setDuration(100);
            animatorxlds.start();
            animatoraxisds.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    Integer progress = (Integer) animation.getAnimatedValue();
                    try{
                        if ( progress != null) {
                            cpAxisDs.setProgress(progress);
                        }else {
                            cpAxisDs.setProgress(0);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
            animatoraxisds.setDuration(100);
            animatoraxisds.start();
            animator3ds.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    Integer progress = (Integer) animation.getAnimatedValue();
                    try{
                        if ( progress != null) {
                            cp3Ds.setProgress(progress);
                        }else {
                            cp3Ds.setProgress(0);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
            animator3ds.setDuration(100);
            animator3ds.start();
            animatorothersds.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    Integer progress = (Integer) animation.getAnimatedValue();
                    try{
                        if ( progress != null) {
                            cpOthersDs.setProgress(progress);
                        }else {
                            cpOthersDs.setProgress(0);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
            animatorothersds.setDuration(100);
            animatorothersds.start();
            // rs
            ValueAnimator animatortselrs = ValueAnimator.ofInt(0, rs.get(0));
            ValueAnimator animatorooredoors = ValueAnimator.ofInt(0, rs.get(1));
            ValueAnimator animatorxlrs = ValueAnimator.ofInt(0, rs.get(2));
            ValueAnimator animatoraxisrs = ValueAnimator.ofInt(0, rs.get(3));
            ValueAnimator animator3rs = ValueAnimator.ofInt(0, rs.get(4));
            ValueAnimator animatorothersrs = ValueAnimator.ofInt(0, rs.get(5));

            // rs
            animatortselrs.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    Integer progress = (Integer) animation.getAnimatedValue();
                    try{
                        if ( progress != null) {
                            cpTselRs.setProgress(progress);
                        }else {
                            cpTselRs.setProgress(0);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
            animatortselrs.setDuration(100);
            animatortselrs.start();
            animatorooredoors.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    Integer progress = (Integer) animation.getAnimatedValue();
                    try{
                        if ( progress != null) {
                            cpOoredooRs.setProgress(progress);
                        }else {
                            cpOoredooRs.setProgress(0);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
            animatorooredoors.setDuration(100);
            animatorooredoors.start();
            animatorxlrs.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    Integer progress = (Integer) animation.getAnimatedValue();
                    try{
                        if ( progress != null) {
                            cpXlRs.setProgress(progress);
                        }else {
                            cpXlRs.setProgress(0);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
            animatorxlrs.setDuration(100);
            animatorxlrs.start();
            animatoraxisrs.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    Integer progress = (Integer) animation.getAnimatedValue();
                    try{
                        if ( progress != null) {
                            cpAxisRs.setProgress(progress);
                        }else {
                            cpAxisRs.setProgress(0);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
            animatoraxisrs.setDuration(100);
            animatoraxisrs.start();
            animator3rs.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    Integer progress = (Integer) animation.getAnimatedValue();
                    try{
                        if ( progress != null) {
                            cp3Rs.setProgress(progress);
                        }else {
                            cp3Rs.setProgress(0);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
            animator3rs.setDuration(100);
            animator3rs.start();
            animatorothersrs.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    Integer progress = (Integer) animation.getAnimatedValue();
                    try{
                        if ( progress != null) {
                            cpOthersRs.setProgress(progress);
                        }else {
                            cpOthersRs.setProgress(0);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
            animatorothersrs.setDuration(100);
            animatorothersrs.start();
            // ms
            ValueAnimator animatortselms = ValueAnimator.ofInt(0, ms.get(0));
            ValueAnimator animatorooredooms = ValueAnimator.ofInt(0, ms.get(1));
            ValueAnimator animatorxlms = ValueAnimator.ofInt(0, ms.get(2));
            ValueAnimator animatoraxisms = ValueAnimator.ofInt(0, ms.get(3));
            ValueAnimator animator3ms = ValueAnimator.ofInt(0, ms.get(4));
            ValueAnimator animatorothersms = ValueAnimator.ofInt(0, ms.get(5));

            // ms
            animatortselms.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    Integer progress = (Integer) animation.getAnimatedValue();
                    try{
                        if ( progress != null) {
                            cpTselMs.setProgress(progress);
                        }else {
                            cpTselMs.setProgress(0);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
            animatortselms.setDuration(100);
            animatortselms.start();
            animatorooredooms.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    Integer progress = (Integer) animation.getAnimatedValue();
                    try{
                        if ( progress != null) {
                            cpOoredooMs.setProgress(progress);
                        }else {
                            cpOoredooMs.setProgress(0);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
            animatorooredooms.setDuration(100);
            animatorooredooms.start();
            animatorxlms.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    Integer progress = (Integer) animation.getAnimatedValue();
                    try{
                        if ( progress != null) {
                            cpXlMs.setProgress(progress);
                        }else {
                            cpXlMs.setProgress(0);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
            animatorxlms.setDuration(100);
            animatorxlms.start();
            animatoraxisms.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    Integer progress = (Integer) animation.getAnimatedValue();
                    try{
                        if ( progress != null) {
                            cpAxisMs.setProgress(progress);
                        }else {
                            cpAxisMs.setProgress(0);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
            animatoraxisms.setDuration(100);
            animatoraxisms.start();
            animator3ms.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    Integer progress = (Integer) animation.getAnimatedValue();
                    try{
                        if ( progress != null) {
                            cp3Ms.setProgress(progress);
                        }else {
                            cp3Ms.setProgress(0);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
            animator3ms.setDuration(100);
            animator3ms.start();
            animatorothersms.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    Integer progress = (Integer) animation.getAnimatedValue();
                    try{
                        if ( progress != null) {
                            cpOthersMs.setProgress(progress);
                        }else {
                            cpOthersMs.setProgress(0);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
            animatorothersms.setDuration(100);
            animatorothersms.start();



            itemView.setOnClickListener(v->{

            });

        }
    }

    public class EmptyViewHolder extends BaseViewHolder {


        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void clear() {

        }

    }

    private void setTextOrnull(TextView textView, String text,String option) {
        if (option.equals(",")){
            DecimalFormat decim = new DecimalFormat("#,###,###");
            textView.setText(decim.format(Long.valueOf(text)));
        }else {
            if (text == null)
                textView.setText("0" + option);
            else {
                textView.setText(text + option);
            }
        }
    }
}
