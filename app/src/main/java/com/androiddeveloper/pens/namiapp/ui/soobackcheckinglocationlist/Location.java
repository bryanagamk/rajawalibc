package com.androiddeveloper.pens.namiapp.ui.soobackcheckinglocationlist;

public class Location {

    private String nama, latitude, longitude, radius, tipe, namaUnit, tanggal;

    public String getNama() {
        return nama;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setRadius(String radius) {
        this.radius = radius;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public void setNamaUnit(String namaUnit) {
        this.namaUnit = namaUnit;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getRadius() {
        return radius;
    }

    public String getTipe() {
        return tipe;
    }

    public String getNamaUnit() {
        return namaUnit;
    }

    public String getTanggal() {
        return tanggal;
    }

    @Override
    public String toString() {
        return "BackCheckingLocationListResponse{" +
                "nama='" + nama + '\'' +
                ", unit='" + namaUnit+ '\'' +
                ", lat='" + latitude + '\'' +
                ", long='" + longitude+ '\'' +
                ", radius='" + radius+ '\'' +
                '}';
    }
}
