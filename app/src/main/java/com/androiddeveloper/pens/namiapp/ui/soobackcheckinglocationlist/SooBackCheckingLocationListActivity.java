package com.androiddeveloper.pens.namiapp.ui.soobackcheckinglocationlist;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;


import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.data.network.model.SooBackCheckingLocationListResponse;
import com.androiddeveloper.pens.namiapp.rajawali.adapter.BackcheckLocationAdapter;
import com.androiddeveloper.pens.namiapp.rajawali.api.APIService;
import com.androiddeveloper.pens.namiapp.rajawali.model.BackcheckLocation;
import com.androiddeveloper.pens.namiapp.ui.backcheckinglocationlist.BackCheckingLocationListActivity;
import com.androiddeveloper.pens.namiapp.ui.base.BaseActivity;
import com.androiddeveloper.pens.namiapp.ui.detailSooBc.DetailBackCheckActivity;
import com.androiddeveloper.pens.namiapp.utils.LocationUtils;
import com.androiddeveloper.pens.namiapp.utils.LocationValidatorUtil;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SooBackCheckingLocationListActivity extends BaseActivity implements SooBackCheckingLocationListMvpView, SooBackCheckingLocationListAdapter.Callback {

    private static final String TAG = "SooBackCheckingLocation";

    @Inject
    SooBackCheckingLocationListMvpPresenter<SooBackCheckingLocationListMvpView> mPresenter;

    @Inject
    SooBackCheckingLocationListAdapter mAdapter;

    @Inject
    LinearLayoutManager mLayoutManager;

    @BindView(R.id.rv_soo_back_checking_location)
    RecyclerView rv;

    @BindView(R.id.search_view_surrounding_bc)
    MaterialSearchView searchViewSurrounding;

    @BindView(R.id.toolbarSearchSurroundingBc)
    Toolbar toolbarSearchSurrounding;

    Handler handler;
    Runnable getLocationRunnable;
    Location mLocation;
    LocationManager lm;
    TelephonyManager telephonyManager;
    GsmCellLocation loc;

    public static Activity bcLocationListActivity;
    private List<SooBackCheckingLocationListResponse> mLocationListResponses;
    private BackcheckLocationAdapter adapter;
    private ArrayList<BackcheckLocation> list;
    double latitude;
    double longitude;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, SooBackCheckingLocationListActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_soo_back_checking_location_list);

        bcLocationListActivity = this;

        getActivityComponent().inject(this);

        if ( isNetworkAvailable() ){
            setUnBinder(ButterKnife.bind(this));
            mPresenter.onAttach(this);
            setUp();
        }
    }

    @Override
    protected void setUp() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 54);
        }

        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        mLocation= lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        if ( mLocation== null ){
            mLocation = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        } else {
            latitude = mLocation.getLatitude();
            longitude = mLocation.getLongitude();
        }

        if (telephonyManager.getPhoneType() == TelephonyManager.PHONE_TYPE_GSM) {
            loc = (GsmCellLocation) telephonyManager.getCellLocation();
        }

        setSupportActionBar(toolbarSearchSurrounding);
        getSupportActionBar().setTitle("Surounding Site & Outlet");

        toolbarSearchSurrounding.setNavigationIcon(R.drawable.ic_action_navigation_arrow_back_inverted);
        toolbarSearchSurrounding.setTitleTextColor(getResources().getColor(R.color.white));
        searchViewSurrounding.setEllipsize(true);

        toolbarSearchSurrounding.setNavigationOnClickListener(v -> {
            onBackPressed();
            if ( mLocation != null ){
                mPresenter.getBackCheckingLocationList(SooBackCheckingLocationListActivity.this,
                        String.valueOf(mLocation.getLatitude()),
                        String.valueOf(mLocation.getLongitude()),
                        String.valueOf(loc.getLac()),
                        String.valueOf(loc.getCid()));
            } else {
                showMessage("Tidak bisa mendapatka lokasi dari GPS");
            }
        });

        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);;
        rv.setLayoutManager(mLayoutManager);
        rv.setItemAnimator(new DefaultItemAnimator());
        rv.setAdapter(mAdapter);
        mAdapter.setCallback(this);

        handler = new Handler();

        showLoading("Mendapatkan lokasi");
        shutDownLoading(25000, "Gagal mendapatkan lokasi");
        LocationUtils.requestLocationUpdateWithCheckPermission(this, new LocationUtils.IUserLocationListener() {

            @Override
            public void onLocationChanged(Location location) {

                if (LocationValidatorUtil.isLocationFromMockProvider(SooBackCheckingLocationListActivity.this, location)){
                    showMessage("Maaf mock lokasi terdeteksi");
                }

                if ( mLocation!= null ){
                    mPresenter.getBackCheckingLocationList(SooBackCheckingLocationListActivity.this,
                            String.valueOf(location.getLatitude()),
                            String.valueOf(location.getLongitude()),
                            String.valueOf(loc.getLac()),
                            String.valueOf(loc.getCid()));
                    mLocation = location;
                } else {
                    showMessage("Tidak bisa mendapatkan lokasi dari GPS coba beberapa saat lagi");
                }

                hideLoading();

            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }

            @Override
            public void onStateChange(State state) {

            }
        });

        searchViewSurrounding.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                try {
                    if (TextUtils.isEmpty(query)){
                        if ( mLocation!= null ){
                            mPresenter.getBackCheckingLocationList(SooBackCheckingLocationListActivity.this,
                                    String.valueOf(mLocation.getLatitude()),
                                    String.valueOf(mLocation.getLongitude()),
                                    String.valueOf(loc.getLac()),
                                    String.valueOf(loc.getCid()));
                        } else {
                            showMessage("Tidak bisa mendapatkan lokasi dari GPS coba beberapa saat lagi");
                        }
                    } else {
                        if ( mAdapter.getItemCount() < 0 ){
                            Toast.makeText(getApplicationContext(), "Data tidak ditemukan!", Toast.LENGTH_SHORT).show();
                        } else {
                            mAdapter.filterByNameLocation(query);
                        }
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                try {
                    if (TextUtils.isEmpty(newText)){
                        if ( mLocation!= null ){
                            mPresenter.getBackCheckingLocationList(SooBackCheckingLocationListActivity.this,
                                    String.valueOf(mLocation.getLatitude()),
                                    String.valueOf(mLocation.getLongitude()),
                                    String.valueOf(loc.getLac()),
                                    String.valueOf(loc.getCid()));
                        } else {
                            showMessage("Tidak bisa mendapatkan lokasi dari GPS coba beberapa saat lagi");
                        }
                    } else {
                        if ( mAdapter.getItemCount() < 0 ){
                            Toast.makeText(getApplicationContext(), "Data tidak ditemukan!", Toast.LENGTH_SHORT).show();
                        } else {
                            mAdapter.filterByNameLocation(newText);
                        }
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }
                return true;
            }
        });

        searchViewSurrounding.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {

            }

            @Override
            public void onSearchViewClosed() {

            }
        });

        Log.d(">>>RJ", "bisa ambil lokasi");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case 54:
                if ( grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED ){
                    return;
                } else {
                    Toast.makeText(this, "Aplikasi membutuhkan ijin untuk berjalan dengan baik", Toast.LENGTH_SHORT).show();
                }
        }
    }

    @Override
    public void onItemLocationListClick(SooBackCheckingLocationListResponse response) {
        Intent intent = new Intent(this, DetailBackCheckActivity.class);
        intent.putExtra("nama", response.getLocation_name());
        intent.putExtra("radius", response.getDistance() + " Meter");
        intent.putExtra("long", response.getLongitude());
        intent.putExtra("lat", response.getLatitude());
        intent.putExtra("alamat", response.getAlamat());
        intent.putExtra("branch", response.getBranch());
        intent.putExtra("regional", response.getRegional());
        intent.putExtra("cluster", response.getCluster());
        intent.putExtra("city", response.getCity());
        intent.putExtra("last_visit", response.getLast_visit());

        intent.putExtra("display_telkomsel", response.getDisplay_telkomsel());
        intent.putExtra("display_ooredoo", response.getDisplay_ooredoo());
        intent.putExtra("display_xl", response.getDisplay_xl());
        intent.putExtra("display_axis", response.getDisplay_axis());
        intent.putExtra("display_three", response.getDisplay_three());
        intent.putExtra("display_others", response.getDisplay_others());

        intent.putExtra("recharge_telkomsel", response.getRecharge_share_telkomsel());
        intent.putExtra("recharge_ooredoo", response.getRecharge_share_ooredoo());
        intent.putExtra("recharge_xl", response.getRecharge_share_xl());
        intent.putExtra("recharge_axis", response.getRecharge_share_axis());
        intent.putExtra("recharge_three", response.getRecharge_share_three());
        intent.putExtra("recharge_others", response.getDisplay_others());

        intent.putExtra("type", response.getLocation_type());
        intent.addFlags(intent.FLAG_ACTIVITY_CLEAR_TOP | intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void updateBackCheckingLocationList(List<SooBackCheckingLocationListResponse> sooBackCheckingLocationListResponseList) {
        mLocationListResponses = sooBackCheckingLocationListResponseList;
        mAdapter.addItems(sooBackCheckingLocationListResponseList);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
//        if ( searchViewSurrounding.isSearchOpen() ){
//
//            searchViewSurrounding.closeSearch();
//        } else {
            super.onBackPressed();
//        }
    }

    @Override
    public void showLoading(String message) {
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(message);
        mProgressDialog.show();
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        if ( mProgressDialog != null ) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        if (handler != null){
//            handler.removeCallbacks(getLocationRunnable);
            mPresenter.onDetach();
        }
        super.onDestroy();
    }
}
