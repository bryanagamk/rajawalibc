package com.androiddeveloper.pens.namiapp.ui.soobackcheckinglocationlist;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.data.network.model.SooBackCheckingLocationListResponse;
import com.androiddeveloper.pens.namiapp.ui.base.BaseViewHolder;
import com.androiddeveloper.pens.namiapp.ui.detailSooBc.DetailBackCheckActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

public class SooBackCheckingLocationListAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private List<SooBackCheckingLocationListResponse> locationList;
    private SooBackCheckingLocationListAdapter.Callback mCallback;

    public SooBackCheckingLocationListAdapter(List<SooBackCheckingLocationListResponse> locationList) {
        this.locationList = locationList;
    }

    public void setCallback(SooBackCheckingLocationListAdapter.Callback callback){
        mCallback = callback;
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType){
            case VIEW_TYPE_NORMAL:
                return new SooBackCheckingLocationListAdapter.MyViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_soo_back_checking_location_item, parent, false));
            case VIEW_TYPE_EMPTY:
            default:
                return new SooBackCheckingLocationListAdapter.EmptyViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_view_location, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemViewType(int position) {
        if ( locationList != null && locationList.size() > 0 ){
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public int getItemCount() {
        return (locationList != null) ? locationList.size() : 1;
    }

    public interface Callback {
        void onItemLocationListClick(SooBackCheckingLocationListResponse response);
    }

    public void addItems(List<SooBackCheckingLocationListResponse> responseList){
        locationList.clear();
        locationList = responseList;
        notifyDataSetChanged();
    }

    public void filterByNameLocation(String query){
        List<SooBackCheckingLocationListResponse> filterList = new ArrayList<>();
        for ( SooBackCheckingLocationListResponse surounding : locationList ){
            if ( surounding.getLocation_name().toLowerCase().contains(query.toLowerCase())
                    || surounding.getLocation_type().toLowerCase().contains(query.toLowerCase())){
                filterList.add(surounding);
            }
        }

        locationList.clear();
        locationList.addAll(filterList);
        notifyDataSetChanged();
    }

    public class MyViewHolder extends BaseViewHolder {

        TextView tv_location, tv_lat, tv_long, tv_radius, tv_last, tv_status;
        ImageView iv_logo;
        LinearLayout ll;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            tv_location = itemView.findViewById(R.id.tv_backcheck_location);
            tv_lat = itemView.findViewById(R.id.tv_backcheck_lat);
            tv_long = itemView.findViewById(R.id.tv_backcheck_long);
            tv_radius = itemView.findViewById(R.id.tv_backcheck_distance);
            tv_last = itemView.findViewById(R.id.tv_backcheck_last_visi);
            tv_status = itemView.findViewById(R.id.tv_backcheck_status);
            iv_logo = itemView.findViewById(R.id.iv_backcheck_logo);
            ll = itemView.findViewById(R.id.linear_backcheck_wrapper);
        }

        @Override
        protected void clear() {

        }

        public void onBind( int position ){
            super.onBind(position);

            tv_location.setText(locationList.get(position).getLocation_name() + " " +locationList.get(position).getLast_visit());
            tv_lat.setText(locationList.get(position).getLatitude());
            tv_long.setText(locationList.get(position).getLongitude());
            tv_radius.setText(locationList.get(position).getDistance() + " Meter");
            tv_last.setText(locationList.get(position).getUser_name());
            tv_status.setText(locationList.get(position).getMessage_allowed_chekin());

            if ( locationList.get(position).getLocation_type().equals("SITE") ){
                iv_logo.setImageResource(R.drawable.tower);
            } else {
                iv_logo.setImageResource(R.drawable.aoc_osk);
            }

            ll.setOnClickListener(view -> {
                if ( mCallback != null ){
                    mCallback.onItemLocationListClick(locationList.get(position));
                }
            });
        }
    }

    public class EmptyViewHolder extends BaseViewHolder {

        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void clear() {

        }
    }
}
