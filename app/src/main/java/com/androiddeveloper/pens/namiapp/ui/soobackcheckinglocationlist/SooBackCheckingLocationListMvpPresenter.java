package com.androiddeveloper.pens.namiapp.ui.soobackcheckinglocationlist;

import android.app.Activity;

import com.androiddeveloper.pens.namiapp.ui.base.MvpPresenter;

/**
 * Created by kmdr7 on 13/01/19
 * <p>
 * Author Andika Ahmad Ramadhan
 * Email Andikahmadr@gmail.com
 * Github https://github.com/kmdrn7
 **/
public interface SooBackCheckingLocationListMvpPresenter<V extends SooBackCheckingLocationListMvpView> extends MvpPresenter<V> {

    void getBackCheckingLocationList(Activity activity, String latitude, String longitude, String lac, String ci);

}
