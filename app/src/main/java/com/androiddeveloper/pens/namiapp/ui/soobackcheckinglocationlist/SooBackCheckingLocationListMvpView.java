package com.androiddeveloper.pens.namiapp.ui.soobackcheckinglocationlist;

import com.androiddeveloper.pens.namiapp.data.network.model.SooBackCheckingLocationListResponse;
import com.androiddeveloper.pens.namiapp.ui.base.MvpView;

import java.util.List;

/**
 * Created by kmdr7 on 13/01/19
 * <p>
 * Author Andika Ahmad Ramadhan
 * Email Andikahmadr@gmail.com
 * Github https://github.com/kmdrn7
 **/
public interface SooBackCheckingLocationListMvpView extends MvpView {

    void updateBackCheckingLocationList(List<SooBackCheckingLocationListResponse> sooBackCheckingLocationListResponseList);

//    void openDetailBackcheckingActivity(SooBackCheckingLocationListResponse response);

}
