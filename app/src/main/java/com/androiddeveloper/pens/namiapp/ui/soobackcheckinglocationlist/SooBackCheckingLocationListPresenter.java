package com.androiddeveloper.pens.namiapp.ui.soobackcheckinglocationlist;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.ui.base.BasePresenter;
import com.androiddeveloper.pens.namiapp.utils.StartupConfig;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by kmdr7 on 13/01/19
 * <p>
 * Author Andika Ahmad Ramadhan
 * Email Andikahmadr@gmail.com
 * Github https://github.com/kmdrn7
 **/
public class SooBackCheckingLocationListPresenter<V extends SooBackCheckingLocationListMvpView> extends BasePresenter<V> implements SooBackCheckingLocationListMvpPresenter<V> {

    @Inject
    public SooBackCheckingLocationListPresenter(DataManager dataManager, CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);
    }

    @Override
    public void getBackCheckingLocationList(Activity activity, String latitude, String longitude, String lac, String ci) {
        Log.v("Tag", "Terpanggil");
        Log.d("Debug : ","Latitude" + latitude + "Longitude : " + longitude);

        String userId = getDataManager().getCurrentUserId();
        String userType = getDataManager().getCurrentUserType().toLowerCase();
        String distOutlet = StartupConfig.getInstance(getDataManager(),false, "RAJAWALI").getRadiusOutletMax();
        String distSite = StartupConfig.getInstance(getDataManager(),false, "RAJAWALI").getRadiusSiteMax();

        Log.d("Debug : ","SOO List || Latitude" + latitude + "Longitude : " + longitude);

        if (userType.equals("mgt1")){
            distOutlet = "1000";
            distSite = "1000";
        }

        getCompositeDisposable().add(getDataManager()
                .getSooBackchekingLocationList(userId, latitude, longitude, distOutlet, distSite, lac, ci)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(backCheckingLocationListResponses -> {
                    if (!isViewAttached()){
                        return;
                    }
                    if (backCheckingLocationListResponses != null && backCheckingLocationListResponses.size() != 0){
                        getMvpView().updateBackCheckingLocationList(backCheckingLocationListResponses);
                        getMvpView().hideLoading();
                    }else{
                        getMvpView().showMessage("Tidak ada Site & Outlet Surrounding");
                        getMvpView().hideLoading();
                    }
                },throwable -> {
                    if (!isViewAttached()){
                        return;
                    }
                    getMvpView().showMessage("Tidak bisa terhubung ke server");
                    getMvpView().hideLoading();
                    Log.d("Backchecking : " ,throwable.toString());
                }));
    }
}
