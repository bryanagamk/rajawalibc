package com.androiddeveloper.pens.namiapp.ui.summary;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.ui.base.BaseActivity;
import com.androiddeveloper.pens.namiapp.ui.summary.fragment.AreaFragment;
import com.androiddeveloper.pens.namiapp.ui.summary.fragment.SummaryType;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SummaryAreaActivity extends BaseActivity implements SummaryAreaMvpView {

    @Inject
    SummaryAreaMvpPresenter<SummaryAreaMvpView> mPresenter;

    @BindView(R.id.search_view_summary)
    MaterialSearchView searchViewSummary;

    @BindView(R.id.toolbarSearchSummary)
    Toolbar toolbarSearchSummary;

    @BindView(R.id.summary_fragment)
    FrameLayout frameLayout;


    private TextView mTextMessage;
    private SummaryType mSummaryType;
    AreaFragment fragment;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_regional:
                    fragment = AreaFragment.newInstance(mSummaryType,"regional");
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.summary_fragment,fragment)
                    .commit();
                    return true;
                case R.id.navigation_branch:
                    fragment = AreaFragment.newInstance(mSummaryType,"branch");
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.summary_fragment,
                                    fragment)
                            .commit();
                    return true;
                case R.id.navigation_cluster:
                    fragment = AreaFragment.newInstance(mSummaryType,"cluster");
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.summary_fragment,
                                    fragment)
                            .commit();
                    return true;
                case R.id.navigation_city:
                    fragment = AreaFragment.newInstance(mSummaryType,"city");
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.summary_fragment,
                                    fragment)
                            .commit();
                    return true;
                case R.id.navigation_aoc:
                    fragment = AreaFragment.newInstance(mSummaryType,"aoc");
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.summary_fragment,
                                    fragment)
                            .commit();
                    return true;
            }
            return false;
        }
    };

    public static Intent getStartIntent(Context context, String userType, SummaryType type) {
        Intent intent = new Intent(context, SummaryAreaActivity.class);
        intent.putExtra("type",type);
        intent.putExtra("area",userType);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary_area);

        getActivityComponent().inject(this);

        if (isNetworkAvailable()) {
            setUnBinder(ButterKnife.bind(this));

            mPresenter.onAttach(this);

            setUp();
        }
    }



    @Override
    protected void setUp() {
        mSummaryType = (SummaryType) getIntent().getSerializableExtra("type");

        setupBottomNavigationMenu(getIntent().getStringExtra("area"));


        setSupportActionBar(toolbarSearchSummary);
        setActionBarTitle();

        toolbarSearchSummary.setNavigationIcon(R.drawable.ic_action_navigation_arrow_back_inverted);
        toolbarSearchSummary.setTitleTextColor(getResources().getColor(R.color.white));
        searchViewSummary.setEllipsize(true);
        toolbarSearchSummary.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });



    }

    private void setActionBarTitle() {
        getSupportActionBar().setTitle(mSummaryType.getTitle());
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    private void setupBottomNavigationMenu(String area){
        Log.d("Error : ",area);
        mTextMessage = findViewById(R.id.message);
        BottomNavigationView navigation = findViewById(R.id.navigation);

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        switch (area.toLowerCase()){
            case "mgt1":
                fragment = AreaFragment.newInstance(mSummaryType,"regional");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.summary_fragment,fragment)
                        .commit();
                break;
            case "mgt2":
                navigation.getMenu().removeItem(R.id.navigation_regional);
                fragment = AreaFragment.newInstance(mSummaryType,"branch");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.summary_fragment,fragment)
                        .commit();
                break;
            case "mgt3":
                navigation.getMenu().removeItem(R.id.navigation_regional);
                navigation.getMenu().removeItem(R.id.navigation_branch);

                fragment = AreaFragment.newInstance(mSummaryType,"cluster");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.summary_fragment,fragment)
                        .commit();
                break;
            case "mgt4":
                navigation.getMenu().removeItem(R.id.navigation_regional);
                navigation.getMenu().removeItem(R.id.navigation_branch);

                fragment = AreaFragment.newInstance(mSummaryType,"cluster");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.summary_fragment,fragment)
                        .commit();
                break;
            case "mgt5":
                navigation.getMenu().removeItem(R.id.navigation_regional);
                navigation.getMenu().removeItem(R.id.navigation_branch);

                fragment = AreaFragment.newInstance(mSummaryType,"cluster");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.summary_fragment,fragment)
                        .commit();
                break;
            case "adm2":
                navigation.getMenu().removeItem(R.id.navigation_regional);
                fragment = AreaFragment.newInstance(mSummaryType,"branch");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.summary_fragment,fragment)
                        .commit();
                break;
            case "adm3":
                navigation.getMenu().removeItem(R.id.navigation_regional);
                navigation.getMenu().removeItem(R.id.navigation_branch);

                fragment = AreaFragment.newInstance(mSummaryType,"cluster");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.summary_fragment,fragment)
                        .commit();
                break;
            case "bm":
                navigation.getMenu().removeItem(R.id.navigation_regional);
                navigation.getMenu().removeItem(R.id.navigation_branch);
                navigation.getMenu().removeItem(R.id.navigation_cluster);

                fragment = AreaFragment.newInstance(mSummaryType,"city");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.summary_fragment,fragment)
                        .commit();
                break;
            case "tl":
                navigation.getMenu().removeItem(R.id.navigation_regional);
                navigation.getMenu().removeItem(R.id.navigation_branch);
                navigation.getMenu().removeItem(R.id.navigation_cluster);

                fragment = AreaFragment.newInstance(mSummaryType,"city");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.summary_fragment,fragment)
                        .commit();
                break;
            case "aoc":
                navigation.getMenu().removeItem(R.id.navigation_regional);
                navigation.getMenu().removeItem(R.id.navigation_branch);
                navigation.getMenu().removeItem(R.id.navigation_cluster);
                navigation.getMenu().removeItem(R.id.navigation_city);
                navigation.getMenu().removeItem(R.id.navigation_aoc);

                fragment = AreaFragment.newInstance(mSummaryType,"regional");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.summary_fragment,fragment)
                        .commit();
                break;
            default:
                navigation.getMenu().removeItem(R.id.navigation_regional);
                navigation.getMenu().removeItem(R.id.navigation_branch);
                navigation.getMenu().removeItem(R.id.navigation_cluster);
                navigation.getMenu().removeItem(R.id.navigation_city);
                navigation.getMenu().removeItem(R.id.navigation_aoc);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (searchViewSummary.isSearchOpen()) {
            searchViewSummary.closeSearch();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_summary, menu);

        MenuItem item = menu.findItem(R.id.action_search_summary);
        searchViewSummary.setMenuItem(item);

        return true;
    }

    public interface OnSearchInterface{
        void search(String query);
    }

    public void onSearchSummarySet(OnSearchInterface onSearch){
        searchViewSummary.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                onSearch.search(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                onSearch.search(newText);
                return true;
            }
        });

        searchViewSummary.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
                //Do some magic
            }

            @Override
            public void onSearchViewClosed() {
                //Do some magic
            }
        });
    }
}