package com.androiddeveloper.pens.namiapp.ui.summary;

import com.androiddeveloper.pens.namiapp.di.PerActivity;
import com.androiddeveloper.pens.namiapp.ui.base.MvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.summary.fragment.SummaryType;

@PerActivity
public interface SummaryAreaMvpPresenter<V extends SummaryAreaMvpView> extends MvpPresenter<V> {

}
