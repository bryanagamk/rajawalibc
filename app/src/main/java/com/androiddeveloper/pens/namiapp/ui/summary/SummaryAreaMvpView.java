package com.androiddeveloper.pens.namiapp.ui.summary;

import com.androiddeveloper.pens.namiapp.data.network.model.NotificationResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceSummary;
import com.androiddeveloper.pens.namiapp.ui.base.MvpView;

import java.util.List;

public interface SummaryAreaMvpView extends MvpView {

}
