package com.androiddeveloper.pens.namiapp.ui.summary;

import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.ui.base.BasePresenter;
import com.androiddeveloper.pens.namiapp.ui.notification.NotificationMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.notification.NotificationMvpView;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by miftahun on 8/22/18.
 */

public class SummaryAreaPresenter<V extends SummaryAreaMvpView> extends BasePresenter<V>
        implements SummaryAreaMvpPresenter<V> {

    @Inject
    public SummaryAreaPresenter(DataManager dataManager, CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);
    }

}
