/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.androiddeveloper.pens.namiapp.ui.summary.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformancePoiOsk;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformancePoiOskResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceSummary;
import com.androiddeveloper.pens.namiapp.di.componen.ActivityComponent;
import com.androiddeveloper.pens.namiapp.ui.base.BaseFragment;
import com.androiddeveloper.pens.namiapp.ui.performancepoiosklist.PerformancePoiOskListAdapter;
import com.androiddeveloper.pens.namiapp.ui.summary.SummaryAreaActivity;
import com.androiddeveloper.pens.namiapp.ui.performancepoiosklist.PerformancePoiOskListActivity;
import com.androiddeveloper.pens.namiapp.ui.summary.performancelistadapter.SummaryPerformanceEventAdapter;
import com.androiddeveloper.pens.namiapp.ui.summary.performancelistadapter.SummaryPerformancePoiOskAdaptiveAdapter;
import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by janisharali on 25/05/17.
 */

public class AreaFragment extends BaseFragment implements
        AreaMvpView, SummaryPerformancePoiOskAdaptiveAdapter.Callback {

    private static final String TAG = "AreaFragment";

    @Inject
    AreaMvpPresenter<AreaMvpView> mPresenter;


    SummaryPerformancePoiOskAdaptiveAdapter mSummaryPerformancePoiOskAdaptiveAdapter;

    SummaryPerformanceEventAdapter mSummaryPerformanceEventAdapter;

    @Inject
    LinearLayoutManager mLayoutManager;

    @BindView(R.id.rv_summary)
    RecyclerView mRecyclerView;

    @BindView(R.id.root)
    ConstraintLayout root;

    @BindView(R.id.shimmer_view_container)
    ShimmerFrameLayout mShimmerViewContainer;

    List<PerformanceSummary> performancePoiOskList;

    public static AreaFragment newInstance(SummaryType type,String territory) {
        Bundle args = new Bundle();
        args.putSerializable("type",type);
        args.putSerializable("territory",territory);
        AreaFragment fragment = new AreaFragment();

        Log.d("Debug ", String.format("Type  : %s, Territory : %s",type,territory));

        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_summary, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null && isNetworkAvailable()) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {
        presenterOnViewPrepared();
    }

    public void presenterOnViewPrepared(){
        SummaryType type = (SummaryType) getArguments().getSerializable("type");
        String territory = (String) getArguments().getSerializable("territory");
        String userId_aoc = "0";
        String username_aoc = "";
        String period_type = "";

        mSummaryPerformancePoiOskAdaptiveAdapter = new SummaryPerformancePoiOskAdaptiveAdapter(new ArrayList<>(), type);
        mSummaryPerformanceEventAdapter = new SummaryPerformanceEventAdapter(new ArrayList<>(), type);

        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mSummaryPerformancePoiOskAdaptiveAdapter.setCallback(this);

        SummaryAreaActivity summaryAreaActivity = (SummaryAreaActivity) getActivity();

        SummaryAreaActivity.OnSearchInterface callback = null;

        if (type == SummaryType.PJP_PTD_POI_OSK || type == SummaryType.PJP_MTD_POI_OSK){
            mRecyclerView.setAdapter(mSummaryPerformancePoiOskAdaptiveAdapter);
            callback = mSummaryPerformancePoiOskAdaptiveAdapter;
        }else if (type == SummaryType.PERFORMANCE_ACTUAL_EVENT){
            mRecyclerView.setAdapter(mSummaryPerformanceEventAdapter);
            callback = mSummaryPerformanceEventAdapter;
        }
        summaryAreaActivity.onSearchSummarySet(callback);

        mPresenter.onViewPrepared(type,territory, userId_aoc, username_aoc, period_type);
    }

    @Override
    public void showErrorPage(String message) {

    }

    @Override
    public boolean isShowingError() {
        return false;
    }

    @Override
    public void finish() {

    }

    @Override
    public void updatePerformanceSummary(List<PerformanceSummary> performanceSummaryList) {
        Log.d("Debug",performanceSummaryList.size() + "");
        performancePoiOskList = performanceSummaryList;
        mSummaryPerformancePoiOskAdaptiveAdapter.addItems(performanceSummaryList);
        mSummaryPerformancePoiOskAdaptiveAdapter.notifyDataSetChanged();
    }


    @Override
    public void updatePerformanceSummaryEvent(List<PerformanceSummary> performanceSummaries) {
        Log.d("Debug",performanceSummaries.size() + "");
        mSummaryPerformanceEventAdapter.addItems(performanceSummaries);

    }


    @Override
    public void showLoading(String message) {
        super.showLoading(message);
        mShimmerViewContainer.startShimmer();
        mShimmerViewContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        // stop animating Shimmer and hide the layout
        mShimmerViewContainer.stopShimmer();
        mShimmerViewContainer.setVisibility(View.GONE);
    }


    @Override
    public void onDestroyView() {
        mPresenter.onDetachFragment();
        super.onDestroyView();
    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmer();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmer();
        super.onPause();
    }

    @Override
    public void onItemLocationListClick(int position) {
        String teritory = (String) getArguments().getSerializable("territory");
        if (teritory != null && teritory.equals("aoc")){
            PerformanceSummary performancePoiOsk =  performancePoiOskList.get(position);
            Log.d(TAG, "onItemLocationListClick: " + performancePoiOsk.getUserId());
            Log.d(TAG, "onItemLocationListClick: " + performancePoiOsk.getUserName());
            SummaryType type = (SummaryType) getArguments().getSerializable("type");
            String territory = "channel";
            String period_type_mtd = "MTD";
            String period_type_ptd = "PTD";
            Intent intent = PerformancePoiOskListActivity.getStartIntent(getActivity());
            intent.putExtra("territory", territory);
            intent.putExtra("type", type);
            intent.putExtra("userid_aoc", performancePoiOsk.getUserId());
            intent.putExtra("username_aoc", performancePoiOsk.getUserName());
            intent.putExtra("period_type_mtd", period_type_mtd);
            intent.putExtra("period_type_ptd", period_type_ptd);
            startActivity(intent);
        }else {
            Toast.makeText(getBaseActivity(), "Pilih tab aoc untuk menampilkan detail aoc", Toast.LENGTH_SHORT).show();
        }
    }
}
