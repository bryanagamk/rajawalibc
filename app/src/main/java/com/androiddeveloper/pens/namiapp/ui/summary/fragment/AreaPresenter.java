/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.androiddeveloper.pens.namiapp.ui.summary.fragment;

import android.annotation.SuppressLint;
import android.content.res.ColorStateList;
import android.util.Log;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceBackchecking;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceEvent;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformancePoiOsk;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceShare;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceShareSummary;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceSummary;
import com.androiddeveloper.pens.namiapp.data.network.model.SummaryArea;
import com.androiddeveloper.pens.namiapp.data.network.model.SummaryEvent;
import com.androiddeveloper.pens.namiapp.data.network.model.SummaryInstitution;
import com.androiddeveloper.pens.namiapp.data.network.model.SummaryInstitutionResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.SummaryOsk;
import com.androiddeveloper.pens.namiapp.data.network.model.SummaryOskResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.SummaryProductiveSurvey;
import com.androiddeveloper.pens.namiapp.data.network.model.SummarySales;
import com.androiddeveloper.pens.namiapp.ui.base.BasePresenter;
import com.androiddeveloper.pens.namiapp.ui.sharepercentage.fragmentshare.ShareType;
import com.androidnetworking.error.ANError;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Single;
import io.reactivex.SingleSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by janisharali on 25/05/17.
 */

public class AreaPresenter<V extends AreaMvpView> extends BasePresenter<V>
        implements AreaMvpPresenter<V> {

    @Inject
    public AreaPresenter(DataManager dataManager,
                         CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);
    }

    @Override
    public void onViewPrepared(SummaryType type,String territory, String userId_aoc, String username_aoc, String period_type) {
        Log.d("Debug : ",type.getType() + territory);
        if (type.getType().equals("ptd")){
            loadPerformancePtdPoiOsk(type, territory, userId_aoc, username_aoc, period_type);
        }else if (type.getType().equals("mtd")){
            loadPerformanceMtdPoiOsk(type, territory, userId_aoc, username_aoc, period_type);
        }else if (type == SummaryType.PERFORMANCE_ACTUAL_EVENT){
            loadPerformanceEvent(type, territory, username_aoc);
        }

    }

    private void loadPerformanceEvent(SummaryType type, String territory, String username_aoc){
        String userId = getDataManager().getCurrentUserId();
        getCompositeDisposable().add(getDataManager().getSummaryPerformanceEvent(userId,territory, username_aoc)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(performanceEventResponse -> {
                    if (!isViewAttached()){
                        return;
                    }
                    if (performanceEventResponse != null && performanceEventResponse.isSuccess()){
                        List<PerformanceSummary> performanceSummaries = performanceEventsAdapter(performanceEventResponse.getSummaryPerformanceEvent(),type);
                        getMvpView().updatePerformanceSummaryEvent(performanceSummaries);
                    }

                    getMvpView().hideLoading();
                },throwable -> {
                    if (!isViewAttached()){
                        return;
                    }
                    Log.d("Error : ",throwable.toString());

                    getMvpView().hideLoading();

                    // handle the error here
                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }


    private void loadPerformancePtdPoiOsk(SummaryType type, String territory, String userId_aoc, String username_aoc, String period_type){
        String userId = getDataManager().getCurrentUserId();
        getCompositeDisposable().add(getDataManager().getSummaryPerformancePoiOsk(userId,territory, userId_aoc, username_aoc, period_type)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(performancePoiOskResponse -> {
                    if (!isViewAttached()){
                        return;
                    }
                    Log.d("DEbug",performancePoiOskResponse.toString());
                    if (performancePoiOskResponse != null && performancePoiOskResponse.isSuccess()){
                        List<PerformanceSummary> performanceSummaries = performanceSummariesPtdConverter(performancePoiOskResponse.getSummaryPerformancePoiOsk(),type);
                        getMvpView().updatePerformanceSummary(performanceSummaries);
                    }

                    getMvpView().hideLoading();
                },throwable -> {
                    if (!isViewAttached()){
                        return;
                    }
                    Log.d("Error : ",throwable.toString());

                    getMvpView().hideLoading();

                    // handle the error here
                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

    private void loadPerformanceMtdPoiOsk(SummaryType type, String territory, String userId_aoc, String username_aoc, String period_type){
        String userId = getDataManager().getCurrentUserId();
        getCompositeDisposable().add(getDataManager().getSummaryPerformancePoiOsk(userId,territory, userId_aoc, username_aoc, period_type)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(performancePoiOskResponse -> {
                    if (!isViewAttached()){
                        return;
                    }
                    Log.d("DEbug",performancePoiOskResponse.toString());
                    if (performancePoiOskResponse != null && performancePoiOskResponse.isSuccess()){
                        List<PerformanceSummary> performanceSummaries = performanceSummariesMtdConverter(performancePoiOskResponse.getSummaryPerformancePoiOsk(),type);
                        getMvpView().updatePerformanceSummary(performanceSummaries);
                    }

                    getMvpView().hideLoading();
                },throwable -> {
                    if (!isViewAttached()){
                        return;
                    }
                    Log.d("Error : ",throwable.toString());

                    getMvpView().hideLoading();

                    // handle the error here
                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

    private List<PerformanceSummary> performanceSummariesPtdConverter(List<PerformancePoiOsk> performanceSummariesPtd,
                                                         SummaryType type){
        if (type == SummaryType.PJP_PTD_POI_OSK){
            List<PerformanceSummary> performanceSummaries = new ArrayList<>();
            for (PerformancePoiOsk summaryPtd : performanceSummariesPtd){
                performanceSummaries.add(summaryPtd.getPjpPtdPoiOsk());
            }
            return performanceSummaries;
        }else{
            return null;
        }
    }

    private List<PerformanceSummary> performanceSummariesMtdConverter(List<PerformancePoiOsk> performanceSummariesMtd,
                                                                      SummaryType type){
        if (type == SummaryType.PJP_MTD_POI_OSK){
            List<PerformanceSummary> performanceSummaries = new ArrayList<>();
            for (PerformancePoiOsk summaryMtd : performanceSummariesMtd){
                performanceSummaries.add(summaryMtd.getPjpMtdPoiOsk());
            }
            return performanceSummaries;
        }else{
            return null;
        }
    }

    private List<PerformanceSummary> performanceEventsAdapter(List<PerformanceEvent> performanceEvents,
                                                              SummaryType type){
        if (type == SummaryType.PERFORMANCE_ACTUAL_EVENT){
            List<PerformanceSummary> performanceSummaryArrayList = new ArrayList<>();
            for (PerformanceEvent performanceEvent: performanceEvents){
                performanceSummaryArrayList.add(performanceEvent.getSummaryPerfomanceEvent());
            }
            return performanceSummaryArrayList;
        }else {
            return null;
        }
    }
}
