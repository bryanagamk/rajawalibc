package com.androiddeveloper.pens.namiapp.ui.summary.fragment;

import com.androiddeveloper.pens.namiapp.R;

/**
 * Created by miftahun on 8/23/18.
 */

public enum SummaryType {
    PJP_PTD_POI_OSK("ptd", "PJP Productivity PTD", R.color.green_sky),
    PJP_MTD_POI_OSK("mtd", "PJP Productivity MTD", R.color.light_sky_blue),
    PERFORMANCE_ACTUAL_EVENT("actual_event", "Actual Event", R.color.actual_event);

    private final String mType;
    private final String mTitle;
    private final int mColorStateList;
    private int mDrawable;

    SummaryType(String type, String title, int colorStateList){
        mType = type;
        mTitle = title;
        mColorStateList = colorStateList;
    }

    SummaryType(String type, String title, int colorStateList, int mDrawable) {
        mType = type;
        mTitle = title;
        mColorStateList = colorStateList;
        this.mDrawable = mDrawable;
    }

    public String getType() {
        return mType;
    }
    public String getTitle() {
        return mTitle;
    }
    public int getColor() {
        return mColorStateList;
    }

    public int getmDrawable() {
        return mDrawable;
    }
}