package com.androiddeveloper.pens.namiapp.ui.summary.performancelistadapter;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceSummary;
import com.androiddeveloper.pens.namiapp.ui.base.BaseViewHolder;
import com.androiddeveloper.pens.namiapp.ui.summary.SummaryAreaActivity;
import com.androiddeveloper.pens.namiapp.ui.summary.fragment.SummaryType;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

public class SummaryPerformanceEventAdapter extends RecyclerView.Adapter<BaseViewHolder> implements SummaryAreaActivity.OnSearchInterface {
    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private SummaryPerformanceEventAdapter.Callback mCallback;
    private List<PerformanceSummary> mSummaryPerformanceEvents;
    private List<PerformanceSummary> mSummaryPerformanceEventsDefault;
    private String mType;
    private SummaryType summaryType;

    ColorStateList color;

    public SummaryPerformanceEventAdapter(List<PerformanceSummary> performanceEventList, SummaryType type) {
        mSummaryPerformanceEvents = performanceEventList;
        summaryType = type;
    }

    public void setCallback(SummaryPerformanceEventAdapter.Callback callback) {
        mCallback = callback;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new SummaryPerformanceEventAdapter.ViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_performance_actual_event_summary, parent, false));
            case VIEW_TYPE_EMPTY:
            default:
                return new SummaryPerformanceEventAdapter.EmptyViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_summary, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mSummaryPerformanceEvents != null && mSummaryPerformanceEvents.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public int getItemCount() {
        if (mSummaryPerformanceEvents != null && mSummaryPerformanceEvents.size() > 0) {
            return mSummaryPerformanceEvents.size();
        } else {
            return 1;
        }
    }

    public void addItems(List<PerformanceSummary> performanceEvents) {
        mSummaryPerformanceEvents.clear();
        mSummaryPerformanceEvents.addAll(performanceEvents);
        mSummaryPerformanceEventsDefault = performanceEvents;
        notifyDataSetChanged();
    }

    @Override
    public void search(String query) {
        List<PerformanceSummary> filteredSummaryPerformaceEvent = new ArrayList<>();
        for (PerformanceSummary performanceEvent : mSummaryPerformanceEventsDefault){
            if (performanceEvent.getTitle().toLowerCase().contains(query.toLowerCase()) ){
                filteredSummaryPerformaceEvent.add(performanceEvent);
            }
        }
        mSummaryPerformanceEvents.clear();
        mSummaryPerformanceEvents.addAll(filteredSummaryPerformaceEvent);
        notifyDataSetChanged();
    }

    public interface Callback {
        void onBlogEmptyViewRetryClick();
    }

    public class ViewHolder extends BaseViewHolder {

        TextView tvTitle;
        TextView tvAchEventCallMtd;
        TextView tvAchEventCallPtd;
        TextView tvActualEventCallMtd;
        TextView tvActualEventCallPtd;
        TextView tvPlanEventCallPtd;
        TextView tvPlanEventCallMtd;
        TextView tvSalesEventCallPtd;
        TextView tvSalesEventCallMtd;
        CardView cardView;


        public ViewHolder(View itemView) {
            super(itemView);

            tvTitle = itemView.findViewById(R.id.tv_title_actual_event_summary);
            tvAchEventCallMtd = itemView.findViewById(R.id.tv_mtd_ach_actual_event_summary);
            tvAchEventCallPtd = itemView.findViewById(R.id.tv_ptd_ach_actual_event_summary);
            tvActualEventCallMtd = itemView.findViewById(R.id.tv_mtd_performance_actual_actual_event_summary);
            tvActualEventCallPtd = itemView.findViewById(R.id.tv_ptd_performance_actual_actual_event_summary);
            tvPlanEventCallPtd = itemView.findViewById(R.id.tv_ptd_target_performance_actual_event_summary);
            tvPlanEventCallMtd = itemView.findViewById(R.id.tv_mtd_target_performance_actual_event_summary);
            tvSalesEventCallPtd = itemView.findViewById(R.id.tv_ptd_sales_performance_actual_event_summary);
            tvSalesEventCallMtd = itemView.findViewById(R.id.tv_mtd_sales_performance_actual_event_summary);
            cardView = itemView.findViewById(R.id.cv_performance_actual_event_summary);
        }

        protected void clear() {

        }

        public void onBind(int position) {
            super.onBind(position);

            PerformanceSummary item = mSummaryPerformanceEvents.get(position);
            Log.d("Debug", mSummaryPerformanceEvents.toString());

            String color = "#" + Integer.toHexString(ContextCompat.getColor(itemView.getContext(), summaryType.getColor()));
            Log.d("Debug : ",color);

            setTextOrnull(tvAchEventCallMtd, item.getTvAchEventCallMtd(), "");
            setTextOrnull(tvAchEventCallPtd, item.getTvAchEventCallPtd(), "");
            setTextOrnull(tvActualEventCallMtd, item.getTvActualEventCallMtd(), ",");
            setTextOrnull(tvActualEventCallPtd, item.getTvActualEventCallPtd(), ",");
            setTextOrnull(tvPlanEventCallMtd, item.getTvPlanEventCallMtd(), ",");
            setTextOrnull(tvPlanEventCallPtd, item.getTvPlanEventCallPtd(), ",");
            setTextOrnull(tvSalesEventCallMtd, item.getTvSalesEventCallMtd(), ",");
            setTextOrnull(tvSalesEventCallPtd, item.getTvSalesEventCallPtd(), ",");

            cardView.setCardBackgroundColor(Color.parseColor(color));
            tvTitle.setText(item.getTitle());

            itemView.setOnClickListener(v -> {

            });
        }
    }

    public class EmptyViewHolder extends BaseViewHolder {


        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void clear() {

        }

    }

    private void setTextOrnull(TextView textView, String text,String option) {
        if (option.equals(",")){
            DecimalFormat decim = new DecimalFormat("#,###,###");
            textView.setText(decim.format(Long.valueOf(text)));
        }else {
            if (text == null)
                textView.setText("0" + option);
            else {
                textView.setText(text + option);
            }
        }
    }
}
