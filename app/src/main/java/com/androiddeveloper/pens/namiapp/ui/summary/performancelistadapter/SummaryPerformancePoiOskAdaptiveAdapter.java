package com.androiddeveloper.pens.namiapp.ui.summary.performancelistadapter;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceSummary;
import com.androiddeveloper.pens.namiapp.ui.base.BaseViewHolder;
import com.androiddeveloper.pens.namiapp.ui.summary.SummaryAreaActivity;
import com.androiddeveloper.pens.namiapp.ui.summary.fragment.AreaFragment;
import com.androiddeveloper.pens.namiapp.ui.summary.fragment.SummaryType;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

public class SummaryPerformancePoiOskAdaptiveAdapter extends RecyclerView.Adapter<BaseViewHolder> implements SummaryAreaActivity.OnSearchInterface {

    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private SummaryPerformancePoiOskAdaptiveAdapter.Callback mCallback;
    private List<PerformanceSummary> mSummaryPerformancePoiOsks;
    private List<PerformanceSummary> mSummaryPerformancePoiOsksDefault;
    private SummaryType mSummaryType;

    ColorStateList color;

    public SummaryPerformancePoiOskAdaptiveAdapter(List<PerformanceSummary> performancePoiOskList, SummaryType summaryType) {
        mSummaryPerformancePoiOsks = performancePoiOskList;
        mSummaryType = summaryType;
    }

    public void setCallback(SummaryPerformancePoiOskAdaptiveAdapter.Callback callback) {
        mCallback = callback;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new SummaryPerformancePoiOskAdaptiveAdapter.ViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pjp_productivity_mtd_ptd_poi_osk, parent, false));
            case VIEW_TYPE_EMPTY:
            default:
                return new SummaryPerformancePoiOskAdaptiveAdapter.EmptyViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_summary, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mSummaryPerformancePoiOsks != null && mSummaryPerformancePoiOsks.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public int getItemCount() {
        if (mSummaryPerformancePoiOsks != null && mSummaryPerformancePoiOsks.size() > 0) {
            return mSummaryPerformancePoiOsks.size();
        } else {
            return 1;
        }
    }

    public void addItems(List<PerformanceSummary> summaryPerformancePoiOsks) {
        mSummaryPerformancePoiOsks.clear();
        mSummaryPerformancePoiOsks.addAll(summaryPerformancePoiOsks);
        mSummaryPerformancePoiOsks = summaryPerformancePoiOsks;
        notifyDataSetChanged();
    }

    @Override
    public void search(String query) {
        List<PerformanceSummary> filteredSummaryPerformancePoiOsk = new ArrayList<>();
        for (PerformanceSummary performancePoiOsk : mSummaryPerformancePoiOsks){
            if (performancePoiOsk.getTitle().toLowerCase().contains(query.toLowerCase()) ){
                filteredSummaryPerformancePoiOsk.add(performancePoiOsk);
            }
        }
        mSummaryPerformancePoiOsks.clear();
        mSummaryPerformancePoiOsks.addAll(filteredSummaryPerformancePoiOsk);
        notifyDataSetChanged();
    }

    public interface Callback {
        void onItemLocationListClick(int position);
    }

    public class ViewHolder extends BaseViewHolder {

        TextView tvPoiAchCall;
        TextView tvPoiPlanCall;
        TextView tvPoiActualCall;
        TextView tvPoiEffectiveCall;
        TextView tvPoiPlanMarketSurvey;
        TextView tvPoiActualMarketSurvey;
        TextView tvPoiActualSales;
        TextView tvOskAchCall;
        TextView tvOskPlanCall;
        TextView tvOskActualCall;
        TextView tvOskEffectiveCall;
        TextView tvOskPlanOutletProd;
        TextView tvOskOutletProd;
        TextView tvOskActualSales;

        TextView tvTitle;
        CardView cvSummaryPerformancePoiOsk;

        public ViewHolder(View itemView) {
            super(itemView);

            tvPoiAchCall = itemView.findViewById(R.id.tv_pjp_ach_call_mtd_ptd_poi);
            tvPoiPlanCall = itemView.findViewById(R.id.tv_pjp_mtd_ptd_plan_actual_call_poi);
            tvPoiActualCall = itemView.findViewById(R.id.tv_pjp_mtd_ptd_actual_actual_call_poi);
            tvPoiEffectiveCall = itemView.findViewById(R.id.tv_pjp_mtd_ptd_effective_call_poi);
            tvPoiPlanMarketSurvey = itemView.findViewById(R.id.tv_pjp_mtd_ptd_plan_market_survey_poi);
            tvPoiActualMarketSurvey = itemView.findViewById(R.id.tv_pjp_mtd_ptd_actual_market_survey_poi);
            tvPoiActualSales = itemView.findViewById(R.id.tv_pjp_mtd_ptd_sales_through_poi);
            tvOskAchCall = itemView.findViewById(R.id.tv_pjp_ach_call_mtd_ptd_osk);
            tvOskPlanCall = itemView.findViewById(R.id.tv_pjp_mtd_ptd_plan_actual_call_osk);
            tvOskActualCall = itemView.findViewById(R.id.tv_pjp_mtd_ptd_actual_actual_call_osk);
            tvOskEffectiveCall = itemView.findViewById(R.id.tv_pjp_mtd_ptd_effective_call_osk);
            tvOskPlanOutletProd = itemView.findViewById(R.id.tv_pjp_mtd_ptd_plan_outlet_prod_osk);
            tvOskOutletProd = itemView.findViewById(R.id.tv_pjp_mtd_ptd_actual_outlet_prod_osk);
            tvOskActualSales = itemView.findViewById(R.id.tv_pjp_mtd_ptd_sales_through_osk);

            cvSummaryPerformancePoiOsk = itemView.findViewById(R.id.cv_pjp_productivity_mtd_ptd_osk_poi);
            tvTitle = itemView.findViewById(R.id.tv_title_type);

        }

        protected void clear() {

        }

        public void onBind(int position) {
            super.onBind(position);

            PerformanceSummary item = mSummaryPerformancePoiOsks.get(position);
            Log.d("Debug",mSummaryPerformancePoiOsks.toString());

            String color = "#" + Integer.toHexString(ContextCompat.getColor(itemView.getContext(), mSummaryType.getColor()));
            Log.d("Debug : ",color);

            tvPoiAchCall.setText(item.getTvPoiAchCall());
            setTextOrnull(tvPoiPlanCall, item.getTvPoiPlanCall(), ",");
            setTextOrnull(tvPoiActualCall, item.getTvPoiActualCall(), ",");
            setTextOrnull(tvPoiEffectiveCall, item.getTvPoiEffectiveCall(), ",");
            setTextOrnull(tvPoiPlanMarketSurvey, item.getTvPoiPlanMarketSurvey(), ",");
            setTextOrnull(tvPoiActualMarketSurvey, item.getTvPoiActualMarketSurvey(), ",");
            setTextOrnull(tvPoiActualSales, item.getTvPoiActualSales(), ",");
            tvOskAchCall.setText(item.getTvOskAchCall());
            setTextOrnull(tvOskPlanCall, item.getTvOskPlanCall(), ",");
            setTextOrnull(tvOskActualCall, item.getTvOskActualCall(), ",");
            setTextOrnull(tvOskEffectiveCall, item.getTvOskEffectiveCall(), ",");
            setTextOrnull(tvOskPlanOutletProd, item.getTvOskPlanOutletProd(), ",");
            setTextOrnull(tvOskOutletProd, item.getTvOskOutletProd(), ",");
            setTextOrnull(tvOskActualSales, item.getTvOskActualSales(), ",");

            cvSummaryPerformancePoiOsk.setCardBackgroundColor(Color.parseColor(color));
            tvTitle.setText(item.getTitle());


            itemView.setOnClickListener(v->{
                if (mCallback != null){
                    mCallback.onItemLocationListClick(position);
                }
            });

        }
    }

    public class EmptyViewHolder extends BaseViewHolder {


        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void clear() {

        }

    }

    private void setTextOrnull(TextView textView, String text,String option) {
        if (option.equals(",")){
            DecimalFormat decim = new DecimalFormat("#,###,###");
            textView.setText(decim.format(Long.valueOf(text)));
        }else {
            if (text == null)
                textView.setText("0" + option);
            else {
                textView.setText(text + option);
            }
        }
    }
}
