package com.androiddeveloper.pens.namiapp.ui.summarybackchecking;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.ui.base.BaseActivity;
import com.androiddeveloper.pens.namiapp.ui.summary.SummaryAreaActivity;
import com.androiddeveloper.pens.namiapp.ui.summary.SummaryAreaMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.summary.SummaryAreaMvpView;
import com.androiddeveloper.pens.namiapp.ui.summary.fragment.AreaFragment;
import com.androiddeveloper.pens.namiapp.ui.summary.fragment.SummaryType;
import com.androiddeveloper.pens.namiapp.ui.summarybackchecking.fragment.BackcheckingType;
import com.androiddeveloper.pens.namiapp.ui.summarybackchecking.fragment.SummaryBackcheckingFragment;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SummaryBackcheckingActivity extends BaseActivity implements SummaryBackcheckingMvpView {

    @Inject
    SummaryBackcheckingMvpPresenter<SummaryBackcheckingMvpView> mPresenter;

    @BindView(R.id.search_view_summary_backchceking)
    MaterialSearchView searchViewSummary;

    @BindView(R.id.toolbarSearchSummaryBackchceking)
    Toolbar toolbarSearchSummary;

    @BindView(R.id.summary_fragment_backchceking)
    FrameLayout frameLayout;


    private TextView mTextMessage;
    private BackcheckingType mBackcheckingType;
    SummaryBackcheckingFragment fragment;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_regional_backchecking:
                    fragment = SummaryBackcheckingFragment.newInstance(mBackcheckingType,"regional");
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.summary_fragment_backchceking,fragment)
                            .commit();
                    return true;
                case R.id.navigation_branch_backchecking:
                    fragment = SummaryBackcheckingFragment.newInstance(mBackcheckingType,"branch");
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.summary_fragment_backchceking,
                                    fragment)
                            .commit();
                    return true;
                case R.id.navigation_cluster_backchecking:
                    fragment = SummaryBackcheckingFragment.newInstance(mBackcheckingType,"cluster");
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.summary_fragment_backchceking,
                                    fragment)
                            .commit();
                    return true;
                case R.id.navigation_user_backchecking:
                    fragment = SummaryBackcheckingFragment.newInstance(mBackcheckingType,"user");
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.summary_fragment_backchceking,
                                    fragment)
                            .commit();
                    return true;
            }
            return false;
        }
    };

    public static Intent getStartIntent(Context context, String userType, BackcheckingType type) {
        Intent intent = new Intent(context, SummaryBackcheckingActivity.class);
        intent.putExtra("type",type);
        intent.putExtra("area",userType);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary_backchecking);
        getActivityComponent().inject(this);

        if (isNetworkAvailable()) {
            setUnBinder(ButterKnife.bind(this));

            mPresenter.onAttach(this);

            setUp();
        }
    }

    @Override
    protected void setUp() {
        mBackcheckingType = (BackcheckingType) getIntent().getSerializableExtra("type");

        setupBottomNavigationMenu(getIntent().getStringExtra("area"));


        setSupportActionBar(toolbarSearchSummary);
        setActionBarTitle();

        toolbarSearchSummary.setNavigationIcon(R.drawable.ic_action_navigation_arrow_back_inverted);
        toolbarSearchSummary.setTitleTextColor(getResources().getColor(R.color.white));
        searchViewSummary.setEllipsize(true);
        toolbarSearchSummary.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });



    }

    private void setActionBarTitle() {
        getSupportActionBar().setTitle(mBackcheckingType.getTitle());
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    private void setupBottomNavigationMenu(String area){
        Log.d("Error : ",area);
        mTextMessage = findViewById(R.id.message);
        BottomNavigationView navigation = findViewById(R.id.navigation_backchecking);

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        switch (area.toLowerCase()){
            case "mgt1":
                fragment = SummaryBackcheckingFragment.newInstance(mBackcheckingType,"regional");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.summary_fragment_backchceking,fragment)
                        .commit();
                break;
            case "mgt2":
                navigation.getMenu().removeItem(R.id.navigation_regional_backchecking);
                fragment = SummaryBackcheckingFragment.newInstance(mBackcheckingType,"branch");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.summary_fragment_backchceking,fragment)
                        .commit();
                break;
            case "mgt3":
                navigation.getMenu().removeItem(R.id.navigation_regional_backchecking);
                navigation.getMenu().removeItem(R.id.navigation_branch_backchecking);

                fragment = SummaryBackcheckingFragment.newInstance(mBackcheckingType,"cluster");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.summary_fragment_backchceking,fragment)
                        .commit();
                break;
            case "mgt4":
                navigation.getMenu().removeItem(R.id.navigation_regional_backchecking);
                navigation.getMenu().removeItem(R.id.navigation_branch_backchecking);

                fragment = SummaryBackcheckingFragment.newInstance(mBackcheckingType,"cluster");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.summary_fragment_backchceking,fragment)
                        .commit();
                break;
            case "mgt5":
                navigation.getMenu().removeItem(R.id.navigation_regional_backchecking);
                navigation.getMenu().removeItem(R.id.navigation_branch_backchecking);

                fragment = SummaryBackcheckingFragment.newInstance(mBackcheckingType,"cluster");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.summary_fragment_backchceking,fragment)
                        .commit();
                break;
            case "adm2":
                navigation.getMenu().removeItem(R.id.navigation_regional_backchecking);
                fragment = SummaryBackcheckingFragment.newInstance(mBackcheckingType,"branch");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.summary_fragment_backchceking,fragment)
                        .commit();
                break;
            case "adm3":
                navigation.getMenu().removeItem(R.id.navigation_regional_backchecking);
                navigation.getMenu().removeItem(R.id.navigation_branch_backchecking);

                fragment = SummaryBackcheckingFragment.newInstance(mBackcheckingType,"cluster");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.summary_fragment_backchceking,fragment)
                        .commit();
                break;
            case "bm":
                navigation.getMenu().removeItem(R.id.navigation_regional_backchecking);
                navigation.getMenu().removeItem(R.id.navigation_branch_backchecking);
                navigation.getMenu().removeItem(R.id.navigation_cluster_backchecking);

                fragment = SummaryBackcheckingFragment.newInstance(mBackcheckingType,"user");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.summary_fragment_backchceking,fragment)
                        .commit();
                break;
            case "tl":
                navigation.getMenu().removeItem(R.id.navigation_regional_backchecking);
                navigation.getMenu().removeItem(R.id.navigation_branch_backchecking);
                navigation.getMenu().removeItem(R.id.navigation_cluster_backchecking);

                fragment = SummaryBackcheckingFragment.newInstance(mBackcheckingType,"user");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.summary_fragment_backchceking,fragment)
                        .commit();
                break;
            case "aoc":
                navigation.getMenu().removeItem(R.id.navigation_regional_backchecking);
                navigation.getMenu().removeItem(R.id.navigation_branch_backchecking);
                navigation.getMenu().removeItem(R.id.navigation_cluster_backchecking);
                navigation.getMenu().removeItem(R.id.navigation_user_backchecking);

                fragment = SummaryBackcheckingFragment.newInstance(mBackcheckingType,"regional");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.summary_fragment_backchceking,fragment)
                        .commit();
                break;
            default:
                navigation.getMenu().removeItem(R.id.navigation_regional_backchecking);
                navigation.getMenu().removeItem(R.id.navigation_branch_backchecking);
                navigation.getMenu().removeItem(R.id.navigation_cluster_backchecking);
                navigation.getMenu().removeItem(R.id.navigation_user_backchecking);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (searchViewSummary.isSearchOpen()) {
            searchViewSummary.closeSearch();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_summary, menu);

        MenuItem item = menu.findItem(R.id.action_search_summary);
        searchViewSummary.setMenuItem(item);

        return true;
    }


    public interface OnSearchInterface{
        void search(String query);
    }

    public void onSearchSummarySet(SummaryBackcheckingActivity.OnSearchInterface onSearch){
        searchViewSummary.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                onSearch.search(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                onSearch.search(newText);
                return true;
            }
        });

        searchViewSummary.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
                //Do some magic
            }

            @Override
            public void onSearchViewClosed() {
                //Do some magic
            }
        });
    }
}
