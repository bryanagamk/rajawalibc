package com.androiddeveloper.pens.namiapp.ui.summarybackchecking;

import com.androiddeveloper.pens.namiapp.di.PerActivity;
import com.androiddeveloper.pens.namiapp.ui.base.MvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.summary.SummaryAreaMvpView;

@PerActivity
public interface SummaryBackcheckingMvpPresenter<V extends SummaryBackcheckingMvpView> extends MvpPresenter<V> {
}
