package com.androiddeveloper.pens.namiapp.ui.summarybackchecking;

import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.ui.base.BasePresenter;
import com.androiddeveloper.pens.namiapp.ui.summary.SummaryAreaMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.summary.SummaryAreaMvpView;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class SummaryBackcheckingPresenter<V extends SummaryBackcheckingMvpView> extends BasePresenter<V>
        implements SummaryBackcheckingMvpPresenter<V> {

    @Inject
    public SummaryBackcheckingPresenter(DataManager dataManager, CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);
    }
}
