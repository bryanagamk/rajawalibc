package com.androiddeveloper.pens.namiapp.ui.summarybackchecking.backcheckinglistadapter;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceSummary;
import com.androiddeveloper.pens.namiapp.ui.base.BaseViewHolder;
import com.androiddeveloper.pens.namiapp.ui.summary.SummaryAreaActivity;
import com.androiddeveloper.pens.namiapp.ui.summary.fragment.SummaryType;
import com.androiddeveloper.pens.namiapp.ui.summarybackchecking.SummaryBackcheckingActivity;
import com.androiddeveloper.pens.namiapp.ui.summarybackchecking.fragment.BackcheckingType;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

public class SummaryPerformanceBackcheckingAdapter extends RecyclerView.Adapter<BaseViewHolder> implements SummaryBackcheckingActivity.OnSearchInterface {
    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private SummaryPerformanceBackcheckingAdapter.Callback mCallback;
    private List<PerformanceSummary> mSummaryPerformanceBackcheckings;
    private List<PerformanceSummary> mSummaryPerformanceBackcheckingsDefault;
    private BackcheckingType summaryType;

    ColorStateList color;

    public SummaryPerformanceBackcheckingAdapter(List<PerformanceSummary> performanceBackceckingList, BackcheckingType type) {
        mSummaryPerformanceBackcheckings = performanceBackceckingList;
        summaryType = type;
    }

    public void setCallback(SummaryPerformanceBackcheckingAdapter.Callback callback) {
        mCallback = callback;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new SummaryPerformanceBackcheckingAdapter.ViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_performance_backchecking_summary, parent, false));
            case VIEW_TYPE_EMPTY:
            default:
                return new SummaryPerformanceBackcheckingAdapter.EmptyViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_summary, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mSummaryPerformanceBackcheckings != null && mSummaryPerformanceBackcheckings.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public int getItemCount() {
        if (mSummaryPerformanceBackcheckings != null && mSummaryPerformanceBackcheckings.size() > 0) {
            return mSummaryPerformanceBackcheckings.size();
        } else {
            return 1;
        }
    }

    public void addItems(List<PerformanceSummary> performanceBackcheckings) {
        mSummaryPerformanceBackcheckings.clear();
        mSummaryPerformanceBackcheckings.addAll(performanceBackcheckings);
        mSummaryPerformanceBackcheckings = performanceBackcheckings;
        notifyDataSetChanged();
    }

    @Override
    public void search(String query) {
        List<PerformanceSummary> filteredSummaryPerformaceBackchecking = new ArrayList<>();
        for (PerformanceSummary performanceBackchecking : mSummaryPerformanceBackcheckings){
            if (performanceBackchecking.getTitle().toLowerCase().contains(query.toLowerCase()) ){
                filteredSummaryPerformaceBackchecking.add(performanceBackchecking);
            }
        }
        mSummaryPerformanceBackcheckings.clear();
        mSummaryPerformanceBackcheckings.addAll(filteredSummaryPerformaceBackchecking);
        notifyDataSetChanged();
    }

    public interface Callback {
        void onItemLocationListClick(int position);
    }

    public class ViewHolder extends BaseViewHolder {

        TextView tvTitle;
        TextView tvPoiAchBc;
        TextView tvPoiTargetBc;
        TextView tvPoiActualBc;
        TextView tvOskAchBc;
        TextView tvOskTargetBc;
        TextView tvOskActualBc;
        CardView cardView;


        public ViewHolder(View itemView) {
            super(itemView);

            tvTitle = itemView.findViewById(R.id.tv_title_type_bc_summary);
            tvPoiAchBc = itemView.findViewById(R.id.tv_ach_bc_poi_summary);
            tvPoiTargetBc = itemView.findViewById(R.id.tv_target_bc_poi_summary);
            tvPoiActualBc = itemView.findViewById(R.id.tv_actual_bc_poi_summary);
            tvOskAchBc = itemView.findViewById(R.id.tv_ach_bc_osk_summary);
            tvOskTargetBc = itemView.findViewById(R.id.tv_target_bc_osk_summary);
            tvOskActualBc = itemView.findViewById(R.id.tv_actual_bc_osk_summary);
            cardView = itemView.findViewById(R.id.cv_bc_performance_summary);
        }

        protected void clear() {

        }

        public void onBind(int position) {
            super.onBind(position);

            PerformanceSummary item = mSummaryPerformanceBackcheckings.get(position);
            Log.d("Debug", mSummaryPerformanceBackcheckings.toString());

            String color = "#" + Integer.toHexString(ContextCompat.getColor(itemView.getContext(), summaryType.getColor()));
            Log.d("Debug : ",color);

            setTextOrnull(tvPoiAchBc, item.getTvPoiAchBc(), "");
            setTextOrnull(tvPoiTargetBc, item.getTvPoiTargetBc(), ",");
            setTextOrnull(tvPoiActualBc, item.getTvPoiActualBc(), ",");
            setTextOrnull(tvOskAchBc, item.getTvOskAchBc(), "");
            setTextOrnull(tvOskTargetBc, item.getTvOskTargetBc(), ",");
            setTextOrnull(tvOskActualBc, item.getTvOskActualBc(), ",");
            cardView.setCardBackgroundColor(Color.parseColor(color));
            tvTitle.setText(item.getTitle());

            itemView.setOnClickListener(v -> {
                if (mCallback != null){
                    mCallback.onItemLocationListClick(position);
                }
            });
        }
    }

    public class EmptyViewHolder extends BaseViewHolder {


        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void clear() {

        }

    }

    private void setTextOrnull(TextView textView, String text,String option) {
        if (option.equals(",")){
            DecimalFormat decim = new DecimalFormat("#,###,###");
            textView.setText(decim.format(Long.valueOf(text)));
        }else {
            if (text == null)
                textView.setText("0" + option);
            else {
                textView.setText(text + option);
            }
        }
    }
}
