package com.androiddeveloper.pens.namiapp.ui.summarybackchecking.fragment;

import com.androiddeveloper.pens.namiapp.R;

public enum BackcheckingType {
    PERFORMANCE_BACKCHECKING("backchecking", "Backchecking Performance", R.color.actual_call_poi);

    private final String mType;
    private final String mTitle;
    private final int mColorStateList;
    private int mDrawable;

    BackcheckingType(String type, String title, int colorStateList){
        mType = type;
        mTitle = title;
        mColorStateList = colorStateList;
    }

    BackcheckingType(String type, String title, int colorStateList, int mDrawable) {
        mType = type;
        mTitle = title;
        mColorStateList = colorStateList;
        this.mDrawable = mDrawable;
    }

    public String getType() {
        return mType;
    }
    public String getTitle() {
        return mTitle;
    }
    public int getColor() {
        return mColorStateList;
    }

    public int getmDrawable() {
        return mDrawable;
    }
}
