package com.androiddeveloper.pens.namiapp.ui.summarybackchecking.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceBackcheckingResponse;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceSummary;
import com.androiddeveloper.pens.namiapp.di.componen.ActivityComponent;
import com.androiddeveloper.pens.namiapp.ui.base.BaseFragment;
import com.androiddeveloper.pens.namiapp.ui.feddbacklist.FeedbackListActivity;
import com.androiddeveloper.pens.namiapp.ui.feedback.FeedbackActivity;
import com.androiddeveloper.pens.namiapp.ui.feedbacklocation.FeedbackLocationActivity;
import com.androiddeveloper.pens.namiapp.ui.performancepoiosklist.PerformancePoiOskListActivity;
import com.androiddeveloper.pens.namiapp.ui.summary.fragment.SummaryType;
import com.androiddeveloper.pens.namiapp.ui.summarybackchecking.backcheckinglistadapter.SummaryPerformanceBackcheckingAdapter;
import com.androiddeveloper.pens.namiapp.ui.summarybackchecking.SummaryBackcheckingActivity;
import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SummaryBackcheckingFragment extends BaseFragment implements
        SummaryBackcheckingFragmentMvpView, SummaryPerformanceBackcheckingAdapter.Callback {
    private static final String TAG = "AreaFragment";

    @Inject
    SummaryBackcheckingFragmentMvpPresenter<SummaryBackcheckingFragmentMvpView> mPresenter;

    SummaryPerformanceBackcheckingAdapter mSummaryPerformanceBackcheckingAdapter;

    @Inject
    LinearLayoutManager mLayoutManager;

    @BindView(R.id.rv_summary_backchecking)
    RecyclerView mRecyclerView;

    @BindView(R.id.root_backchecking)
    ConstraintLayout root;

    @BindView(R.id.shimmer_view_container_backchecking)
    ShimmerFrameLayout mShimmerViewContainer;

    List<PerformanceSummary> performanceBackcheckingResponses;

    public static SummaryBackcheckingFragment newInstance(BackcheckingType type, String territory) {
        Bundle args = new Bundle();
        args.putSerializable("type",type);
        args.putSerializable("territory",territory);
        SummaryBackcheckingFragment fragment = new SummaryBackcheckingFragment();

        Log.d("Debug ", String.format("Type  : %s, Territory : %s",type,territory));

        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_summary_backchecking, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null && isNetworkAvailable()) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {
        BackcheckingType type = (BackcheckingType) getArguments().getSerializable("type");
        String territory = (String) getArguments().getSerializable("territory");
        String userId_aoc = "0";
        String username_aoc = "";


        mSummaryPerformanceBackcheckingAdapter = new SummaryPerformanceBackcheckingAdapter(new ArrayList<>(), type);

        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mSummaryPerformanceBackcheckingAdapter.setCallback(this);

        SummaryBackcheckingActivity summaryBackcheckingActivity = (SummaryBackcheckingActivity) getActivity();

        SummaryBackcheckingActivity.OnSearchInterface callback = null;

        if (type == BackcheckingType.PERFORMANCE_BACKCHECKING){
            mRecyclerView.setAdapter(mSummaryPerformanceBackcheckingAdapter);
            callback = mSummaryPerformanceBackcheckingAdapter;
        }
        summaryBackcheckingActivity.onSearchSummarySet(callback);

        mPresenter.onViewPrepared(type,territory, userId_aoc, username_aoc);

    }

    @Override
    public void showErrorPage(String message) {

    }

    @Override
    public boolean isShowingError() {
        return false;
    }

    @Override
    public void finish() {

    }

    @Override
    public void updatePerformanceSummaryBackchecking(List<PerformanceSummary> performanceBackcheckingList) {
        Log.d("Debug",performanceBackcheckingList.size() + "");
        performanceBackcheckingResponses = performanceBackcheckingList;
        mSummaryPerformanceBackcheckingAdapter.addItems(performanceBackcheckingList);
        mSummaryPerformanceBackcheckingAdapter.notifyDataSetChanged();
    }


    @Override
    public void showLoading(String message) {
        super.showLoading(message);
        mShimmerViewContainer.startShimmer();
        mShimmerViewContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        // stop animating Shimmer and hide the layout
        mShimmerViewContainer.stopShimmer();
        mShimmerViewContainer.setVisibility(View.GONE);
    }


    @Override
    public void onDestroyView() {
        mPresenter.onDetachFragment();
        super.onDestroyView();
    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmer();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmer();
        super.onPause();
    }

    @Override
    public void onItemLocationListClick(int position) {
        String teritory = (String) getArguments().getSerializable("territory");
        if (teritory != null && teritory.equals("user")){
            PerformanceSummary performanceSummary =  performanceBackcheckingResponses.get(position);
            Log.d(TAG, "onItemLocationListClick: " + performanceSummary.getUserId());
            Log.d(TAG, "onItemLocationListClick: " + performanceSummary.getUserName());
            Intent intent = FeedbackActivity.getStartIntent(getActivity(), "");
            intent.putExtra("userid_user", performanceSummary.getUserId());
            intent.putExtra("username_leader", performanceSummary.getUserName());
            startActivity(intent);
        }else {
            Toast.makeText(getBaseActivity(), "Pilih tab user untuk menampilkan detail user", Toast.LENGTH_SHORT).show();
        }
    }
}
