package com.androiddeveloper.pens.namiapp.ui.summarybackchecking.fragment;

import com.androiddeveloper.pens.namiapp.data.network.model.Backchecking;
import com.androiddeveloper.pens.namiapp.ui.base.MvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.summary.fragment.AreaMvpView;
import com.androiddeveloper.pens.namiapp.ui.summary.fragment.SummaryType;

public interface SummaryBackcheckingFragmentMvpPresenter<V extends SummaryBackcheckingFragmentMvpView>
        extends MvpPresenter<V> {

    void onViewPrepared(BackcheckingType type, String territory, String userId_aoc, String username_aoc);
}
