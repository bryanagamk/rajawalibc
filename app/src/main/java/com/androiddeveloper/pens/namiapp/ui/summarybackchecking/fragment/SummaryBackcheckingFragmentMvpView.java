package com.androiddeveloper.pens.namiapp.ui.summarybackchecking.fragment;

import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceSummary;
import com.androiddeveloper.pens.namiapp.ui.base.MvpView;

import java.util.List;

public interface SummaryBackcheckingFragmentMvpView extends MvpView {
    void updatePerformanceSummaryBackchecking(List<PerformanceSummary> performanceBackcheckingList);
}
