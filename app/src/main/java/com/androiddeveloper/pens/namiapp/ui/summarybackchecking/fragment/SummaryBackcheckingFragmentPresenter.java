package com.androiddeveloper.pens.namiapp.ui.summarybackchecking.fragment;

import android.util.Log;

import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceBackchecking;
import com.androiddeveloper.pens.namiapp.data.network.model.PerformanceSummary;
import com.androiddeveloper.pens.namiapp.ui.base.BasePresenter;
import com.androiddeveloper.pens.namiapp.ui.summary.fragment.AreaMvpPresenter;
import com.androiddeveloper.pens.namiapp.ui.summary.fragment.AreaMvpView;
import com.androiddeveloper.pens.namiapp.ui.summary.fragment.SummaryType;
import com.androidnetworking.error.ANError;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class SummaryBackcheckingFragmentPresenter<V extends SummaryBackcheckingFragmentMvpView> extends BasePresenter<V>
        implements SummaryBackcheckingFragmentMvpPresenter<V> {

    @Inject
    public SummaryBackcheckingFragmentPresenter(DataManager dataManager, CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);
    }

    @Override
    public void onViewPrepared(BackcheckingType type, String territory, String userId_aoc, String username_aoc) {
        Log.d("Debug : ",type.getType() + territory);
        getMvpView().showLoading("");
        if (type == BackcheckingType.PERFORMANCE_BACKCHECKING){
            loadPerformanceBackchecking(type, territory);
        }
    }

    private void loadPerformanceBackchecking(BackcheckingType type,String territory){
        String userId = getDataManager().getCurrentUserId();
        getCompositeDisposable().add(getDataManager().getSummaryPerformanceBackchecking(userId,territory)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(performanceBackcheckingResponse -> {
                    if (!isViewAttached()){
                        return;
                    }
                    if (performanceBackcheckingResponse != null && performanceBackcheckingResponse.isSuccess()){
                        List<PerformanceSummary> performanceSummaries = performanceBackcheckingsAdapter(performanceBackcheckingResponse.getSummaryBackchecking(),type);
                        getMvpView().updatePerformanceSummaryBackchecking(performanceSummaries);
                    }

                    getMvpView().hideLoading();
                },throwable -> {
                    if (!isViewAttached()){
                        return;
                    }
                    Log.d("Error : ",throwable.toString());

                    getMvpView().hideLoading();

                    // handle the error here
                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

    private List<PerformanceSummary> performanceBackcheckingsAdapter(List<PerformanceBackchecking> performanceBackcheckings,
                                                                     BackcheckingType type){
        if (type == BackcheckingType.PERFORMANCE_BACKCHECKING){
            List<PerformanceSummary> performanceSummaryArrayList = new ArrayList<>();
            for (PerformanceBackchecking performanceBackchecking: performanceBackcheckings){
                performanceSummaryArrayList.add(performanceBackchecking.getSummaryPerfomanceBc());
            }
            return performanceSummaryArrayList;
        }else {
            return null;
        }
    }
}
