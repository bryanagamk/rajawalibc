package com.androiddeveloper.pens.namiapp.ui.verifiynumber;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androiddeveloper.pens.namiapp.R;
import com.androiddeveloper.pens.namiapp.ui.base.BaseActivity;
import com.androiddeveloper.pens.namiapp.ui.dasboard.DasboardActivity;
import com.androiddeveloper.pens.namiapp.ui.dashboardbc.ActivityDashboardBC;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VerifyNumberActivity extends BaseActivity implements VerifyNumberMvpView {

    private static final int RESOLVE_HINT = 3;
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 6;
    private String deviceId = "-1";
    @BindView(R.id.et_verify_number)
    EditText etVerifyNumber;

    @BindView(R.id.btn_verify)
    Button btnVerifiy;

    @BindView(R.id.progressbar)
    ProgressBar progressBar;

    @BindView(R.id.tv_result)
    TextView tvResult;

    @BindView(R.id.line_info_logo_title)
    LinearLayout lineInfoLogoTitle;

    @BindView(R.id.line_info_phone_number)
    LinearLayout lineInfoPhoneNumber;

    @BindView(R.id.line_input_phone_number)
    LinearLayout lineInputPhoneNumber;

    @BindView(R.id.line_info_btn_verify)
    LinearLayout lineInfoBtnVerify;

    @BindView(R.id.line_sub_text_verify_number)
    LinearLayout lineSubTextVerifyNumber;

    @BindView(R.id.tv_sub_text_verify_number)
    TextView tvSubTextVerifyNumber;

    @Inject
    VerifyNumberMvpPresenter<VerifyNumberMvpView> mPresenter;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, VerifyNumberActivity.class);
        return intent;
    }

    Animation uptodown,downtoup;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_number);

        getActivityComponent().inject(this);

        if (isNetworkAvailable()){
            setUnBinder(ButterKnife.bind(this));

            mPresenter.onAttach(this);

            setUp();
        }

    }

    @Override
    protected void setUp() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // anim
        uptodown = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.uptodown);
        downtoup = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.downtoup);
        lineInfoLogoTitle.setAnimation(uptodown);
        lineInfoPhoneNumber.setAnimation(downtoup);
    }

    @OnClick(R.id.btn_verify)
    public void onClickBtnVerify(View view){
        onVerifyProcess();
    }

    private void onVerifyProcess() {
        hideKeyboard();
        if (deviceId.equals("-1")){
            getDeviceId(this);
        }else{
//            PhoneAuthProvider.OnVerificationStateChangedCallbacks callbacks = getVerificationCallback();
            String phoneNumber = etVerifyNumber.getText().toString();

            //for bypassing
            mPresenter.sendImeiDataAndPhoneNumberToServer(deviceId,phoneNumber);

//            mPresenter.validatePhoneNumber(phoneNumber,this,getVerificationCallback());
        }
    }

    @Override
    public void showLoading(String message) {
        super.showLoading(message);
        lineInfoBtnVerify.setVisibility(View.GONE);
        lineInputPhoneNumber.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        tvResult.setVisibility(View.VISIBLE);
        lineSubTextVerifyNumber.setVisibility(View.VISIBLE);
        tvResult.setText(message);
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        lineInfoBtnVerify.setVisibility(View.VISIBLE);
        lineInputPhoneNumber.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        tvResult.setVisibility(View.GONE);
        lineSubTextVerifyNumber.setVisibility(View.GONE);
    }

    public void getDeviceId(Context activityContext) {
        TelephonyManager telephonyManager = (TelephonyManager) activityContext.getApplicationContext()
                .getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(activityContext, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_PHONE_STATE},
                    MY_PERMISSIONS_REQUEST_READ_CONTACTS);
        }else{
            deviceId =  telephonyManager.getDeviceId();
            onVerifyProcess();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    onVerifyProcess();
                }
            }
        }
    }

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks getVerificationCallback(){
        return new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                Toast.makeText(VerifyNumberActivity.this,  "automatic user login", Toast.LENGTH_SHORT).show();
                mPresenter.validatePhoneCredential(phoneAuthCredential,VerifyNumberActivity.this,onCompleteListenerResult());
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                if (e instanceof FirebaseAuthInvalidCredentialsException){
                    Toast.makeText(VerifyNumberActivity.this, "Invalid Reqeust", Toast.LENGTH_SHORT).show();
                }else if (e instanceof FirebaseTooManyRequestsException){
                    Toast.makeText(VerifyNumberActivity.this, "Sms Quota for has been excedeed", Toast.LENGTH_SHORT).show();
                }
                Toast.makeText(VerifyNumberActivity.this, "Gagal Login", Toast.LENGTH_SHORT).show();
                Log.d("Error : ", e.toString());

                hideLoading();
            }

            @Override
            public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                super.onCodeSent(s, forceResendingToken);
                Toast.makeText(VerifyNumberActivity.this, "Mengirim SMS Konfirmasi", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCodeAutoRetrievalTimeOut(String s) {
                super.onCodeAutoRetrievalTimeOut(s);
                PhoneAuthCredential credential = PhoneAuthProvider.getCredential(s, "312");
                mPresenter.validatePhoneCredential(credential,VerifyNumberActivity.this,onCompleteListenerResult());
            }
        };
    }

    private OnCompleteListener<AuthResult> onCompleteListenerResult(){
        return task -> {
            if (task.isSuccessful()){
                Toast.makeText(VerifyNumberActivity.this, "Mengirim Nomer ke server dan deviceid ke server "
                        , Toast.LENGTH_SHORT).show();
                mPresenter.sendImeiDataAndPhoneNumberToServer(deviceId,task.getResult().getUser().getPhoneNumber());
            }else{
                Log.d("Error : ",task.toString());
                Toast.makeText(VerifyNumberActivity.this, "Login Gagal / nomer salah", Toast.LENGTH_SHORT).show();
                hideLoading();
            }
        };
    }

    @Override
    public void openDashboardActivity() {
        Intent intent = DasboardActivity.getStartIntent(this);
        startActivity(intent);
        finish();
    }

    @Override
    public void openDashboardBCActivity() {
        Intent intent = ActivityDashboardBC.getStartIntent(this);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }
}
