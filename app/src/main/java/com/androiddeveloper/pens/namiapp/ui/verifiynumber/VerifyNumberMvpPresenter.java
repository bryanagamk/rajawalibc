package com.androiddeveloper.pens.namiapp.ui.verifiynumber;


import android.app.Activity;

import com.androiddeveloper.pens.namiapp.di.PerActivity;
import com.androiddeveloper.pens.namiapp.ui.base.MvpPresenter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;


/**
 * Created by miftahun on 3/8/18.
 */

@PerActivity
public interface VerifyNumberMvpPresenter<V extends VerifyNumberMvpView> extends MvpPresenter<V> {

    void validatePhoneNumber(String phoneNumber, Activity activity, PhoneAuthProvider.OnVerificationStateChangedCallbacks callbacks);

    void validatePhoneCredential(PhoneAuthCredential credential, Activity activity, OnCompleteListener<AuthResult> callback);

    void sendImeiDataAndPhoneNumberToServer(String imei, String phoneData);

    void validateLoginToServer(String deviceId, String userId);

    void saveLoginUserToPrefs();

}
