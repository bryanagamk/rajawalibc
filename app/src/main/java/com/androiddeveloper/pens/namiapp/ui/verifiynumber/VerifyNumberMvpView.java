package com.androiddeveloper.pens.namiapp.ui.verifiynumber;


import com.androiddeveloper.pens.namiapp.ui.base.MvpView;

/**
 * Created by miftahun on 3/8/18.
 */

public interface VerifyNumberMvpView extends MvpView {

    void openDashboardActivity();

    void openDashboardBCActivity();
}
