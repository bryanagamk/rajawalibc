package com.androiddeveloper.pens.namiapp.ui.verifiynumber;


import android.app.Activity;
import android.net.wifi.WifiManager;
import android.util.Log;

import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.data.network.model.LoginResponse;
import com.androiddeveloper.pens.namiapp.ui.base.BasePresenter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 *  Created by miftahun on 3/8/18.
 */

public class VerifyNumberPresenter<V extends VerifyNumberMvpView> extends BasePresenter<V>
        implements VerifyNumberMvpPresenter<V> {

    private static final String TAG = "VerifyNumberPresenter";

    @Override
    public void onAttach(V mvpView) {
        super.onAttach(mvpView);
        if (getDataManager().getCurrentUserLoggedInMode() == DataManager.LoggedInMode.LOGGED_IN_MODE_PHONE_LOGIN.getType()){
            if ( getDataManager().getCurrentApplicationType().contains("NAMI") ){
                getMvpView().openDashboardActivity();
            } else {
                getMvpView().openDashboardBCActivity();
            }
        }
    }

    @Inject
    public VerifyNumberPresenter(DataManager dataManager,
                                 CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);
    }

    @Override
    public void validatePhoneNumber(String phoneNumber,
                                          Activity activity,
                                          PhoneAuthProvider.OnVerificationStateChangedCallbacks callbacks) {
        getMvpView().showLoading("Waiting to validate phone number");
        getCompositeDisposable().add(
                getDataManager().validatePhoneNumber(phoneNumber,activity,callbacks)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe()
        );
    }

    @Override
    public void validatePhoneCredential(PhoneAuthCredential credential, Activity activity, OnCompleteListener<AuthResult> callback) {
        getCompositeDisposable().add(
                getDataManager().signInWithPhoneAuthCredential(credential, activity, callback)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe()
        );
    }

    @Override
    public void sendImeiDataAndPhoneNumberToServer(String imei, String phoneData) {
        validateLoginToServer(imei,phoneData);
    }

    @Override
    public void validateLoginToServer(String deviceId, String userId) {
        Log.d("Number : ",userId);
        getCompositeDisposable().add(getDataManager().sentLoginUser(deviceId,userId.replace("+",""))
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(loginResponse -> {
            if (!isViewAttached()){
                return;
            }
            Log.d("Login response : ",loginResponse.toString());
            if (!loginResponse.getSuccess()){
                if (loginResponse.getUser() != null && loginResponse.getUser().getMessage().equals("true") ){
                    if (loginResponse.getUser().getMessage().toLowerCase().equals("true")){
                        saveLoginUserToPrefs();
                        getMvpView().openDashboardActivity();
                        getDataManager().updateUserInfo(
                                "",
                                loginResponse.getUser().getUserId(),
                                DataManager.LoggedInMode.LOGGED_IN_MODE_PHONE_LOGIN,
                                null,
                                loginResponse.getUser().getUserName(),
                                loginResponse.getUser().getUserType(),
                                loginResponse.getUser().getTitle(),
                                loginResponse.getUser().getPathImage(),
                                "NAMI");

                        getMvpView().hideLoading();
                    }else{
                        getMvpView().showMessage("Error disini");
                        getMvpView().hideLoading();
                    }
                } else {
                    // TODO: Tambahkan login untuk rajawali ketika user tidak ada di mynami
                    getCompositeDisposable().add(getDataManager().sentLoginRajawali(deviceId, userId.replace("+",""))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(loginRajawaliResponse -> {
                        if ( !isViewAttached() ){
                            return;
                        } else {
                            if ( loginRajawaliResponse.getSuccess() ){
                                if ( loginRajawaliResponse.getUser().getMessage().toLowerCase().equals("true") ){
                                    saveLoginUserToPrefs();
                                    getMvpView().openDashboardBCActivity();
                                    getDataManager().updateUserInfo(
                                            "",
                                            loginRajawaliResponse.getUser().getUserId(),
                                            DataManager.LoggedInMode.LOGGED_IN_MODE_PHONE_LOGIN,
                                            null,
                                            loginRajawaliResponse.getUser().getUserName(),
                                            loginRajawaliResponse.getUser().getUserType(),
                                            loginRajawaliResponse.getUser().getTitle(),
                                            loginRajawaliResponse.getUser().getPathImage(),
                                            "RAJAWALI");

                                    getMvpView().hideLoading();
                                }else{
                                    getMvpView().showMessage("Error disini");
                                    getMvpView().hideLoading();
                                }
                                return;
                            } else {
                                getMvpView().showMessage("Maaf nomer anda tidak valid");
                                getMvpView().hideLoading();
                            }
                        }
                    }));
                }
                return;
            }
//            Log.d("Debug",loginResponse.toString());
//            if (loginResponse.getSuccess() || loginResponse.getUser().getMessage().equals("true")){
//                Log.d("Debug","Login masuk");
//                saveLoginUserToPrefs();
//                getMvpView().openDashboardActivity();
//                getDataManager().updateUserInfo(
//                        "",
//                        loginResponse.getUser().getUserId(),
//                        DataManager.LoggedInMode.LOGGED_IN_MODE_PHONE_LOGIN,
//                        null,
//                        loginResponse.getUser().getUserName(),
//                        loginResponse.getUser().getUserType(),
//                        loginResponse.getUser().getTitle(),
//                        loginResponse.getUser().getPathImage());
//
//                getMvpView().hideLoading();
//            }else {
//                getMvpView().showMessage(loginResponse.getUser().getMessage());
//                getMvpView().hideLoading();
//            }
        }, throwable -> {
            if (!isViewAttached()){
                return;
            }
            getMvpView().showMessage("Terjadi Kesalahan pada server");
            Log.d("Error", throwable.toString());
            getMvpView().hideLoading();
        }));
    }

    @Override
    public void saveLoginUserToPrefs() {
        getDataManager().setCurrentUserLoggedInMode(DataManager.LoggedInMode.LOGGED_IN_MODE_PHONE_LOGIN);
    }


}
