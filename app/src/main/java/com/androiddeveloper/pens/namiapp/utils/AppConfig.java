package com.androiddeveloper.pens.namiapp.utils;

/**
 * Created by miftahun on 7/17/18.
 * <p>
 * Author Miftahun Najat
 * Email miftahunajat@gmail.com
 * Github https://github.com/miftahunajat
 */

public class AppConfig {
    public static final Boolean IS_DEBUG_MODE  = false;
    private static String SERVER_URL;

    public static String getServerUrl() {
        if(IS_DEBUG_MODE)
            SERVER_URL =  "http://163.53.193.92/nami4.0dev/";
        else
            SERVER_URL =  "http://163.53.193.92/nami4.0/";
        return SERVER_URL;
    }
}
