package com.androiddeveloper.pens.namiapp.utils;

import com.androiddeveloper.pens.namiapp.R;

/**
 * Created by miftahun on 7/20/18.
 * <p>
 * Author Miftahun Najat
 * Email miftahunajat@gmail.com
 * Github https://github.com/miftahunajat
 */

public enum MenuEnum {
    PJP_MENU(R.id.line_info_pjp),
    EVENT_MENU(R.id.line_info_event),
    BACKCHECKING_MENU(R.id.line_info_back_checking),
    FEEDBACK_MENU(R.id.line_info_feedback);

    private final int mType;

    MenuEnum(int type) {
        mType = type;
    }

    public int getType() {
        return mType;
    }
}