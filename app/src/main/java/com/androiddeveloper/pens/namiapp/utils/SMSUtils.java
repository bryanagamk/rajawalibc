package com.androiddeveloper.pens.namiapp.utils;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

/**
 * Created by miftahun on 8/12/18.
 */

public class SMSUtils {
    // public static final String INBOX = "content://sms/inbox";
// public static final String SENT = "content://sms/sent";
// public static final String DRAFT = "content://sms/draft";
    public static String getSms(Context context){
        Cursor cursor = context.getContentResolver().query(Uri.parse("content://sms/inbox"), null, null, null, null);

        if (cursor.moveToFirst()) { // must check the result to prevent exception
                String msgData = "";
                for(int idx=0;idx<cursor.getColumnCount();idx++)
                {
                    msgData += " " + cursor.getColumnName(idx) + ":" + cursor.getString(idx);
                }
                // use msgData
            return msgData;
        } else {
            return null;
            // empty box, no SMS
        }
    }



}
