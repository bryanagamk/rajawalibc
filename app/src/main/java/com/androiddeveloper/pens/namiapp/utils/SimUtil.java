package com.androiddeveloper.pens.namiapp.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;

/**
 * Created by miftahun on 7/1/18.
 * <p>
 * Author Miftahun Najat
 * Email miftahunajat@gmail.com
 * Github https://github.com/miftahunajat
 */

public final class SimUtil {

    private static final int PERMISSION_READ_PHONE_STATE = 3;

    private SimUtil() {

    }

    public static String getSimIccId(Context activityContext) {
        String iccIdNumber = null;
        TelephonyManager telephonyManager = (TelephonyManager) activityContext.getApplicationContext()
                .getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(activityContext, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            return null;
        } else {
            iccIdNumber = telephonyManager.getSimSerialNumber();
        }
        return iccIdNumber;
    }
}
