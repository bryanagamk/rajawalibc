package com.androiddeveloper.pens.namiapp.utils;

import android.util.Log;

import com.androiddeveloper.pens.namiapp.data.DataManager;
import com.androiddeveloper.pens.namiapp.data.network.model.BasicConfigResponse;

import java.util.HashMap;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by miftahun on 7/18/18.
 * <p>
 * Author Miftahun Najat
 * Email miftahunajat@gmail.com
 * Github https://github.com/miftahunajat
 */

public class StartupConfig {

    private static StartupConfig instance;

    private HashMap<String, String> startupConfig = new HashMap<>();

    private StartupConfig(HashMap<String, String> param){
        startupConfig = param;
    }

    public static StartupConfig getInstance(DataManager dataManager, boolean restart){
        if (instance == null ||  restart){
            dataManager.getServerConfig()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(basicConfigResponses -> {
                        Log.d("StartupConfig : ",basicConfigResponses.toString());
                        HashMap<String, String> configs = new HashMap<>();
                        for (BasicConfigResponse response:
                                basicConfigResponses ) {
                            configs.put(response.getParamVariable(), response.getParamValue());
                        }
                        instance = new StartupConfig(configs);
                    },throwable -> {
                        Log.d("StartupConfig : ", "Can't Connect to internet");
                    });
        }
        return instance;
    }

    public static StartupConfig getInstance(DataManager dataManager, boolean restart, String appType){
        if (instance == null ||  restart){
            if ( appType.equals("RAJAWALI") ){
                dataManager.getServerConfigRajawali()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(basicConfigResponses -> {
                            Log.d("StartupConfig : ",basicConfigResponses.toString());
                            HashMap<String, String> configs = new HashMap<>();
                            for (BasicConfigResponse response:
                                    basicConfigResponses ) {
                                configs.put(response.getParamVariable(), response.getParamValue());
                            }
                            instance = new StartupConfig(configs);
                        },throwable -> {
                            Log.d("StartupConfig : ", "Can't Connect to internet");
                        });
            }
        }
        return instance;
    }

    public String getBackcheckingOssOskRadius(){
        return startupConfig.get("radius_backchecking_oss_osk_inmeters");
    }

    public String getBackcheckingInstitutionRadius(){
        return startupConfig.get("radius_backchecking_institution_inmeters");
    }

    public String getRadiusEventShowInmeters(){
        return startupConfig.get("radius_event_show_inmeters");
    }

    public String getRadiusEventInmeters(){
        return startupConfig.get("radius_event_inmeters");
    }

    public String getCheckinOssOskRadius(){
        if (AppConfig.IS_DEBUG_MODE){
            return "2000";
        }else{
            return startupConfig.get("radius_oss_osk_inmeters");
        }

    }

    public String getCheckinInstitutionRadius(){
        if (AppConfig.IS_DEBUG_MODE){
         return "2000";
        }else{
            return startupConfig.get("radius_institution_inmeters");
        }
    }

    public String getRadiusOutletMax(){
        return startupConfig.get("radius_outlet_max");
    }

    public String getRadiusSiteMax(){
        return startupConfig.get("radius_site_max");
    }
}


